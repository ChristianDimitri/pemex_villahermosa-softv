using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using Softv.Providers;
using Softv.Entities;

namespace Softv.BAL
{
  /// <summary>
  /// Class                   : Softv.BAL.MotAtenTel.cs
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : MotAtenTel entity
  /// File                    : MotAtenTelEntity.cs
  /// Creation date           : 17/01/2011
  /// Creation time           : 06:35:35 p.m.
  /// </summary>
  [DataObject]
  [Serializable]
  public class MotAtenTel
  {

      #region attributes

      private int?  _Clv_Motivo;
      /// <summary>
      /// Clv_Motivo
      /// </summary>
      public int? Clv_Motivo
      {
          get { return _Clv_Motivo; }
          set { _Clv_Motivo = value; }
      }

      private String _Descripcion;
      /// <summary>
      /// Descripcion
      /// </summary>
      public String Descripcion
      {
          get { return _Descripcion; }
          set { _Descripcion = value; }
      }
      #endregion

      #region Constructors
      public MotAtenTel(){}

      public MotAtenTel(int? Clv_Motivo, String Descripcion)
      {
          this._Clv_Motivo = Clv_Motivo;
          this._Descripcion = Descripcion;
      }
      #endregion

      #region static Methods

      /// <summary>
      ///Adds MotAtenTel
      /// </summary>
      [DataObjectMethod(DataObjectMethodType.Insert, true)]
     public static int Add(int? Clv_Motivo, String Descripcion)
      {
          MotAtenTelEntity entity_MotAtenTel = new MotAtenTelEntity();
          entity_MotAtenTel.Clv_Motivo = Clv_Motivo;
          entity_MotAtenTel.Descripcion = Descripcion;
          int result = ProviderSoftv.MotAtenTel.AddMotAtenTel(entity_MotAtenTel);
          return result;
      }

      /// <summary>
      /// Delete MotAtenTel
      /// </summary>
      [DataObjectMethod(DataObjectMethodType.Delete, true)]
      public static int Delete(int? Clv_Motivo)
      {
          MotAtenTelEntity entity = new MotAtenTelEntity();
          entity.Clv_Motivo = Clv_Motivo;
          int resultado = ProviderSoftv.MotAtenTel.DeleteMotAtenTel(Clv_Motivo);
          return resultado;
      }

      /// <summary>
      /// Edit MotAtenTel
      /// </summary>
      [DataObjectMethod(DataObjectMethodType.Update, true)]
      public static int Edit(int? Clv_Motivo, String Descripcion)
      {
          MotAtenTelEntity entity_MotAtenTel = new MotAtenTelEntity();
          entity_MotAtenTel.Clv_Motivo = Clv_Motivo;
          entity_MotAtenTel.Descripcion = Descripcion;
          int result = ProviderSoftv.MotAtenTel.EditMotAtenTel(entity_MotAtenTel);
          return result;
      }

      /// <summary>
      /// Get MotAtenTel
      /// </summary>
      [DataObjectMethod(DataObjectMethodType.Select, true)]
      public static List<MotAtenTel> GetAll()
      {
          List<MotAtenTel> list = new List<MotAtenTel>();
          List<MotAtenTelEntity> entities = new List<MotAtenTelEntity>();
          entities = ProviderSoftv.MotAtenTel.GetMotAtenTel();
          if(entities != null)
          {
              foreach(MotAtenTelEntity gMotAtenTel in  entities)
              {
                  MotAtenTel entity_MotAtenTel = new MotAtenTel(gMotAtenTel.Clv_Motivo, gMotAtenTel.Descripcion);
                  list.Add(entity_MotAtenTel);
              }
          }
          return list;
      }

      /// <summary>
      /// ConsultaPorId MotAtenTel
      /// </summary>
      [DataObjectMethod(DataObjectMethodType.Select)]
      public static MotAtenTel GetOne(int? Clv_Motivo)
      {
          MotAtenTel entity_MotAtenTel = null;
          MotAtenTelEntity result = ProviderSoftv.MotAtenTel.GetMotAtenTelById(Clv_Motivo);
          if (result != null)
              entity_MotAtenTel = new MotAtenTel(result.Clv_Motivo, result.Descripcion);
          return entity_MotAtenTel;
      }

      public MotAtenTel Instance()
      {
          return new MotAtenTel();
      }
      #endregion

      #region Instance Methods
      /// <summary>
      ///Adds MotAtenTel
      /// </summary>
      public int Add()
      {
          int result = Add(this.Clv_Motivo, this.Descripcion);
          return result;
      }

      /// <summary>
      /// Delete MotAtenTel
      /// </summary>
      public int Delete()
      {
          int result = Delete( this.Clv_Motivo);
          return result;
      }
      /// <summary>
      /// Edit MotAtenTel
      /// </summary>
      public int Edit()
      {
          int result = Edit(this.Clv_Motivo, this.Descripcion);
          return result;
      }
      #endregion
  }
}
