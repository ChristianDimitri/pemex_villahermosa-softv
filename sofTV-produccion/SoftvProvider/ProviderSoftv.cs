﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Softv.Providers
{
    public class ProviderSoftv
    {
        public static RangoLimiteDeCreditoProvider RangoLimiteDeCredito
        {
            get { return RangoLimiteDeCreditoProvider.Instance; }
        }

        public static LimiteDeCreditoProvider LimiteDeCredito
        {
            get { return LimiteDeCreditoProvider.Instance; }
        }

        public static DetLimiteDeCreditoProvider DetLimiteDeCredito
        {
            get { return DetLimiteDeCreditoProvider.Instance; }
        }

        public static ColoniaProvider Colonia
        {
            get { return ColoniaProvider.Instance; }
        }

        public static CiudadProvider Ciudad
        {
            get { return CiudadProvider.Instance; }
        }

        public static ServicioProvider Servicio
        {
            get { return ServicioProvider.Instance; }
        }

        public static MesaControlUsuarioProvider MesaControlUsuario
        {
            get { return MesaControlUsuarioProvider.Instance; }
        }

        public static CvecolciuProvider Cvecolciu
        {
            get { return CvecolciuProvider.Instance; }
        }

        public static RelColoniasSerProvider RelColoniasSer
        {
            get { return RelColoniasSerProvider.Instance; }
        }

        public static MotAtenTelProvider MotAtenTel
        {
            get { return MotAtenTelProvider.Instance; }
        }

        public static AtenTelProvider AtenTel
        {
            get { return AtenTelProvider.Instance; }
        }

        public static EstadosDeCuentaProvider EstadosDeCuenta
        {
            get { return EstadosDeCuentaProvider.Instance; }
        }

        public static PeriodosDeCobroProvider PeriodosDeCobro
        {
            get { return PeriodosDeCobroProvider.Instance; }
        }

        public static CicloProvider Ciclo
        {
            get { return CicloProvider.Instance; }
        }
        public static RelCicloSectorProvider RelCicloSector
        {
            get { return RelCicloSectorProvider.Instance; }
        }

        // DescargaMaterialProvider 

		public static DescargaMaterialProvider DescargaMaterial
        {
            get { return DescargaMaterialProvider.Instance; }
        }

		// RelMaterialTrabajoProvider 

        public static RelMaterialTrabajoProvider RelMaterialTrabajo
        {
            get { return RelMaterialTrabajoProvider.Instance; }
        }

        // TrabajoProvider 

        public static TrabajoProvider Trabajo
        {
            get { return TrabajoProvider.Instance; }
        }
    }
}
