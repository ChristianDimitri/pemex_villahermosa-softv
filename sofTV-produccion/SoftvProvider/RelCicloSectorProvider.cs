using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;

namespace Softv.Providers
{
  /// <summary>
  /// Class                   : Softv.Providers.RelCicloSectorProvider
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : RelCicloSector Provider
  /// File                    : RelCicloSectorProvider.cs
  /// Creation date           : 28/01/2011
  /// Creation time           : 04:30:28 p.m.
  /// </summary>
  public abstract class RelCicloSectorProvider : Globals.DataAccess
  {
      /// <summary>
      /// La instancia del RelCicloSector de servicios de bd
      /// </summary>
      private static RelCicloSectorProvider _Instance = null;

      private static ObjectHandle obj;
      /// <summary>
      /// Generates a RelCicloSector instance
      /// </summary>
      public static RelCicloSectorProvider Instance
      {
          get
          {
              if (_Instance == null)
              {
                obj = Activator.CreateInstance(
                   SoftvSettings.Settings.RelCicloSector.Assembly,
                   SoftvSettings.Settings.RelCicloSector.DataClass);
              _Instance = (RelCicloSectorProvider)obj.Unwrap();
              }
              return _Instance;
          }
      }

      /// <summary>
      /// Provider's default constructor
      /// </summary>
      public RelCicloSectorProvider()
      {
      }
      /// <summary>
      /// Abstract method to add RelCicloSector
      /// </summary>
      /// <param name="RelCicloSector"></param>
      /// <returns></returns>
      public abstract long AddRelCicloSector(RelCicloSectorEntity entity_RelCicloSector);

      /// <summary>
      /// Abstract method to delete RelCicloSector
      /// </summary>
      public abstract long DeleteRelCicloSector(long? Id_Ciclo);

      /// <summary>
      /// Abstract method to update RelCicloSector
      /// </summary>
      public abstract long EditRelCicloSector(RelCicloSectorEntity entity_RelCicloSector);

      /// <summary>
      /// Abstract method to get all RelCicloSector
      /// </summary>
      public abstract List<RelCicloSectorEntity> GetRelCicloSector();

      /// <summary>
      /// Abstract method to get by id
      /// </summary>
      public abstract RelCicloSectorEntity GetRelCicloSectorById(long? Id_Ciclo_Sector);

      /// <summary>
      /// Converts data from reader to entity
      /// </summary>
      protected virtual RelCicloSectorEntity GetRelCicloSectorFromReader(IDataReader reader)
      {
          RelCicloSectorEntity entity_RelCicloSector = null;
          try
          {
              entity_RelCicloSector = new RelCicloSectorEntity();
              entity_RelCicloSector.Id_Ciclo_Sector = reader["Id_Ciclo_Sector"] ==  DBNull.Value ? null : ((long?)(reader["Id_Ciclo_Sector"]));
              entity_RelCicloSector.Id_Ciclo = reader["Id_Ciclo"] ==  DBNull.Value ? null : ((long?)(reader["Id_Ciclo"]));
              entity_RelCicloSector.Id_Sector = reader["Id_Sector"] ==  DBNull.Value ? null : ((long?)(reader["Id_Sector"]));
          }
          catch (Exception ex)
          {
              throw new Exception("Error converting data to entity", ex);
          }
          return entity_RelCicloSector;
      }
  }
}
