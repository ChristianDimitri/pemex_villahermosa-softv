Imports System.Data.SqlClient
Imports System.Text
Public Class FrmDetOrSer
    Dim op_cabl As Integer = 0
    Dim bnd As Integer = 0
    Dim numNet As Integer = 0
    Dim numTel As Integer = 0
    Dim numDig As Integer = 0
    Dim numTv As Integer = 0
    Dim errorBND As Integer = 0
    Dim errorDuplicado As Integer = 0
    Private Sub FrmDetOrSer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        HayConex(0)
        'Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        Me.Dime_Que_servicio_Tiene_clienteTableAdapter.Connection = CON
        Me.Dime_Que_servicio_Tiene_clienteTableAdapter.Fill(Me.Procedimientosarnoldo4.Dime_Que_servicio_Tiene_cliente, GloContratoord)

        'Me.MUESTRATRABAJOSTableAdapter.Connection = CON
        'Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, CInt(Me.ComboBox4.SelectedValue))
        MUESTRATRABAJOS_2(CInt(Me.ComboBox4.SelectedValue))
        Me.ComboBox2.Text = ""
        CON.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim clavedetordSer As Long = 0
        Dim CON As New SqlConnection(MiConexion)
        Dim CON1 As New SqlConnection(MiConexion)

        validaDetOrdserDuplicado(gloClave, Me.ComboBox2.SelectedValue, 1)
        If errorDuplicado = 1 Then
            MsgBox("El Trabajo Ya Existe En La Lista", MsgBoxStyle.Information)
            Exit Sub
        End If

        validaDetOrdserDuplicado(gloClave, Me.ComboBox2.SelectedValue, 2)
        If errorDuplicado = 0 Then
            MsgBox("No Puedes Mezclar �rdenes De Proceso Autom�tico Y Proceso Manual", MsgBoxStyle.Information)
            Exit Sub
        End If

        eClv_TipSer = Me.ComboBox4.SelectedValue
        CON.Open()
        If IsNumeric(Me.ComboBox2.SelectedValue) = True And Len(Trim(Me.ComboBox2.Text)) > 0 Then
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BCABM" Then
                MsgBox("La baja de Cablemodem se genera de forma automatica en el momento que todos los Servicios pase a Baja ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BAPAR" Then
                MsgBox("La baja de aparato digital se genera de forma automatica en el momento que todos los servicios se pase a baja ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "CANEX" Then
                HayConex(0)
                If Me.ContadorTextBox.Text > 0 Then
                    MsgBox("Existe una Orden de Contrataci�n de Extenci�n por lo cual se puede agregar este concepto ", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "CONEX" Then
                HayConex(1)
                If Me.ContadorTextBox.Text > 0 Then
                    MsgBox("Existe una Orden de Cancelaci�n de Extenci�n por lo cual se puede agregar este concepto ", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            validaInternetStatus(GloContratoord)
            DIME_SITELYNET(GloContratoord)
            validaOrdenBPAQU(GloContratoord, gloClave)

            If numTel = 1 Then
                If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BPAQU" Then
                    If (numTv = 0 And numDig = 0) Or (numNet = 0 And numDig = 0) Then
                        MsgBox("Necesita De Un Servicio Para Mantener La Telefonia, La Orden No Sera Generada", MsgBoxStyle.Information)
                        Me.Close()
                        CON1.Open()
                        FrmOrdSer.CONORDSERTableAdapter.Connection = CON1
                        FrmOrdSer.CONORDSERTableAdapter.Delete(gloClave, 0)
                        CON1.Close()
                        FrmOrdSer.Close()

                    End If
                End If
                If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "RETLI" Then
                    If (numNet = 0 And numDig = 0) Then
                        MsgBox("Necesita De Un Servicio Para Mantener La Telefonia, La Orden No Sera Generada", MsgBoxStyle.Information)
                        Me.Close()
                        CON1.Open()
                        FrmOrdSer.CONORDSERTableAdapter.Connection = CON1
                        FrmOrdSer.CONORDSERTableAdapter.Delete(gloClave, 0)
                        CON1.Close()
                        FrmOrdSer.Close()
                    End If
                End If
                If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BPAQD" Then
                    If (numNet = 0 And numTv = 0) Then
                        MsgBox("Necesita De Un Servicio Para Mantener La Telefonia, La Orden No Sera Generada", MsgBoxStyle.Information)
                        Me.Close()
                        CON1.Open()
                        FrmOrdSer.CONORDSERTableAdapter.Connection = CON1
                        FrmOrdSer.CONORDSERTableAdapter.Delete(gloClave, 0)
                        CON1.Close()
                        FrmOrdSer.Close()
                    End If
                End If

                If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "CCABM" Or Mid(Trim(Me.ComboBox2.Text), 1, 6) = "CMAMTA" Then
                    MsgBox("El Cliente Cuenta Con Telefonia Activa, La Orden No Sera Generada", MsgBoxStyle.Information)
                    Me.Close()
                    CON1.Open()
                    FrmOrdSer.CONORDSERTableAdapter.Connection = CON1
                    FrmOrdSer.CONORDSERTableAdapter.Delete(gloClave, 0)
                    CON1.Close()
                    FrmOrdSer.Close()
                End If
            End If

            If Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BPAQT" Then
                If bnd = 1 And errorBND = 0 Then
                    If (MsgBox("Desea Pasar Los Servicios De Internet A Baja", MsgBoxStyle.YesNo)) = MsgBoxResult.Yes Then
                        GloAuxBaja = 0
                        InsertaRelClientesBnd(GloContratoord, 0)
                        InsertaEnDetOrdSerBAPQT(gloClv_Orden, "BPAQU", GloObs_OrdSer, GloSeRealiza_OrdSer)
                    Else
                        GloAuxBaja = 1
                        InsertaRelClientesBnd(GloContratoord, 1)
                    End If
                End If
            End If

            GloClv_Trabajo_OrdSer = Me.ComboBox2.SelectedValue
            GloTrabajo_OrdSer = Me.ComboBox2.Text
            If Len(Trim(Me.ObsTextBox1.Text)) > 0 Then
                GloObs_OrdSer = Me.ObsTextBox1.Text
            Else
                GloObs_OrdSer = ""
            End If
            GloSeRealiza_OrdSer = Me.SeRealizaCheckBox.Checked
            GloBndTrabajo = True
            GLOTRABAJO = Mid(Trim(GloTrabajo_OrdSer), 1, 5)
            Me.CONDetOrdSerTableAdapter.Connection = CON
            Me.CONDetOrdSerTableAdapter.Insert(gloClv_Orden, GloClv_Trabajo_OrdSer, GloObs_OrdSer, GloSeRealiza_OrdSer, GloDetClave)



            If Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CAMDO" Or Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CANET" Or Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CADIG" Or Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CAMDF" Then
                FrmCAMDO.Show()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CONEX" Then
                FrmCONEX.Show()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CEXTE" Then
                FrmCEXTE.Show()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CANEX" Then
                FrmCANEX.Show()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "ICABM" Then
                FrmRelCablemodemClientes.Show()
            ElseIf Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BCABM" Then
                FrmRelCablemodemClientes.Show()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CCABM" Or Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CMTCM" Then
                'FrmRelCablemodemClientes.Show()
                'Eric----------------
                'FrmICABMAsigna.Show()
                op_cabl = MsgBox("�El Cablemodem Que Se Va Instalar es Inal�mbrico?", MsgBoxStyle.YesNo)
                If op_cabl = 6 Then
                    LoctipoCablemdm = 2
                    bndCCABM = True
                ElseIf op_cabl = 7 Then
                    LoctipoCablemdm = 1
                    bndCCABM = True
                End If
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 6) = "CMAMTA" Then
                LoctipoCablemdm = 1
                bndCCABM = True
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CCABT" Then
                LoctipoCablemdm = 1
                bndCCABM = True
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "CAPAR" Then
                'FrmRelCablemodemClientes.Show()
                'Eric----------------
                'FrmIAPARAsigna.Show()
            ElseIf (Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "IPAQU" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BPAQU" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "DPAQU" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "RPAQU") Then
                FrmrRelPaquetesdelCliente.Show()
            ElseIf (Mid(GloTrabajo_OrdSer, 1, 6) = "IPAQUT" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BPAQT" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "DPAQT" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "RPAQT") Then
                FrmrRelPaquetesdelCliente.Show()
            ElseIf Mid(GloTrabajo_OrdSer, 1, 5) = "BPAAD" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BSEDI" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "ASDIG" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "DSDIG" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "RSDIG" Then
                FrmrRelPaquetesdelCliente.Show()
            ElseIf (Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "IPAQD" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "BPAQD" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "DPAQD" Or Mid(Trim(Me.ComboBox2.Text), 1, 5) = "RPAQD") Then
                FrmrRelPaquetesdelClienteDigital.Show()
            ElseIf Mid(Trim(GloTrabajo_OrdSer), 1, 5) = "IAPAR" Then
                FrmRelCablemodemClientesDigital.Show()
            End If

            If GLOTRABAJO = "RETLI" Then
                Me.GuardaMotivoCanServTableAdapter.Connection = CON
                Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 1, 0, 0, 0)
            End If
            Me.Close()
        Else
            MsgBox("No a Seleccionado un Servicio al Cliente")
        End If
        CON.Close()

    End Sub

    Private Sub HayConex(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.Dimesihay_ConexTableAdapter.Connection = CON
            Me.Dimesihay_ConexTableAdapter.Fill(Me.NewSofTvDataSet.Dimesihay_Conex, New System.Nullable(Of Long)(CType(gloClv_Orden, Long)), op)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub


   
    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)

        Me.ComboBox2.Text = ""
        'Me.MUESTRATRABAJOSTableAdapter.Connection = CON
        'Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, CInt(Me.ComboBox4.SelectedValue))
        MUESTRATRABAJOS_2(CInt(Me.ComboBox4.SelectedValue))
        Me.ComboBox2.Text = ""
        CON.Close()
    End Sub
    Private Sub InsertaEnDetOrdSerBAPQT(ByVal clv_orden As Long, ByVal trabajo As String, ByVal obs As String, ByVal serealiza As Boolean)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("InsertaEnDetOrdSerBAPQT", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = clv_orden
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Trabajo", SqlDbType.VarChar, 10)
        par2.Direction = ParameterDirection.Input
        par2.Value = trabajo
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Obs", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Input
        par3.Value = obs
        com.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@SeRealiza", SqlDbType.Bit)
        par4.Direction = ParameterDirection.Input
        par4.Value = serealiza
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
    Private Sub InsertaRelClientesBnd(ByVal clv_orden As Long, ByVal bnd As Integer)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("InsertaRelClientesBnd", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = clv_orden
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@bnd", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = bnd
        com.Parameters.Add(par2)


        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
    Private Sub validaInternetStatus(ByVal CONTRATO As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("validaInternetStatus", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@contrato", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = CONTRATO
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@bnd", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)


        Try
            con.Open()
            com.ExecuteNonQuery()

            bnd = CInt(par2.Value)

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
    Private Sub DIME_SITELYNET(ByVal contratoOrd As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("DIME_SITELYNET", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = contratoOrd
        com.Parameters.Add(par1)
        Dim par2 As New SqlParameter("@NUMNET", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)
        Dim par3 As New SqlParameter("@NUMTEL", SqlDbType.Int)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)
        Dim par4 As New SqlParameter("@TV", SqlDbType.Int)
        par4.Direction = ParameterDirection.Output
        com.Parameters.Add(par4)
        Dim par5 As New SqlParameter("@DIG", SqlDbType.Int)
        par5.Direction = ParameterDirection.Output
        com.Parameters.Add(par5)

        Try
            con.Open()
            com.ExecuteNonQuery()

            numNet = CInt(par2.Value)
            numTel = CInt(par3.Value)
            numTv = CInt(par4.Value)
            numDig = CInt(par5.Value)

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
    Private Sub validaOrdenBPAQU(ByVal CONTRATO As Long, ByVal orden As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("validaOrdenBPAQU", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@contrato", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = CONTRATO
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@clv_orden", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = orden
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@error", SqlDbType.Int)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)


        Try
            con.Open()
            com.ExecuteNonQuery()

            errorBND = CInt(par3.Value)

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
    Private Sub validaDetOrdserDuplicado(ByVal clv_orden As Long, ByVal clv_trabajo As Long, ByVal OP As Integer)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("validaDetOrdserDuplicado", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_orden", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = clv_orden
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@clv_trabajo", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = clv_trabajo
        com.Parameters.Add(par2)

        Dim par4 As New SqlParameter("@OP", SqlDbType.BigInt)
        par4.Direction = ParameterDirection.Input
        par4.Value = OP
        com.Parameters.Add(par4)

        Dim par3 As New SqlParameter("@error", SqlDbType.Int)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)


        Try
            con.Open()
            com.ExecuteNonQuery()

            errorDuplicado = CInt(par3.Value)

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
    Public Sub MUESTRATRABAJOS_2(ByVal clv_tipser As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MUESTRATRABAJOS_2 ")
        strSQL.Append(CStr(clv_tipser))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBox2.DisplayMember = "Descripcion"
            Me.ComboBox2.ValueMember = "Clv_Trabajo"
            Me.ComboBox2.DataSource = bindingSource

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

End Class