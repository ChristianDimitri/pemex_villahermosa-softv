Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Public Class FrmDesconexionTempo
    Dim orden As Long = 0
    Private customersByCityReport As ReportDocument

    Private Sub FrmDesconexionTempo_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GLOCONTRATOSEL > 0 Then
            Me.TextBox1.Text = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0
        End If
    End Sub

    Private Sub FrmDesconexionTempo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLidia.DimeTipSer_CualEsPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Dim cone1 As New SqlClient.SqlConnection(MiConexion)
        cone1.Open()
        Me.DimeTipSer_CualEsPrincipalTableAdapter.Connection = cone1
        Me.DimeTipSer_CualEsPrincipalTableAdapter.Fill(Me.DataSetLidia.DimeTipSer_CualEsPrincipal)
        cone1.Close()
        colorea(Me, Me.Name)

    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        Dim comando As SqlClient.SqlCommand
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing

        If IsNumeric(Me.TextBox1.Text) = True Then
            CON.Open()
            Me.Dame_statusTableAdapter.Connection = CON
            Me.Dame_statusTableAdapter.Fill(Me.DataSetLidia.Dame_status, Me.TextBox1.Text)
            CON.Close()
            If Me.TextBox2.Text = "I" Or Me.TextBox2.Text = "T" Then
                'Me.DescoTempoTableAdapter1.Connection = CON
                'Me.DescoTempoTableAdapter1.Fill(Me.DataSetLidia.DescoTempo, Me.TextBox1.Text, eRes, eMsg, CLng(orden))                
                CON.Open()
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "DescoTempo "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@Res", SqlDbType.Int)
                    Dim prm2 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
                    Dim prm3 As New SqlParameter("@Clv_ordengenera", SqlDbType.BigInt)

                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output

                    prm.Value = Me.TextBox1.Text
                    prm1.Value = 0
                    prm2.Value = " "
                    prm3.Value = 0
                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    eRes = prm1.Value
                    eMsg = prm2.Value
                    orden = prm3.Value
                End With
                CON.Close()
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    bitsist(GloUsuario, Me.TextBox1.Text, LocGloSistema, "Desconexi�n Temporal por Contrato", "", "Gener� una Desconexion Temporal", "Gener� la Orden de Desconexi�n Temporal: " + CStr(orden), LocClv_Ciudad)
                    ConfigureCrystalReports()
                    MsgBox("El Proceso Finaliz� con �xito", MsgBoxStyle.Information)
                End If
            Else
                MsgBox("Este Cliente no puede Afectarse por este Proceso pues no se encuentra Instalado o en Desconexi�n Temporal", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Capture un Contato V�lido")
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim cone2 As New SqlClient.SqlConnection(MiConexion)
        cone2.Open()
        Me.DimeTipSer_CualEsPrincipalTableAdapter.Connection = cone2
        Me.DimeTipSer_CualEsPrincipalTableAdapter.Fill(Me.DataSetLidia.DimeTipSer_CualEsPrincipal)
        cone2.Close()
        GLOCONTRATOSEL = 0
        GloClv_TipSer = Me.TextBox3.Text
        FrmSelCliente.Show()
    End Sub
    Private Sub ConfigureCrystalReports()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Nothing
            Dim OpOrdenar As String = "0"

            Dim reportPath As String = Nothing

            If IdSistema = "AG" Then

                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
            ElseIf IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            End If


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, CStr(GloClv_TipSer))
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, 1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, 0)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, 0)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, 0)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, 0)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(6, 1)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(7, 0)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(8, 0)
            '@Clv_OrdenIni bigint
            customersByCityReport.SetParameterValue(9, CLng(orden))
            ',@Clv_OrdenFin bigint
            customersByCityReport.SetParameterValue(10, CLng(orden))
            ',@Fec1Ini Datetime
            customersByCityReport.SetParameterValue(11, "01/01/1900")
            ',@Fec1Fin Datetime,
            customersByCityReport.SetParameterValue(12, "01/01/1900")
            '@Fec2Ini Datetime
            customersByCityReport.SetParameterValue(13, "01/01/1900")
            ',@Fec2Fin Datetime
            customersByCityReport.SetParameterValue(14, "01/01/1900")
            ',@Clv_Trabajo int
            customersByCityReport.SetParameterValue(15, 0)
            ',@Clv_Colonia int
            customersByCityReport.SetParameterValue(16, 0)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(17, OpOrdenar)

            mySelectFormula = "Orden " & GloNom_TipSer
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora, CInt(a))
            If a = 1 Then
                MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
                Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If
            '--SetDBLogonForReport(connectionInfo)
            CON.Close()
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


End Class