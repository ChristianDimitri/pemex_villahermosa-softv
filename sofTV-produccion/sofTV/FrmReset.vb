Imports System.Data.SqlClient
Public Class FrmReset
    Dim Clv_TipSer As Integer
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Private TipSer As String = Nothing
    Private Mac As String = Nothing
    Private Sub GuardaDatosBitacora()
        Try
            Dim Cont As Integer = 0
            If Me.RadioButton1.Checked = True Then
                TipSer = Me.RadioButton1.Text
            Else
                TipSer = Me.RadioButton2.Text
            End If
            
            Mac = Me.Clv_CableModemTextBox.Text
            bitsist(GloUsuario, CLng(Me.TextBox1.Text), LocGloSistema, Me.Text, "Se Reseteo Aparato", "", "Servicio: " + TipSer + " / Clave de Tarjeta � CableModem: " + Mac, LocClv_Ciudad)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        eContrato = 0
        BrwSelContrato.Show()
    End Sub

    Private Sub FrmReset_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eContrato > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            Me.TextBox1.Text = eContrato
            CON.Open()
            Me.DameClientesActivosTableAdapter.Connection = CON
            Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
            CON.Close()
        End If

    End Sub

    Private Sub FrmReset_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        eContrato = 0
        If Me.RadioButton1.Checked = True Then
            Clv_TipSer = 3
        End If
        If Me.RadioButton2.Checked = True Then
            Clv_TipSer = 2
        End If
    End Sub
    Private Sub BuscarCliente()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameClientesActivosTableAdapter.Connection = CON
        Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
        CON.Close()
        BuscarServ()
    End Sub
    Private Sub BuscarServ()
        Dim CON As New SqlConnection(MiConexion)
        eRes = 0
        eMsg = ""
        CON.Open()
        Me.MuestraServCteResetTableAdapter.Connection = CON
        Me.MuestraServCteResetTableAdapter.Fill(Me.DataSetEric.MuestraServCteReset, Me.TextBox1.Text, Clv_TipSer, eRes, eMsg)
        CON.Close()
        If eRes = 1 Then
            MsgBox(eMsg)
        End If


    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.RadioButton1.Checked = True Then
            Clv_TipSer = 3
            If IsNumeric(Me.TextBox1.Text) = True Then
                BuscarCliente()
            End If
        
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If Me.RadioButton2.Checked = True Then
            Clv_TipSer = 2
            If IsNumeric(Me.TextBox1.Text) = True Then
                BuscarCliente()
            End If
        
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.MuestraServCteResetDataGridView.RowCount > 0 Then
            eRes = 0
            eMsg = ""
            If Me.ContratoTextBox.Text = 0 Or Me.Clv_CableModemTextBox.Text = 0 Then
                BuscarServ()
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Me.ResetServCteTableAdapter.Connection = CON
            Me.ResetServCteTableAdapter.Fill(Me.DataSetEric.ResetServCte, Me.ContratoTextBox.Text, Me.Clv_CableModemTextBox.Text, Clv_TipSer, eRes, eMsg)
            CON.Close()
            GuardaDatosBitacora()
            MsgBox(eMsg)


        Else
            MsgBox("Selecciona Un Aparato a Resetear", , "Atenci�n")
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(Me.TextBox1.Text) = True Then
                BuscarCliente()
            End If
        End If
    End Sub

    
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub
End Class