﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualBasic

Public Class FrmSeleccionaLimite

    Private Sub FrmSeleccionaLimite_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LLENATABLACOMPARACION()
        LLENACOMBOSUCURSALES()
    End Sub

    Private Sub CancelarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelarButton.Click
        Me.Close()
    End Sub

    Private Sub AceptarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AceptarButton.Click
        If Me.ConllamadaCheckBox.CheckState = False And Me.SinLlamadaCheckBox.CheckState = False Then
            MsgBox("Seleccione un Status de Llamada", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.ConllamadaCheckBox.Checked Then
            GloConLlamada = 1
        Else
            GloConLlamada = 0
        End If

        If Me.SinLlamadaCheckBox.Checked Then
            GloSinLlamada = 1
        Else
            GloSinLlamada = 0
        End If

        GloComparacion = Me.ComparacionComboBox.SelectedValue.ToString
        GloLimite = Me.LimiteNumeric.Value
        Locformulario = 10
        GloLimiteSucursaL = Me.SucursalComboBox.SelectedValue.ToString
        FrmImprimirContrato.Show()
        Me.Close()
    End Sub

    Private Sub LLENATABLACOMPARACION()
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC LLENATABLACOMPARACION ")

        Dim DA As New SqlDataAdapter(StrSql.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.ComparacionComboBox.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub LLENACOMBOSUCURSALES()
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC LLENACOMBOSUCURSALES")

        Dim DA As New SqlDataAdapter(StrSql.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.SucursalComboBox.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
End Class