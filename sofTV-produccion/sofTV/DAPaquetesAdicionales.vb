﻿Imports System.Data
Imports System.Data.SqlClient
Public Class DAPaqueteAdicional
    'Const MiConexion As String = "Data Source=George-PC\GSQL;Initial Catalog=Newsoftv ;Persist Security Info=True;User ID=sa;Password=06011975;Connect Timeout=0"

    Public Sub DAPaqueteAdicion()
        'Construnctor
    End Sub

    'OBTIENE LOS PAQUETES ADICIONALES QUE ESTAN LIGADOS A UN PLAN TARIFARIO Y LOS QUE NO ESTAN LIGADOS
    Public Function GetPaquetesDisponiblesYAsignados(ByVal Clv_Servicio As Integer) As DataSet

        Dim conn As New SqlConnection(MiConexion)
        Dim Adaptador As New SqlDataAdapter
        Dim dsPaquetesAdicionales As New DataSet

        Try


            Dim comando As New SqlClient.SqlCommand("CargaPaquesAdicionalesDisponiblesYAsignadosAServiciosTel", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 5

            Adaptador.SelectCommand = comando
            Adaptador.SelectCommand.Parameters.Add("@clv_servicio", SqlDbType.Int)
            Adaptador.SelectCommand.Parameters(0).Direction = ParameterDirection.Input
            Adaptador.SelectCommand.Parameters(0).Value = Clv_Servicio
            Adaptador.Fill(dsPaquetesAdicionales)

            conn.Open()
            comando.Dispose()

        Catch ex As Exception

            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        Finally

            conn.Close()
            conn.Dispose()

        End Try

        Return dsPaquetesAdicionales
    End Function

    Public Function GetPaquetesParaAsignar(ByVal Clv_UnicaNet As Integer) As DataSet

        Dim conn As New SqlConnection(MiConexion)
        Dim Adaptador As New SqlDataAdapter
        Dim dsPaquetesAdicionales As New DataSet

        Try


            Dim comando As New SqlClient.SqlCommand("CargaPaquetesParaAsignar", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 5

            Adaptador.SelectCommand = comando

            Adaptador.SelectCommand.Parameters.Add("@CLV_UNICANET", SqlDbType.Int)
            Adaptador.SelectCommand.Parameters(0).Direction = ParameterDirection.Input
            Adaptador.SelectCommand.Parameters(0).Value = Clv_UnicaNet

            Adaptador.Fill(dsPaquetesAdicionales)

            conn.Open()
            comando.Dispose()

        Catch ex As Exception

            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        Finally

            conn.Close()
            conn.Dispose()

        End Try

        Return dsPaquetesAdicionales
    End Function

    'AGREGA RELACIÓN ENTRE PAQUETE ADICIONAL Y PLAN TARIFARIO
    Public Function Add_RelPaqueteAdicionalServTel(ByVal Clv_Servicio As Integer, ByVal Clv_PaqAdic As Integer, ByVal Contratacion As Double, ByVal Mensualidad As Double, ByVal CostoAdicional As Double) As Integer

        Dim conn As New SqlConnection(MiConexion)
        Dim IdRel As Integer = 0
        Try

            Dim comando As New SqlClient.SqlCommand("Add_RelPaqueteAdicionalServTel", conn)


            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 5

            comando.Parameters.Add("@Clv_Servicio", SqlDbType.Int)
            comando.Parameters(0).Direction = ParameterDirection.Input
            comando.Parameters(0).Value = Clv_Servicio

            comando.Parameters.Add("@Clv_PaqAdic", SqlDbType.Int)
            comando.Parameters(1).Direction = ParameterDirection.Input
            comando.Parameters(1).Value = Clv_PaqAdic

            comando.Parameters.Add("@Contratacion", SqlDbType.Money)
            comando.Parameters(2).Direction = ParameterDirection.Input
            comando.Parameters(2).Value = Contratacion

            comando.Parameters.Add("@Mensualidad", SqlDbType.Money)
            comando.Parameters(3).Direction = ParameterDirection.Input
            comando.Parameters(3).Value = Mensualidad

            comando.Parameters.Add("@CostoAdicional", SqlDbType.Money)
            comando.Parameters(4).Direction = ParameterDirection.Input
            comando.Parameters(4).Value = CostoAdicional

            comando.Parameters.Add("@IDRel", SqlDbType.Int)
            comando.Parameters(5).Direction = ParameterDirection.Output
            comando.Parameters(5).Value = 0



            conn.Open()
            comando.ExecuteNonQuery()
            IdRel = comando.Parameters(5).Value
            comando.Dispose()

        Catch ex As Exception

            MsgBox("Error al tratar de agregar un Paquete Adicional:" & ex.Message, MsgBoxStyle.Exclamation)

        Finally

            conn.Close()
            conn.Dispose()

        End Try

        Return IdRel
    End Function

    'ELIMINA RELACIÓN ENTRE PAQUETE ADICIONAL Y PLAN TARIFARIO
    Public Sub Delete_RelPaqueteAdicionalServTel(ByVal idRel As Integer)

        Dim conn As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlClient.SqlCommand("Delete_RelPaqueteAdicionalServTel", conn)


            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 5

            comando.Parameters.Add("@IDRel", SqlDbType.Int)
            comando.Parameters(0).Direction = ParameterDirection.Input
            comando.Parameters(0).Value = idRel

            conn.Open()
            comando.ExecuteNonQuery()
            comando.Dispose()

        Catch ex As Exception
            MsgBox("Error al tratar de eliminar el Paquete Adicional del Plan Tarifario: " & ex.Message, MsgBoxStyle.Exclamation)
        Finally

            conn.Close()
            conn.Dispose()

        End Try


    End Sub
End Class
