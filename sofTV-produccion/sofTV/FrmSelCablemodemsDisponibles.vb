Imports System.Data.SqlClient

Public Class FrmSelCablemodemsDisponibles

    Public Sub MUESTRACABLEMODEMS_disponibles_2_IAPAR(ByVal MOp As Long, ByVal MClv_Tecnico As Long, ByVal MClv_Orden As Long)
        '@Op BIGINT,@Clv_Tecnico bigint,@Clv_Orden bigint
        Dim conn As New SqlConnection(MiConexion)
        Try

            Dim reader As SqlDataReader
            Dim cmd As New SqlCommand("MUESTRACABLEMODEMS_disponibles_2_IAPAR", conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@Op", SqlDbType.BigInt).Value = MOp
            cmd.Parameters.Add("@Clv_Tecnico", SqlDbType.BigInt).Value = MClv_Tecnico
            cmd.Parameters.Add("@Clv_Orden", SqlDbType.BigInt).Value = MClv_Orden
            conn.Open()
            reader = cmd.ExecuteReader()

            Using reader
                While reader.Read
                    Me.ComboBox2.Items.Add(reader.GetValue(0))
                    Me.ComboBox1.Items.Add(reader.GetValue(1))
                    'Me.Clave.Text = reader.GetValue(1)
                    'Me.Descripcion.Text = reader.GetValue(2)
                End While
            End Using
            conn.Close()
        Catch ex As Exception
            If conn.State <> ConnectionState.Closed Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloBndClv_CablemodemSel = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.ComboBox2.Text) = True Then
            GloClv_CablemodemSel = Me.ComboBox2.Text
            GloMacCablemodemSel = Me.ComboBox1.Text
            GloBndClv_CablemodemSel = True
        Else
            GloClv_CablemodemSel = 0
            GloMacCablemodemSel = ""
            GloBndClv_CablemodemSel = False
            If IdSistema <> "LO" And IdSistema <> "YU" Or IdSistema = "SA" Then
                MsgBox("No se Seleccionado un Cablemodem", MsgBoxStyle.Information)
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "SA" Then
                MsgBox("No se Seleccionado un ATA", MsgBoxStyle.Information)
            End If
        End If
        Me.Close()
    End Sub

    Private Sub FrmSelCablemodemsDisponibles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        colorea(Me, Me.Name)
        If GloClv_TipSer = 2 Then
            'Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Connection = CON
            'Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRACABLEMODEMS_disponibles_2, 0, Locclv_tec)
            MUESTRACABLEMODEMS_disponibles_2_IAPAR(0, Locclv_tec, gloClv_Orden)
        ElseIf GloClv_TipSer = 3 Then
            'Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Connection = CON
            'Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRACABLEMODEMS_disponibles_2, 3, Locclv_tec)
            MUESTRACABLEMODEMS_disponibles_2_IAPAR(3, Locclv_tec, gloClv_Orden)
        ElseIf GloClv_TipSer = 5 Then
            'Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Connection = CON
            'Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRACABLEMODEMS_disponibles_2, 3, Locclv_tec)
            MUESTRACABLEMODEMS_disponibles_2_IAPAR(5, Locclv_tec, gloClv_Orden)
        End If
        'CON.Close()
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        If ComboBox2.SelectedIndex >= 0 Then
            ComboBox1.SelectedIndex = ComboBox2.SelectedIndex
        End If
        If IsNumeric(Me.ComboBox2.Text) = True Then
            GloClv_CablemodemSel = Me.ComboBox2.Text
            GloMacCablemodemSel = Me.ComboBox1.Text
        End If
    End Sub


    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label5.Click

    End Sub

    Private Sub ComboBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.LostFocus
        If ComboBox1.SelectedIndex < 0 Then
            Me.ComboBox2.Text = ""
            Me.ComboBox1.SelectedIndex = -1
            Me.ComboBox2.SelectedIndex = -1
            GloClv_CablemodemSel = 0
            GloMacCablemodemSel = 0
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedIndex >= 0 Then
            ComboBox2.SelectedIndex = ComboBox1.SelectedIndex
        End If
    End Sub
End Class