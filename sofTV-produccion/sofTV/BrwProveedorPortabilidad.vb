﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
Imports System.Text
Imports System.IO.StreamReader
Imports System.IO.File
Public Class BrwProveedorPortabilidad

    Private Sub BrwProveedorPortabilidad_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        BuscaMuestraProveedor(1, 0, "")
    End Sub

    Private Sub Form3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        BuscaMuestraProveedor(1, 0, "")
    End Sub
    Private Sub BuscaMuestraProveedor(ByVal op As Integer, ByVal clave As Long, ByVal nombre As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC BuscaMuestraProveedor ")
        strSQL.Append(CStr(op) & ",")
        strSQL.Append(CStr(clave) & ",")
        strSQL.Append("'" & nombre & "'")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable

            Me.DataGridView1.DataSource = bindingSource

            Me.DataGridView1.Columns(0).Width = 100
            Me.DataGridView1.Columns(1).Width = 450

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(Me.TextBox2.Text) = True Then
            BuscaMuestraProveedor(3, CLng(Me.TextBox2.Text), "")
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Len(Me.TextBox2.Text) > 0 Then
            BuscaMuestraProveedor(2, CLng(Me.TextBox2.Text), "")
        End If

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        OPCIONPP = "N"
        FrmDatosProveedorPortabilidad.Show()
    End Sub
    Private Sub datos()
        Try
            Me.folio.Text = (Me.DataGridView1.Item(0, Me.DataGridView1.CurrentRow.Index).Value)
            Me.nombre.Text = (Me.DataGridView1.Item(1, Me.DataGridView1.CurrentRow.Index).Value)

        Catch ex As Exception

        End Try
    End Sub
    Private Sub DataGridView1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        datos()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If IsNumeric(Me.folio.Text) = True Then
            OPCIONPP = "C"
            IdPP = CLng(Me.folio.Text)
            FrmDatosProveedorPortabilidad.Show()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If IsNumeric(Me.folio.Text) = True Then
            OPCIONPP = "M"
            IdPP = CLng(Me.folio.Text)
            
            FrmDatosProveedorPortabilidad.Show()
        End If
    End Sub
End Class