Imports System.Data.SqlClient
Public Class FrmSelPaquete_Reactiva
    Private Sub FrmSelPaquete_Reactiva_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ReactivacionTableAdapter.Connection = CON
            Me.ReactivacionTableAdapter.Fill(Me.DataSetLidia.Reactivacion, tempocontrato)
            Me.ComboBox1.Text = ""

            'If Me.TextBox1.Text = 2 Then
            '    eContratoRec = tempocontrato
            '    Dim resp As MsgBoxResult = MsgBoxResult.Cancel
            '    GloClv_Cablemodem = 0
            '    'GloClv_Servicio = 0
            '    GloClv_TipSer = 2
            '    resp = MsgBox("� El Cliente cuenta con Cablemodem Propio ? ", MsgBoxStyle.YesNoCancel)
            '    If resp = MsgBoxResult.Yes Then
            '        eCabModPropio = False
            '        FrmCabModPropio.Show()
            '    ElseIf resp = MsgBoxResult.No Then
            '        eCabModPropio = True
            '        resp = MsgBox("� El modem es Al�mbrico ? ", MsgBoxStyle.YesNoCancel)
            '        If resp = MsgBoxResult.Yes Then
            '            GloTipoCablemodem = 1
            '            ' "Alambrico"
            '        ElseIf resp = MsgBoxResult.No Then
            '            GloTipoCablemodem = 2
            '            '"Inalambrico"
            '        End If
            '        pasa = 1
            '        FrmSelServicios.Show()
            '        Me.Close()
            '    ElseIf resp = MsgBoxResult.Cancel Then
            '        Me.Close()
            '        pasa = 0
            '    End If
            'End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.ComboBox1.Text = "" Then
            MsgBox("Se Requiere que se Elija un Servicio a Reactivar")
        Else
            eContratoRec = tempocontrato
            GloClv_Servicio = Me.ComboBox1.SelectedValue
            GloClv_TipSer = Me.TextBox1.Text
            pasa = 1
            'FrmReactivarContrato.Activate()
            Me.Close()
            Me.Dispose()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim resultado As MsgBoxResult = MsgBoxResult.Cancel
        resultado = MsgBox("� No se Guardar�n los Cambios, Est� Seguro de que Desea Cancelar ?", MsgBoxStyle.YesNo)
        If resultado = MsgBoxResult.Yes Then
            eContratoRec = 0
            pasa = 2
            Me.Close()
        End If
    End Sub
End Class