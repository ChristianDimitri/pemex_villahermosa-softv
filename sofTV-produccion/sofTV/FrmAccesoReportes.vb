Imports System.Data.SqlClient
Public Class FrmAccesoReportes
    Private Sub Permiso()
        Dim CON As New SqlConnection(MiConexion)
        Try
            Dim bnd As Integer = 0
            Dim clv_usuario As Integer = 0
            If GloTipoUsuario = 40 Or GloTipoUsuario = 42 Then
                bnd = 1
            Else
                CON.Open()
                Me.Dame_clvtipusuarioTableAdapter.Connection = CON
                Me.Dame_clvtipusuarioTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_clvtipusuario, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, clv_usuario)
                CON.Close()
                Select Case clv_usuario
                    Case 1
                        CON.Open()
                        Me.VerAcceso_ChecaTableAdapter.Connection = CON
                        Me.VerAcceso_ChecaTableAdapter.Fill(Me.DataSetEdgarRev2.VerAcceso_Checa, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, New System.Nullable(Of Integer)(CType(40, Integer)), bnd)
                        CON.Close()
                    Case 40
                        CON.Open()
                        Me.VerAcceso_ChecaTableAdapter.Connection = CON
                        Me.VerAcceso_ChecaTableAdapter.Fill(Me.DataSetEdgarRev2.VerAcceso_Checa, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, New System.Nullable(Of Integer)(CType(40, Integer)), bnd)
                        CON.Close()
                    Case 42 And IdSistema = "AG"
                        CON.Open()
                        Me.VerAcceso_ChecaTableAdapter.Connection = CON
                        Me.VerAcceso_ChecaTableAdapter.Fill(Me.DataSetEdgarRev2.VerAcceso_Checa, Me.Clv_UsuarioTextBox.Text, Me.PasaporteTextBox.Text, New System.Nullable(Of Integer)(CType(40, Integer)), bnd)
                        CON.Close()
                    Case Else
                        bnd = 0
                End Select
            End If
           
            If bnd = 1 And eBndContratoF = True Then
                eOpVentas = 40
                FrmSelTipServE.Show()
                Me.Close()
            ElseIf bnd = 1 And LReporte1 = True Then
                LReporte1 = False
                FrmSelContratoRango.Show()
                Me.Close()
            ElseIf bnd = 1 And LReporte2 = True Then
                LReporte2 = False
                FrmMiMenu.reporte_Paquetes()
                Me.Close()
            ElseIf bnd = 1 And LReportecombo = True Then
                LReportecombo = False
                FrmMiMenu.reporte_combo()
                Me.Close()
            ElseIf bnd = 1 Then
                bndreporte = True
                Me.Close()
            Else
                MsgBox("No Tiene Acceso ", MsgBoxStyle.Information)
                Me.Close()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Permiso()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        GloPermisoCortesia = 0
        Me.Close()
    End Sub

    Private Sub FrmAccesopoUsuario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If GloOpPermiso = 0 Then
            Me.CMBClv_UsuarioLabel.Text = "Login del Administrador : "
            Me.CMBPasswordLabel.Text = "Contraseņa del Administrador :"
        End If
    End Sub

    Private Sub Clv_UsuarioTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Clv_UsuarioTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Permiso()
        End If
    End Sub

    Private Sub PasaporteTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PasaporteTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Permiso()
        End If
    End Sub

End Class