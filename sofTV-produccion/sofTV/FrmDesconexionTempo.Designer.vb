<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDesconexionTempo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Dame_statusBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.DimeTipSer_CualEsPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.DescoTempoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DescoTempoTableAdapter = New sofTV.DataSetEricTableAdapters.DescoTempoTableAdapter()
        Me.Dame_statusTableAdapter = New sofTV.DataSetLidiaTableAdapters.Dame_statusTableAdapter()
        Me.DimeTipSer_CualEsPrincipalTableAdapter = New sofTV.DataSetLidiaTableAdapters.DimeTipSer_CualEsPrincipalTableAdapter()
        Me.DescoTempoBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DescoTempoTableAdapter1 = New sofTV.DataSetLidiaTableAdapters.DescoTempoTableAdapter()
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.Dame_Impresora_OrdenesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Impresora_OrdenesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Dame_Impresora_OrdenesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.Dame_statusBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimeTipSer_CualEsPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DescoTempoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DescoTempoBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_Impresora_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(75, 9)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(253, 16)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Proceso de Desconexión Temporal"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(47, 54)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(74, 16)
        Me.CMBLabel2.TabIndex = 1
        Me.CMBLabel2.Text = "Contrato :"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(127, 54)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(166, 22)
        Me.TextBox1.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(50, 114)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(132, 35)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_statusBindingSource, "status", True))
        Me.TextBox2.Location = New System.Drawing.Point(289, 221)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(84, 20)
        Me.TextBox2.TabIndex = 4
        '
        'Dame_statusBindingSource
        '
        Me.Dame_statusBindingSource.DataMember = "Dame_status"
        Me.Dame_statusBindingSource.DataSource = Me.DataSetLidia
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(207, 114)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(132, 35)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "&SALIR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(311, 55)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(28, 19)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "..."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'TextBox3
        '
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DimeTipSer_CualEsPrincipalBindingSource, "Clv_TipSer", True))
        Me.TextBox3.Location = New System.Drawing.Point(169, 211)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(87, 20)
        Me.TextBox3.TabIndex = 7
        '
        'DimeTipSer_CualEsPrincipalBindingSource
        '
        Me.DimeTipSer_CualEsPrincipalBindingSource.DataMember = "DimeTipSer_CualEsPrincipal"
        Me.DimeTipSer_CualEsPrincipalBindingSource.DataSource = Me.DataSetLidia
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DescoTempoBindingSource
        '
        Me.DescoTempoBindingSource.DataMember = "DescoTempo"
        Me.DescoTempoBindingSource.DataSource = Me.DataSetEric
        '
        'DescoTempoTableAdapter
        '
        Me.DescoTempoTableAdapter.ClearBeforeFill = True
        '
        'Dame_statusTableAdapter
        '
        Me.Dame_statusTableAdapter.ClearBeforeFill = True
        '
        'DimeTipSer_CualEsPrincipalTableAdapter
        '
        Me.DimeTipSer_CualEsPrincipalTableAdapter.ClearBeforeFill = True
        '
        'DescoTempoBindingSource1
        '
        Me.DescoTempoBindingSource1.DataMember = "DescoTempo"
        Me.DescoTempoBindingSource1.DataSource = Me.DataSetLidia
        '
        'DescoTempoTableAdapter1
        '
        Me.DescoTempoTableAdapter1.ClearBeforeFill = True
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dame_Impresora_OrdenesBindingSource
        '
        Me.Dame_Impresora_OrdenesBindingSource.DataMember = "Dame_Impresora_Ordenes"
        Me.Dame_Impresora_OrdenesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Dame_Impresora_OrdenesTableAdapter
        '
        Me.Dame_Impresora_OrdenesTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmDesconexionTempo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(360, 169)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.MaximizeBox = False
        Me.Name = "FrmDesconexionTempo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Desconexión Temporal"
        CType(Me.Dame_statusBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimeTipSer_CualEsPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DescoTempoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DescoTempoBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_Impresora_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents Dame_statusBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_statusTableAdapter As sofTV.DataSetLidiaTableAdapters.Dame_statusTableAdapter
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents DimeTipSer_CualEsPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DimeTipSer_CualEsPrincipalTableAdapter As sofTV.DataSetLidiaTableAdapters.DimeTipSer_CualEsPrincipalTableAdapter
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents DescoTempoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DescoTempoTableAdapter As sofTV.DataSetEricTableAdapters.DescoTempoTableAdapter
    Friend WithEvents DescoTempoBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DescoTempoTableAdapter1 As sofTV.DataSetLidiaTableAdapters.DescoTempoTableAdapter
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Dame_Impresora_OrdenesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Impresora_OrdenesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Dame_Impresora_OrdenesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
