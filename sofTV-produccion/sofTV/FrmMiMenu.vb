'Imports sofTV.Org.Mentalis.Multimedia
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Collections.Generic

Public Class FrmMiMenu

    'Dim v As New VideoFile(RutaReportes + "\Read.avi", Me.Panel1)
    ' Puede cambiar más opciones de impresión mediante la propiedad PrintOptions de ReportDocument

    Dim j As Integer = 0
    Dim Proceso As New System.Diagnostics.Process
    Dim proceso2 As New System.Diagnostics.Process
    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'FrmClientes.Show()
        BrwClientes.Show()
    End Sub

    Private Sub MenuItem19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        '     BrwCalles.Button1.Visible = False
        BrwCalles.Show()
    End Sub

    Private Sub MenuItem18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwColonias.Show()
    End Sub

    Private Sub MenuItem20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwCiudades.Show()
    End Sub

    Private Sub MenuItem14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwTiposServicios.Show()
    End Sub

    Private Sub MenuItem15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwServicios.Show()
    End Sub

    Private Sub MenuItem16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwCablemodems.Show()
    End Sub

    Private Sub MenuItem21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwPromociones.Show()
    End Sub

    Private Sub MenuItem22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwSucursales.Show()
    End Sub

    Private Sub MenuItem23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwBancos.Show()
    End Sub

    Private Sub MenuItem24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwUsuarios.Show()
    End Sub
    Private Sub MenuItem26_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwCajas.Show()
    End Sub

    Private Sub MenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwPuestoTec.Show()
    End Sub

    Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwTrabajos.Show()
    End Sub

    Private Sub MenuItem12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BRWTECNICOS.Show()
    End Sub

    Private Sub MenuItem58_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BRWORDSER.Show()
    End Sub

    Private Sub MenuItem66_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BRWQUEJAS.Show()
    End Sub

    Private Sub MenuItem68_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwAnttelQuejas.Show()
    End Sub

    Private Sub MenuItem7_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwTiposColonia.Show()
    End Sub

    Private Sub MenuItem73_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub MenuItem67_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwAgenda.Show()
    End Sub

    Private Sub MenuItem74_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub MenuItem17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BRWMotivoCancelacion.Show()
    End Sub


    Private Sub MenuItem126_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BRWBUSCACNR.Show()
    End Sub

    Private Sub MenuItem83_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmImprimir.Show()
    End Sub

    Private Sub MenuItem124_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmGenerales_Sistema.Show()
    End Sub

    Private Sub MenuItem13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ClientesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientesToolStripMenuItem1.Click
        If IdSistema = "VA" Or IdSistema = "LO" Or IdSistema = "YU" Then
            BrwClientes2.Show()
        Else
            BrwClientes.Show()
        End If
    End Sub

    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        bitsist(GloUsuario, 0, LocGloSistema, "Salida Del Sistema", "", "Salida Del Sistema", GloSucursal, LocClv_Ciudad)
        End
    End Sub

    Private Sub OrdenesDeServicioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OrdenesDeServicioToolStripMenuItem.Click
        BRWORDSER.Show()
    End Sub


    Private Sub AtenciónTelefónicaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AtenciónTelefónicaToolStripMenuItem.Click
        BrwAnttelQuejas.Show()
    End Sub

    Private Sub ReportesVariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReportesVariosToolStripMenuItem.Click
        FrmImprimir.Show()
    End Sub

    Private Sub GeneralesDelSistemaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesDelSistemaToolStripMenuItem.Click
        opcFrm = 2
        Acceso_TipoServicios.Show()

    End Sub

    Private Sub InterfasCablemodemsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InterfasCablemodemsToolStripMenuItem.Click
        'opcFrm = 3
        BRWBUSCACNR.Show()
        'Acceso_TipoServicios.Show()

    End Sub

    Private Sub ClasificaciónTécnicaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClasificaciónTécnicaToolStripMenuItem.Click
        BrwPuestoTec.Show()
    End Sub

    Private Sub ServiciosAlClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServiciosAlClienteToolStripMenuItem.Click

        Dim frm_servicioCliente As New SoftvNew.BrwTrabajos
        frm_servicioCliente.ShowDialog()

        'BrwTrabajos.Show()
    End Sub

    'Private Sub TecnicosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TecnicosToolStripMenuItem.Click
    '    BRWTECNICOS.Show()
    'End Sub

    Private Sub TiposDeServicioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TiposDeServicioToolStripMenuItem.Click
        opcFrm = 1
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub ServiciosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServiciosToolStripMenuItem.Click
        BrwServicios.Show()
    End Sub

    Private Sub CableModemsYAparatosDigitalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CableModemsYAparatosDigitalesToolStripMenuItem.Click
        BrwCablemodems.Show()
    End Sub

    Private Sub ColoniasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ColoniasToolStripMenuItem.Click
        'BrwColonias.Show()
        BrwColoniasBueno.Show()
    End Sub

    Private Sub TiposDeColoniasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TiposDeColoniasToolStripMenuItem.Click
        BrwTiposColonia.Show()
    End Sub

    Private Sub CallesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CallesToolStripMenuItem.Click
        BrwCalles.Show()
    End Sub

    Private Sub CiudadesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CiudadesToolStripMenuItem.Click
        BrwCiudades.Show()
    End Sub

    Private Sub PromocionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PromocionesToolStripMenuItem.Click
        BrwPromociones.Show()
    End Sub

    Private Sub SucursalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SucursalesToolStripMenuItem.Click
        BrwSucursales.Show()
    End Sub

    Private Sub BancosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BancosToolStripMenuItem.Click
        BrwBancos.Show()
    End Sub

    Private Sub UsuariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UsuariosToolStripMenuItem.Click
        BrwUsuarios.Show()
    End Sub

    Private Sub CajasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CajasToolStripMenuItem.Click
        BrwCajas.Show()
    End Sub

    Private Sub MotivosDeCancelaciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MotivosDeCancelaciónToolStripMenuItem.Click
        BRWMotivoCancelacion.Show()
    End Sub

    Private Sub FrmMiMenu_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If BndSiCorrerDesconexion = True Then
            BndSiCorrerDesconexion = False
            TextClv_Session.Text = LocClv_session
            If BndGloDesPeriodo = 1 Then
                desconexionSA()
            ElseIf BndGloDesPeriodo = 2 Then
                desconexionSA2()
            End If
        End If

    End Sub

    Private Sub FrmMiMenu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        bitsist(GloUsuario, 0, LocGloSistema, "Salida Del Sistema", "", "Salida Del Sistema", GloSucursal, LocClv_Ciudad)
        End
    End Sub

    Private Sub FrmMiMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DataSetLidia.MUESTRAIMAGEN' Puede moverla o quitarla según sea necesario.
        'Me.MUESTRAIMAGENTableAdapter.Fill(Me.DataSetLidia.MUESTRAIMAGEN)

        SP_Dame_General_sistema_II()

        BndGloDesPeriodo = 0

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        Me.DameEspecifTableAdapter.Connection = CON
        Me.DameEspecifTableAdapter.Fill(Me.DataSetLidia.DameEspecif, ColorBut, ColorLetraBut, ColorMenu, ColorMenuLetra, ColorBwr, ColorBwrLetra, ColorGrid, ColorForm, ColorLabel, ColorLetraLabel, ColorLetraForm)
        CON.Close()
        bitsist(GloUsuario, 0, LocGloSistema, "Entrada Sistema", "", "Entrada Al Sistema", GloSucursal, LocClv_Ciudad)

        'Dim asm As Reflection.Assembly = Reflection.Assembly.LoadFrom(Application.StartupPath + "\sofTV.exe")
        'For Each frm As Form In     'sofTV.My.Application.OpenForms
        '   colorea(Me, Me.Name)
        'Next
        'Dim name As String = My.Forms.BrwrSectores.Name
        'Dim form As Form
        'form = New Form
        'form = CType(name, Form)
        'form.Show()
        Me.CMBLabel1.Text = GloCiudad
        If IdSistema = "TO" Then
            Me.GeneralesDeBancosToolStripMenuItem.Visible = False
            Me.GeneralesDeInterfacesDigitalesToolStripMenuItem.Visible = False
            Me.GeneralesDeInterfacesInternetToolStripMenuItem.Visible = False
            Me.GeneralesDeOXXOToolStripMenuItem.Visible = False
            Me.EncargadosDelSistemaToolStripMenuItem.Visible = False
        End If
        If IdSistema <> "TO" Then
            Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Visible = False
            Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Visible = False
            Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Visible = False
        End If
        If IdSistema = "SA" Then
            'Me.ProcesosDeServicioPremiumToolStripMenuItem.Visible = False
            Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Visible = True
            Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Visible = True
            Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Visible = True
            Me.ProcesoDeCierreDeMesToolStripMenuItem.Visible = True
            Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Visible = True
            'Me.SegundoPeriodoToolStripMenuItem1.Visible = False
            Me.GeneralesDeOXXOToolStripMenuItem.Visible = False
            Me.DepuraciónDeÓrdenesToolStripMenuItem.Visible = True
            Me.MesajesInstantaneosToolStripMenuItem.Visible = False
            Me.DesconexionToolStripMenuItem.Visible = True
        End If
        If IdSistema = "VA" Then
            Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Visible = True
            Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Visible = True
            Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Visible = True
            Me.ProcesoDeCierreDeMesToolStripMenuItem.Visible = True
            'Me.ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Visible = True
            Me.CargosEspecialesToolStripMenuItem.Visible = True
            Me.PreciosDeArticulosDeInstalaciónToolStripMenuItem.Visible = True
            Me.ClientesConAdeudoDeMaterialToolStripMenuItem.Visible = True
            'Me.ProcesoDeReactivaciónDeContratoToolStripMenuItem.Visible = False
            Me.MesajesInstantaneosToolStripMenuItem.Visible = False
            Me.ActivaciónPaqueteDePruebaToolStripMenuItem.Visible = False
            Me.ProcesoDeReactivaciónDeContratoToolStripMenuItem.Visible = False
        End If
        'If SubCiudad <> "TV" And SubCiudad <> "VA" And SubCiudad <> "SA" Then
        '    Me.PolizaToolStripMenuItem.Visible = False
        'End If

        CON.Open()
        Me.MUESTRAIMAGENTableAdapter.Connection = CON
        Me.MUESTRAIMAGENTableAdapter.Fill(Me.DataSetLidia.MUESTRAIMAGEN)
        'LLENA LA TABLA DE MENUS AUTOMATICAMENTE
        Me.DameTipoUsusarioTableAdapter.Connection = CON
        Me.DameTipoUsusarioTableAdapter.Fill(Me.DataSetLidia.DameTipoUsusario, GloUsuario, GloTipoUsuario)
        'Me.DamePermisosTableAdapter.Connection = CON
        'Me.DamePermisosTableAdapter.Fill(Me.DataSetLidia.DamePermisos, GloTipoUsuario, Me.Text, 1, glolec, gloescr, gloctr)
        CON.Close()
        'Me.RecorrerEstructuraMenu(Me.MenuStrip1)
        ' Me.RecorrerFormularios()

        'Validación de Menus de la barra principal
        CatálogosToolStripMenuItem.Enabled = TipoAccesoMenusPerfiles(GloTipoUsuario, "CatálogosToolStripMenuItem")
        ProcesosToolStripMenuItem.Enabled = TipoAccesoMenusPerfiles(GloTipoUsuario, "ProcesosToolStripMenuItem")
        ReportesToolStripMenuItem.Enabled = TipoAccesoMenusPerfiles(GloTipoUsuario, "ReportesToolStripMenuItem")
        GeneralesToolStripMenuItem.Enabled = TipoAccesoMenusPerfiles(GloTipoUsuario, "GeneralesToolStripMenuItem")

        colorea(Me, Me.Name)

        If IdSistema = "TO" Then
            'Me.TecnicosToolStripMenuItem.Visible = False
            Me.CatálogoDeVentasToolStripMenuItem.Visible = False
            Me.VentasToolStripMenuItem.Visible = False
            Me.ReportesDelCanalToolStripMenuItem.Visible = False
            Me.CableModemsYAparatosDigitalesToolStripMenuItem.Visible = False
            Me.InterfasCablemodemsToolStripMenuItem.Visible = False
            Me.InterfasDecodificadoresToolStripMenuItem.Visible = False
            Me.ContratoMaestroToolStripMenuItem.Visible = False
        End If
        If IdSistema = "AG" Then
            Me.TelefoníaToolStripMenuItem.Visible = False
            'Me.GeneralesProsaBancomerToolStripMenuItem.Visible = True
        End If
        If IdSistema = "LO" Or IdSistema = "YU" Then
            Me.EstadoDeCuentaToolStripMenuItem.Visible = False
            'Me.EstadoDeCuentaTelefoníaToolStripMenuItem.Visible = True
            Me.TelefoníaToolStripMenuItem.Visible = True
            Me.TelefoníaToolStripMenuItem1.Visible = True
            Me.CargosEspecialesToolStripMenuItem.Visible = True
            'Procesos
            Me.CambioDeClienteAClienteNormalToolStripMenuItem.Visible = False
            Me.CambioDeClienteASoloInternetToolStripMenuItem.Visible = False
            Me.CortesToolStripMenuItem.Visible = True
            Me.ReprocesamientoPorClienteToolStripMenuItem.Visible = True
            Me.ProcesoDeReactivaciónDeContratoToolStripMenuItem.Visible = False
            'Reportes
            Me.InterfazCablemodemsToolStripMenuItem.Visible = False
            Me.InterfazDigitalesToolStripMenuItem.Visible = False
            Me.CarteraEjecutivaToolStripMenuItem.Visible = False
            Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem.Visible = False
            Me.DesgloceDeMensualidadesAdelantadasToolStripMenuItem.Visible = False
            Me.PuntosDeAntigüedadToolStripMenuItem.Visible = False
            ''Reportes Ventas
            Me.PelículasToolStripMenuItem.Visible = False
            Me.PPVToolStripMenuItem.Visible = False
            'Generales
            Me.InterfasCablemodemsToolStripMenuItem.Visible = False
            Me.InterfasDecodificadoresToolStripMenuItem.Visible = False
            Me.GeneralesDeInterfacesDigitalesToolStripMenuItem.Visible = False
            Me.GeneralesDeInterfacesInternetToolStripMenuItem.Visible = False
            Me.GeneralesDeOXXOToolStripMenuItem.Visible = False
            'Me.ReferenciasBancariasToolStripMenuItem1.Visible = True
            Me.ProcesamientoDeLlamadasToolStripMenuItem.Visible = True
            Me.ProcesamientoDeCDRToolStripMenuItem.Visible = True
        End If

        If IdSistema <> "LO" And IdSistema <> "YU" And IdSistema <> "SA" Then
            TelefoníaToolStripMenuItem.Visible = False
        End If
        'ElseIf IdSistema <> "AG" Then
        '    Me.ImporteDeMensualidadesAdelantadasToolStripMenuItem.Visible = False
        'End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_usuario", SqlDbType.VarChar, GloUsuario, 10)
        BaseII.CreateMyParameter("@Nombre", ParameterDirection.Output, SqlDbType.VarChar, 250)
        BaseII.ProcedimientoOutPut("UspDameNombreUsuario")
        Me.Label2.Text = ""
        Me.Label2.Text = BaseII.dicoPar("@Nombre").ToString()
    End Sub

    Private Sub PromotoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PromotoresToolStripMenuItem.Click
        BrwVendedores.Show()
    End Sub

    Private Sub SeriesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeriesToolStripMenuItem.Click
        BrwCatalogoSeries.Show()
    End Sub

    Private Sub OrdenesDeServicioToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OrdenesDeServicioToolStripMenuItem1.Click
        GloRepAreaTec = "O"
        Frmareatecnica.Show()
    End Sub

    Private Sub QuejasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuejasToolStripMenuItem1.Click
        BRWQUEJAS.Show()
    End Sub

    Private Sub EstadosDeCarteraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EstadosDeCarteraToolStripMenuItem.Click
        FrmCarteras.Show()
    End Sub

    Private Sub QuejasToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuejasToolStripMenuItem2.Click
        GloRepAreaTec = "Q"
        FrmQuejas.Show()
    End Sub

    Private Sub ÁreasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ÁreasToolStripMenuItem.Click
        BrwTap.Show()
    End Sub

    Private Sub TiposDePromotoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TiposDePromotoresToolStripMenuItem.Click

    End Sub

    Private Sub SectoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SectoresToolStripMenuItem.Click
        BrwSectore.Show()
    End Sub

    'Private Sub RecorrerEstructuraMenu(ByVal oMenu As MenuStrip)
    '    Dim CON As New SqlConnection(MiConexion)
    '    Dim menu As ToolStripMenuItem

    '    For Each oOpcionMenu In oMenu.Items
    '        CON.Open()
    '        Me.DamePermisosTableAdapter.Connection = CON
    '        Me.DamePermisosTableAdapter.Fill(Me.DataSetLidia.DamePermisos, GloTipoUsuario, oOpcionMenu.Text, oOpcionMenu.Name, 1, glolec, gloescr, gloctr)
    '        menu = New ToolStripMenuItem
    '        menu = oOpcionMenu
    '        If gloctr = 1 Then
    '            menu.Enabled = False
    '        End If
    '        menu = Nothing
    '        j = j + 1
    '        Me.ALTASMENUSTableAdapter.Connection = CON
    '        Me.ALTASMENUSTableAdapter.Fill(Me.DataSetLidia.ALTASMENUS, oOpcionMenu.Text, 1, oOpcionMenu.Name, 10)
    '        CON.Close()
    '        If oOpcionMenu.DropDownItems.Count > 0 Then
    '            Me.RecorrerSubmenu(oOpcionMenu.DropDownItems, "----")
    '        End If
    '    Next
    'End Sub

    'Private Sub RecorrerSubmenu(ByVal oSubmenuItems As ToolStripItemCollection, ByVal sGuiones As String)
    '    Dim CON As New SqlConnection(MiConexion)
    '    Dim submenu As ToolStripItem
    '    For Each oSubitem As ToolStripItem In oSubmenuItems
    '        If oSubitem.GetType Is GetType(ToolStripMenuItem) Then
    '            CON.Open()
    '            Me.DamePermisosTableAdapter.Connection = CON
    '            Me.DamePermisosTableAdapter.Fill(Me.DataSetLidia.DamePermisos, GloTipoUsuario, oOpcionMenu.Text + " " + oSubitem.Text, oSubitem.Name, 1, glolec, gloescr, gloctr)
    '            submenu = New ToolStripMenuItem
    '            submenu = oSubitem
    '            If gloctr = 1 Then
    '                submenu.Enabled = False
    '            End If
    '            submenu = Nothing
    '            Me.ALTASMENUSTableAdapter.Connection = CON
    '            Me.ALTASMENUSTableAdapter.Fill(Me.DataSetLidia.ALTASMENUS, (oOpcionMenu.Text + " " + oSubitem.Text), 1, oSubitem.Name, j)
    '            CON.Close()
    '            Menu = Nothing
    '            If CType(oSubitem, ToolStripMenuItem).DropDownItems.Count > 0 Then
    '                Me.RecorrerSubmenu(CType(oSubitem, ToolStripMenuItem).DropDownItems, sGuiones & "----")
    '            End If
    '        End If
    '    Next
    'End Sub

    'Public Sub RecorrerFormularios()
    '    Dim CON As New SqlConnection(MiConexion)
    '    'DESDE UN PROYECTO DE FUERA 
    '    'Dim asm As System.Reflection.Assembly = System.Reflection.Assembly.LoadFile("c:\prog.exe")
    '    ' Obtengo el ensamblado 
    '    Reflection.Assembly.LoadFile(Application.StartupPath + "Softv.exe")
    '    Dim asm As Reflection.Assembly = Reflection.Assembly.GetCallingAssembly
    '    Dim t As System.Type
    '    ' Obtengo todos los tipos definidos en el ensamblado
    '    ' Obtengo el tipo System.Windows.Forms.Form 
    '    ' Recorro los tipos buscando aquellos que 
    '    ' hereden de System.Windows.Forms.Form
    '    'For Each CT As Control In asm.GetObjectData

    '    For Each t In asm.GetTypes()
    '        Dim base As Type = t.BaseType
    '        If base Is GetType(System.Windows.Forms.Form) Then
    '            base = t.BaseType
    '        End If
    '        If base Is GetType(System.Windows.Forms.Form) Then
    '            CON.Open()
    '            Me.ALTASformsTableAdapter.Connection = CON
    '            Me.ALTASformsTableAdapter.Fill(Me.DataSetLidia.ALTASforms, t.Name, 1, 0, t.Name)
    '            CON.Close()
    '        End If

    '    Next
    'End Sub
    Private Sub LlamadasTelefónicasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LlamadasTelefónicasToolStripMenuItem.Click
        GloRepAreaTec = "L"
        FrmLlamadasTelefonicasReporte.Show()
    End Sub

    Private Sub GeneralesDeInterfacesDigitalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesDeInterfacesDigitalesToolStripMenuItem.Click
        opcFrm = 7
        Acceso_TipoServicios.Show()

    End Sub

    Private Sub InterfasDecodificadoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InterfasDecodificadoresToolStripMenuItem.Click
        ' opcFrm = 4
        BRWCNRDIG.Show()
        'Acceso_TipoServicios.Show()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click


    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        GloClv_tipser2 = InputBox("Dame el Clave de Servicio", "Prueba")
        FrmSelServRep.Show()

    End Sub

    Private Sub GeneralesDeOXXOToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesDeOXXOToolStripMenuItem.Click
        opcFrm = 8
        Acceso_TipoServicios.Show()

    End Sub

    Private Sub ReportesDinamicosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ConfiguracionDelSistemaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfiguracionDelSistemaToolStripMenuItem.Click
        With Proceso
            .StartInfo.FileName = RutaReportes + "\Softkinssoftv.exe"
            .StartInfo.WindowStyle = ProcessWindowStyle.Normal
            .StartInfo.WorkingDirectory = System.Environment.CurrentDirectory
            .Start()
        End With
        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Acceso a Softv Skin", "", "", LocClv_Ciudad)
    End Sub

    Private Sub ReporteDinámicosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteDinámicosToolStripMenuItem.Click
        With proceso2
            .StartInfo.FileName = Application.StartupPath + "\Reportes\" + "\WizardReport.exe"
            .StartInfo.WindowStyle = ProcessWindowStyle.Normal
            .StartInfo.WorkingDirectory = System.Environment.CurrentDirectory
            .Start()
        End With
    End Sub

    Private Sub GeneralesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub GeneralesDeBancosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesDeBancosToolStripMenuItem.Click

    End Sub

    Private Sub ProcesosDeCobranzaBancariaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        opcFrm = 5
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub BancosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BancosToolStripMenuItem1.Click
        opcFrm = 6
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub MotivosDeCancelaciónFacturasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MotivosDeCancelaciónFacturasToolStripMenuItem.Click
        eReImpresion = 0
        BrwMotivosCancelacionFactura.Show()
    End Sub

    Private Sub MotivosDeReImpresiónFacturasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MotivosDeReImpresiónFacturasToolStripMenuItem.Click
        eReImpresion = 1
        BrwMotivosCancelacionFactura.Show()
    End Sub

    Private Sub GeneralesDeInterfacesInternetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesDeInterfacesInternetToolStripMenuItem.Click
        opcFrm = 5
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub RangosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RangosToolStripMenuItem.Click
        BrwRangos.Show()
    End Sub

    Private Sub PrecioDeComisionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrecioDeComisionesToolStripMenuItem.Click
        'BrwRangosComisiones.Show()
        FrmRangosComisiones.Show()
    End Sub

    Private Sub CalcularComisionesPorVendedorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub VentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VentasToolStripMenuItem.Click

    End Sub

    Private Sub CalcularComisionesPorVendedorToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalcularComisionesPorVendedorToolStripMenuItem1.Click
        'FrmComision.Show()
        FrmRepGralVentas.Show()
    End Sub


    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub



    Private Sub SoftvToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SoftvToolStripMenuItem.Click
        With Proceso
            .StartInfo.FileName = RutaReportes + "\Rutas_Reportes.exe"
            .StartInfo.WindowStyle = ProcessWindowStyle.Normal
            .StartInfo.WorkingDirectory = System.Environment.CurrentDirectory
            .Start()
        End With
        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Acceso a Ruta Reportes Softv", "", "", LocClv_Ciudad)
    End Sub

    Private Sub FacsoftvToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FacsoftvToolStripMenuItem.Click
        With Proceso
            .StartInfo.FileName = RutaReportes + "\Ruta_rep_facsoftv.exe"
            .StartInfo.WindowStyle = ProcessWindowStyle.Normal
            .StartInfo.WorkingDirectory = System.Environment.CurrentDirectory
            .Start()
        End With
        bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Acceso a Ruta Reportes FacSoftv", "", "", LocClv_Ciudad)
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmSelColonias_Rep.Show()

    End Sub

    Private Sub EncargadosDelSistemaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EncargadosDelSistemaToolStripMenuItem.Click
        BrwEncargadosEmails.Show()
    End Sub



    Private Sub PrimerPeriodoToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If IdSistema = "SA" Then
            opcFrm = 16
        Else
            opcFrm = 10
        End If
        Acceso_TipoServicios.Show()

    End Sub

    'Public Sub desconexion()
    '    Dim CON As New SqlConnection(MiConexion)
    '    Try
    '        Dim Respuesta As MsgBoxResult
    '        Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Primer Periodo >  ?", MsgBoxStyle.OkCancel)
    '        If Respuesta = MsgBoxResult.Ok Then
    '            bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Primer Periodo", "", "Generó Proceso de Desconexión Primer Periodo", GloSucursal, LocClv_Ciudad)
    '            CON.Open()
    '            Dim comando As SqlClient.SqlCommand
    '            comando = New SqlClient.SqlCommand
    '            With comando
    '                .Connection = CON
    '                .CommandText = "EXEC Proceso_Cierre_Mes_PRIMER_Periodo 1"
    '                .CommandType = CommandType.Text
    '                .CommandTimeout = 0
    '                .ExecuteReader()
    '            End With
    '            CON.Close()
    '            MsgBox(" Proceso Finalizado con Éxito ")
    '        End If
    '    Catch ex As Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try
    'End Sub

    Public Sub desconexion()
        Me.BackgroundWorker1.RunWorkerAsync()
    End Sub

    'Public Sub desconexion2()
    '    Dim CON As New SqlConnection(MiConexion)
    '    Try
    '        Dim Respuesta As MsgBoxResult
    '        Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Segundo Periodo > ?", MsgBoxStyle.OkCancel)
    '        If Respuesta = MsgBoxResult.Ok Then
    '            bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Segundo Periodo", "", "Generó Proceso de Desconexión Segundo Periodo", GloSucursal, LocClv_Ciudad)
    '            CON.Open()

    '            Dim comando As SqlClient.SqlCommand
    '            comando = New SqlClient.SqlCommand
    '            With comando
    '                .Connection = CON
    '                .CommandText = "EXEC Proceso_Cierre_Mes_Primer_Periodo 2"
    '                .CommandType = CommandType.Text
    '                .CommandTimeout = 0
    '                .ExecuteReader()
    '            End With
    '            CON.Close()
    '            MsgBox(" Proceso Finalizado con Éxito ")
    '        End If
    '    Catch ex As Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try
    'End Sub


    Public Sub desconexion2()
        Me.BackgroundWorker2.RunWorkerAsync()
    End Sub

    Private Sub SegundoPeriodoToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        opcFrm = 11
        Acceso_TipoServicios.Show()

    End Sub

    Private Sub AvisosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AvisosToolStripMenuItem1.Click
        BwrAvisos.Show()
    End Sub




    Private Sub Button2_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        FrmSelPeriodo2.Show()
    End Sub

    Private Sub ClavesTécnicasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClavesTécnicasToolStripMenuItem.Click

    End Sub

    Private Sub AnálisisDePenetraciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AnálisisDePenetraciónToolStripMenuItem.Click
        FrmRepPenetracion.Show()
    End Sub

    Private Sub ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcesoDeDesconexiónTemporalPorContratoToolStripMenuItem.Click
        opcFrm = 13
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub ContratoMaestroToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoMaestroToolStripMenuItem.Click
        opcFrm = 15
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub ActivaciónPaqueteDePruebaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActivaciónPaqueteDePruebaToolStripMenuItem.Click
        FrmLoginPremium.Show()
    End Sub

    Private Sub PaquetesPremiumToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        eOpPPE = 1
        FrmSelFechasPPE.Show()
    End Sub


    Private Sub PreciosDeArticulosDeInstalaciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreciosDeArticulosDeInstalaciónToolStripMenuItem.Click
        FrmCapArtiAcom.Show()
    End Sub

    Private Sub ClientesConAdeudoDeMaterialToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientesConAdeudoDeMaterialToolStripMenuItem.Click
        Locbndrep = 1
        FrmImprimirContrato.Show()
    End Sub

    Private Sub BitácoraDePruebasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BitácoraDePruebasToolStripMenuItem.Click
        eOpPPE = 2
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ProcesoDeReactivaciónDeContratoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcesoDeReactivaciónDeContratoToolStripMenuItem.Click
        FrmReactivarContrato.Show()
    End Sub

    Private Sub DepuraciónDeÓrdenesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DepuraciónDeÓrdenesToolStripMenuItem.Click
        FrmDepOrd.Show()
    End Sub

    Private Sub CambioDeServicioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambioDeServicioToolStripMenuItem.Click
        'FrmCamServCte.Show()
        BrwCamServCte.Show()
    End Sub


    Private Sub ReporteDePaquetesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteDePaquetesToolStripMenuItem.Click
        If (GloTipoUsuario = 40 And IdSistema = "AG") Or (IdSistema <> "AG") Then
            reporte_Paquetes()
        ElseIf GloTipoUsuario <> 40 Then
            LReporte2 = True
            FrmAccesoReportes.Show()
        End If
    End Sub
    Public Sub reporte_Paquetes()
        Dim CON As New SqlConnection(MiConexion)
        Dim CON1 As New SqlConnection(MiConexion)
        LocOp = 30
        CON.Open()
        Me.Valida_periodo_reportesTableAdapter.Connection = CON
        Me.Valida_periodo_reportesTableAdapter.Fill(Me.DataSetarnoldo.Valida_periodo_reportes, LocValidaHab)
        CON.Close()
        If LocClv_session > 0 Then
            CON.Open()
            Me.Borra_Separacion_ClientesTableAdapter.Connection = CON
            Me.Borra_Separacion_ClientesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_Separacion_Clientes, LocClv_session)
            Me.Borrar_Tablas_Reporte_nuevoTableAdapter.Connection = CON
            Me.Borrar_Tablas_Reporte_nuevoTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borrar_Tablas_Reporte_nuevo, LocClv_session)
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON.Close()
        ElseIf LocClv_session = 0 Then
            CON1.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON1
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON1.Close()
        End If
        FrmTipoClientes.Show()
    End Sub
    Public Sub reporte_Importe_mensualidades_adelantados()
        Dim CON As New SqlConnection(MiConexion)
        Dim CON1 As New SqlConnection(MiConexion)
        LocOp = 80
        CON.Open()
        Me.Valida_periodo_reportesTableAdapter.Connection = CON
        Me.Valida_periodo_reportesTableAdapter.Fill(Me.DataSetarnoldo.Valida_periodo_reportes, LocValidaHab)
        CON.Close()
        If LocClv_session > 0 Then
            Borra_Tablas_Reporte_mensualidades(LocClv_session)
            CON.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON.Close()
        ElseIf LocClv_session = 0 Then
            CON1.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON1
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON1.Close()
        End If
        FrmSelTipServRep.Show()
    End Sub
    Private Sub PrimerPeriodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrimerPeriodoToolStripMenuItem.Click
        opcFrm = 17
        Acceso_TipoServicios.Show()
    End Sub

    Public Sub desconexionSA()

        Try
            Using CON2 As New SqlConnection(MiConexion)
                'Dim CON2 As New SqlConnection(MiConexion)
                Dim Respuesta As MsgBoxResult
                Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Primer Periodo >  ?", MsgBoxStyle.OkCancel)
                If Respuesta = MsgBoxResult.Ok Then
                    bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Primer Periodo", "", "Generó Proceso de Desconexión Primer Periodo", GloSucursal, LocClv_Ciudad)
                    CON2.Open()
                    Dim comando As SqlClient.SqlCommand
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON2
                        .CommandText = "EXEC PROCESODESCONEXIONTV 1," & Me.TextClv_Session.Text
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                        .ExecuteReader()
                    End With
                    CON2.Close()
                    MsgBox(" Proceso Finalizado con Éxito ")
                End If
            End Using
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message & " " & ex.ToString)
        End Try
    End Sub

    Public Sub desconexionSA2()

        Try
            Using CON2 As New SqlConnection(MiConexion)
                'Dim CON2 As New SqlConnection(MiConexion)
                Dim Respuesta As MsgBoxResult
                Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Segundo Periodo >  ?", MsgBoxStyle.OkCancel)
                If Respuesta = MsgBoxResult.Ok Then
                    bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Segundo Periodo", "", "Generó Proceso de Desconexión Segundo Periodo", GloSucursal, LocClv_Ciudad)
                    CON2.Open()
                    Dim comando As SqlClient.SqlCommand
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON2
                        .CommandText = "EXEC PROCESODESCONEXIONTV 2," & Me.TextClv_Session.Text
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                        .ExecuteReader()
                    End With
                    CON2.Close()
                    MsgBox(" Proceso Finalizado con Éxito ")
                End If
            End Using
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message & " " & ex.ToString)
        End Try
    End Sub

    Public Sub desconexionSAInternet()

        Try
            Using CON2 As New SqlConnection(MiConexion)
                'Dim CON2 As New SqlConnection(MiConexion)
                Dim Respuesta As MsgBoxResult
                Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Primer Periodo >  ?", MsgBoxStyle.OkCancel)
                If Respuesta = MsgBoxResult.Ok Then
                    bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Primer Periodo", "", "Generó Proceso de Desconexión Primer Periodo", GloSucursal, LocClv_Ciudad)
                    CON2.Open()
                    Dim comando As SqlClient.SqlCommand
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON2
                        .CommandText = "EXEC SAHUAYO_PROCESO_DESCONEXION_TODOS_SERVICIOSINTERNET 1,1"
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                        .ExecuteReader()
                    End With
                    CON2.Close()
                    MsgBox(" Proceso Finalizado con Éxito ")
                End If
            End Using
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message & " " & ex.ToString)
        End Try
    End Sub



    Public Sub CierredeMesSA()
        Dim CON2 As New SqlConnection(MiConexion)
        Try
            Dim Respuesta As MsgBoxResult
            Respuesta = MsgBox("¿ Deseas Correr el Proceso de Cierre de Mes del < Primer Periodo >  ?", MsgBoxStyle.OkCancel)
            If Respuesta = MsgBoxResult.Ok Then
                bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Cierre de Mes Primer Periodo", "", "Generó Proceso de Cierre de Mes Primer Periodo", GloSucursal, LocClv_Ciudad)
                CON2.Open()
                Dim comando As SqlClient.SqlCommand
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON2
                    .CommandText = "EXEC SAHUAYO_PROCESO_CIERREDEMES_TODOS_SERVICIOS 1,1"
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    .ExecuteReader()
                End With
                CON2.Close()
                MsgBox(" Proceso Finalizado con Éxito ")
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ListadoDeActividadesDelTécnicoToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeActividadesDelTécnicoToolStripMenuItem1.Click
        Dim CON As New SqlConnection(MiConexion)
        If LocClv_session > 0 Then
            CON.Close()
            Me.Borra_temporales_trabajosTableAdapter.Connection = CON
            Me.Borra_temporales_trabajosTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_temporales_trabajos, LocClv_session)
            LocClv_session = 0
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON.Close()
        ElseIf LocClv_session = 0 Then
            CON.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON.Close()
        End If
        FrmSelTecnico_Rep.Show()
    End Sub

    Private Sub ResumenDeVentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenDeVentasToolStripMenuItem.Click
        eOpPPE = 3
        FrmSelFechasPPE.Show()
    End Sub


    Private Sub CorreoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CorreoToolStripMenuItem.Click
        '''''Se Comenta cuando se va a hacer pruebas
        'Using CON As New SqlConnection(MiConexion)
        '    CON.Open()
        '    LocOp = 23
        '    Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
        '    Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
        '    GloClv_tipser2 = 0
        '    CON.Close()
        'End Using
        'FrmTipoClientes.Show()

        '''''Se comenta cuando se va a Publicar
        FrmCorreo.Show()
    End Sub

    Private Sub MenuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles MenuStrip1.ItemClicked

    End Sub

    Private Sub ResetearAparatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResetearAparatosToolStripMenuItem.Click
        FrmReset.Show()
    End Sub

    Private Sub PelículasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PelículasToolStripMenuItem.Click
        eOpPPE = 1
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ResumenVendedoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenVendedoresToolStripMenuItem.Click
        eOpPPE = 4
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub NotasDeCréditoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwNotasdeCredito.Show()
    End Sub

    'Private Sub ConsolidadoVentasYOficinasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsolidadoVentasYOficinasToolStripMenuItem.Click
    '    eOpVentas = 11
    '    eBndGraf = True
    '    FrmSelFechasPPE.Show()
    'End Sub

    'Private Sub ConsilidadoOficinasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsilidadoOficinasToolStripMenuItem.Click
    '    eOpVentas = 12
    '    eBndGraf = True
    '    FrmSelFechasPPE.Show()
    'End Sub

    'Private Sub ConsolidadoVentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsolidadoVentasToolStripMenuItem.Click
    '    eOpVentas = 13
    '    eBndGraf = True
    '    FrmSelFechasPPE.Show()
    'End Sub

    'Private Sub OficinaEnParticularToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OficinaEnParticularToolStripMenuItem.Click
    '    eOpVentas = 14
    '    eBndGraf = True
    '    FrmSelFechasPPE.Show()
    'End Sub

    'Private Sub VendedorEnParticularToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VendedorEnParticularToolStripMenuItem.Click
    '    eOpVentas = 15
    '    eBndGraf = True
    '    FrmSelFechasPPE.Show()
    'End Sub

    'Private Sub NúmeroDeVentasVendedotesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NúmeroDeVentasVendedotesToolStripMenuItem.Click
    '    eOpVentas = 16
    '    eBndGraf = True
    '    eBndVen = True
    '    FrmSelFechasPPE.Show()
    'End Sub
    'Private Sub PorPaquetesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PorPaquetesToolStripMenuItem.Click
    '    eOpVentas = 17
    '    eBndGraf = True
    '    FrmSelFechasPPE.Show()
    '    'FrmSelTipServE.Show()
    'End Sub

    Private Sub ClientesVariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientesVariosToolStripMenuItem.Click
        Using CON As New SqlConnection(MiConexion)
            CON.Open()
            LocOp = 22
            eBndMenIns = True
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            GloClv_tipser2 = 3
            CON.Close()
        End Using
        FrmTipoClientes.Show()
    End Sub

    Private Sub ClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClienteToolStripMenuItem.Click
        FrmMsjPorCliente.Show()
    End Sub

    Private Sub VisitasDelClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VisitasDelClienteToolStripMenuItem.Click
        BrwVisitas.Show()
    End Sub

    Private Sub MotivosDeLlamadaAtenciónAClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MotivosDeLlamadaAtenciónAClientesToolStripMenuItem.Click
        BrwMotAtenTel.Show()
    End Sub

    Private Sub PruebaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PruebaToolStripMenuItem.Click
        BrwProgramaciones.Show()
        'Using CON As New SqlConnection(MiConexion)
        '    CON.Open()
        '    LocOp = 22
        '    eBndMenIns = True
        '    Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
        '    Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
        '    GloClv_tipser2 = 3
        'End Using
        'Programacion = 27
        'FrmTipoClientes.Show()
        'FrmProgramacion_msjs.Show()
    End Sub

    Private Sub AtenciónTelefónicaToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AtenciónTelefónicaToolStripMenuItem1.Click
        BrwAtenTel.Show()
    End Sub

    Private Sub EricToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmGraficas.Show()
    End Sub
    Private Sub GráficasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GráficasToolStripMenuItem.Click
        FrmGraficas.Show()
    End Sub

    Private Sub CambioDeClienteASoloInternetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambioDeClienteASoloInternetToolStripMenuItem.Click
        LocProceso = 1
        FrmCambioClienteSoloInternet.Show()
    End Sub

    Private Sub CambioDeClienteAClienteNormalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambioDeClienteAClienteNormalToolStripMenuItem.Click
        LocProceso = 0
        FrmCambioClienteSoloInternet.Show()
    End Sub

    Private Sub ProcesosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcesosToolStripMenuItem.Click

    End Sub
    Private Sub AgendaDeActividadesDelTécnicoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgendaDeActividadesDelTécnicoToolStripMenuItem.Click
        FrmSelRepAgendaTecnico.Show()
    End Sub

    Private Sub PolizaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PolizaToolStripMenuItem.Click
        FrmPolizaDiario.Show()
    End Sub

    Private Sub ProgramacionesDeMesnajesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProgramacionesDeMesnajesToolStripMenuItem.Click
        BrwProgramaciones.Show()
    End Sub

    Private Sub GeneraPolizaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwPolizas.Show()
    End Sub

    Private Sub MetasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmMetas.Show()
    End Sub

    Private Sub GeneralesToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesToolStripMenuItem.Click

    End Sub

    Private Sub MetasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmMetasRep.Show()
    End Sub

    Private Sub MedidoresDeIngresosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmMetasIngresos.Show()
    End Sub

    Private Sub ReportesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReportesToolStripMenuItem.Click

    End Sub

    Private Sub DesgloceDeMensualidadesAdelantadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DesgloceDeMensualidadesAdelantadasToolStripMenuItem.Click
        BndDesPagAde = True
        FrmSelCd_Cartera.Show()
    End Sub

    Private Sub ContratoForzosoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoForzosoToolStripMenuItem.Click
        'eBndContratoF = True
        If (GloTipoUsuario = 40 And IdSistema = "AG") Or (IdSistema <> "AG") Then
            eOpVentas = 40
            'FrmSelTipServE.Show()
            FrmContratoF.Show()
        Else
            FrmAccesoReportes.Show()
        End If
    End Sub

    Private Sub RToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RToolStripMenuItem.Click
        If (GloTipoUsuario = 40 And IdSistema = "AG") Or (IdSistema <> "AG") Then
            FrmSelContratoRango.Show()
        ElseIf (GloTipoUsuario <> 40 And IdSistema = "AG") Then
            LReporte1 = True
            FrmAccesoReportes.Show()
        End If
    End Sub

    Private Sub MedidoresDeCarteraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmMetasCarteras.Show()
    End Sub

    Private Sub ServiciosDeVentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServiciosDeVentasToolStripMenuItem.Click
        FrmMetas.Show()
    End Sub

    Private Sub IngresosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresosToolStripMenuItem.Click
        FrmMetasIngresos.Show()
    End Sub

    Private Sub CarteraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CarteraToolStripMenuItem.Click
        FrmMetasCarteras.Show()
    End Sub

    Private Sub MedidoresToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MedidoresToolStripMenuItem1.Click
        FrmMetasRep.Show()
    End Sub


    Private Sub GrupoDeVentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GrupoDeVentasToolStripMenuItem.Click
        FrmGrupoVentas.Show()
    End Sub

    Private Sub BitacoraDelSistemaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BitacoraDelSistemaToolStripMenuItem.Click
        opcFrm = 19
        Acceso_TipoServicios.Show()
    End Sub



    Private Sub AgendaToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgendaToolStripMenuItem1.Click
        BrwAgenda.Show()
    End Sub

    Private Sub IndividualesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IndividualesToolStripMenuItem.Click
        FrmMetasInd.Show()
    End Sub

    Private Sub CargosEspecialesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'FrmCargosEspeciales.Show()
        My.Forms.BrwCargosEspeciales.Show()
    End Sub

    Private Sub CargosEspecialesToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargosEspecialesToolStripMenuItem.Click

        FrmCargoBonif.Show()

    End Sub

    Private Sub Medidores2ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Medidores2ToolStripMenuItem.Click
        FrmMetasRep2.Show()
    End Sub

    Private Sub DescuentosComboToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DescuentosComboToolStripMenuItem.Click
        'MsgBox("Temporalmente en fuera de servicio.")
        BrwDescuentoCombo.Show()
    End Sub


    Private Sub PaisesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PaisesToolStripMenuItem.Click
        BrwPaises.Show()
    End Sub

    Private Sub TipoPaquetesAdicionalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoPaquetesAdicionalesToolStripMenuItem.Click
        BrwTipoPaquetesAdicTel.Show()
    End Sub

    Private Sub NumérosDeTeléfonoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumérosDeTeléfonoToolStripMenuItem.Click
        BrwCatalogoTelefonos.Show()
    End Sub

    Private Sub PaquetesAdicionalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PaquetesAdicionalesToolStripMenuItem.Click
        BrwPaqueteAdicional.Show()
    End Sub

    Private Sub ServiciosDigitalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServiciosDigitalesToolStripMenuItem.Click
        BrwServiciosDig.Show()
    End Sub

    Private Sub ImporteDeMensualidadesAdelantadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImporteDeMensualidadesAdelantadasToolStripMenuItem.Click
        reporte_Importe_mensualidades_adelantados()
    End Sub


    Private Sub PPVToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PPVToolStripMenuItem.Click
        FrmRepPPV.Show()
    End Sub

    Private Sub EstadoDeCuentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EstadoDeCuentaToolStripMenuItem.Click
        LEdo_Cuenta = True
        LEdo_Cuenta2 = False
        FrmEstado_Cuenta.Comienzo_Reporte()
        FrmTipoClientes.Show()
    End Sub
    Public Sub reporte_combo()
        Dim CON As New SqlConnection(MiConexion)
        Dim CON1 As New SqlConnection(MiConexion)
        LocOp = 90
        CON.Open()
        Me.Valida_periodo_reportesTableAdapter.Connection = CON
        Me.Valida_periodo_reportesTableAdapter.Fill(Me.DataSetarnoldo.Valida_periodo_reportes, LocValidaHab)
        CON.Close()
        If LocClv_session > 0 Then
            CON.Open()
            FrmImprimirContrato.borra_tablas_Reporte_clientes_combo(LocClv_session)
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON.Close()
        ElseIf LocClv_session = 0 Then
            CON1.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = CON1
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            CON1.Close()
        End If
        FrmTipoClientes.Show()
    End Sub

    Private Sub ReporteClientesConComboToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteClientesConComboToolStripMenuItem.Click
        If (GloTipoUsuario = 40 And IdSistema = "AG") Or (IdSistema <> "AG") Then
            reporte_combo()
        ElseIf GloTipoUsuario <> 40 Then
            LReportecombo = True
            FrmAccesoReportes.Show()
        End If
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim CON As New SqlConnection(MiConexion)
        Try
            Dim Respuesta As MsgBoxResult
            Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Primer Periodo >  ?", MsgBoxStyle.OkCancel)
            If Respuesta = MsgBoxResult.Ok Then
                bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Primer Periodo", "", "Generó Proceso de Desconexión Primer Periodo", GloSucursal, LocClv_Ciudad)
                CON.Open()
                Me.BackgroundWorker1.ReportProgress(50)
                Dim comando As SqlClient.SqlCommand
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "EXEC Proceso_Cierre_Mes_PRIMER_Periodo 1"
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    .ExecuteReader()
                    e.Result = comando
                End With
                CON.Close()
                Me.BackgroundWorker1.ReportProgress(100)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        'Me.ProgressBar1.Value = e.ProgressPercentage
        'If v.Status <> StatusInfo.Playing Then
        '    v.Repeat = True
        '    v.Size = Me.Panel1.Size
        '    v.Location = Me.Panel1.Location
        '    v.Play()
        'End If
        If Formtrabajando.Visible = False Then
            Formtrabajando.Show()
        End If
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        'MsgBox(" Proceso Finalizado con Éxito ")
        'v.StopPlay()
        'v.Dispose()
        Formtrabajando.Close()
    End Sub

    Private Sub BackgroundWorker2_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork
        Dim CON As New SqlConnection(MiConexion)
        Try
            Dim Respuesta As MsgBoxResult
            Respuesta = MsgBox("¿ Deseas Correr el Proceso de Desconexión del < Segundo Periodo > ?", MsgBoxStyle.OkCancel)
            If Respuesta = MsgBoxResult.Ok Then
                bitsist(GloUsuario, 0, LocGloSistema, "Proceso de Desconexión Segundo Periodo", "", "Generó Proceso de Desconexión Segundo Periodo", GloSucursal, LocClv_Ciudad)
                CON.Open()
                Me.BackgroundWorker2.ReportProgress(50)
                Dim comando As SqlClient.SqlCommand
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "EXEC Proceso_Cierre_Mes_Primer_Periodo 2"
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    .ExecuteReader()
                    e.Result = comando
                End With
                CON.Close()
                Me.BackgroundWorker2.ReportProgress(100)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BackgroundWorker2_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker2.ProgressChanged
        If Formtrabajando.Visible = False Then
            Formtrabajando.Show()
        End If
    End Sub

    Private Sub BackgroundWorker2_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker2.RunWorkerCompleted
        Formtrabajando.Close()
    End Sub

    Private Sub CarteraEjecutivaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CarteraEjecutivaToolStripMenuItem.Click
        FrmSelFechaCarteraEjec.Show()
    End Sub

    Private Sub ServiciosContratadosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServiciosContratadosToolStripMenuItem.Click
        eOpPPE = 0
        eOpVentas = 55
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ListadoDeClientesPorStausConAdeudoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDeClientesPorStausConAdeudoToolStripMenuItem.Click
        LEdo_Cuenta2 = True
        LEdo_Cuenta = False
        FrmEstado_Cuenta.Comienzo_Reporte()
        FrmTipoClientes.Show()
    End Sub

    Private Sub CancelacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelacionesToolStripMenuItem.Click
        eOpPPE = 0
        eOpVentas = 56
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub MarcacionesEspecialesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MarcacionesEspecialesToolStripMenuItem.Click
        BwrMarcacion.Show()
    End Sub

    Private Sub EquiposALaVentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EquiposALaVentaToolStripMenuItem.Click
        BwrEquipoVenta.Show()
    End Sub

    Private Sub BitácoraDeCorreosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BitácoraDeCorreosToolStripMenuItem.Click
        eOpPPE = 0
        eOpVentas = 57
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub CódigosMéxicoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CódigosMéxicoToolStripMenuItem.Click
        BwrCodigosMexico.Show()
    End Sub

    Private Sub VentasTotalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VentasTotalesToolStripMenuItem.Click
        eOpPPE = 0
        eOpVentas = 61
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ServicioBasicoYCanalesPremiumToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServicioBasicoYCanalesPremiumToolStripMenuItem.Click
        'If IdSistema = "SA" Then
        '    opcFrm = 16
        'End If
        'Acceso_TipoServicios.Show()
    End Sub

    Private Sub InternetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If IdSistema = "SA" Then
            opcFrm = 26
        End If
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub PuntosDeAntigüedadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PuntosDeAntigüedadToolStripMenuItem.Click
        eOpPPE = 0
        eOpVentas = 100
        FrmSelFechasPPE.Show()

    End Sub

    Private Sub InterfazCablemodemsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InterfazCablemodemsToolStripMenuItem.Click
        eOpPPE = 0
        eOpVentas = 62
        eOpCNRCNRDIG = 0
        eServicio = "de Cablemodems"
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub InterfazDigitalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InterfazDigitalesToolStripMenuItem.Click
        eOpPPE = 0
        eOpVentas = 62
        eOpCNRCNRDIG = 1
        eServicio = "Digital"
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ResumenDeClientesPorServicioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenDeClientesPorServicioToolStripMenuItem.Click
        eOpVentas = 63
        FrmImprimirComision.Show()
    End Sub

    Private Sub TarifasEspecialesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TarifasEspecialesToolStripMenuItem.Click
        FrmTarifasEspeciales.Show()
    End Sub

    Private Sub ReferenciasBancariasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwRefBancarias.Show()
    End Sub

    Private Sub RecuperaciónDeCarteraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecuperaciónDeCarteraToolStripMenuItem.Click
        BrwClientesPorRecuperar.Show()
    End Sub

    Private Sub CortesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CortesToolStripMenuItem.Click
        FrmCortes.Show()
    End Sub



    Private Sub AuxiliarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        eOpVentas = 64
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub GeneralesProsaBancomerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralesProsaBancomerToolStripMenuItem.Click
        opcFrm = 20
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub EstadoDeCuentaTelefoníaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ImpresionEstadosDeCuenta.Show()
        'Dim ProcessId As Integer = Nothing
        'ProcessId = Shell("Http://" & GloServerName & "/llamadasweb/consultas.aspx")
        'ProcessId = Shell("http://localhost:3448/LlamadasWeb/consultas.aspx")
        'http://localhost:3448/LlamadasWeb/imprime.aspx
    End Sub

    Private Sub ReprocesamientoPorClienteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReprocesamientoPorClienteToolStripMenuItem.Click
        FrmReprocesamiento.Show()
    End Sub

    Private Sub ProcesamientoDeLlamadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcesamientoDeLlamadasToolStripMenuItem.Click
        FrmProcesamiento_Llamadas.Show()
    End Sub

    Private Sub ProcesamientoDeCDRToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcesamientoDeCDRToolStripMenuItem.Click
        FrmProcesamiento_CDR.Show()
    End Sub

    Private Sub ImpresiónDeEstadosDeCuentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImpresiónDeEstadosDeCuentaToolStripMenuItem.Click
        Dim ImpresionEstadosDeCuenta_DEV As New ImpresionEdoCuenta_DEV
        ImpresionEstadosDeCuenta_DEV.Show()
        'ImpresionEstadosDeCuenta.Show()
    End Sub

    Private Sub AuxToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AuxToolStripMenuItem.Click
        eOpVentas = 64
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ReferenciasBancariasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BrwRefBancarias.Show()
    End Sub

    Private Sub ImpresiónDeEstadosDeCuentaPorPáginaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ImpresionEstadosDeCuenta2.Show()
    End Sub

    Private Sub ImpresiónDeEstadosDeCuentaPágina2NToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImpresiónDeEstadosDeCuentaPágina2NToolStripMenuItem.Click
        ImpresionEstadosDeCuenta2.Show()
    End Sub

    Private Sub HotelesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HotelesToolStripMenuItem.Click
        eOpVentas = 65
        FrmImprimirComision.Show()
    End Sub

    Private Sub OrdenesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OrdenesToolStripMenuItem.Click
        FrmSelFechasRep.Show()
    End Sub

    Private Sub GerencialToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GerencialToolStripMenuItem.Click
        eOpVentas = 67
        FrmImprimirComision.Show()
    End Sub

    Private Sub CancelacionDeVentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelacionDeVentasToolStripMenuItem.Click
        eOpPPE = 0
        eOpVentas = 68
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub ReporteDePermanenciaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteDePermanenciaToolStripMenuItem.Click
        eOpPPE = 0
        eOpVentas = 69
        FrmRepPermanencia.Show()
    End Sub

    Private Sub ResumenVentasPorStatusToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResumenVentasPorStatusToolStripMenuItem.Click
        eOpPPE = 0
        eOpVentas = 70
        FrmRepPermanencia.Show()
    End Sub

    Private Sub ClasificaciónDeTarifasDeLlamadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClasificaciónDeTarifasDeLlamadasToolStripMenuItem.Click
        BrwClasificacionLlamadas.Show()
    End Sub

    Private Sub ServicioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ConfiguracionDeComandosDeTelefoniaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfiguracionDeComandosDeTelefoniaToolStripMenuItem.Click

    End Sub

    Private Sub ServiciosConfiguradosEnGalleryToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ServiciosConfiguradosEnGalleryToolStripMenuItem.Click
        BrwPaquetesGallery_Tel.Show()
    End Sub

    Private Sub ConfiguracionDeComandosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfiguracionDeComandosToolStripMenuItem.Click
        FrmComandos_Servicios.Show()
    End Sub

    Private Sub CToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CToolStripMenuItem.Click
        BwrCliPortabilidad.Show()
    End Sub

    Private Sub ProveedoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProveedoresToolStripMenuItem.Click
        BrwProveedorPortabilidad.Show()
    End Sub

    Private Sub HomologaciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HomologaciónToolStripMenuItem.Click
        FrmCatalogoHomologacion.Show()
    End Sub

    Private Sub ImpresiónDeEstadosDeCuentaPorPeriodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImpresiónDeEstadosDeCuentaPorPeriodoToolStripMenuItem.Click
        ImpresionEdoCuenta_DEV.Show()
    End Sub

    Private Sub ClientesPortabilidadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientesPortabilidadToolStripMenuItem.Click
        FrmSelecFechaPorta.Show()
    End Sub

    Private Sub AparatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'FrmAparatosDisponibles.Show()
    End Sub

    'Private Sub ReprocesamientoPorClienteToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReprocesamientoPorClienteToolStripMenuItem1.Click
    '    FrmReprocesamiento.Show()
    'End Sub



    Private Sub RecontrataciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecontrataciónToolStripMenuItem.Click
        'eOpPPE = 0
        'eOpVentas = 77
        'FrmSelFechasPPE.Show()
        FrmRecontratacion.Show()
    End Sub

    Private Sub RecepcionDeAparatosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecepcionDeAparatosToolStripMenuItem.Click
        FrmAparatosDisponibles.Show()
    End Sub

    Private Sub RecepcionDeAparatosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecepcionDeAparatosToolStripMenuItem1.Click
        FrmImprimeRecepcion.Show()
    End Sub

    Private Sub CambioDeNumeroTelefonicoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambioDeNumeroTelefonicoToolStripMenuItem.Click
        FrmCambioDeNumero.Show()
    End Sub

    Private Sub RecontrataciónToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecontrataciónToolStripMenuItem1.Click
        eOpPPE = 0
        eOpVentas = 77
        FrmSelFechasPPE.Show()
    End Sub

    Private Sub RegistroClientesEnLímiteDeCrécitoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegistroClientesEnLímiteDeCrécitoToolStripMenuItem.Click
        FrmLlamadasLimiteDeCredito.Show()
    End Sub

    Private Sub PrimToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrimToolStripMenuItem.Click
        'Proceso de Desconexion Primer Periodo
        BndGloDesPeriodo = 1
        If IdSistema = "SA" Then
            opcFrm = 16
        End If
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub SegundoPeriodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SegundoPeriodoToolStripMenuItem.Click
        'Proceso de Desconexion Segundo Periodo
        BndGloDesPeriodo = 2
        If IdSistema = "SA" Then
            opcFrm = 16
        End If
        Acceso_TipoServicios.Show()
    End Sub

    Private Sub ReporteDeRoboDeSeñalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteDeRoboDeSeñalToolStripMenuItem.Click
        'SAUL reporte Robo de Señal
        Dim rDocument As New ReportDocument()
        Dim Fecha As FrmSelFechas = New FrmSelFechas
        Dim Ciudad As FrmSelCiudad = New FrmSelCiudad
        Dim Colonia As FrmSelColonia_Rep21 = New FrmSelColonia_Rep21
        Dim Cobrado As FrmCobrado = New FrmCobrado
        Dim Reporte As FrmImprimirComision = New FrmImprimirComision
        LocOp = 66666
        Fecha.ShowDialog()
        If Fecha.DialogResult = DialogResult.OK Then
            Try
                ObtieneClave()
                Ciudad.ShowDialog()
                If Ciudad.DialogResult = Windows.Forms.DialogResult.OK Then
                    Colonia.ShowDialog()
                    If Colonia.DialogResult = Windows.Forms.DialogResult.OK Then
                        Cobrado.ShowDialog()
                        If Cobrado.DialogResult = Windows.Forms.DialogResult.OK Then
                            rDocument = New ReportDocument()

                            Dim fechaI As Date = Nothing
                            Dim fechaF As Date = Nothing
                            GloBndSelFecha = False
                            fechaI = GloFecha_Ini
                            fechaF = GloFecha_Fin

                            rDocument.Load(RutaReportes.ToString + "\ReporteRoboSeñal222.rpt")

                            Dim DS As New DataSet
                            DS.Clear()
                            BaseII.limpiaParametros()

                            BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, fechaI)
                            BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, fechaF)
                            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, LocClv_session)
                            BaseII.CreateMyParameter("@Tipo", SqlDbType.Int, CInt(Cobrado.Tipo))

                            Dim listatablas As New List(Of String)
                            listatablas.Add("uspConsultaRoboSeñal")

                            DS = BaseII.ConsultaDS("uspConsultaRoboSeñal", listatablas)

                            rDocument.SetDataSource(DS)
                            rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                            eTituloComision = "Del " & fechaI & " al " & fechaF
                            rDocument.DataDefinition.FormulaFields("Fecha").Text = "'" & eTituloComision & "'"

                            Reporte.CrystalReportViewer1.ReportSource = rDocument
                            Reporte.CrystalReportViewer1.Zoom(75)
                            eOpVentas = 0
                            Reporte.Show()
                        End If
                    End If
                End If
                LocOp = 0
            Catch ex As CrystalDecisions.CrystalReports.Engine.ParameterFieldCurrentValueException
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub ObtieneClave()
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("DameClv_Session_Servicios", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
            LocClv_session = parametro.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    'SAUL reporte Robo de Señal(FIN)

    Private Sub ProductosSatToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ProductosSatToolStripMenuItem.Click
        Catalogos33.Module1.MiConexion = MiConexion
        Dim frmBrwClaveProdServ As New Catalogos33.BrwClaveProdServ
        frmBrwClaveProdServ.ShowDialog()
    End Sub

End Class
