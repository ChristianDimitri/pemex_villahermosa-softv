<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCatalogoNoTel
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim Numero_telLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim Ultima_FechaAsigLabel As System.Windows.Forms.Label
        Dim Ultima_Fecha_LiberacionLabel As System.Windows.Forms.Label
        Dim Ult_Cliente_AsignadoLabel As System.Windows.Forms.Label
        Dim Fecha_CuarentenaLabel As System.Windows.Forms.Label
        Dim StatusLabel1 As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCatalogoNoTel))
        Me.CMBPanel1 = New System.Windows.Forms.Panel()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Consulta_Cat_Num_TelefonoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientosarnoldo4 = New sofTV.Procedimientosarnoldo4()
        Me.Muestra_Estado_No_TelefonicoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2()
        Me.CMBTextBox1 = New System.Windows.Forms.TextBox()
        Me.StatusComboBox = New System.Windows.Forms.ComboBox()
        Me.Muestra_Status_No_TelefonicoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.Fecha_CuarentenaTextBox = New System.Windows.Forms.TextBox()
        Me.Numero_telTextBox = New System.Windows.Forms.TextBox()
        Me.Ult_Cliente_AsignadoTextBox = New System.Windows.Forms.TextBox()
        Me.Ultima_Fecha_LiberacionTextBox = New System.Windows.Forms.TextBox()
        Me.Ultima_FechaAsigTextBox = New System.Windows.Forms.TextBox()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Consulta_Cat_Num_TelefonoTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Consulta_Cat_Num_TelefonoTableAdapter()
        Me.Muestra_Status_No_TelefonicoTableAdapter = New sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Status_No_TelefonicoTableAdapter()
        Me.Muestra_Estado_No_TelefonicoTableAdapter = New sofTV.DataSetLidia2TableAdapters.Muestra_Estado_No_TelefonicoTableAdapter()
        ClaveLabel = New System.Windows.Forms.Label()
        Numero_telLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        Ultima_FechaAsigLabel = New System.Windows.Forms.Label()
        Ultima_Fecha_LiberacionLabel = New System.Windows.Forms.Label()
        Ult_Cliente_AsignadoLabel = New System.Windows.Forms.Label()
        Fecha_CuarentenaLabel = New System.Windows.Forms.Label()
        StatusLabel1 = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Me.CMBPanel1.SuspendLayout()
        CType(Me.Consulta_Cat_Num_TelefonoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_Estado_No_TelefonicoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_Status_No_TelefonicoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Cat_Num_TelefonoBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveLabel.Location = New System.Drawing.Point(81, 41)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(56, 16)
        ClaveLabel.TabIndex = 3
        ClaveLabel.Text = "Clave :"
        '
        'Numero_telLabel
        '
        Numero_telLabel.AutoSize = True
        Numero_telLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_telLabel.Location = New System.Drawing.Point(7, 77)
        Numero_telLabel.Name = "Numero_telLabel"
        Numero_telLabel.Size = New System.Drawing.Size(130, 16)
        Numero_telLabel.TabIndex = 5
        Numero_telLabel.Text = "No. De Teléfono :"
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel.Location = New System.Drawing.Point(35, 381)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(53, 16)
        StatusLabel.TabIndex = 7
        StatusLabel.Text = "status:"
        '
        'Ultima_FechaAsigLabel
        '
        Ultima_FechaAsigLabel.AutoSize = True
        Ultima_FechaAsigLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Ultima_FechaAsigLabel.Location = New System.Drawing.Point(367, 53)
        Ultima_FechaAsigLabel.Name = "Ultima_FechaAsigLabel"
        Ultima_FechaAsigLabel.Size = New System.Drawing.Size(212, 16)
        Ultima_FechaAsigLabel.TabIndex = 9
        Ultima_FechaAsigLabel.Text = "Ultima Fecha De Asignación :"
        '
        'Ultima_Fecha_LiberacionLabel
        '
        Ultima_Fecha_LiberacionLabel.AutoSize = True
        Ultima_Fecha_LiberacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Ultima_Fecha_LiberacionLabel.Location = New System.Drawing.Point(371, 86)
        Ultima_Fecha_LiberacionLabel.Name = "Ultima_Fecha_LiberacionLabel"
        Ultima_Fecha_LiberacionLabel.Size = New System.Drawing.Size(208, 16)
        Ultima_Fecha_LiberacionLabel.TabIndex = 11
        Ultima_Fecha_LiberacionLabel.Text = "Ultima Fecha De Liberación :"
        '
        'Ult_Cliente_AsignadoLabel
        '
        Ult_Cliente_AsignadoLabel.AutoSize = True
        Ult_Cliente_AsignadoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Ult_Cliente_AsignadoLabel.Location = New System.Drawing.Point(397, 148)
        Ult_Cliente_AsignadoLabel.Name = "Ult_Cliente_AsignadoLabel"
        Ult_Cliente_AsignadoLabel.Size = New System.Drawing.Size(182, 16)
        Ult_Cliente_AsignadoLabel.TabIndex = 13
        Ult_Cliente_AsignadoLabel.Text = "Ultimo Cliente Asignado :"
        '
        'Fecha_CuarentenaLabel
        '
        Fecha_CuarentenaLabel.AutoSize = True
        Fecha_CuarentenaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_CuarentenaLabel.Location = New System.Drawing.Point(413, 116)
        Fecha_CuarentenaLabel.Name = "Fecha_CuarentenaLabel"
        Fecha_CuarentenaLabel.Size = New System.Drawing.Size(166, 16)
        Fecha_CuarentenaLabel.TabIndex = 15
        Fecha_CuarentenaLabel.Text = "Fecha De Cuarentena :"
        '
        'StatusLabel1
        '
        StatusLabel1.AutoSize = True
        StatusLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        StatusLabel1.Location = New System.Drawing.Point(78, 109)
        StatusLabel1.Name = "StatusLabel1"
        StatusLabel1.Size = New System.Drawing.Size(59, 16)
        StatusLabel1.TabIndex = 16
        StatusLabel1.Text = "Status :"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.Location = New System.Drawing.Point(72, 147)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(65, 16)
        Label1.TabIndex = 19
        Label1.Text = "Estado :"
        '
        'CMBPanel1
        '
        Me.CMBPanel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CMBPanel1.Controls.Add(Me.ComboBox1)
        Me.CMBPanel1.Controls.Add(Label1)
        Me.CMBPanel1.Controls.Add(Me.CMBTextBox1)
        Me.CMBPanel1.Controls.Add(StatusLabel1)
        Me.CMBPanel1.Controls.Add(Me.StatusComboBox)
        Me.CMBPanel1.Controls.Add(ClaveLabel)
        Me.CMBPanel1.Controls.Add(Me.ClaveTextBox)
        Me.CMBPanel1.Controls.Add(Me.Fecha_CuarentenaTextBox)
        Me.CMBPanel1.Controls.Add(Numero_telLabel)
        Me.CMBPanel1.Controls.Add(Fecha_CuarentenaLabel)
        Me.CMBPanel1.Controls.Add(Me.Numero_telTextBox)
        Me.CMBPanel1.Controls.Add(Me.Ult_Cliente_AsignadoTextBox)
        Me.CMBPanel1.Controls.Add(Ult_Cliente_AsignadoLabel)
        Me.CMBPanel1.Controls.Add(Me.Ultima_Fecha_LiberacionTextBox)
        Me.CMBPanel1.Controls.Add(Ultima_FechaAsigLabel)
        Me.CMBPanel1.Controls.Add(Ultima_Fecha_LiberacionLabel)
        Me.CMBPanel1.Controls.Add(Me.Ultima_FechaAsigTextBox)
        Me.CMBPanel1.Location = New System.Drawing.Point(21, 42)
        Me.CMBPanel1.Name = "CMBPanel1"
        Me.CMBPanel1.Size = New System.Drawing.Size(722, 206)
        Me.CMBPanel1.TabIndex = 0
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_Cat_Num_TelefonoBindingSource, "Estado", True))
        Me.ComboBox1.DataSource = Me.Muestra_Estado_No_TelefonicoBindingSource
        Me.ComboBox1.DisplayMember = "Status"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(139, 144)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(173, 24)
        Me.ComboBox1.TabIndex = 20
        Me.ComboBox1.ValueMember = "clv_status"
        '
        'Consulta_Cat_Num_TelefonoBindingSource
        '
        Me.Consulta_Cat_Num_TelefonoBindingSource.DataMember = "Consulta_Cat_Num_Telefono"
        Me.Consulta_Cat_Num_TelefonoBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'Procedimientosarnoldo4
        '
        Me.Procedimientosarnoldo4.DataSetName = "Procedimientosarnoldo4"
        Me.Procedimientosarnoldo4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Muestra_Estado_No_TelefonicoBindingSource
        '
        Me.Muestra_Estado_No_TelefonicoBindingSource.DataMember = "Muestra_Estado_No_Telefonico"
        Me.Muestra_Estado_No_TelefonicoBindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBTextBox1
        '
        Me.CMBTextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox1.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox1.Location = New System.Drawing.Point(370, 25)
        Me.CMBTextBox1.Name = "CMBTextBox1"
        Me.CMBTextBox1.Size = New System.Drawing.Size(344, 15)
        Me.CMBTextBox1.TabIndex = 18
        Me.CMBTextBox1.Text = "Fechas De"
        Me.CMBTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'StatusComboBox
        '
        Me.StatusComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_Cat_Num_TelefonoBindingSource, "status", True))
        Me.StatusComboBox.DataSource = Me.Muestra_Status_No_TelefonicoBindingSource
        Me.StatusComboBox.DisplayMember = "Status"
        Me.StatusComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusComboBox.FormattingEnabled = True
        Me.StatusComboBox.Location = New System.Drawing.Point(139, 108)
        Me.StatusComboBox.Name = "StatusComboBox"
        Me.StatusComboBox.Size = New System.Drawing.Size(173, 24)
        Me.StatusComboBox.TabIndex = 17
        Me.StatusComboBox.ValueMember = "clv_status"
        '
        'Muestra_Status_No_TelefonicoBindingSource
        '
        Me.Muestra_Status_No_TelefonicoBindingSource.DataMember = "Muestra_Status_No_Telefonico"
        Me.Muestra_Status_No_TelefonicoBindingSource.DataSource = Me.Procedimientosarnoldo4
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Cat_Num_TelefonoBindingSource, "Clave", True))
        Me.ClaveTextBox.Enabled = False
        Me.ClaveTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveTextBox.Location = New System.Drawing.Point(139, 41)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.Size = New System.Drawing.Size(80, 22)
        Me.ClaveTextBox.TabIndex = 4
        '
        'Fecha_CuarentenaTextBox
        '
        Me.Fecha_CuarentenaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Cat_Num_TelefonoBindingSource, "Fecha_Cuarentena", True))
        Me.Fecha_CuarentenaTextBox.Enabled = False
        Me.Fecha_CuarentenaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_CuarentenaTextBox.Location = New System.Drawing.Point(579, 113)
        Me.Fecha_CuarentenaTextBox.Name = "Fecha_CuarentenaTextBox"
        Me.Fecha_CuarentenaTextBox.Size = New System.Drawing.Size(135, 22)
        Me.Fecha_CuarentenaTextBox.TabIndex = 16
        '
        'Numero_telTextBox
        '
        Me.Numero_telTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Cat_Num_TelefonoBindingSource, "numero_tel", True))
        Me.Numero_telTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numero_telTextBox.Location = New System.Drawing.Point(139, 74)
        Me.Numero_telTextBox.MaxLength = 10
        Me.Numero_telTextBox.Name = "Numero_telTextBox"
        Me.Numero_telTextBox.Size = New System.Drawing.Size(173, 22)
        Me.Numero_telTextBox.TabIndex = 6
        '
        'Ult_Cliente_AsignadoTextBox
        '
        Me.Ult_Cliente_AsignadoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Cat_Num_TelefonoBindingSource, "Ult_Cliente_Asignado", True))
        Me.Ult_Cliente_AsignadoTextBox.Enabled = False
        Me.Ult_Cliente_AsignadoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ult_Cliente_AsignadoTextBox.Location = New System.Drawing.Point(581, 145)
        Me.Ult_Cliente_AsignadoTextBox.Name = "Ult_Cliente_AsignadoTextBox"
        Me.Ult_Cliente_AsignadoTextBox.Size = New System.Drawing.Size(133, 22)
        Me.Ult_Cliente_AsignadoTextBox.TabIndex = 14
        '
        'Ultima_Fecha_LiberacionTextBox
        '
        Me.Ultima_Fecha_LiberacionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Cat_Num_TelefonoBindingSource, "Ultima_Fecha_Liberacion", True))
        Me.Ultima_Fecha_LiberacionTextBox.Enabled = False
        Me.Ultima_Fecha_LiberacionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ultima_Fecha_LiberacionTextBox.Location = New System.Drawing.Point(579, 84)
        Me.Ultima_Fecha_LiberacionTextBox.Name = "Ultima_Fecha_LiberacionTextBox"
        Me.Ultima_Fecha_LiberacionTextBox.Size = New System.Drawing.Size(135, 22)
        Me.Ultima_Fecha_LiberacionTextBox.TabIndex = 12
        '
        'Ultima_FechaAsigTextBox
        '
        Me.Ultima_FechaAsigTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Cat_Num_TelefonoBindingSource, "Ultima_FechaAsig", True))
        Me.Ultima_FechaAsigTextBox.Enabled = False
        Me.Ultima_FechaAsigTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ultima_FechaAsigTextBox.Location = New System.Drawing.Point(579, 53)
        Me.Ultima_FechaAsigTextBox.Name = "Ultima_FechaAsigTextBox"
        Me.Ultima_FechaAsigTextBox.Size = New System.Drawing.Size(135, 22)
        Me.Ultima_FechaAsigTextBox.TabIndex = 10
        '
        'Consulta_Cat_Num_TelefonoBindingNavigator
        '
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.AddNewItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.BindingSource = Me.Consulta_Cat_Num_TelefonoBindingSource
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.CountItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem})
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Name = "Consulta_Cat_Num_TelefonoBindingNavigator"
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.PositionItem = Nothing
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Size = New System.Drawing.Size(755, 25)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.TabIndex = 1
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(77, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem
        '
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Name = "Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem"
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'StatusTextBox
        '
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Cat_Num_TelefonoBindingSource, "status", True))
        Me.StatusTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusTextBox.Location = New System.Drawing.Point(94, 381)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(100, 22)
        Me.StatusTextBox.TabIndex = 8
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(596, 263)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(147, 36)
        Me.Button5.TabIndex = 23
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Consulta_Cat_Num_TelefonoTableAdapter
        '
        Me.Consulta_Cat_Num_TelefonoTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Status_No_TelefonicoTableAdapter
        '
        Me.Muestra_Status_No_TelefonicoTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Estado_No_TelefonicoTableAdapter
        '
        Me.Muestra_Estado_No_TelefonicoTableAdapter.ClearBeforeFill = True
        '
        'FrmCatalogoNoTel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(755, 310)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Consulta_Cat_Num_TelefonoBindingNavigator)
        Me.Controls.Add(Me.CMBPanel1)
        Me.Controls.Add(StatusLabel)
        Me.Controls.Add(Me.StatusTextBox)
        Me.MaximizeBox = False
        Me.Name = "FrmCatalogoNoTel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cátalogo De No. De Teléfono."
        Me.CMBPanel1.ResumeLayout(False)
        Me.CMBPanel1.PerformLayout()
        CType(Me.Consulta_Cat_Num_TelefonoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientosarnoldo4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_Estado_No_TelefonicoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_Status_No_TelefonicoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Cat_Num_TelefonoBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.ResumeLayout(False)
        Me.Consulta_Cat_Num_TelefonoBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBPanel1 As System.Windows.Forms.Panel
    Friend WithEvents Procedimientosarnoldo4 As sofTV.Procedimientosarnoldo4
    Friend WithEvents Consulta_Cat_Num_TelefonoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Cat_Num_TelefonoTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Consulta_Cat_Num_TelefonoTableAdapter
    Friend WithEvents Consulta_Cat_Num_TelefonoBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_Cat_Num_TelefonoBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Numero_telTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Ultima_FechaAsigTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Ultima_Fecha_LiberacionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Ult_Cliente_AsignadoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_CuarentenaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Muestra_Status_No_TelefonicoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Status_No_TelefonicoTableAdapter As sofTV.Procedimientosarnoldo4TableAdapters.Muestra_Status_No_TelefonicoTableAdapter
    Friend WithEvents StatusComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CMBTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents Muestra_Estado_No_TelefonicoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Estado_No_TelefonicoTableAdapter As sofTV.DataSetLidia2TableAdapters.Muestra_Estado_No_TelefonicoTableAdapter
End Class
