Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class FrmRepPenetracion
    Private customersByCityReport As ReportDocument
    Dim princ As String = Nothing
    Dim oprep As String = Nothing
    Dim Titulo As String = Nothing
    Private Sub ConfigureCrystalReportsNew6()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim Contrataciones As Boolean = False
            Dim Instalaciones As Boolean = False
            Dim Fuera_Area As Boolean = False
            Dim Cancelaciones As Boolean = False
            Dim reportPath As String = Nothing

            Dim mySelectFormula As String = Titulo
            Dim Subformula As String = Nothing

            mySelectFormula = "Pen�traci�n Por Calle y Colonia Del "

            reportPath = RutaReportes + "\ReportePenetracion_CalleyColonia.rpt"

            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@op
            customersByCityReport.SetParameterValue(0, GloClv_tipser2)
            '@clv_colonia
            customersByCityReport.SetParameterValue(1, Locclv_colonia)
            '@clv_calle
            customersByCityReport.SetParameterValue(2, Locclv_calle)
            '@clv_txt
            customersByCityReport.SetParameterValue(3, Locclv_txt)

            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait

            CrystalReportViewer1.ReportSource = customersByCityReport

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & LocDesP & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & GloSucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia").Text = "' Colonia: " & LocDesPC & "'"
            customersByCityReport.DataDefinition.FormulaFields("Calle").Text = "'" & LocDesPCa & "'"

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()

    End Sub

    Private Sub FrmRepPenetracion_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Locbndpen1 = True Then
            Locbndpen1 = False

            Dame_Datos_Rep_P(GloClv_tipser2, Locclv_colonia, Locclv_calle, Locclv_txt)
            If CInt(Me.SalidasTapsTextBox.Text) > 0 Then
                ConfigureCrystalReportsNew6()
            Else
                MsgBox("En esa Colonia(s) y/o Calle(s) no existen salidas en los Taps", MsgBoxStyle.Information)
            End If

            '    Dim CON As New SqlConnection(MiConexion)
            '    CON.Open()
            '    Me.Dame_Datos_Rep_PTableAdapter.Connection = CON
            '    Me.Dame_Datos_Rep_PTableAdapter.Fill(Me.DataSetarnoldo.Dame_Datos_Rep_P, GloClv_tipser2, Locclv_colonia, Locclv_calle, Locclv_txt)
            '    CON.Close()

            '    If CInt(Me.SalidasTapsTextBox.Text) > 0 Then
            '        Me.TextBox2.Text = Math.Round(((CDbl(Me.ITextBox.Text) + CDbl(Me.DTextBox.Text) + CDbl(Me.STextBox.Text)) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
            '        Me.TextBox3.Text = Math.Round((CDbl(Me.ITextBox.Text) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
            '        ConfigureCrystalReportsNew6()
            '    Else
            '        MsgBox("En esa Colonia(s) y/o Calle(s) no existen salidas en los Taps", MsgBoxStyle.Information)
            '    End If
        End If
    End Sub

    Private Sub FrmRepPenetracion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)

        princ = Me.ComboBox4.SelectedValue.ToString
        GloClv_tipser2 = Me.ComboBox4.SelectedValue
        Select Case princ
            Case "1"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            Case "2"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            Case "3"
                Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
                Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
        End Select
        CON.Close()
        colorea(Me, Me.Name)
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
            GloClv_tipser2 = ComboBox4.SelectedValue
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Catalogo_Reportes_PenetracionTableAdapter.Connection = CON
            Me.Catalogo_Reportes_PenetracionTableAdapter.Fill(Me.DataSetarnoldo.Catalogo_Reportes_Penetracion, Me.ComboBox4.SelectedValue)
            CON.Close()
        End If
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value) = True Then
            oprep = Me.DataGridView1.SelectedCells(0).Value.ToString
            Titulo = Me.DataGridView1.SelectedCells(1).Value.ToString
            If oprep = "1" Then
                My.Forms.FrmSelDatosP.Show()
            End If
            If oprep = "2" Then
                'Ottra cosa
            End If
        End If
    End Sub

    Private Sub Dame_Datos_Rep_P(ByVal Op As Integer, ByVal Clv_Colonia As Integer, ByVal Clv_Calle As Integer, ByVal clv_txt As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("Dame_Datos_Rep_P", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@op", Op)
        CMD.Parameters.AddWithValue("@clv_colonia", Clv_Colonia)
        CMD.Parameters.AddWithValue("@Clv_Calle", Clv_Calle)
        CMD.Parameters.AddWithValue("@clv_txt", clv_txt)

        Dim READER As SqlDataReader

        Try
            CON.Open()
            READER = CMD.ExecuteReader

            While (READER.Read)
                Me.CTextBox.Text = READER(2).ToString
                Me.ITextBox.Text = READER(3).ToString
                Me.DTextBox.Text = READER(4).ToString
                Me.STextBox.Text = READER(5).ToString
                Me.BTextBox.Text = READER(6).ToString
                Me.FTextBox.Text = READER(7).ToString
                Me.SalidasTapsTextBox.Text = READER(8).ToString
                Me.TotalTextBox.Text = READER(0).ToString
            End While

            If IsNumeric(Me.SalidasTapsTextBox.Text) = True Then
                If CLng(Me.SalidasTapsTextBox.Text) > 0 Then
                    Me.TextBox2.Text = Math.Round(((CDbl(Me.ITextBox.Text) + CDbl(Me.DTextBox.Text) + CDbl(Me.STextBox.Text)) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
                    Me.TextBox3.Text = Math.Round((CDbl(Me.ITextBox.Text) / CDbl(Me.SalidasTapsTextBox.Text)) * 100, 2)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
End Class