﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRegistroLlamadas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.CMBLabel6 = New System.Windows.Forms.Label()
        Me.CONTRATOTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.STATUSTextBox = New System.Windows.Forms.TextBox()
        Me.USUARIOTextBox = New System.Windows.Forms.TextBox()
        Me.GeneradoDateTime = New System.Windows.Forms.DateTimePicker()
        Me.LLAMADADateTime = New System.Windows.Forms.DateTimePicker()
        Me.AceptarButton = New System.Windows.Forms.Button()
        Me.CancelarButton = New System.Windows.Forms.Button()
        Me.CMBLabel8 = New System.Windows.Forms.Label()
        Me.SOLUCIONTextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel9 = New System.Windows.Forms.Label()
        Me.RESPUESTATextBox = New System.Windows.Forms.TextBox()
        Me.TelefonoTextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel10 = New System.Windows.Forms.Label()
        Me.LimiteTextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel11 = New System.Windows.Forms.Label()
        Me.ConsumoTextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel12 = New System.Windows.Forms.Label()
        Me.SucursalTextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel13 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(75, 33)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(69, 15)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Contrato :"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(78, 60)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(66, 15)
        Me.CMBLabel2.TabIndex = 1
        Me.CMBLabel2.Text = "Nombre :"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(12, 170)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(132, 15)
        Me.CMBLabel3.TabIndex = 2
        Me.CMBLabel3.Text = "Fecha Generación :"
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(89, 195)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(55, 15)
        Me.CMBLabel4.TabIndex = 3
        Me.CMBLabel4.Text = "Status :"
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(30, 251)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(114, 15)
        Me.CMBLabel5.TabIndex = 4
        Me.CMBLabel5.Text = "Fecha Llamada :"
        '
        'CMBLabel6
        '
        Me.CMBLabel6.AutoSize = True
        Me.CMBLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel6.Location = New System.Drawing.Point(79, 222)
        Me.CMBLabel6.Name = "CMBLabel6"
        Me.CMBLabel6.Size = New System.Drawing.Size(65, 15)
        Me.CMBLabel6.TabIndex = 5
        Me.CMBLabel6.Text = "Usuario :"
        '
        'CONTRATOTextBox
        '
        Me.CONTRATOTextBox.Location = New System.Drawing.Point(150, 30)
        Me.CONTRATOTextBox.Name = "CONTRATOTextBox"
        Me.CONTRATOTextBox.ReadOnly = True
        Me.CONTRATOTextBox.Size = New System.Drawing.Size(101, 21)
        Me.CONTRATOTextBox.TabIndex = 0
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.Location = New System.Drawing.Point(150, 57)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.ReadOnly = True
        Me.NOMBRETextBox.Size = New System.Drawing.Size(292, 21)
        Me.NOMBRETextBox.TabIndex = 1
        '
        'STATUSTextBox
        '
        Me.STATUSTextBox.Location = New System.Drawing.Point(150, 192)
        Me.STATUSTextBox.Name = "STATUSTextBox"
        Me.STATUSTextBox.ReadOnly = True
        Me.STATUSTextBox.Size = New System.Drawing.Size(292, 21)
        Me.STATUSTextBox.TabIndex = 6
        '
        'USUARIOTextBox
        '
        Me.USUARIOTextBox.Location = New System.Drawing.Point(150, 219)
        Me.USUARIOTextBox.Name = "USUARIOTextBox"
        Me.USUARIOTextBox.ReadOnly = True
        Me.USUARIOTextBox.Size = New System.Drawing.Size(292, 21)
        Me.USUARIOTextBox.TabIndex = 7
        '
        'GeneradoDateTime
        '
        Me.GeneradoDateTime.CustomFormat = "yyyy/MM/dd"
        Me.GeneradoDateTime.Enabled = False
        Me.GeneradoDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.GeneradoDateTime.Location = New System.Drawing.Point(150, 165)
        Me.GeneradoDateTime.Name = "GeneradoDateTime"
        Me.GeneradoDateTime.Size = New System.Drawing.Size(200, 21)
        Me.GeneradoDateTime.TabIndex = 5
        '
        'LLAMADADateTime
        '
        Me.LLAMADADateTime.CustomFormat = "yyyy/MM/dd"
        Me.LLAMADADateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.LLAMADADateTime.Location = New System.Drawing.Point(150, 246)
        Me.LLAMADADateTime.Name = "LLAMADADateTime"
        Me.LLAMADADateTime.Size = New System.Drawing.Size(200, 21)
        Me.LLAMADADateTime.TabIndex = 8
        Me.LLAMADADateTime.Value = New Date(2010, 8, 20, 0, 0, 0, 0)
        '
        'AceptarButton
        '
        Me.AceptarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AceptarButton.Location = New System.Drawing.Point(159, 546)
        Me.AceptarButton.Name = "AceptarButton"
        Me.AceptarButton.Size = New System.Drawing.Size(128, 35)
        Me.AceptarButton.TabIndex = 12
        Me.AceptarButton.Text = "&Guardar"
        Me.AceptarButton.UseVisualStyleBackColor = True
        '
        'CancelarButton
        '
        Me.CancelarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CancelarButton.Location = New System.Drawing.Point(304, 546)
        Me.CancelarButton.Name = "CancelarButton"
        Me.CancelarButton.Size = New System.Drawing.Size(128, 35)
        Me.CancelarButton.TabIndex = 13
        Me.CancelarButton.Text = "&Cancelar"
        Me.CancelarButton.UseVisualStyleBackColor = True
        '
        'CMBLabel8
        '
        Me.CMBLabel8.AutoSize = True
        Me.CMBLabel8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel8.Location = New System.Drawing.Point(12, 300)
        Me.CMBLabel8.Name = "CMBLabel8"
        Me.CMBLabel8.Size = New System.Drawing.Size(132, 15)
        Me.CMBLabel8.TabIndex = 14
        Me.CMBLabel8.Text = "Respuesta Cliente :"
        '
        'SOLUCIONTextBox
        '
        Me.SOLUCIONTextBox.Location = New System.Drawing.Point(150, 425)
        Me.SOLUCIONTextBox.Multiline = True
        Me.SOLUCIONTextBox.Name = "SOLUCIONTextBox"
        Me.SOLUCIONTextBox.Size = New System.Drawing.Size(292, 118)
        Me.SOLUCIONTextBox.TabIndex = 11
        '
        'CMBLabel9
        '
        Me.CMBLabel9.AutoSize = True
        Me.CMBLabel9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel9.Location = New System.Drawing.Point(4, 421)
        Me.CMBLabel9.Name = "CMBLabel9"
        Me.CMBLabel9.Size = New System.Drawing.Size(140, 15)
        Me.CMBLabel9.TabIndex = 16
        Me.CMBLabel9.Text = "Solución Propuesta :"
        '
        'RESPUESTATextBox
        '
        Me.RESPUESTATextBox.Location = New System.Drawing.Point(150, 300)
        Me.RESPUESTATextBox.Multiline = True
        Me.RESPUESTATextBox.Name = "RESPUESTATextBox"
        Me.RESPUESTATextBox.Size = New System.Drawing.Size(292, 119)
        Me.RESPUESTATextBox.TabIndex = 10
        '
        'TelefonoTextBox
        '
        Me.TelefonoTextBox.Location = New System.Drawing.Point(150, 84)
        Me.TelefonoTextBox.Name = "TelefonoTextBox"
        Me.TelefonoTextBox.ReadOnly = True
        Me.TelefonoTextBox.Size = New System.Drawing.Size(292, 21)
        Me.TelefonoTextBox.TabIndex = 2
        '
        'CMBLabel10
        '
        Me.CMBLabel10.AutoSize = True
        Me.CMBLabel10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel10.Location = New System.Drawing.Point(73, 87)
        Me.CMBLabel10.Name = "CMBLabel10"
        Me.CMBLabel10.Size = New System.Drawing.Size(71, 15)
        Me.CMBLabel10.TabIndex = 17
        Me.CMBLabel10.Text = "Teléfono :"
        '
        'LimiteTextBox
        '
        Me.LimiteTextBox.Location = New System.Drawing.Point(150, 111)
        Me.LimiteTextBox.Name = "LimiteTextBox"
        Me.LimiteTextBox.ReadOnly = True
        Me.LimiteTextBox.Size = New System.Drawing.Size(292, 21)
        Me.LimiteTextBox.TabIndex = 3
        '
        'CMBLabel11
        '
        Me.CMBLabel11.AutoSize = True
        Me.CMBLabel11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel11.Location = New System.Drawing.Point(19, 114)
        Me.CMBLabel11.Name = "CMBLabel11"
        Me.CMBLabel11.Size = New System.Drawing.Size(125, 15)
        Me.CMBLabel11.TabIndex = 19
        Me.CMBLabel11.Text = "Límite de Crédito :"
        '
        'ConsumoTextBox
        '
        Me.ConsumoTextBox.Location = New System.Drawing.Point(150, 138)
        Me.ConsumoTextBox.Name = "ConsumoTextBox"
        Me.ConsumoTextBox.ReadOnly = True
        Me.ConsumoTextBox.Size = New System.Drawing.Size(292, 21)
        Me.ConsumoTextBox.TabIndex = 4
        '
        'CMBLabel12
        '
        Me.CMBLabel12.AutoSize = True
        Me.CMBLabel12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel12.Location = New System.Drawing.Point(68, 141)
        Me.CMBLabel12.Name = "CMBLabel12"
        Me.CMBLabel12.Size = New System.Drawing.Size(75, 15)
        Me.CMBLabel12.TabIndex = 21
        Me.CMBLabel12.Text = "Consumo :"
        '
        'SucursalTextBox
        '
        Me.SucursalTextBox.Location = New System.Drawing.Point(150, 273)
        Me.SucursalTextBox.Name = "SucursalTextBox"
        Me.SucursalTextBox.ReadOnly = True
        Me.SucursalTextBox.Size = New System.Drawing.Size(292, 21)
        Me.SucursalTextBox.TabIndex = 9
        '
        'CMBLabel13
        '
        Me.CMBLabel13.AutoSize = True
        Me.CMBLabel13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel13.Location = New System.Drawing.Point(79, 276)
        Me.CMBLabel13.Name = "CMBLabel13"
        Me.CMBLabel13.Size = New System.Drawing.Size(71, 15)
        Me.CMBLabel13.TabIndex = 22
        Me.CMBLabel13.Text = "Sucursal :"
        '
        'FrmRegistroLlamadas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(454, 589)
        Me.Controls.Add(Me.SucursalTextBox)
        Me.Controls.Add(Me.CMBLabel13)
        Me.Controls.Add(Me.ConsumoTextBox)
        Me.Controls.Add(Me.CMBLabel12)
        Me.Controls.Add(Me.LimiteTextBox)
        Me.Controls.Add(Me.CMBLabel11)
        Me.Controls.Add(Me.TelefonoTextBox)
        Me.Controls.Add(Me.CMBLabel10)
        Me.Controls.Add(Me.RESPUESTATextBox)
        Me.Controls.Add(Me.CMBLabel9)
        Me.Controls.Add(Me.SOLUCIONTextBox)
        Me.Controls.Add(Me.CMBLabel8)
        Me.Controls.Add(Me.CancelarButton)
        Me.Controls.Add(Me.AceptarButton)
        Me.Controls.Add(Me.LLAMADADateTime)
        Me.Controls.Add(Me.GeneradoDateTime)
        Me.Controls.Add(Me.USUARIOTextBox)
        Me.Controls.Add(Me.STATUSTextBox)
        Me.Controls.Add(Me.NOMBRETextBox)
        Me.Controls.Add(Me.CONTRATOTextBox)
        Me.Controls.Add(Me.CMBLabel6)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FrmRegistroLlamadas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Registro de Llamadas"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel6 As System.Windows.Forms.Label
    Friend WithEvents CONTRATOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents STATUSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents USUARIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GeneradoDateTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents LLAMADADateTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents AceptarButton As System.Windows.Forms.Button
    Friend WithEvents CancelarButton As System.Windows.Forms.Button
    Friend WithEvents CMBLabel8 As System.Windows.Forms.Label
    Friend WithEvents SOLUCIONTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel9 As System.Windows.Forms.Label
    Friend WithEvents RESPUESTATextBox As System.Windows.Forms.TextBox
    Friend WithEvents TelefonoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel10 As System.Windows.Forms.Label
    Friend WithEvents LimiteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel11 As System.Windows.Forms.Label
    Friend WithEvents ConsumoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel12 As System.Windows.Forms.Label
    Friend WithEvents SucursalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel13 As System.Windows.Forms.Label
End Class
