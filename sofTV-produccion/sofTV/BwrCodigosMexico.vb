Imports System.Data.SqlClient
Public Class BwrCodigosMexico
    Public KeyAscii As Short
    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function
    Private Sub Busca(ByVal opx As Integer)
        Try
            Dim ConBwr As New SqlConnection(MiConexion)
            If opx = 1 Then
                ConBwr.Open()
                Me.Consulta_CodigosMexicoBrowseTableAdapter.Connection = ConBwr
                Me.Consulta_CodigosMexicoBrowseTableAdapter.Fill(Me.DataSetLidia2.consulta_CodigosMexicoBrowse, 0, "", "", opx)
                ConBwr.Close()
            ElseIf opx = 2 Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    ConBwr.Open()
                    Me.Consulta_CodigosMexicoBrowseTableAdapter.Connection = ConBwr
                    Me.Consulta_CodigosMexicoBrowseTableAdapter.Fill(Me.DataSetLidia2.consulta_CodigosMexicoBrowse, CInt(Me.TextBox1.Text), "", "", opx)
                    ConBwr.Close()
                End If
                ElseIf opx = 3 Then
                    ConBwr.Open()
                    Me.Consulta_CodigosMexicoBrowseTableAdapter.Connection = ConBwr
                    Me.Consulta_CodigosMexicoBrowseTableAdapter.Fill(Me.DataSetLidia2.consulta_CodigosMexicoBrowse, 0, Me.TextBox2.Text, "", opx)
                    ConBwr.Close()
                ElseIf opx = 4 Then
                    ConBwr.Open()
                    Me.Consulta_CodigosMexicoBrowseTableAdapter.Connection = ConBwr
                    Me.Consulta_CodigosMexicoBrowseTableAdapter.Fill(Me.DataSetLidia2.consulta_CodigosMexicoBrowse, 0, "", Me.TextBox3.Text, opx)
                    ConBwr.Close()
                End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        FrmCodigosMexico.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If (IsNumeric(Me.Clv_CodigoTextBox.Text) = True) Then
            opcion = "C"
            clv_codigomex = Me.Clv_CodigoTextBox.Text
            FrmCodigosMexico.Show()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If (IsNumeric(Me.Clv_CodigoTextBox.Text) = True) Then
            opcion = "M"
            clv_codigomex = Me.Clv_CodigoTextBox.Text
            FrmCodigosMexico.Show()
        End If
    End Sub

    Private Sub BwrCodigosMexico_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bec_bnd = True Then
            bec_bnd = False
            Busca(1)
        End If
    End Sub

    Private Sub BwrCodigosMexico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(2)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.TextBox2.Text.Length > 0 Then
            Busca(3)
        End If
    End Sub

    Private Sub TextBox2_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox2.Text.Length > 0 Then
                Busca(3)
            End If
        End If
    End Sub


    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If Me.TextBox3.Text.Length > 0 Then
            Busca(4)
        End If
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox3.Text.Length > 0 Then
                Busca(4)
            End If
        End If
    End Sub

    Private Sub Consulta_CodigosMexicoBrowseDataGridView_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Consulta_CodigosMexicoBrowseDataGridView.DoubleClick
        If (IsNumeric(Me.Clv_CodigoTextBox.Text) = True) Then
            opcion = "C"
            clv_codigomex = Me.Clv_CodigoTextBox.Text
            FrmCodigosMexico.Show()
        End If
    End Sub
End Class