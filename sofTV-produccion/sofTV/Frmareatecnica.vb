
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.Data.SqlClient
Imports System.Text

Public Class Frmareatecnica
    Delegate Sub Reporte(ByVal op As String, ByVal Titulop As String)
    Public customersByCityReport As ReportDocument
    Dim op As String = Nothing
    Public bnd As Boolean = False
    Public bnd2 As Boolean = False
    Dim Titulo As String = Nothing
    Private opreporte As Integer = 0
    Public OpTecnico As Integer = 0
    Public Clv_Tecnico_Rep As Integer = 0
    Public Activado As Boolean = False



    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        Try
            If op = 1 Then
                If IdSistema = "SA" Then
                    ConfigureCrystalReports_NewXml(op, Titulo)
                    Exit Sub
                End If
            End If


            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Status As String = Nothing

            If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
                StatusPen = "1"
                If Me.EjecutadasCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    Status = Status & "Pendientes,"
                Else
                    Status = "Pendientes"
                End If
            End If
            If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
                StatusEje = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    If Me.PendientesCheckBox.Checked = True Then
                        Status = Status & "Ejecutadas"
                    Else
                        Status = Status & "Ejecutadas,"
                    End If
                Else
                    Status = "Ejecutadas"
                End If
            End If
            If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
                StatusVis = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.EjecutadasCheckBox.Checked = True Then
                    Status = Status & "Visita"
                Else
                    Status = "Visita"
                End If
            End If



            If Me.NUMINILbl.Text.Length > 0 Then
                If IsNumeric(Me.NUMINILbl.Text) = True Then
                    If CLng(Me.NUMINILbl.Text) > 0 Then
                        Op1 = "1"
                    End If
                End If
            End If
            If Me.FECSOLINI.Text.Length > 0 Then

                Op2 = "1"
            End If
            If Me.FECEJEINI.Text.Length > 0 Then
                Op3 = "1"
            End If
            If Me.NOMTRABAJO.Text.Length > 0 Then
                Op4 = "1"
            End If
            If Me.NOMCOLONIA.Text.Length > 0 Then
                Op5 = "1"
            End If

            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "0"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton4.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing

            If op = 0 Then
                If IdSistema <> "SA" And IdSistema <> "VA" Then
                    reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo.rpt"
                ElseIf IdSistema = "SA" Or IdSistema = "VA" Then
                    reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo_SA.rpt"
                End If
            ElseIf op = 1 Then
                If IdSistema = "TO" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
                ElseIf IdSistema = "AG" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
                ElseIf IdSistema = "SA" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
                ElseIf IdSistema = "VA" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
                ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
                End If
            ElseIf op = 2 Then
                reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo_SA_2.rpt"
            End If



            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, 0)
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, Op1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, Op2)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, Op3)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, Op4)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, Op5)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(6, StatusPen)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(7, StatusEje)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(8, StatusVis)
            '@Clv_OrdenIni bigint
            If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
            customersByCityReport.SetParameterValue(9, CLng(Num1))
            ',@Clv_OrdenFin bigint
            If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
            customersByCityReport.SetParameterValue(10, CLng(Num2))
            ',@Fec1Ini Datetime
            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            customersByCityReport.SetParameterValue(11, Fec1Ini)
            ',@Fec1Fin Datetime,
            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            customersByCityReport.SetParameterValue(12, Fec1Fin)
            '@Fec2Ini Datetime
            If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
            customersByCityReport.SetParameterValue(13, Fec2Ini)
            ',@Fec2Fin Datetime
            If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
            customersByCityReport.SetParameterValue(14, Fec2Fin)
            ',@Clv_Trabajo int
            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            customersByCityReport.SetParameterValue(15, nclv_trabajo)
            ',@Clv_Colonia int
            If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
            customersByCityReport.SetParameterValue(16, nClv_colonia)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(17, OpOrdenar)
            ',@OpTecnico
            customersByCityReport.SetParameterValue(18, OpTecnico)
            ',@Clv_Tecnico_Rep
            customersByCityReport.SetParameterValue(19, Clv_Tecnico_Rep)


            mySelectFormula = "Listado de Ordenes de Servicio"
            If op = 0 Then
                customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
                'customersByCityReport.DataDefinition.FormulaFields("nomservicio").Text = "'" & Me.ComboBox4.Text & " con status " & Status & "'"
            ElseIf op = 1 Then
                mySelectFormula = "Orden De Servicio: "
                customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            End If

            '--SetDBLogonForReport(connectionInfo)

            If Me.CrystalReportViewer1.InvokeRequired Then
                Dim d As New Reporte(AddressOf ConfigureCrystalReports)
                Me.Invoke(d, New Object() {op, Titulo})
            Else
                CrystalReportViewer1.ReportSource = customersByCityReport
            End If


            'CrystalReportViewer1.ReportSource = customersByCityReport

            customersByCityReport = Nothing
            bnd = True

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReports_NewXml(ByVal op As String, ByVal Titulo As String)
        Try
            Dim cnn As New SqlConnection(MiConexion)

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Status As String = Nothing

            If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
                StatusPen = "1"
                If Me.EjecutadasCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    Status = Status & "Pendientes,"
                Else
                    Status = "Pendientes"
                End If
            End If
            If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
                StatusEje = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    If Me.PendientesCheckBox.Checked = True Then
                        Status = Status & "Ejecutadas"
                    Else
                        Status = Status & "Ejecutadas,"
                    End If
                Else
                    Status = "Ejecutadas"
                End If
            End If
            If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
                StatusVis = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.EjecutadasCheckBox.Checked = True Then
                    Status = Status & "Visita"
                Else
                    Status = "Visita"
                End If
            End If



            If Me.NUMINILbl.Text.Length > 0 Then
                If IsNumeric(Me.NUMINILbl.Text) = True Then
                    If CLng(Me.NUMINILbl.Text) > 0 Then
                        Op1 = "1"
                    End If
                End If
            End If
            If Me.FECSOLINI.Text.Length > 0 Then

                Op2 = "1"
            End If
            If Me.FECEJEINI.Text.Length > 0 Then
                Op3 = "1"
            End If
            If Me.NOMTRABAJO.Text.Length > 0 Then
                Op4 = "1"
            End If
            If Me.NOMCOLONIA.Text.Length > 0 Then
                Op5 = "1"
            End If

            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "0"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton4.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\RepOrdSerNew.rpt"

 
            '',@StatusVis bit,
            'customersByCityReport.SetParameterValue(8, StatusVis)
            '@Clv_OrdenIni bigint
            If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
            'customersByCityReport.SetParameterValue(9, CLng(Num1))
            ',@Clv_OrdenFin bigint
            If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
            'customersByCityReport.SetParameterValue(10, CLng(Num2))
            '',@Fec1Ini Datetime
            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            'customersByCityReport.SetParameterValue(11, Fec1Ini)
            ',@Fec1Fin Datetime,
            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            'customersByCityReport.SetParameterValue(12, Fec1Fin)
            '@Fec2Ini Datetime
            If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
            'customersByCityReport.SetParameterValue(13, Fec2Ini)
            ',@Fec2Fin Datetime
            If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
            ' customersByCityReport.SetParameterValue(14, Fec2Fin)
            ',@Clv_Trabajo int
            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            'customersByCityReport.SetParameterValue(15, nclv_trabajo)
            ',@Clv_Colonia int
            If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
            'customersByCityReport.SetParameterValue(16, nClv_colonia)
            ',@OpOrden int
            'customersByCityReport.SetParameterValue(17, OpOrdenar)
            '',@OpTecnico
            'customersByCityReport.SetParameterValue(18, OpTecnico)
            '',@Clv_Tecnico_Rep
            'customersByCityReport.SetParameterValue(19, Clv_Tecnico_Rep)


            Dim cmd As New SqlCommand("ReporteAreaTecnicaOrdSer", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            '@Clv_TipSer int,@op1 smallint,@op2 smallint,@op3 smallint,@op4 smallint,
            '@op5 smallint,@StatusPen bit,@StatusEje bit,@StatusVis bit,
            '@Clv_OrdenIni bigint,@Clv_OrdenFin bigint,@Fec1Ini Datetime,@Fec1Fin Datetime,
            '@Fec2Ini Datetime,@Fec2Fin Datetime,@Clv_Trabajo int,@Clv_Colonia int,@OpOrden int
            Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = 0
            cmd.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = Op1
            cmd.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Op2
            cmd.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Op3
            cmd.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Op4
            cmd.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Op5
            cmd.Parameters.Add(parametro6)

            Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
            parametro7.Direction = ParameterDirection.Input
            If StatusPen = "1" Then
                parametro7.Value = True
            Else
                parametro7.Value = False
            End If
            cmd.Parameters.Add(parametro7)

            Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
            parametro8.Direction = ParameterDirection.Input
            If StatusEje = "1" Then
                parametro8.Value = True
            Else
                parametro8.Value = False
            End If
            cmd.Parameters.Add(parametro8)

            Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
            parametro9.Direction = ParameterDirection.Input
            If StatusVis = "1" Then
                parametro9.Value = True
            Else
                parametro9.Value = False
            End If
            cmd.Parameters.Add(parametro9)

            Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
            parametro10.Direction = ParameterDirection.Input
            parametro10.Value = CLng(Num1)
            cmd.Parameters.Add(parametro10)

            Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
            parametro11.Direction = ParameterDirection.Input
            parametro11.Value = CLng(Num2)
            cmd.Parameters.Add(parametro11)

            Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro12.Direction = ParameterDirection.Input
            parametro12.Value = Fec1Ini
            cmd.Parameters.Add(parametro12)

            Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
            parametro13.Direction = ParameterDirection.Input
            parametro13.Value = Fec1Fin
            cmd.Parameters.Add(parametro13)

            Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro14.Direction = ParameterDirection.Input
            parametro14.Value = Fec2Ini
            cmd.Parameters.Add(parametro14)

            Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
            parametro15.Direction = ParameterDirection.Input
            parametro15.Value = Fec2Fin
            cmd.Parameters.Add(parametro15)

            Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro16.Direction = ParameterDirection.Input
            parametro16.Value = nclv_trabajo
            cmd.Parameters.Add(parametro16)

            Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
            parametro17.Direction = ParameterDirection.Input
            parametro17.Value = nClv_colonia
            cmd.Parameters.Add(parametro17)

            Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
            parametro18.Direction = ParameterDirection.Input
            parametro18.Value = OpOrdenar
            cmd.Parameters.Add(parametro18)

            Dim da As New SqlDataAdapter(cmd)

            Dim data1 As New DataTable()
            Dim data2 As New DataTable()
            Dim data3 As New DataTable()
            Dim data4 As New DataTable()
            Dim data5 As New DataTable()
            Dim data6 As New DataTable()

            da.Fill(data1)



            Dim cmd2 As New SqlCommand("DameDatosGenerales_2 ", cnn)
            cmd2.CommandType = CommandType.StoredProcedure
            Dim da2 As New SqlDataAdapter(cmd2)


           


            Dim ds As New DataSet()
            da.Fill(data1)
            da2.Fill(data2)


            data1.TableName = "ReporteAreaTecnicaOrdSer"
            data2.TableName = "DameDatosGenerales_2"


            ds.Tables.Add(data1)
            ds.Tables.Add(data2)



            customersByCityReport.Load(reportPath)


            customersByCityReport.SetDataSource(ds)



            CrystalReportViewer1.ReportSource = customersByCityReport



            customersByCityReport = Nothing
            bnd = True

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Frmareatecnica_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBusca_NumOrden = True Then
            GloBusca_NumOrden = False
            Me.NUMFINLBL.Text = GLONUMCLV_ORDEN_FIN
            Me.NUMINILbl.Text = GLONUMCLV_ORDEN_INI
            GLONUMCLV_ORDEN_FIN = 0
            GLONUMCLV_ORDEN_INI = 0
        End If
        If op = "1" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECSOLINI.Text = GloFecha_Ini
            Me.FECSOLFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If op = "2" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECEJEINI.Text = GloFecha_Ini
            Me.FECEJEFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If GloBndTrabajo = True Then
            GloBndTrabajo = False
            Me.CLV_TRABAJO.Text = GloNumClv_Trabajo
            Me.NOMTRABAJO.Text = GLONOMTRABAJO
        End If
        If GlobndClv_Colonia = True Then
            GlobndClv_Colonia = False
            Me.CLV_COLONIA.Text = GloNumClv_Colonia
            Me.NOMCOLONIA.Text = GLONOMCOLONIA
        End If

    End Sub

    Private Sub Frmareatecnica_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Activado = False
    End Sub

  



    Private Sub Frmareatecnica_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Activado = True

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        CONTecnicos()

        CON.Close()
    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub LIMPIA()
        Me.CheckBox1.CheckState = CheckState.Unchecked
        Me.CheckBox2.CheckState = CheckState.Unchecked
        Me.CheckBox3.CheckState = CheckState.Unchecked
        Me.CheckBox5.CheckState = CheckState.Unchecked
        Me.CheckBox4.CheckState = CheckState.Unchecked
        Me.NUMFINLBL.Text = 0
        Me.NUMINILbl.Text = 0
        Me.FECSOLFIN.Text = ""
        Me.FECSOLINI.Text = ""
        Me.FECEJEFIN.Text = ""
        Me.FECEJEINI.Text = ""
        Me.CLV_TRABAJO.Text = 0
        Me.NOMTRABAJO.Text = ""
        Me.CLV_COLONIA.Text = 0
        Me.NOMCOLONIA.Text = ""
    End Sub



    Private Sub PendientesCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PendientesCheckBox.CheckedChanged
        If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
            GloPendientes = 1
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
        Else
            GloPendientes = 0
        End If
    End Sub

    Private Sub EjecutadasCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EjecutadasCheckBox.CheckedChanged
        If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
            GloEjecutadas = 1
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
        Else
            GloEjecutadas = 0
        End If
    End Sub

    Private Sub VisitaCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VisitaCheckBox.CheckedChanged
        If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
            GloVisita = 1
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
        Else
            GloVisita = 0
        End If
    End Sub



    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            op = "0"
            FrmSelOrdSer.Show()
        Else
            Me.NUMINILbl.Text = 0
            Me.NUMFINLBL.Text = 0
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            op = "1"
            FrmSelFechas.Show()
        Else
            Me.FECSOLFIN.Text = ""
            Me.FECSOLINI.Text = ""
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            op = "2"
            FrmSelFechas.Show()
        Else
            Me.FECEJEINI.Text = ""
            Me.FECEJEFIN.Text = ""
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            op = "3"
            GloClv_TipSer = 0
            FrmSelTrabajo.Show()
        Else
            Me.CLV_TRABAJO.Text = 0
            Me.NOMTRABAJO.Text = ""
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            op = "4"
            FrmSelColonia.Show()
        Else
            Me.CLV_COLONIA.Text = 0
            Me.NOMCOLONIA.Text = ""
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        opreporte = 1
        If Me.CheckBox6.Checked = True Then
            OpTecnico = 0
        Else
            OpTecnico = 1
            Clv_Tecnico_Rep = Me.ComboTec.SelectedValue.ToString
        End If
        ConfigureCrystalReports_NewXmlListado(0, "")
        'Me.BackgroundWorker1.RunWorkerAsync()
        'PantallaProcesando.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opreporte = 2
        ' ConfigureCrystalReports_NewXml(1, "")
        'Me.BackgroundWorker1.RunWorkerAsync()
        'PantallaProcesando.Show()
        ConfigureCrystalReports_NewXml(1, "")
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Try
            Select Case opreporte
                Case 1
                    ConfigureCrystalReports_NewXmlListado(0, "")
                Case 2
                    ConfigureCrystalReports_NewXml(1, "")
                Case 3
                    ConfigureCrystalReports_NewXmlListado2(2, "")
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub
    Private Sub ConfigureCrystalReports_NewXmlListado(ByVal op As String, ByVal Titulo As String)
        Try
            Dim cnn As New SqlConnection(MiConexion)
            customersByCityReport = New ReportDocument

            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Status As String = Nothing

            If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
                StatusPen = "1"
                If Me.EjecutadasCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    Status = Status & "Pendientes,"
                Else
                    Status = "Pendientes"
                End If
            End If
            If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
                StatusEje = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    If Me.PendientesCheckBox.Checked = True Then
                        Status = Status & "Ejecutadas"
                    Else
                        Status = Status & "Ejecutadas,"
                    End If
                Else
                    Status = "Ejecutadas"
                End If
            End If
            If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
                StatusVis = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.EjecutadasCheckBox.Checked = True Then
                    Status = Status & "Visita"
                Else
                    Status = "Visita"
                End If
            End If
            If Me.CheckBox6.CheckState = CheckState.Unchecked Then
                OpTecnico = 0
            Else
                OpTecnico = 1
                Clv_Tecnico_Rep = Me.ComboTec.SelectedValue.ToString
            End If
            If Me.NUMINILbl.Text.Length > 0 Then
                If IsNumeric(Me.NUMINILbl.Text) = True Then
                    If CLng(Me.NUMINILbl.Text) > 0 Then
                        Op1 = "1"
                    End If
                End If
            End If
            If Me.FECSOLINI.Text.Length > 0 Then

                Op2 = "1"
            End If
            If Me.FECEJEINI.Text.Length > 0 Then
                Op3 = "1"
            End If
            If Me.NOMTRABAJO.Text.Length > 0 Then
                Op4 = "1"
            End If
            If Me.NOMCOLONIA.Text.Length > 0 Then
                Op5 = "1"
            End If
            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "0"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton4.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo_SA.rpt"


            '@Clv_OrdenIni bigint
            If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
            'customersByCityReport.SetParameterValue(9, CLng(Num1))
            ',@Clv_OrdenFin bigint
            If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
            'customersByCityReport.SetParameterValue(10, CLng(Num2))
            '',@Fec1Ini Datetime
            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            'customersByCityReport.SetParameterValue(11, Fec1Ini)
            ',@Fec1Fin Datetime,
            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            'customersByCityReport.SetParameterValue(12, Fec1Fin)
            '@Fec2Ini Datetime
            If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
            'customersByCityReport.SetParameterValue(13, Fec2Ini)
            ',@Fec2Fin Datetime
            If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
            ' customersByCityReport.SetParameterValue(14, Fec2Fin)
            ',@Clv_Trabajo int
            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)

            If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)



            Dim cmd As New SqlCommand("ReporteAreaTecnicaOrdSer_Prueba", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            '@Clv_TipSer int
            Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = 0
            cmd.Parameters.Add(parametro1)
            '@op1 smallint
            Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = Op1
            cmd.Parameters.Add(parametro2)
            '@op2 smallint
            Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Op2
            cmd.Parameters.Add(parametro3)
            '@op3 smallint
            Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Op3
            cmd.Parameters.Add(parametro4)
            '@op4 smallint
            Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Op4
            cmd.Parameters.Add(parametro5)
            '@op5 smallint
            Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Op5
            cmd.Parameters.Add(parametro6)
            '@StatusPen bit
            Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
            parametro7.Direction = ParameterDirection.Input
            If StatusPen = "1" Then
                parametro7.Value = True
            Else
                parametro7.Value = False
            End If
            cmd.Parameters.Add(parametro7)
            '@StatusEje bit
            Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
            parametro8.Direction = ParameterDirection.Input
            If StatusEje = "1" Then
                parametro8.Value = True
            Else
                parametro8.Value = False
            End If
            cmd.Parameters.Add(parametro8)
            '@StatusVis bit
            Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
            parametro9.Direction = ParameterDirection.Input
            If StatusVis = "1" Then
                parametro9.Value = True
            Else
                parametro9.Value = False
            End If
            cmd.Parameters.Add(parametro9)
            '@Clv_OrdenIni bigint
            Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
            parametro10.Direction = ParameterDirection.Input
            parametro10.Value = CLng(Num1)
            cmd.Parameters.Add(parametro10)
            '@Clv_OrdenFin bigint
            Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
            parametro11.Direction = ParameterDirection.Input
            parametro11.Value = CLng(Num2)
            cmd.Parameters.Add(parametro11)
            '@Fec1Ini Datetime
            Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro12.Direction = ParameterDirection.Input
            parametro12.Value = Fec1Ini
            cmd.Parameters.Add(parametro12)
            '@Fec1Fin Datetime
            Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
            parametro13.Direction = ParameterDirection.Input
            parametro13.Value = Fec1Fin
            cmd.Parameters.Add(parametro13)
            '@Fec2Ini Datetime
            Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro14.Direction = ParameterDirection.Input
            parametro14.Value = Fec2Ini
            cmd.Parameters.Add(parametro14)
            '@Fec2Fin Datetime
            Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
            parametro15.Direction = ParameterDirection.Input
            parametro15.Value = Fec2Fin
            cmd.Parameters.Add(parametro15)
            '@Clv_Trabajo int
            Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro16.Direction = ParameterDirection.Input
            parametro16.Value = nclv_trabajo
            cmd.Parameters.Add(parametro16)
            '@Clv_Colonia int
            Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
            parametro17.Direction = ParameterDirection.Input
            parametro17.Value = nClv_colonia
            cmd.Parameters.Add(parametro17)
            '@OpOrden int
            Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
            parametro18.Direction = ParameterDirection.Input
            parametro18.Value = OpOrdenar
            cmd.Parameters.Add(parametro18)

            '@OpTecnico int, 
            Dim parametro19 As New SqlParameter("@OpTecnico", SqlDbType.Int)
            parametro19.Direction = ParameterDirection.Input
            'If StatusPen = "1" Then
            parametro19.Value = OpTecnico
            'ElseIf StatusEje = "1" Or StatusVis = "1" Then
            'parametro19.Value = OpTecnicoimpresora
            'End If
            cmd.Parameters.Add(parametro19)
            '@Clv_Tecnico_Rep int
            Dim parametro20 As New SqlParameter("@Clv_Tecnico_Rep", SqlDbType.Int)
            parametro20.Direction = ParameterDirection.Input
            'If StatusPen = "1" Then
            parametro20.Value = Clv_Tecnico_Rep
       
            cmd.Parameters.Add(parametro20)

            'Dim parametro21 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            'parametro21.Direction = ParameterDirection.Input
            'parametro21.Value = LocClv_session
            'cmd.Parameters.Add(parametro21)

            Dim da As New SqlDataAdapter(cmd)

            Dim data1 As New DataTable()


            Dim ds As New DataSet()

            da.Fill(data1)

            data1.TableName = "ReporteAreaTecnicaOrdSer_Prueba"
            Dim colString As DataColumn = New DataColumn("StatusPen")
            colString.DataType = System.Type.GetType("System.String")
            data1.Columns.Add(colString)
            If data1.Rows.Count > 0 Then

                If StatusPen = "1" Then

                    For Each dr As DataRow In data1.Rows
                        dr("StatusPen") = "1"
                    Next

                    'data1.Rows(0)("StatusPen") = "1"
                Else
                    For Each dr As DataRow In data1.Rows
                        dr("StatusPen") = "0"
                    Next
                    'data1.Rows(0)("StatusPen") = "0"
                End If
            End If

            ds.Tables.Add(data1)



            customersByCityReport.Load(reportPath)
            mySelectFormula = "Listado de Ordenes de Servicio"

            customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"


            SetDBReport(ds, customersByCityReport)

            If Me.CrystalReportViewer1.InvokeRequired Then
                Dim d As New Reporte(AddressOf ConfigureCrystalReports)
                Me.Invoke(d, New Object() {op, Titulo})
            Else
                CrystalReportViewer1.ReportSource = customersByCityReport
            End If

            bnd = True

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReports_NewXmlListado2(ByVal op As String, ByVal Titulo As String)
        Try
            Dim cnn As New SqlConnection(MiConexion)
            customersByCityReport = New ReportDocument
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Status As String = Nothing


            If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
                StatusPen = "1"
                If Me.EjecutadasCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    Status = Status & "Pendientes,"
                Else
                    Status = "Pendientes"
                End If
            End If
            If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
                StatusEje = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.VisitaCheckBox.Checked = True Then
                    If Me.PendientesCheckBox.Checked = True Then
                        Status = Status & "Ejecutadas"
                    Else
                        Status = Status & "Ejecutadas,"
                    End If
                Else
                    Status = "Ejecutadas"
                End If
            End If
            If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
                StatusVis = "1"
                If Me.PendientesCheckBox.Checked = True Or Me.EjecutadasCheckBox.Checked = True Then
                    Status = Status & "Visita"
                Else
                    Status = "Visita"
                End If
            End If
            If Me.CheckBox6.CheckState = CheckState.Unchecked Then
                OpTecnico = 0
            Else
                OpTecnico = 1
                Clv_Tecnico_Rep = Me.ComboTec.SelectedValue.ToString
            End If
            If Me.NUMINILbl.Text.Length > 0 Then
                If IsNumeric(Me.NUMINILbl.Text) = True Then
                    If CLng(Me.NUMINILbl.Text) > 0 Then
                        Op1 = "1"
                    End If
                End If
            End If
            If Me.FECSOLINI.Text.Length > 0 Then

                Op2 = "1"
            End If
            If Me.FECEJEINI.Text.Length > 0 Then
                Op3 = "1"
            End If
            If Me.NOMTRABAJO.Text.Length > 0 Then
                Op4 = "1"
            End If
            If Me.NOMCOLONIA.Text.Length > 0 Then
                Op5 = "1"
            End If
            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            If Me.RadioButton1.Checked = True Then
                OpOrdenar = "0"
            ElseIf Me.RadioButton2.Checked = True Then
                OpOrdenar = "1"
            ElseIf Me.RadioButton4.Checked = True Then
                OpOrdenar = "2"
            End If

            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\ReporteOrdenesListadoNuevo_SA_2.rpt"

            '@Clv_OrdenIni bigint
            If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
            'customersByCityReport.SetParameterValue(9, CLng(Num1))
            ',@Clv_OrdenFin bigint
            If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
            'customersByCityReport.SetParameterValue(10, CLng(Num2))
            '',@Fec1Ini Datetime
            If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
            'customersByCityReport.SetParameterValue(11, Fec1Ini)
            ',@Fec1Fin Datetime,
            If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
            'customersByCityReport.SetParameterValue(12, Fec1Fin)
            '@Fec2Ini Datetime
            If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
            'customersByCityReport.SetParameterValue(13, Fec2Ini)
            ',@Fec2Fin Datetime
            If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
            ' customersByCityReport.SetParameterValue(14, Fec2Fin)
            ',@Clv_Trabajo int
            If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
            'customersByCityReport.SetParameterValue(15, nclv_trabajo)
            ',@Clv_Colonia int
            If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)


            Dim cmd As New SqlCommand("ReporteAreaTecnicaOrdSer_Prueba", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0


            '@Clv_TipSer int
            Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = 0
            cmd.Parameters.Add(parametro1)
            '@op1 smallint
            Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = Op1
            cmd.Parameters.Add(parametro2)
            '@op2 smallint
            Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Op2
            cmd.Parameters.Add(parametro3)
            '@op3 smallint
            Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Op3
            cmd.Parameters.Add(parametro4)
            '@op4 smallint
            Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Op4
            cmd.Parameters.Add(parametro5)
            '@op5 smallint
            Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Op5
            cmd.Parameters.Add(parametro6)
            '@StatusPen bit
            Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
            parametro7.Direction = ParameterDirection.Input
            If StatusPen = "1" Then
                parametro7.Value = True
            Else
                parametro7.Value = False
            End If
            cmd.Parameters.Add(parametro7)
            '@StatusEje bit
            Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
            parametro8.Direction = ParameterDirection.Input
            If StatusEje = "1" Then
                parametro8.Value = True
            Else
                parametro8.Value = False
            End If
            cmd.Parameters.Add(parametro8)
            '@StatusVis bit
            Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
            parametro9.Direction = ParameterDirection.Input
            If StatusVis = "1" Then
                parametro9.Value = True
            Else
                parametro9.Value = False
            End If
            cmd.Parameters.Add(parametro9)
            '@Clv_OrdenIni bigint
            Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
            parametro10.Direction = ParameterDirection.Input
            parametro10.Value = CLng(Num1)
            cmd.Parameters.Add(parametro10)
            '@Clv_OrdenFin bigint
            Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
            parametro11.Direction = ParameterDirection.Input
            parametro11.Value = CLng(Num2)
            cmd.Parameters.Add(parametro11)
            '@Fec1Ini Datetime
            Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro12.Direction = ParameterDirection.Input
            parametro12.Value = Fec1Ini
            cmd.Parameters.Add(parametro12)
            '@Fec1Fin Datetime
            Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
            parametro13.Direction = ParameterDirection.Input
            parametro13.Value = Fec1Fin
            cmd.Parameters.Add(parametro13)
            '@Fec2Ini Datetime
            Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro14.Direction = ParameterDirection.Input
            parametro14.Value = Fec2Ini
            cmd.Parameters.Add(parametro14)
            '@Fec2Fin Datetime
            Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
            parametro15.Direction = ParameterDirection.Input
            parametro15.Value = Fec2Fin
            cmd.Parameters.Add(parametro15)
            '@Clv_Trabajo int
            Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro16.Direction = ParameterDirection.Input
            parametro16.Value = nclv_trabajo
            cmd.Parameters.Add(parametro16)
            '@Clv_Colonia int
            Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
            parametro17.Direction = ParameterDirection.Input
            parametro17.Value = nClv_colonia
            cmd.Parameters.Add(parametro17)
            '@OpOrden int
            Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
            parametro18.Direction = ParameterDirection.Input
            parametro18.Value = OpOrdenar
            cmd.Parameters.Add(parametro18)

            '@OpTecnico int, 
            Dim parametro19 As New SqlParameter("@OpTecnico", SqlDbType.Int)
            parametro19.Direction = ParameterDirection.Input
            'If StatusPen = "1" Then
            parametro19.Value = OpTecnico

            cmd.Parameters.Add(parametro19)

            Dim parametro20 As New SqlParameter("@Clv_Tecnico_Rep", SqlDbType.Int)
            parametro20.Direction = ParameterDirection.Input

            parametro20.Value = Clv_Tecnico_Rep

            cmd.Parameters.Add(parametro20)

            'Dim parametro21 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            'parametro21.Direction = ParameterDirection.Input
            'parametro21.Value = LocClv_session
            'cmd.Parameters.Add(parametro21)

            Dim da As New SqlDataAdapter(cmd)



            Dim ds As New DataSet()
            da.Fill(ds)

            ds.Tables(0).TableName = "ReporteAreaTecnicaOrdSer_Prueba"
            Dim colString As DataColumn = New DataColumn("StatusPen")
            colString.DataType = System.Type.GetType("System.String")
            ds.Tables(0).Columns.Add(colString)
            If ds.Tables(0).Rows.Count > 0 Then

                If StatusPen = "1" Then

                    For Each dr As DataRow In ds.Tables(0).Rows
                        dr("StatusPen") = "1"
                    Next

                    'data1.Rows(0)("StatusPen") = "1"
                Else
                    For Each dr As DataRow In ds.Tables(0).Rows
                        dr("StatusPen") = "0"
                    Next
                    'data1.Rows(0)("StatusPen") = "0"
                End If
            End If

            'ds.Tables.Add(data1)
            customersByCityReport.Load(reportPath)
            mySelectFormula = "Listado de Ordenes de Servicio"
            'If op = 0 Then
            customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"





            SetDBReport(ds, customersByCityReport)

            If Me.CrystalReportViewer1.InvokeRequired Then
                Dim d As New Reporte(AddressOf ConfigureCrystalReports)
                Me.Invoke(d, New Object() {op, Titulo})
            Else
                CrystalReportViewer1.ReportSource = customersByCityReport
            End If

            bnd = True

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        PantallaProcesando.Close()
    End Sub

    Private Sub CONTecnicos()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        strSQL.Append("EXEC CONTecReporte ")

        'MsgBox(strSQL.ToString)

        Dim dataTable As New DataTable
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboTec.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox6.CheckedChanged
        If Me.CheckBox6.Checked = True Then
            Me.ComboTec.Enabled = False
        Else
            Me.ComboTec.Enabled = True
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        opreporte = 3
        If Me.CheckBox6.Checked = True Then
            OpTecnico = 0
        Else
            OpTecnico = 1
            Clv_Tecnico_Rep = Me.ComboTec.SelectedValue.ToString
        End If
        ConfigureCrystalReports_NewXmlListado2(2, "")
        'Me.BackgroundWorker1.RunWorkerAsync()
        'PantallaProcesando.Show()


    End Sub
End Class