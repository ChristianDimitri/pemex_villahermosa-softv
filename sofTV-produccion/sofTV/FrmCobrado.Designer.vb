﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCobrado
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbxCobrado = New System.Windows.Forms.CheckBox()
        Me.cbxNoCobrados = New System.Windows.Forms.CheckBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.CMBlblMensaje = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'cbxCobrado
        '
        Me.cbxCobrado.AutoSize = True
        Me.cbxCobrado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCobrado.Location = New System.Drawing.Point(73, 58)
        Me.cbxCobrado.Name = "cbxCobrado"
        Me.cbxCobrado.Size = New System.Drawing.Size(95, 20)
        Me.cbxCobrado.TabIndex = 0
        Me.cbxCobrado.Text = "Cobrados"
        Me.cbxCobrado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.cbxCobrado.UseVisualStyleBackColor = True
        '
        'cbxNoCobrados
        '
        Me.cbxNoCobrados.AutoSize = True
        Me.cbxNoCobrados.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxNoCobrados.Location = New System.Drawing.Point(73, 97)
        Me.cbxNoCobrados.Name = "cbxNoCobrados"
        Me.cbxNoCobrados.Size = New System.Drawing.Size(119, 20)
        Me.cbxNoCobrados.TabIndex = 1
        Me.cbxNoCobrados.Text = "No Cobrados"
        Me.cbxNoCobrados.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.cbxNoCobrados.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(35, 153)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(86, 32)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(158, 153)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(86, 32)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'CMBlblMensaje
        '
        Me.CMBlblMensaje.AutoSize = True
        Me.CMBlblMensaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblMensaje.Location = New System.Drawing.Point(60, 19)
        Me.CMBlblMensaje.Name = "CMBlblMensaje"
        Me.CMBlblMensaje.Size = New System.Drawing.Size(133, 16)
        Me.CMBlblMensaje.TabIndex = 4
        Me.CMBlblMensaje.Text = "Seleccione Status"
        Me.CMBlblMensaje.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'FrmCobrado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 208)
        Me.Controls.Add(Me.CMBlblMensaje)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.cbxNoCobrados)
        Me.Controls.Add(Me.cbxCobrado)
        Me.Name = "FrmCobrado"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione Status"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxCobrado As System.Windows.Forms.CheckBox
    Friend WithEvents cbxNoCobrados As System.Windows.Forms.CheckBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents CMBlblMensaje As System.Windows.Forms.Label
End Class
