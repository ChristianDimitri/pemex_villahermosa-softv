﻿Imports System.Data
Imports System.Data.SqlClient
Public Class FrmCambioDeNumero
    Dim RESPUESTA As Integer = 0
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
        eGloContrato = 0
        GLOCONTRATOSEL = 0
        GloClv_TipSer = 0
        GloOpcionCANUM = 0
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        GloOpcionCANUM = 999
        FrmSelCliente.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.TextBox1.Text) = True Then
            eGloContrato = CInt(Me.TextBox1.Text)

            validaSiTieneNumTelefonia(eGloContrato)
            If RESPUESTA = 1 Then
                FrmSelecNumTelNew.Show()
            Else
                MsgBox("El Contrato No Cuenta Con Numero Telefonico")
                Exit Sub
            End If


            'If tel_old = 0 Or tel_new = 0 Then
            '    MsgBox("No se le a asignado numero")
            '    Exit Sub
            'Else
            '    guardaBitacora(newcontrato, tel_old, tel_new, 1)
            'End If
        End If

    End Sub
    Private Sub guardaBitacoraCANUM(ByVal contrato As Long, ByVal tel_old As Integer, ByVal tel_nes As Integer, ByVal OP As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("guardaBitacoraCANUM", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@tel_old", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = tel_old
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@tel_new", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = tel_nes
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@OP", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = OP
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub FrmCambioDeNumero_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        Me.TextBox1.Text = GLOCONTRATOSEL

        If GloOpcion = 666 Then
            If tel_old = 0 Or tel_new = 0 Then
                MsgBox("No se le a asignado numero")
                GloOpcion = 0
                Exit Sub
            Else
                guardaBitacoraCANUM(newcontrato, tel_old, tel_new, 1)

                Me.Close()
                GloOpcion = 0
            End If
        End If
    End Sub

    Private Sub FrmCambioDeNumero_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub
    Private Sub validaSiTieneNumTelefonia(ByVal CONTRATO As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("validaSiTieneNumTelefonia", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@RESPUESTA", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)



        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            RESPUESTA = CInt(parametro2.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
   
End Class