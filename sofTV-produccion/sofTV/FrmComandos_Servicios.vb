﻿
Imports System.Data.SqlClient

Public Class FrmComandos_Servicios

    Dim ID_eliminando As Integer = 0
    Dim MTablas As String = "Comandos_Tel"
    Dim Mvalues As String = "Id, Comando"
    Dim MValores As String = ""
    Dim MCondicion As String = "Id in (1,3,6) "
    Dim MTipMov As Integer = 0
    Dim MBNDIDENTITY As Boolean = False
    Dim MClv_ID As Long = 0

    Public Sub llena_Grid(ByVal id_comando As Integer)
        Try
            'MiConexion = "Data Source=TeamEdgar-PC;Initial Catalog=Sahuayo ;Persist Security Info=True;User ID=sa;Password=06011975;Connect Timeout=0"
            Dim conn As New SqlConnection(MiConexion)
            conn.Open()
            Dim comando As New SqlClient.SqlCommand("UpRel_Comandos_ServiciosTel", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0
            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando
            '@Tablas varchar(150),@values varchar(max),@Valores varchar(max),@Condicion varchar(150),
            '@TipMov int,@BNDIDENTITY BIT,@CLV_ID BIGINT OUTPUT

            'Agrego el parámetro
            Adaptador.SelectCommand.Parameters.Add("@IdComando", SqlDbType.Int).Value = id_comando

            Dim Dataset As New DataSet
            'Creamos nuestros BidingSource
            Dim Bs As New BindingSource
            'Llenamos el Adaptador con el DataSet
            Adaptador.Fill(Dataset, "resultadoClasificacion")
            Bs.DataSource = Dataset.Tables("resultadoClasificacion")
            dgvResultadosClasificacion.DataSource = Bs
            


            conn.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Public Sub Guarda_Grid(ByVal id_comando As Integer, ByVal iD_TEL As Integer, ByVal BND As Boolean)
        'MiConexion = "Data Source=TeamEdgar-PC;Initial Catalog=Sahuayo ;Persist Security Info=True;User ID=sa;Password=06011975;Connect Timeout=0"
        Dim conn As New SqlConnection(MiConexion)
        Try

            Dim cmd As New SqlCommand("UpGuardarRel_Comandos_ServiciosTel", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim prm1 As New SqlParameter("@IdComando", SqlDbType.Int)
            prm1.Direction = ParameterDirection.Input
            prm1.Value = id_comando
            cmd.Parameters.Add(prm1)

            Dim prm2 As New SqlParameter("@ID_TEL", SqlDbType.Int)
            prm2.Direction = ParameterDirection.Input
            prm2.Value = iD_TEL
            cmd.Parameters.Add(prm2)

            Dim prm3 As New SqlParameter("@BND", SqlDbType.Bit)
            prm3.Direction = ParameterDirection.Input
            prm3.Value = BND
            cmd.Parameters.Add(prm3)
           
            conn.Open()
            Dim i As Integer = cmd.ExecuteNonQuery
            

            
            conn.Close()
            
        Catch ex As Exception

            If conn.State <> ConnectionState.Closed Then conn.Close()
            MsgBox(ex.Message)

        End Try
    End Sub


    Public Sub cargaReales(ByVal Tablas As String, ByVal values As String, ByVal Valores As String, ByVal Condicion As String, ByVal TipMov As Integer, ByVal BNDIDENTITY As Boolean)
        Try
            'MiConexion = "Data Source=TeamEdgar-PC;Initial Catalog=Sahuayo ;Persist Security Info=True;User ID=sa;Password=06011975;Connect Timeout=0"
            Dim conn As New SqlConnection(MiConexion)
            conn.Open()
            Dim comando As New SqlClient.SqlCommand("UP_PROCESA_TODO", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0
            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando
            '@Tablas varchar(150),@values varchar(max),@Valores varchar(max),@Condicion varchar(150),
            '@TipMov int,@BNDIDENTITY BIT,@CLV_ID BIGINT OUTPUT

            'Agrego el parámetro
            Adaptador.SelectCommand.Parameters.Add("@Tablas", SqlDbType.VarChar, 150).Value = Tablas
            Adaptador.SelectCommand.Parameters.Add("@values", SqlDbType.VarChar, 250).Value = values
            Adaptador.SelectCommand.Parameters.Add("@Valores", SqlDbType.VarChar, 250).Value = Valores
            Adaptador.SelectCommand.Parameters.Add("@Condicion", SqlDbType.VarChar, 250).Value = Condicion
            Adaptador.SelectCommand.Parameters.Add("@TipMov", SqlDbType.Int).Value = TipMov
            Adaptador.SelectCommand.Parameters.Add("@BNDIDENTITY", SqlDbType.Bit).Value = BNDIDENTITY
            Adaptador.SelectCommand.Parameters.Add("@CLV_ID", SqlDbType.BigInt).Value = 0


            Dim Dataset As New DataSet
            'Creamos nuestros BidingSource
            Dim Bs As New BindingSource
            'Llenamos el Adaptador con el DataSet
            Adaptador.Fill(Dataset, "resultadoClasificacion")
            Bs.DataSource = Dataset.Tables("resultadoClasificacion")
            'dgvResultadosClasificacion.DataSource = Bs
            Me.ComboBox1.DataSource = Bs
            Me.ComboBox1.DisplayMember = "Comando"
            Me.ComboBox2.DataSource = Bs
            Me.ComboBox2.DisplayMember = "Id"


            conn.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub Desplegar(ByVal Tablas As String, ByVal values As String, ByVal Valores As String, ByVal Condicion As String, ByVal TipMov As Integer, ByVal BNDIDENTITY As Boolean)
        'MiConexion = "Data Source=TeamEdgar-PC;Initial Catalog=Sahuayo ;Persist Security Info=True;User ID=sa;Password=06011975;Connect Timeout=0"
        Dim conn As New SqlConnection(MiConexion)
        Try

            Dim reader As SqlDataReader
            Dim cmd As New SqlCommand("UP_PROCESA_TODO", conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@Tablas", SqlDbType.VarChar, 150).Value = Tablas
            cmd.Parameters.Add("@values", SqlDbType.VarChar, 250).Value = values
            cmd.Parameters.Add("@Valores", SqlDbType.VarChar, 250).Value = Valores
            cmd.Parameters.Add("@Condicion", SqlDbType.VarChar, 250).Value = Condicion
            cmd.Parameters.Add("@TipMov", SqlDbType.Int).Value = TipMov
            cmd.Parameters.Add("@BNDIDENTITY", SqlDbType.Bit).Value = BNDIDENTITY
            cmd.Parameters.Add("@CLV_ID", SqlDbType.BigInt).Value = 0
            conn.Open()
            reader = cmd.ExecuteReader()

            Using reader
                While reader.Read
                    Me.ComboBox1.Items.Add(reader.GetValue(0))
                    Me.ComboBox2.Items.Add(reader.GetValue(1))
                    'Me.Clave.Text = reader.GetValue(1)
                    'Me.Descripcion.Text = reader.GetValue(2)
                End While
            End Using
            conn.Close()
        Catch ex As Exception
            If conn.State <> ConnectionState.Closed Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub FrmComandos_Servicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'MCondicion = ""
        MTipMov = 0
        Desplegar(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
    End Sub


    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedIndex >= 0 Then
            ComboBox2.SelectedIndex = ComboBox1.SelectedIndex
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        If ComboBox2.SelectedIndex >= 0 Then
            Me.ComboBox1.SelectedIndex = Me.ComboBox2.SelectedIndex
            llena_Grid(Me.ComboBox1.Text)
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If IsNumeric(Me.ComboBox1.Text) = True Then


            Dim i As Integer = 0

            For i = 0 To Me.dgvResultadosClasificacion.RowCount - 1
                Guarda_Grid(Me.ComboBox1.Text, Me.dgvResultadosClasificacion.Rows.Item(i).Cells.Item(0).Value, Me.dgvResultadosClasificacion.Rows.Item(i).Cells.Item(2).Value)
            Next
            llena_Grid(Me.ComboBox1.Text)
        Else
            MsgBox("Seleccione un Comando ", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub dgvResultadosClasificacion_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvResultadosClasificacion.CellContentClick

    End Sub
End Class