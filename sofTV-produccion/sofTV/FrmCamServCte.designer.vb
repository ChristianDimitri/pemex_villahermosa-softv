<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCamServCte
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim CONTRATOLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim CALLELabel As System.Windows.Forms.Label
        Dim COLONIALabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim CIUDADLabel As System.Windows.Forms.Label
        Dim ConceptoLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCamServCte))
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.DameServCteDataGridView = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DameServCteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric
        Me.ContratoTextBox = New System.Windows.Forms.TextBox
        Me.ContratoNetTextBox = New System.Windows.Forms.TextBox
        Me.TipSerTextBox = New System.Windows.Forms.TextBox
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox
        Me.Clv_ServicioTextBox = New System.Windows.Forms.TextBox
        Me.Clv_CableModemTextBox = New System.Windows.Forms.TextBox
        Me.MacCableModemTextBox = New System.Windows.Forms.TextBox
        Me.DameServPosiblesCteDataGridView = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DameServPosiblesCteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Clv_ServicioTextBox1 = New System.Windows.Forms.TextBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox
        Me.DameClientesActivosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CALLETextBox = New System.Windows.Forms.TextBox
        Me.COLONIATextBox = New System.Windows.Forms.TextBox
        Me.NUMEROTextBox = New System.Windows.Forms.TextBox
        Me.CIUDADTextBox = New System.Windows.Forms.TextBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.ConceptoComboBox = New System.Windows.Forms.ComboBox
        Me.MuestraTipServEricBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BindingNavigator1 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer
        Me.GroupBoxPosible = New System.Windows.Forms.GroupBox
        Me.DataGridViewPosible = New System.Windows.Forms.DataGridView
        Me.GroupBoxActual = New System.Windows.Forms.GroupBox
        Me.DataGridViewActual = New System.Windows.Forms.DataGridView
        Me.DameServCteTableAdapter = New sofTV.DataSetEricTableAdapters.DameServCteTableAdapter
        Me.DameServPosiblesCteTableAdapter = New sofTV.DataSetEricTableAdapters.DameServPosiblesCteTableAdapter
        Me.DameClientesActivosTableAdapter = New sofTV.DataSetEricTableAdapters.DameClientesActivosTableAdapter
        Me.MuestraTipServEricTableAdapter = New sofTV.DataSetEricTableAdapters.MuestraTipServEricTableAdapter
        CONTRATOLabel = New System.Windows.Forms.Label
        NOMBRELabel = New System.Windows.Forms.Label
        CALLELabel = New System.Windows.Forms.Label
        COLONIALabel = New System.Windows.Forms.Label
        NUMEROLabel = New System.Windows.Forms.Label
        CIUDADLabel = New System.Windows.Forms.Label
        ConceptoLabel = New System.Windows.Forms.Label
        CType(Me.DameServCteDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameServCteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameServPosiblesCteDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameServPosiblesCteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DameClientesActivosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator1.SuspendLayout()
        Me.ToolStripContainer1.ContentPanel.SuspendLayout()
        Me.ToolStripContainer1.TopToolStripPanel.SuspendLayout()
        Me.ToolStripContainer1.SuspendLayout()
        Me.GroupBoxPosible.SuspendLayout()
        CType(Me.DataGridViewPosible, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxActual.SuspendLayout()
        CType(Me.DataGridViewActual, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CONTRATOLabel
        '
        CONTRATOLabel.AutoSize = True
        CONTRATOLabel.Location = New System.Drawing.Point(27, 46)
        CONTRATOLabel.Name = "CONTRATOLabel"
        CONTRATOLabel.Size = New System.Drawing.Size(69, 15)
        CONTRATOLabel.TabIndex = 39
        CONTRATOLabel.Text = "Contrato :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Location = New System.Drawing.Point(30, 74)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(66, 15)
        NOMBRELabel.TabIndex = 40
        NOMBRELabel.Text = "Nombre :"
        '
        'CALLELabel
        '
        CALLELabel.AutoSize = True
        CALLELabel.Location = New System.Drawing.Point(49, 103)
        CALLELabel.Name = "CALLELabel"
        CALLELabel.Size = New System.Drawing.Size(48, 15)
        CALLELabel.TabIndex = 41
        CALLELabel.Text = "Calle :"
        '
        'COLONIALabel
        '
        COLONIALabel.AutoSize = True
        COLONIALabel.Location = New System.Drawing.Point(32, 131)
        COLONIALabel.Name = "COLONIALabel"
        COLONIALabel.Size = New System.Drawing.Size(64, 15)
        COLONIALabel.TabIndex = 42
        COLONIALabel.Text = "Colonia :"
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Location = New System.Drawing.Point(398, 99)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(23, 15)
        NUMEROLabel.TabIndex = 43
        NUMEROLabel.Text = "# :"
        '
        'CIUDADLabel
        '
        CIUDADLabel.AutoSize = True
        CIUDADLabel.Location = New System.Drawing.Point(36, 164)
        CIUDADLabel.Name = "CIUDADLabel"
        CIUDADLabel.Size = New System.Drawing.Size(60, 15)
        CIUDADLabel.TabIndex = 44
        CIUDADLabel.Text = "Ciudad :"
        '
        'ConceptoLabel
        '
        ConceptoLabel.AutoSize = True
        ConceptoLabel.Location = New System.Drawing.Point(494, 27)
        ConceptoLabel.Name = "ConceptoLabel"
        ConceptoLabel.Size = New System.Drawing.Size(118, 15)
        ConceptoLabel.TabIndex = 45
        ConceptoLabel.Text = "Tipo de Servicio :"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Location = New System.Drawing.Point(107, 39)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 21)
        Me.TextBox1.TabIndex = 2
        '
        'DameServCteDataGridView
        '
        Me.DameServCteDataGridView.AllowUserToAddRows = False
        Me.DameServCteDataGridView.AllowUserToDeleteRows = False
        Me.DameServCteDataGridView.AutoGenerateColumns = False
        Me.DameServCteDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7})
        Me.DameServCteDataGridView.DataSource = Me.DameServCteBindingSource
        Me.DameServCteDataGridView.Location = New System.Drawing.Point(6, 20)
        Me.DameServCteDataGridView.Name = "DameServCteDataGridView"
        Me.DameServCteDataGridView.ReadOnly = True
        Me.DameServCteDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DameServCteDataGridView.Size = New System.Drawing.Size(483, 257)
        Me.DameServCteDataGridView.TabIndex = 25
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Contrato"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Contrato"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "ContratoNet"
        Me.DataGridViewTextBoxColumn2.HeaderText = "ContratoNet"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "TipSer"
        Me.DataGridViewTextBoxColumn3.HeaderText = "TipSer"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Descripcion"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Servicio"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Clv_CableModem"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Clv_CableModem"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "MacCableModem"
        Me.DataGridViewTextBoxColumn7.HeaderText = "MacCableModem"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 200
        '
        'DameServCteBindingSource
        '
        Me.DameServCteBindingSource.DataMember = "DameServCte"
        Me.DameServCteBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameServCteBindingSource, "Contrato", True))
        Me.ContratoTextBox.Location = New System.Drawing.Point(334, 21)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.ReadOnly = True
        Me.ContratoTextBox.Size = New System.Drawing.Size(10, 21)
        Me.ContratoTextBox.TabIndex = 26
        Me.ContratoTextBox.TabStop = False
        '
        'ContratoNetTextBox
        '
        Me.ContratoNetTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameServCteBindingSource, "ContratoNet", True))
        Me.ContratoNetTextBox.Location = New System.Drawing.Point(350, 21)
        Me.ContratoNetTextBox.Name = "ContratoNetTextBox"
        Me.ContratoNetTextBox.ReadOnly = True
        Me.ContratoNetTextBox.Size = New System.Drawing.Size(10, 21)
        Me.ContratoNetTextBox.TabIndex = 27
        Me.ContratoNetTextBox.TabStop = False
        '
        'TipSerTextBox
        '
        Me.TipSerTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameServCteBindingSource, "TipSer", True))
        Me.TipSerTextBox.Location = New System.Drawing.Point(366, 21)
        Me.TipSerTextBox.Name = "TipSerTextBox"
        Me.TipSerTextBox.ReadOnly = True
        Me.TipSerTextBox.Size = New System.Drawing.Size(10, 21)
        Me.TipSerTextBox.TabIndex = 28
        Me.TipSerTextBox.TabStop = False
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameServCteBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(382, 21)
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.ReadOnly = True
        Me.DescripcionTextBox.Size = New System.Drawing.Size(10, 21)
        Me.DescripcionTextBox.TabIndex = 29
        Me.DescripcionTextBox.TabStop = False
        '
        'Clv_ServicioTextBox
        '
        Me.Clv_ServicioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameServCteBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox.Location = New System.Drawing.Point(398, 21)
        Me.Clv_ServicioTextBox.Name = "Clv_ServicioTextBox"
        Me.Clv_ServicioTextBox.ReadOnly = True
        Me.Clv_ServicioTextBox.Size = New System.Drawing.Size(10, 21)
        Me.Clv_ServicioTextBox.TabIndex = 30
        Me.Clv_ServicioTextBox.TabStop = False
        '
        'Clv_CableModemTextBox
        '
        Me.Clv_CableModemTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameServCteBindingSource, "Clv_CableModem", True))
        Me.Clv_CableModemTextBox.Location = New System.Drawing.Point(414, 21)
        Me.Clv_CableModemTextBox.Name = "Clv_CableModemTextBox"
        Me.Clv_CableModemTextBox.ReadOnly = True
        Me.Clv_CableModemTextBox.Size = New System.Drawing.Size(10, 21)
        Me.Clv_CableModemTextBox.TabIndex = 31
        Me.Clv_CableModemTextBox.TabStop = False
        '
        'MacCableModemTextBox
        '
        Me.MacCableModemTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameServCteBindingSource, "MacCableModem", True))
        Me.MacCableModemTextBox.Location = New System.Drawing.Point(430, 21)
        Me.MacCableModemTextBox.Name = "MacCableModemTextBox"
        Me.MacCableModemTextBox.ReadOnly = True
        Me.MacCableModemTextBox.Size = New System.Drawing.Size(10, 21)
        Me.MacCableModemTextBox.TabIndex = 32
        Me.MacCableModemTextBox.TabStop = False
        '
        'DameServPosiblesCteDataGridView
        '
        Me.DameServPosiblesCteDataGridView.AllowUserToAddRows = False
        Me.DameServPosiblesCteDataGridView.AllowUserToDeleteRows = False
        Me.DameServPosiblesCteDataGridView.AutoGenerateColumns = False
        Me.DameServPosiblesCteDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9})
        Me.DameServPosiblesCteDataGridView.DataSource = Me.DameServPosiblesCteBindingSource
        Me.DameServPosiblesCteDataGridView.Location = New System.Drawing.Point(6, 20)
        Me.DameServPosiblesCteDataGridView.Name = "DameServPosiblesCteDataGridView"
        Me.DameServPosiblesCteDataGridView.ReadOnly = True
        Me.DameServPosiblesCteDataGridView.Size = New System.Drawing.Size(357, 247)
        Me.DameServPosiblesCteDataGridView.TabIndex = 33
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Clv_Servicio"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Descripcion"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Servicio"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 200
        '
        'DameServPosiblesCteBindingSource
        '
        Me.DameServPosiblesCteBindingSource.DataMember = "DameServPosiblesCte"
        Me.DameServPosiblesCteBindingSource.DataSource = Me.DataSetEric
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.DameServCteDataGridView)
        Me.GroupBox1.Controls.Add(Me.Clv_ServicioTextBox)
        Me.GroupBox1.Controls.Add(Me.MacCableModemTextBox)
        Me.GroupBox1.Controls.Add(Me.TipSerTextBox)
        Me.GroupBox1.Controls.Add(Me.Clv_ServicioTextBox1)
        Me.GroupBox1.Controls.Add(Me.ContratoTextBox)
        Me.GroupBox1.Controls.Add(Me.Clv_CableModemTextBox)
        Me.GroupBox1.Controls.Add(Me.DescripcionTextBox)
        Me.GroupBox1.Controls.Add(Me.ContratoNetTextBox)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(26, 337)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(495, 283)
        Me.GroupBox1.TabIndex = 34
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Servicios Actuales del Cliente"
        '
        'Clv_ServicioTextBox1
        '
        Me.Clv_ServicioTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameServPosiblesCteBindingSource, "Clv_Servicio", True))
        Me.Clv_ServicioTextBox1.Location = New System.Drawing.Point(446, 21)
        Me.Clv_ServicioTextBox1.Name = "Clv_ServicioTextBox1"
        Me.Clv_ServicioTextBox1.ReadOnly = True
        Me.Clv_ServicioTextBox1.Size = New System.Drawing.Size(10, 21)
        Me.Clv_ServicioTextBox1.TabIndex = 37
        Me.Clv_ServicioTextBox1.TabStop = False
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(213, 38)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(44, 23)
        Me.Button3.TabIndex = 38
        Me.Button3.Text = "..."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.DameServPosiblesCteDataGridView)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(533, 337)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(369, 283)
        Me.GroupBox2.TabIndex = 35
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Posibles Cambios de Servicio"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(868, 677)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 38
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(107, 68)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.ReadOnly = True
        Me.NOMBRETextBox.Size = New System.Drawing.Size(284, 22)
        Me.NOMBRETextBox.TabIndex = 41
        Me.NOMBRETextBox.TabStop = False
        '
        'DameClientesActivosBindingSource
        '
        Me.DameClientesActivosBindingSource.DataMember = "DameClientesActivos"
        Me.DameClientesActivosBindingSource.DataSource = Me.DataSetEric
        '
        'CALLETextBox
        '
        Me.CALLETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CALLETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CALLE", True))
        Me.CALLETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLETextBox.Location = New System.Drawing.Point(107, 97)
        Me.CALLETextBox.Name = "CALLETextBox"
        Me.CALLETextBox.ReadOnly = True
        Me.CALLETextBox.Size = New System.Drawing.Size(284, 22)
        Me.CALLETextBox.TabIndex = 42
        Me.CALLETextBox.TabStop = False
        '
        'COLONIATextBox
        '
        Me.COLONIATextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.COLONIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "COLONIA", True))
        Me.COLONIATextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIATextBox.Location = New System.Drawing.Point(107, 125)
        Me.COLONIATextBox.Name = "COLONIATextBox"
        Me.COLONIATextBox.ReadOnly = True
        Me.COLONIATextBox.Size = New System.Drawing.Size(284, 22)
        Me.COLONIATextBox.TabIndex = 43
        Me.COLONIATextBox.TabStop = False
        '
        'NUMEROTextBox
        '
        Me.NUMEROTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NUMEROTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "NUMERO", True))
        Me.NUMEROTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROTextBox.Location = New System.Drawing.Point(428, 97)
        Me.NUMEROTextBox.Name = "NUMEROTextBox"
        Me.NUMEROTextBox.ReadOnly = True
        Me.NUMEROTextBox.Size = New System.Drawing.Size(172, 22)
        Me.NUMEROTextBox.TabIndex = 44
        Me.NUMEROTextBox.TabStop = False
        '
        'CIUDADTextBox
        '
        Me.CIUDADTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CIUDADTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameClientesActivosBindingSource, "CIUDAD", True))
        Me.CIUDADTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADTextBox.Location = New System.Drawing.Point(107, 158)
        Me.CIUDADTextBox.Name = "CIUDADTextBox"
        Me.CIUDADTextBox.ReadOnly = True
        Me.CIUDADTextBox.Size = New System.Drawing.Size(284, 22)
        Me.CIUDADTextBox.TabIndex = 45
        Me.CIUDADTextBox.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(ConceptoLabel)
        Me.GroupBox3.Controls.Add(Me.ConceptoComboBox)
        Me.GroupBox3.Controls.Add(CONTRATOLabel)
        Me.GroupBox3.Controls.Add(Me.Button3)
        Me.GroupBox3.Controls.Add(CIUDADLabel)
        Me.GroupBox3.Controls.Add(Me.CIUDADTextBox)
        Me.GroupBox3.Controls.Add(NUMEROLabel)
        Me.GroupBox3.Controls.Add(Me.NOMBRETextBox)
        Me.GroupBox3.Controls.Add(Me.NUMEROTextBox)
        Me.GroupBox3.Controls.Add(NOMBRELabel)
        Me.GroupBox3.Controls.Add(Me.TextBox1)
        Me.GroupBox3.Controls.Add(Me.CALLETextBox)
        Me.GroupBox3.Controls.Add(COLONIALabel)
        Me.GroupBox3.Controls.Add(CALLELabel)
        Me.GroupBox3.Controls.Add(Me.COLONIATextBox)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(118, 84)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(784, 200)
        Me.GroupBox3.TabIndex = 46
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Datos del Cliente"
        '
        'ConceptoComboBox
        '
        Me.ConceptoComboBox.DataSource = Me.MuestraTipServEricBindingSource
        Me.ConceptoComboBox.DisplayMember = "Concepto"
        Me.ConceptoComboBox.FormattingEnabled = True
        Me.ConceptoComboBox.Location = New System.Drawing.Point(497, 46)
        Me.ConceptoComboBox.Name = "ConceptoComboBox"
        Me.ConceptoComboBox.Size = New System.Drawing.Size(276, 23)
        Me.ConceptoComboBox.TabIndex = 46
        Me.ConceptoComboBox.ValueMember = "Clv_TipSer"
        '
        'MuestraTipServEricBindingSource
        '
        Me.MuestraTipServEricBindingSource.DataMember = "MuestraTipServEric"
        Me.MuestraTipServEricBindingSource.DataSource = Me.DataSetEric
        '
        'BindingNavigator1
        '
        Me.BindingNavigator1.AddNewItem = Nothing
        Me.BindingNavigator1.CountItem = Nothing
        Me.BindingNavigator1.DeleteItem = Nothing
        Me.BindingNavigator1.Dock = System.Windows.Forms.DockStyle.None
        Me.BindingNavigator1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton2})
        Me.BindingNavigator1.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator1.MoveFirstItem = Nothing
        Me.BindingNavigator1.MoveLastItem = Nothing
        Me.BindingNavigator1.MoveNextItem = Nothing
        Me.BindingNavigator1.MovePreviousItem = Nothing
        Me.BindingNavigator1.Name = "BindingNavigator1"
        Me.BindingNavigator1.PositionItem = Nothing
        Me.BindingNavigator1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.BindingNavigator1.Size = New System.Drawing.Size(133, 25)
        Me.BindingNavigator1.TabIndex = 47
        Me.BindingNavigator1.TabStop = True
        Me.BindingNavigator1.Text = "BindingNavigator1"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToolStripButton2.Size = New System.Drawing.Size(123, 22)
        Me.ToolStripButton2.Text = "&HACER CAMBIO"
        '
        'ToolStripContainer1
        '
        '
        'ToolStripContainer1.ContentPanel
        '
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.GroupBoxPosible)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.GroupBox2)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.GroupBoxActual)
        Me.ToolStripContainer1.ContentPanel.Controls.Add(Me.GroupBox1)
        Me.ToolStripContainer1.ContentPanel.Size = New System.Drawing.Size(1004, 685)
        Me.ToolStripContainer1.Location = New System.Drawing.Point(8, 12)
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        Me.ToolStripContainer1.Size = New System.Drawing.Size(1004, 710)
        Me.ToolStripContainer1.TabIndex = 48
        Me.ToolStripContainer1.Text = "ToolStripContainer1"
        '
        'ToolStripContainer1.TopToolStripPanel
        '
        Me.ToolStripContainer1.TopToolStripPanel.Controls.Add(Me.BindingNavigator1)
        Me.ToolStripContainer1.TopToolStripPanel.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        '
        'GroupBoxPosible
        '
        Me.GroupBoxPosible.Controls.Add(Me.DataGridViewPosible)
        Me.GroupBoxPosible.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBoxPosible.Location = New System.Drawing.Point(533, 253)
        Me.GroupBoxPosible.Name = "GroupBoxPosible"
        Me.GroupBoxPosible.Size = New System.Drawing.Size(369, 283)
        Me.GroupBoxPosible.TabIndex = 38
        Me.GroupBoxPosible.TabStop = False
        Me.GroupBoxPosible.Text = "Posibles Cambios de Servicio"
        Me.GroupBoxPosible.Visible = False
        '
        'DataGridViewPosible
        '
        Me.DataGridViewPosible.AllowUserToAddRows = False
        Me.DataGridViewPosible.AllowUserToDeleteRows = False
        Me.DataGridViewPosible.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewPosible.Location = New System.Drawing.Point(21, 37)
        Me.DataGridViewPosible.Name = "DataGridViewPosible"
        Me.DataGridViewPosible.ReadOnly = True
        Me.DataGridViewPosible.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewPosible.Size = New System.Drawing.Size(332, 230)
        Me.DataGridViewPosible.TabIndex = 38
        '
        'GroupBoxActual
        '
        Me.GroupBoxActual.Controls.Add(Me.DataGridViewActual)
        Me.GroupBoxActual.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBoxActual.Location = New System.Drawing.Point(26, 253)
        Me.GroupBoxActual.Name = "GroupBoxActual"
        Me.GroupBoxActual.Size = New System.Drawing.Size(495, 283)
        Me.GroupBoxActual.TabIndex = 37
        Me.GroupBoxActual.TabStop = False
        Me.GroupBoxActual.Text = "Servicios Actuales del Cliente"
        Me.GroupBoxActual.Visible = False
        '
        'DataGridViewActual
        '
        Me.DataGridViewActual.AllowUserToAddRows = False
        Me.DataGridViewActual.AllowUserToDeleteRows = False
        Me.DataGridViewActual.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewActual.Location = New System.Drawing.Point(19, 36)
        Me.DataGridViewActual.Name = "DataGridViewActual"
        Me.DataGridViewActual.ReadOnly = True
        Me.DataGridViewActual.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders
        Me.DataGridViewActual.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewActual.Size = New System.Drawing.Size(450, 229)
        Me.DataGridViewActual.TabIndex = 37
        '
        'DameServCteTableAdapter
        '
        Me.DameServCteTableAdapter.ClearBeforeFill = True
        '
        'DameServPosiblesCteTableAdapter
        '
        Me.DameServPosiblesCteTableAdapter.ClearBeforeFill = True
        '
        'DameClientesActivosTableAdapter
        '
        Me.DameClientesActivosTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipServEricTableAdapter
        '
        Me.MuestraTipServEricTableAdapter.ClearBeforeFill = True
        '
        'FrmCamServCte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 734)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.ToolStripContainer1)
        Me.Name = "FrmCamServCte"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de Servicios"
        CType(Me.DameServCteDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameServCteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameServPosiblesCteDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameServPosiblesCteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.DameClientesActivosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.MuestraTipServEricBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator1.ResumeLayout(False)
        Me.BindingNavigator1.PerformLayout()
        Me.ToolStripContainer1.ContentPanel.ResumeLayout(False)
        Me.ToolStripContainer1.TopToolStripPanel.ResumeLayout(False)
        Me.ToolStripContainer1.TopToolStripPanel.PerformLayout()
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        Me.GroupBoxPosible.ResumeLayout(False)
        CType(Me.DataGridViewPosible, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxActual.ResumeLayout(False)
        CType(Me.DataGridViewActual, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents DameServCteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameServCteTableAdapter As sofTV.DataSetEricTableAdapters.DameServCteTableAdapter
    Friend WithEvents DameServCteDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratoNetTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TipSerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ServicioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_CableModemTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MacCableModemTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DameServPosiblesCteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameServPosiblesCteTableAdapter As sofTV.DataSetEricTableAdapters.DameServPosiblesCteTableAdapter
    Friend WithEvents DameServPosiblesCteDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Clv_ServicioTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DameClientesActivosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClientesActivosTableAdapter As sofTV.DataSetEricTableAdapters.DameClientesActivosTableAdapter
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CALLETextBox As System.Windows.Forms.TextBox
    Friend WithEvents COLONIATextBox As System.Windows.Forms.TextBox
    Friend WithEvents NUMEROTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CIUDADTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents BindingNavigator1 As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents MuestraTipServEricBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipServEricTableAdapter As sofTV.DataSetEricTableAdapters.MuestraTipServEricTableAdapter
    Friend WithEvents ConceptoComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBoxPosible As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridViewPosible As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBoxActual As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridViewActual As System.Windows.Forms.DataGridView
End Class
