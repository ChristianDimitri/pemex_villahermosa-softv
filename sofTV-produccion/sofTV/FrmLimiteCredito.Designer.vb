﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLimiteCredito
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLimiteCredito))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbLimiteCredito = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbPagosPuntuales = New System.Windows.Forms.TextBox()
        Me.BindingNavigator4 = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.GuardarToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.tbSalir = New System.Windows.Forms.Button()
        Me.tbCreditoFijo = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.rbCreditoLimitado = New System.Windows.Forms.RadioButton()
        Me.rbCreditoFijo = New System.Windows.Forms.RadioButton()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.panelCreditoLimitado = New System.Windows.Forms.Panel()
        Me.panelCreditoFijo = New System.Windows.Forms.Panel()
        CType(Me.BindingNavigator4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BindingNavigator4.SuspendLayout()
        Me.panelCreditoLimitado.SuspendLayout()
        Me.panelCreditoFijo.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(1, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(129, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Límite de Crédito : "
        '
        'tbLimiteCredito
        '
        Me.tbLimiteCredito.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLimiteCredito.Location = New System.Drawing.Point(130, 45)
        Me.tbLimiteCredito.Name = "tbLimiteCredito"
        Me.tbLimiteCredito.ReadOnly = True
        Me.tbLimiteCredito.Size = New System.Drawing.Size(185, 21)
        Me.tbLimiteCredito.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(127, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Pagos Puntuales : "
        '
        'tbPagosPuntuales
        '
        Me.tbPagosPuntuales.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPagosPuntuales.Location = New System.Drawing.Point(130, 18)
        Me.tbPagosPuntuales.Name = "tbPagosPuntuales"
        Me.tbPagosPuntuales.ReadOnly = True
        Me.tbPagosPuntuales.Size = New System.Drawing.Size(185, 21)
        Me.tbPagosPuntuales.TabIndex = 4
        '
        'BindingNavigator4
        '
        Me.BindingNavigator4.AddNewItem = Nothing
        Me.BindingNavigator4.CountItem = Nothing
        Me.BindingNavigator4.DeleteItem = Nothing
        Me.BindingNavigator4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigator4.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GuardarToolStripButton3})
        Me.BindingNavigator4.Location = New System.Drawing.Point(0, 0)
        Me.BindingNavigator4.MoveFirstItem = Nothing
        Me.BindingNavigator4.MoveLastItem = Nothing
        Me.BindingNavigator4.MoveNextItem = Nothing
        Me.BindingNavigator4.MovePreviousItem = Nothing
        Me.BindingNavigator4.Name = "BindingNavigator4"
        Me.BindingNavigator4.PositionItem = Nothing
        Me.BindingNavigator4.Size = New System.Drawing.Size(732, 25)
        Me.BindingNavigator4.TabIndex = 49
        Me.BindingNavigator4.Text = "BindingNavigator4"
        '
        'GuardarToolStripButton3
        '
        Me.GuardarToolStripButton3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.GuardarToolStripButton3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GuardarToolStripButton3.Image = CType(resources.GetObject("GuardarToolStripButton3.Image"), System.Drawing.Image)
        Me.GuardarToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.GuardarToolStripButton3.Name = "GuardarToolStripButton3"
        Me.GuardarToolStripButton3.Size = New System.Drawing.Size(80, 22)
        Me.GuardarToolStripButton3.Text = "&Guardar"
        '
        'tbSalir
        '
        Me.tbSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSalir.Location = New System.Drawing.Point(645, 171)
        Me.tbSalir.Name = "tbSalir"
        Me.tbSalir.Size = New System.Drawing.Size(75, 23)
        Me.tbSalir.TabIndex = 50
        Me.tbSalir.Text = "&Salir"
        Me.tbSalir.UseVisualStyleBackColor = True
        '
        'tbCreditoFijo
        '
        Me.tbCreditoFijo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCreditoFijo.Location = New System.Drawing.Point(98, 18)
        Me.tbCreditoFijo.Name = "tbCreditoFijo"
        Me.tbCreditoFijo.Size = New System.Drawing.Size(211, 21)
        Me.tbCreditoFijo.TabIndex = 52
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 15)
        Me.Label3.TabIndex = 51
        Me.Label3.Text = "Crédito Fijo :"
        '
        'rbCreditoLimitado
        '
        Me.rbCreditoLimitado.AutoSize = True
        Me.rbCreditoLimitado.Checked = True
        Me.rbCreditoLimitado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbCreditoLimitado.Location = New System.Drawing.Point(32, 36)
        Me.rbCreditoLimitado.Name = "rbCreditoLimitado"
        Me.rbCreditoLimitado.Size = New System.Drawing.Size(131, 19)
        Me.rbCreditoLimitado.TabIndex = 53
        Me.rbCreditoLimitado.TabStop = True
        Me.rbCreditoLimitado.Text = "Crédito Limitado"
        Me.rbCreditoLimitado.UseVisualStyleBackColor = True
        '
        'rbCreditoFijo
        '
        Me.rbCreditoFijo.AutoSize = True
        Me.rbCreditoFijo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbCreditoFijo.Location = New System.Drawing.Point(387, 36)
        Me.rbCreditoFijo.Name = "rbCreditoFijo"
        Me.rbCreditoFijo.Size = New System.Drawing.Size(99, 19)
        Me.rbCreditoFijo.TabIndex = 54
        Me.rbCreditoFijo.Text = "Crédito Fijo"
        Me.rbCreditoFijo.UseVisualStyleBackColor = True
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(732, 206)
        Me.ShapeContainer1.TabIndex = 55
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 370
        Me.LineShape1.X2 = 370
        Me.LineShape1.Y1 = 67
        Me.LineShape1.Y2 = 164
        '
        'panelCreditoLimitado
        '
        Me.panelCreditoLimitado.Controls.Add(Me.Label2)
        Me.panelCreditoLimitado.Controls.Add(Me.Label1)
        Me.panelCreditoLimitado.Controls.Add(Me.tbLimiteCredito)
        Me.panelCreditoLimitado.Controls.Add(Me.tbPagosPuntuales)
        Me.panelCreditoLimitado.Location = New System.Drawing.Point(28, 66)
        Me.panelCreditoLimitado.Name = "panelCreditoLimitado"
        Me.panelCreditoLimitado.Size = New System.Drawing.Size(325, 100)
        Me.panelCreditoLimitado.TabIndex = 56
        '
        'panelCreditoFijo
        '
        Me.panelCreditoFijo.Controls.Add(Me.Label3)
        Me.panelCreditoFijo.Controls.Add(Me.tbCreditoFijo)
        Me.panelCreditoFijo.Location = New System.Drawing.Point(387, 65)
        Me.panelCreditoFijo.Name = "panelCreditoFijo"
        Me.panelCreditoFijo.Size = New System.Drawing.Size(319, 100)
        Me.panelCreditoFijo.TabIndex = 57
        '
        'FrmLimiteCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(732, 206)
        Me.Controls.Add(Me.panelCreditoFijo)
        Me.Controls.Add(Me.panelCreditoLimitado)
        Me.Controls.Add(Me.rbCreditoFijo)
        Me.Controls.Add(Me.rbCreditoLimitado)
        Me.Controls.Add(Me.tbSalir)
        Me.Controls.Add(Me.BindingNavigator4)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.Name = "FrmLimiteCredito"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Límite de Credito"
        CType(Me.BindingNavigator4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BindingNavigator4.ResumeLayout(False)
        Me.BindingNavigator4.PerformLayout()
        Me.panelCreditoLimitado.ResumeLayout(False)
        Me.panelCreditoLimitado.PerformLayout()
        Me.panelCreditoFijo.ResumeLayout(False)
        Me.panelCreditoFijo.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbLimiteCredito As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbPagosPuntuales As System.Windows.Forms.TextBox
    Friend WithEvents BindingNavigator4 As System.Windows.Forms.BindingNavigator
    Friend WithEvents GuardarToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents tbSalir As System.Windows.Forms.Button
    Friend WithEvents tbCreditoFijo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents rbCreditoLimitado As System.Windows.Forms.RadioButton
    Friend WithEvents rbCreditoFijo As System.Windows.Forms.RadioButton
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents panelCreditoLimitado As System.Windows.Forms.Panel
    Friend WithEvents panelCreditoFijo As System.Windows.Forms.Panel
End Class
