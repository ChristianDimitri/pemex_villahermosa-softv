Public Class FrmNotasdeCredito

    Private Sub Consulta_NotaCreditoBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consulta_NotaCreditoBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.Consulta_NotaCreditoBindingSource.EndEdit()
        Me.Consulta_NotaCreditoTableAdapter.Update(Me.DataSetLidia.Consulta_NotaCredito)

    End Sub

    Private Sub busca()
        Try
            Me.Consulta_NotaCreditoTableAdapter.Fill(Me.DataSetLidia.Consulta_NotaCredito, gloClvNota)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmNotasdeCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetarnoldo.Dame_fecha_hora_serv' Puede moverla o quitarla seg�n sea necesario.
        Me.Dame_fecha_hora_servTableAdapter.Fill(Me.DataSetarnoldo.Dame_fecha_hora_serv)
        If opcion = "N" Then
            Me.Consulta_NotaCreditoBindingSource.AddNew()
            Me.Panel1.Enabled = True
        ElseIf opcion = "M" Or opcion = "C" Then
            busca()
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Me.Consulta_NotaCreditoBindingSource.CancelEdit()
        Me.Close()
    End Sub
End Class