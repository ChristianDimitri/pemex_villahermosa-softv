Imports System.Data.SqlClient
Public Class FrmAgregaCategoriaTipoLlamadas
    Dim Opcion As String = ""
    Dim id_clasificacion As Integer = 0
    Private Sub FrmAgregaCategoriaTipoLlamadas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        cargaClasificaciones()
        bnd_llamadas_tel = True
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.gboxNuevaClasificacio.Enabled = False
        Me.txtNombreClasificacion.Text = ""
        Me.txtDescripcionClasificacion.Text = ""
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If Opcion = "N" Then
            If Me.validaCampos() Then
                If agrega_clasificacion_llamadas() Then
                    MsgBox("La Nueva Clasificacion ha sido agregada con �xito: " & Me.txtNombreClasificacion.Text, MsgBoxStyle.Information)
                    cargaClasificaciones()
                Else
                    MsgBox("No se pudo agregar la nueva Clasificaci�n, es probable que ya exista una Clasificaci�n con las �smas especificaciones", MsgBoxStyle.Exclamation)
                End If
            Else
                MsgBox("Debes especificar un Nombre V�lido para la nueva Clasificaci�n", MsgBoxStyle.Exclamation)
            End If
        ElseIf Opcion = "M" Then
            If Me.validaCampos() Then
                If modifica_clasificacion_llamadas() Then
                    MsgBox("La Clasificacion ha sido modificada con �xito: " & Me.txtNombreClasificacion.Text, MsgBoxStyle.Information)
                    cargaClasificaciones()
                Else
                    MsgBox("No se pudo modificar la Clasificaci�n.", MsgBoxStyle.Exclamation)
                End If
            Else
                MsgBox("Debes especificar un Nombre V�lido para la nueva Clasificaci�n", MsgBoxStyle.Exclamation)
            End If
        End If
    End Sub
    Private Function agrega_clasificacion_llamadas() As Boolean

        Dim valido As Integer = 0
        Dim regresar As Boolean = False

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "sp_agrega_clasificacion_tipo_llamadas"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 8

                Dim prmNombre As New SqlParameter("@nombre_clasificacion", SqlDbType.VarChar, 250)
                prmNombre.Value = Me.txtNombreClasificacion.Text
                .Parameters.Add(prmNombre)

                Dim prmDescripcion As New SqlParameter("@descripcion", SqlDbType.VarChar, 250)
                If Me.txtDescripcionClasificacion.Text = "" Then
                    Me.txtDescripcionClasificacion.Text = ""
                End If
                prmDescripcion.Value = Me.txtDescripcionClasificacion.Text
                .Parameters.Add(prmDescripcion)

                Dim prmValido As New SqlParameter("@valido", SqlDbType.Int, 1)
                prmValido.Direction = ParameterDirection.Output
                prmValido.Value = 0
                .Parameters.Add(prmValido)

                Dim i As Integer = .ExecuteNonQuery()

                valido = prmValido.Value
                If valido = 1 Then
                    Return True
                Else
                    Return False
                End If
            End With
            CON80.Close()
        Catch ex As Exception

        End Try
    End Function
    Private Function validaCampos() As Boolean
        If Me.txtNombreClasificacion.Text = "" Then
            Me.txtNombreClasificacion.BackColor = Color.Red
            Return False
        Else
            Return True
        End If
    End Function
    Private Sub txtNombreClasificacion_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNombreClasificacion.TextChanged
        Me.txtNombreClasificacion.BackColor = Color.White
        If Me.txtNombreClasificacion.Text = "" Then
            Me.btnGuardar.Enabled = False
        Else
            Me.btnGuardar.Enabled = True
        End If
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Opcion = "N"
        Me.btnGuardar.Text = "Guardar la Nueva Clasificaci�n"
        Me.gboxNuevaClasificacio.Enabled = True
    End Sub
    Private Sub cargaClasificaciones()
        Try
            Dim conn As New SqlConnection(MiConexion)
            conn.Open()
            Dim comando As New SqlClient.SqlCommand("sp_carga_clasificacion_llamadas", conn)
            comando.CommandType = CommandType.StoredProcedure
            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando
            Dim Dataset As New DataSet
            'Creamos nuestros BidingSource
            Dim Bs As New BindingSource
            'Llenamos el Adaptador con el DataSet
            Adaptador.Fill(Dataset, "resultadoClasificacion")
            Bs.DataSource = Dataset.Tables("resultadoClasificacion")
            dgvResultadosClasificacion.DataSource = Bs
            conn.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        limpiaCampos()
    End Sub

    Private Sub dgvResultadosClasificacion_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvResultadosClasificacion.CurrentCellChanged
        Try
            Me.id_clasificacion = Me.dgvResultadosClasificacion.CurrentRow.Cells(0).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Opcion = "M"
        Me.gboxNuevaClasificacio.Enabled = True
        Me.btnGuardar.Text = "Guardar los Cambios"
        editaClasificaciones()
    End Sub
    Private Sub editaClasificaciones()
        Try
            Me.txtNombreClasificacion.Text = Me.dgvResultadosClasificacion.CurrentRow.Cells(1).Value.ToString
            Me.txtDescripcionClasificacion.Text = Me.dgvResultadosClasificacion.CurrentRow.Cells(2).Value.ToString
        Catch ex As Exception
            MsgBox("No se puedo cargar la Clasificaci�n seleccionada para su edici�n. Ocurrio un error.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub limpiaCampos()
        Me.txtNombreClasificacion.Text = ""
        Me.txtDescripcionClasificacion.Text = ""
    End Sub
    Private Function modifica_clasificacion_llamadas() As Boolean

        Dim regresar As Boolean = False

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "sp_modifica_clasificacion_llamadas"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 8

                Dim prmID As New SqlParameter("@id", SqlDbType.Int, 50)
                prmID.Value = Me.dgvResultadosClasificacion.CurrentRow.Cells(0).Value
                .Parameters.Add(prmID)

                Dim prmNombre As New SqlParameter("@nombre", SqlDbType.VarChar, 250)
                prmNombre.Value = Me.txtNombreClasificacion.Text
                .Parameters.Add(prmNombre)

                Dim prmDescripcion As New SqlParameter("@descripcion", SqlDbType.VarChar, 250)
                If Me.txtDescripcionClasificacion.Text = "" Then
                    Me.txtDescripcionClasificacion.Text = ""
                End If
                prmDescripcion.Value = Me.txtDescripcionClasificacion.Text
                .Parameters.Add(prmDescripcion)

                Dim i As Integer = .ExecuteNonQuery()

                Return True
            End With
            CON80.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Dim resp As Integer = 0
        resp = MsgBox("�Confirmas que deseas borrar la Clasificaci�n: " & Me.dgvResultadosClasificacion.CurrentRow.Cells(1).Value.ToString & "?", MsgBoxStyle.OkCancel)
        If resp = 1 Then
            If eliminar_clasificacion_llamadas() = True Then
                MsgBox("La Clasificaci�n " & Me.dgvResultadosClasificacion.CurrentRow.Cells(1).Value.ToString & " se ha eliminado correctamente.", MsgBoxStyle.Information)
            Else
                MsgBox("No se ha eliminado, la Clasificaci�n por que est� asignada con alguna tarifa de cobro.", MsgBoxStyle.Exclamation)
            End If
            Me.cargaClasificaciones()
        End If
    End Sub
    Private Function eliminar_clasificacion_llamadas() As Boolean

        Dim Resp As Integer = 8
        Dim regresar As Boolean = False

        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()
        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "sp_elimina_clasificacion_llamadas"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 8

                Dim prmID As New SqlParameter("@id", SqlDbType.Int, 50)
                prmID.Value = Me.dgvResultadosClasificacion.CurrentRow.Cells(0).Value
                .Parameters.Add(prmID)

                Dim prmResp As New SqlParameter("@resp", SqlDbType.Int, 50)
                prmResp.Direction = ParameterDirection.Output
                prmResp.Value = 0
                .Parameters.Add(prmResp)

                Dim i As Integer = .ExecuteNonQuery()

                Resp = prmResp.Value

                If Resp = 1 Then
                    Return True
                ElseIf Resp = 0 Then
                    Return False
                End If

            End With
            CON80.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function
End Class