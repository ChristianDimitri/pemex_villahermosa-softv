<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDescargaMaterialTec
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MuestraDetalleBitacoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.MuestraDescripcionArticuloBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.OK = New System.Windows.Forms.Button()
        Me.Muestra_Detalle_BitacoraTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_Detalle_BitacoraTableAdapter()
        Me.Muestra_Descripcion_ArticuloTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Muestra_Descripcion_ArticuloTableAdapter()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Inserta_Valores_TempoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Valores_TempoTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Valores_TempoTableAdapter()
        Me.Borra_Inserta_TempoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Inserta_TempoTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Borra_Inserta_TempoTableAdapter()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR()
        Me.ConRel_Session_TecnicosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConRel_Session_TecnicosTableAdapter = New sofTV.DataSetEDGARTableAdapters.ConRel_Session_TecnicosTableAdapter()
        Me.ConRel_Session_TecnicosDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Movimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoBitacora = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ValidaDuplicadoRel_Session_TecnicosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaDuplicadoRel_Session_TecnicosTableAdapter = New sofTV.DataSetEDGARTableAdapters.ValidaDuplicadoRel_Session_TecnicosTableAdapter()
        Me.ValidaExistenciasTecnicosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaExistenciasTecnicosTableAdapter = New sofTV.DataSetEDGARTableAdapters.ValidaExistenciasTecnicosTableAdapter()
        Me.RespuestaTextBox = New System.Windows.Forms.TextBox()
        Me.Dame_FolioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_FolioTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Dame_FolioTableAdapter()
        Me.Inserta_Bitacora_tecBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Bitacora_tecTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Bitacora_tecTableAdapter()
        Me.Inserta_Rel_Bitacora_OrdenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Rel_Bitacora_OrdenTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Rel_Bitacora_OrdenTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.NoArticuloTextBox = New System.Windows.Forms.TextBox()
        Me.ClaveTextBox = New System.Windows.Forms.TextBox()
        Me.ConceptoTextBox = New System.Windows.Forms.TextBox()
        Me.CantidadSurtidaTextBox = New System.Windows.Forms.TextBox()
        Me.MovimientoTextBox = New System.Windows.Forms.TextBox()
        Me.NoBitacoraTextBox = New System.Windows.Forms.TextBox()
        Me.DimeSiTieneunaBitacoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgarRev2 = New sofTV.DataSetEdgarRev2()
        Me.Borra_Inserta_Tempo_Por_ServicioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Inserta_Tempo_Por_ServicioTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.Borra_Inserta_Tempo_Por_ServicioTableAdapter()
        Me.INSERTA_Borra_Articulo_BitacoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.INSERTA_Borra_Articulo_BitacoraTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.INSERTA_Borra_Articulo_BitacoraTableAdapter()
        Me.Quita_SEBORROART_SALTECBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Quita_SEBORROART_SALTECTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.Quita_SEBORROART_SALTECTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.DimeSiTieneunaBitacoraTableAdapter = New sofTV.DataSetEdgarRev2TableAdapters.DimeSiTieneunaBitacoraTableAdapter()
        Me.Clv_Tipo = New System.Windows.Forms.TextBox()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.Inserta_RelCobraDescBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_RelCobraDescTableAdapter = New sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_RelCobraDescTableAdapter()
        Me.MetrajeFinTxt = New System.Windows.Forms.TextBox()
        Me.MetrajeIniTxt = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Almacen = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.DataSetLidia = New sofTV.DataSetLidia()
        Me.Revisa_Tempo_OrdBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Revisa_Tempo_OrdTableAdapter = New sofTV.DataSetLidiaTableAdapters.Revisa_Tempo_OrdTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.MuestraDetalleBitacoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraDescripcionArticuloBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Valores_TempoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Inserta_TempoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRel_Session_TecnicosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRel_Session_TecnicosDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaDuplicadoRel_Session_TecnicosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaExistenciasTecnicosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_FolioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Bitacora_tecBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Rel_Bitacora_OrdenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimeSiTieneunaBitacoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Inserta_Tempo_Por_ServicioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.INSERTA_Borra_Articulo_BitacoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Quita_SEBORROART_SALTECBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_RelCobraDescBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Revisa_Tempo_OrdBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(35, 9)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(305, 16)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Detalle de Entrega de Material a Tecnicos."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(60, 110)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(187, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Clasificación del Material:"
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(253, 110)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(344, 21)
        Me.ComboBox1.TabIndex = 1
        '
        'MuestraDetalleBitacoraBindingSource
        '
        Me.MuestraDetalleBitacoraBindingSource.DataMember = "Muestra_Detalle_Bitacora"
        Me.MuestraDetalleBitacoraBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(179, 140)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(471, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Descripción de Articulo"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(35, 140)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(138, 16)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Clave"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(656, 140)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 16)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Cantidad"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(38, 159)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(135, 21)
        Me.ComboBox2.TabIndex = 2
        '
        'MuestraDescripcionArticuloBindingSource
        '
        Me.MuestraDescripcionArticuloBindingSource.DataMember = "Muestra_Descripcion_Articulo"
        Me.MuestraDescripcionArticuloBindingSource.DataSource = Me.DataSetarnoldo
        '
        'ComboBox3
        '
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(179, 159)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(471, 21)
        Me.ComboBox3.TabIndex = 3
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(656, 159)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(112, 20)
        Me.TextBox1.TabIndex = 4
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(632, 515)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 9
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'OK
        '
        Me.OK.BackColor = System.Drawing.Color.DarkOrange
        Me.OK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.OK.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK.Location = New System.Drawing.Point(307, 515)
        Me.OK.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(155, 33)
        Me.OK.TabIndex = 7
        Me.OK.Text = "&ACEPTAR"
        Me.OK.UseVisualStyleBackColor = False
        '
        'Muestra_Detalle_BitacoraTableAdapter
        '
        Me.Muestra_Detalle_BitacoraTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Descripcion_ArticuloTableAdapter
        '
        Me.Muestra_Descripcion_ArticuloTableAdapter.ClearBeforeFill = True
        '
        'TextBox2
        '
        Me.TextBox2.Enabled = False
        Me.TextBox2.Location = New System.Drawing.Point(498, 427)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(49, 20)
        Me.TextBox2.TabIndex = 15
        Me.TextBox2.TabStop = False
        '
        'Inserta_Valores_TempoBindingSource
        '
        Me.Inserta_Valores_TempoBindingSource.DataMember = "Inserta_Valores_Tempo"
        Me.Inserta_Valores_TempoBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Valores_TempoTableAdapter
        '
        Me.Inserta_Valores_TempoTableAdapter.ClearBeforeFill = True
        '
        'Borra_Inserta_TempoBindingSource
        '
        Me.Borra_Inserta_TempoBindingSource.DataSource = Me.DataSetarnoldo
        Me.Borra_Inserta_TempoBindingSource.Position = 0
        '
        'Borra_Inserta_TempoTableAdapter
        '
        Me.Borra_Inserta_TempoTableAdapter.ClearBeforeFill = True
        '
        'TextBox3
        '
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraDescripcionArticuloBindingSource, "CLAVE", True))
        Me.TextBox3.Enabled = False
        Me.TextBox3.Location = New System.Drawing.Point(569, 441)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(46, 20)
        Me.TextBox3.TabIndex = 16
        Me.TextBox3.TabStop = False
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConRel_Session_TecnicosBindingSource
        '
        Me.ConRel_Session_TecnicosBindingSource.DataMember = "ConRel_Session_Tecnicos"
        Me.ConRel_Session_TecnicosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'ConRel_Session_TecnicosTableAdapter
        '
        Me.ConRel_Session_TecnicosTableAdapter.ClearBeforeFill = True
        '
        'ConRel_Session_TecnicosDataGridView
        '
        Me.ConRel_Session_TecnicosDataGridView.AllowUserToAddRows = False
        Me.ConRel_Session_TecnicosDataGridView.AllowUserToDeleteRows = False
        Me.ConRel_Session_TecnicosDataGridView.AutoGenerateColumns = False
        Me.ConRel_Session_TecnicosDataGridView.BackgroundColor = System.Drawing.Color.White
        Me.ConRel_Session_TecnicosDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.Movimiento, Me.NoBitacora})
        Me.ConRel_Session_TecnicosDataGridView.DataSource = Me.ConRel_Session_TecnicosBindingSource
        Me.ConRel_Session_TecnicosDataGridView.GridColor = System.Drawing.Color.WhiteSmoke
        Me.ConRel_Session_TecnicosDataGridView.Location = New System.Drawing.Point(38, 222)
        Me.ConRel_Session_TecnicosDataGridView.MultiSelect = False
        Me.ConRel_Session_TecnicosDataGridView.Name = "ConRel_Session_TecnicosDataGridView"
        Me.ConRel_Session_TecnicosDataGridView.ReadOnly = True
        Me.ConRel_Session_TecnicosDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConRel_Session_TecnicosDataGridView.Size = New System.Drawing.Size(730, 271)
        Me.ConRel_Session_TecnicosDataGridView.TabIndex = 17
        Me.ConRel_Session_TecnicosDataGridView.TabStop = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "NoArticulo"
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 5
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Clave"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Clave"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 125
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "concepto"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Concepto"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 400
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "CantidadSurtida"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Cantidad Surtida"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 150
        '
        'Movimiento
        '
        Me.Movimiento.DataPropertyName = "Movimiento"
        Me.Movimiento.HeaderText = ""
        Me.Movimiento.Name = "Movimiento"
        Me.Movimiento.ReadOnly = True
        Me.Movimiento.Width = 5
        '
        'NoBitacora
        '
        Me.NoBitacora.DataPropertyName = "NoBitacora"
        Me.NoBitacora.HeaderText = "NoBitacora"
        Me.NoBitacora.Name = "NoBitacora"
        Me.NoBitacora.ReadOnly = True
        '
        'ValidaDuplicadoRel_Session_TecnicosBindingSource
        '
        Me.ValidaDuplicadoRel_Session_TecnicosBindingSource.DataMember = "ValidaDuplicadoRel_Session_Tecnicos"
        Me.ValidaDuplicadoRel_Session_TecnicosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'ValidaDuplicadoRel_Session_TecnicosTableAdapter
        '
        Me.ValidaDuplicadoRel_Session_TecnicosTableAdapter.ClearBeforeFill = True
        '
        'ValidaExistenciasTecnicosBindingSource
        '
        Me.ValidaExistenciasTecnicosBindingSource.DataMember = "ValidaExistenciasTecnicos"
        Me.ValidaExistenciasTecnicosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'ValidaExistenciasTecnicosTableAdapter
        '
        Me.ValidaExistenciasTecnicosTableAdapter.ClearBeforeFill = True
        '
        'RespuestaTextBox
        '
        Me.RespuestaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RespuestaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ValidaExistenciasTecnicosBindingSource, "Respuesta", True))
        Me.RespuestaTextBox.ForeColor = System.Drawing.Color.White
        Me.RespuestaTextBox.Location = New System.Drawing.Point(704, 162)
        Me.RespuestaTextBox.Name = "RespuestaTextBox"
        Me.RespuestaTextBox.Size = New System.Drawing.Size(38, 13)
        Me.RespuestaTextBox.TabIndex = 19
        Me.RespuestaTextBox.TabStop = False
        '
        'Dame_FolioBindingSource
        '
        Me.Dame_FolioBindingSource.DataMember = "Dame_Folio"
        Me.Dame_FolioBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Dame_FolioTableAdapter
        '
        Me.Dame_FolioTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Bitacora_tecBindingSource
        '
        Me.Inserta_Bitacora_tecBindingSource.DataMember = "Inserta_Bitacora_tec"
        Me.Inserta_Bitacora_tecBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Bitacora_tecTableAdapter
        '
        Me.Inserta_Bitacora_tecTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Rel_Bitacora_OrdenBindingSource
        '
        Me.Inserta_Rel_Bitacora_OrdenBindingSource.DataMember = "Inserta_Rel_Bitacora_Orden"
        Me.Inserta_Rel_Bitacora_OrdenBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Rel_Bitacora_OrdenTableAdapter
        '
        Me.Inserta_Rel_Bitacora_OrdenTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(38, 186)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(109, 30)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "A&GREGAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(210, 343)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(127, 30)
        Me.Button2.TabIndex = 21
        Me.Button2.TabStop = False
        Me.Button2.Text = "&MODIFICAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(153, 186)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(127, 30)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "&ELIMINAR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(470, 515)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(155, 33)
        Me.Button4.TabIndex = 8
        Me.Button4.Text = "&GUARDAR"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'NoArticuloTextBox
        '
        Me.NoArticuloTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRel_Session_TecnicosBindingSource, "NoArticulo", True))
        Me.NoArticuloTextBox.Location = New System.Drawing.Point(691, 343)
        Me.NoArticuloTextBox.Name = "NoArticuloTextBox"
        Me.NoArticuloTextBox.Size = New System.Drawing.Size(35, 20)
        Me.NoArticuloTextBox.TabIndex = 24
        Me.NoArticuloTextBox.TabStop = False
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRel_Session_TecnicosBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(14, 25)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.Size = New System.Drawing.Size(95, 20)
        Me.ClaveTextBox.TabIndex = 25
        Me.ClaveTextBox.TabStop = False
        '
        'ConceptoTextBox
        '
        Me.ConceptoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRel_Session_TecnicosBindingSource, "concepto", True))
        Me.ConceptoTextBox.Location = New System.Drawing.Point(115, 25)
        Me.ConceptoTextBox.Name = "ConceptoTextBox"
        Me.ConceptoTextBox.Size = New System.Drawing.Size(489, 20)
        Me.ConceptoTextBox.TabIndex = 26
        Me.ConceptoTextBox.TabStop = False
        '
        'CantidadSurtidaTextBox
        '
        Me.CantidadSurtidaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRel_Session_TecnicosBindingSource, "CantidadSurtida", True))
        Me.CantidadSurtidaTextBox.Location = New System.Drawing.Point(610, 25)
        Me.CantidadSurtidaTextBox.Name = "CantidadSurtidaTextBox"
        Me.CantidadSurtidaTextBox.Size = New System.Drawing.Size(111, 20)
        Me.CantidadSurtidaTextBox.TabIndex = 27
        Me.CantidadSurtidaTextBox.TabStop = False
        '
        'MovimientoTextBox
        '
        Me.MovimientoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConRel_Session_TecnicosBindingSource, "Movimiento", True))
        Me.MovimientoTextBox.Location = New System.Drawing.Point(615, 360)
        Me.MovimientoTextBox.Name = "MovimientoTextBox"
        Me.MovimientoTextBox.Size = New System.Drawing.Size(35, 20)
        Me.MovimientoTextBox.TabIndex = 28
        Me.MovimientoTextBox.TabStop = False
        '
        'NoBitacoraTextBox
        '
        Me.NoBitacoraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DimeSiTieneunaBitacoraBindingSource, "Folio", True))
        Me.NoBitacoraTextBox.Enabled = False
        Me.NoBitacoraTextBox.Location = New System.Drawing.Point(253, 51)
        Me.NoBitacoraTextBox.Name = "NoBitacoraTextBox"
        Me.NoBitacoraTextBox.Size = New System.Drawing.Size(96, 20)
        Me.NoBitacoraTextBox.TabIndex = 0
        Me.NoBitacoraTextBox.TabStop = False
        '
        'DimeSiTieneunaBitacoraBindingSource
        '
        Me.DimeSiTieneunaBitacoraBindingSource.DataMember = "DimeSiTieneunaBitacora"
        Me.DimeSiTieneunaBitacoraBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'DataSetEdgarRev2
        '
        Me.DataSetEdgarRev2.DataSetName = "DataSetEdgarRev2"
        Me.DataSetEdgarRev2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Borra_Inserta_Tempo_Por_ServicioBindingSource
        '
        Me.Borra_Inserta_Tempo_Por_ServicioBindingSource.DataMember = "Borra_Inserta_Tempo_Por_Servicio"
        Me.Borra_Inserta_Tempo_Por_ServicioBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'Borra_Inserta_Tempo_Por_ServicioTableAdapter
        '
        Me.Borra_Inserta_Tempo_Por_ServicioTableAdapter.ClearBeforeFill = True
        '
        'INSERTA_Borra_Articulo_BitacoraBindingSource
        '
        Me.INSERTA_Borra_Articulo_BitacoraBindingSource.DataMember = "INSERTA_Borra_Articulo_Bitacora"
        Me.INSERTA_Borra_Articulo_BitacoraBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'INSERTA_Borra_Articulo_BitacoraTableAdapter
        '
        Me.INSERTA_Borra_Articulo_BitacoraTableAdapter.ClearBeforeFill = True
        '
        'Quita_SEBORROART_SALTECBindingSource
        '
        Me.Quita_SEBORROART_SALTECBindingSource.DataMember = "Quita_SEBORROART_SALTEC"
        Me.Quita_SEBORROART_SALTECBindingSource.DataSource = Me.DataSetEdgarRev2
        '
        'Quita_SEBORROART_SALTECTableAdapter
        '
        Me.Quita_SEBORROART_SALTECTableAdapter.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(145, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 16)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "No. Bitacora :"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Button7)
        Me.Panel1.Controls.Add(Me.Button6)
        Me.Panel1.Controls.Add(Me.ClaveTextBox)
        Me.Panel1.Controls.Add(Me.ConceptoTextBox)
        Me.Panel1.Controls.Add(Me.CantidadSurtidaTextBox)
        Me.Panel1.Location = New System.Drawing.Point(52, 235)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(705, 86)
        Me.Panel1.TabIndex = 32
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(610, 5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(111, 17)
        Me.Label8.TabIndex = 30
        Me.Label8.Text = "Cantidad"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label7.Location = New System.Drawing.Point(115, 5)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(489, 17)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Descripción"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label6.Location = New System.Drawing.Point(11, 5)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 17)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "Clave "
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(539, 53)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(182, 30)
        Me.Button7.TabIndex = 24
        Me.Button7.TabStop = False
        Me.Button7.Text = "&Cancelar Modificacion"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(353, 55)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(180, 27)
        Me.Button6.TabIndex = 23
        Me.Button6.TabStop = False
        Me.Button6.Text = "&Guardar Modificacion"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'DimeSiTieneunaBitacoraTableAdapter
        '
        Me.DimeSiTieneunaBitacoraTableAdapter.ClearBeforeFill = True
        '
        'Clv_Tipo
        '
        Me.Clv_Tipo.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_Tipo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_Tipo.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_Tipo.Location = New System.Drawing.Point(603, 91)
        Me.Clv_Tipo.Name = "Clv_Tipo"
        Me.Clv_Tipo.Size = New System.Drawing.Size(17, 13)
        Me.Clv_Tipo.TabIndex = 33
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_RelCobraDescBindingSource
        '
        Me.Inserta_RelCobraDescBindingSource.DataMember = "Inserta_RelCobraDesc"
        Me.Inserta_RelCobraDescBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_RelCobraDescTableAdapter
        '
        Me.Inserta_RelCobraDescTableAdapter.ClearBeforeFill = True
        '
        'MetrajeFinTxt
        '
        Me.MetrajeFinTxt.Location = New System.Drawing.Point(723, 159)
        Me.MetrajeFinTxt.Name = "MetrajeFinTxt"
        Me.MetrajeFinTxt.Size = New System.Drawing.Size(61, 20)
        Me.MetrajeFinTxt.TabIndex = 34
        '
        'MetrajeIniTxt
        '
        Me.MetrajeIniTxt.Location = New System.Drawing.Point(656, 159)
        Me.MetrajeIniTxt.Name = "MetrajeIniTxt"
        Me.MetrajeIniTxt.Size = New System.Drawing.Size(61, 20)
        Me.MetrajeIniTxt.TabIndex = 35
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(659, 124)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(112, 16)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "Metraje"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(656, 140)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(61, 16)
        Me.Label10.TabIndex = 37
        Me.Label10.Text = "Inicio"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(723, 140)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(61, 16)
        Me.Label11.TabIndex = 38
        Me.Label11.Text = "Fin"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Almacen
        '
        Me.Almacen.DisplayMember = "Nombre"
        Me.Almacen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Almacen.FormattingEnabled = True
        Me.Almacen.Location = New System.Drawing.Point(253, 77)
        Me.Almacen.Name = "Almacen"
        Me.Almacen.Size = New System.Drawing.Size(344, 23)
        Me.Almacen.TabIndex = 428
        Me.Almacen.ValueMember = "clv_Tecnico"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(169, 77)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(76, 16)
        Me.Label12.TabIndex = 430
        Me.Label12.Text = "Almacen :"
        '
        'ComboBox4
        '
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(615, 8)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(70, 21)
        Me.ComboBox4.TabIndex = 431
        Me.ComboBox4.Visible = False
        '
        'ComboBox5
        '
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(615, 35)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(70, 21)
        Me.ComboBox5.TabIndex = 432
        Me.ComboBox5.Visible = False
        '
        'DataSetLidia
        '
        Me.DataSetLidia.DataSetName = "DataSetLidia"
        Me.DataSetLidia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Revisa_Tempo_OrdBindingSource
        '
        Me.Revisa_Tempo_OrdBindingSource.DataMember = "Revisa_Tempo_Ord"
        Me.Revisa_Tempo_OrdBindingSource.DataSource = Me.DataSetLidia
        '
        'Revisa_Tempo_OrdTableAdapter
        '
        Me.Revisa_Tempo_OrdTableAdapter.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmDescargaMaterialTec
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(788, 562)
        Me.Controls.Add(Me.ComboBox5)
        Me.Controls.Add(Me.ComboBox4)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Almacen)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.MetrajeIniTxt)
        Me.Controls.Add(Me.MetrajeFinTxt)
        Me.Controls.Add(Me.Clv_Tipo)
        Me.Controls.Add(Me.ConRel_Session_TecnicosDataGridView)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.NoBitacoraTextBox)
        Me.Controls.Add(Me.MovimientoTextBox)
        Me.Controls.Add(Me.NoArticuloTextBox)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.RespuestaTextBox)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.ComboBox3)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button4)
        Me.Name = "FrmDescargaMaterialTec"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Descarga Material Técnicos"
        CType(Me.MuestraDetalleBitacoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraDescripcionArticuloBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Valores_TempoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Inserta_TempoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRel_Session_TecnicosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRel_Session_TecnicosDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaDuplicadoRel_Session_TecnicosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaExistenciasTecnicosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_FolioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Bitacora_tecBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Rel_Bitacora_OrdenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimeSiTieneunaBitacoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgarRev2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Inserta_Tempo_Por_ServicioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.INSERTA_Borra_Articulo_BitacoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Quita_SEBORROART_SALTECBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_RelCobraDescBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Revisa_Tempo_OrdBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents MuestraDetalleBitacoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents MuestraDescripcionArticuloBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Detalle_BitacoraTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_Detalle_BitacoraTableAdapter
    Friend WithEvents Muestra_Descripcion_ArticuloTableAdapter As sofTV.DataSetarnoldoTableAdapters.Muestra_Descripcion_ArticuloTableAdapter
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Inserta_Valores_TempoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Valores_TempoTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Valores_TempoTableAdapter
    Friend WithEvents Borra_Inserta_TempoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Inserta_TempoTableAdapter As sofTV.DataSetarnoldoTableAdapters.Borra_Inserta_TempoTableAdapter
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents ConRel_Session_TecnicosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRel_Session_TecnicosTableAdapter As sofTV.DataSetEDGARTableAdapters.ConRel_Session_TecnicosTableAdapter
    Friend WithEvents ConRel_Session_TecnicosDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ValidaDuplicadoRel_Session_TecnicosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaDuplicadoRel_Session_TecnicosTableAdapter As sofTV.DataSetEDGARTableAdapters.ValidaDuplicadoRel_Session_TecnicosTableAdapter
    Friend WithEvents ValidaExistenciasTecnicosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaExistenciasTecnicosTableAdapter As sofTV.DataSetEDGARTableAdapters.ValidaExistenciasTecnicosTableAdapter
    Friend WithEvents RespuestaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Dame_FolioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_FolioTableAdapter As sofTV.DataSetarnoldoTableAdapters.Dame_FolioTableAdapter
    Friend WithEvents Inserta_Bitacora_tecBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Bitacora_tecTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Bitacora_tecTableAdapter
    Friend WithEvents Inserta_Rel_Bitacora_OrdenBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Rel_Bitacora_OrdenTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Rel_Bitacora_OrdenTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Movimiento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoBitacora As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoArticuloTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConceptoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CantidadSurtidaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MovimientoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoBitacoraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataSetEdgarRev2 As sofTV.DataSetEdgarRev2
    Friend WithEvents Borra_Inserta_Tempo_Por_ServicioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Inserta_Tempo_Por_ServicioTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.Borra_Inserta_Tempo_Por_ServicioTableAdapter
    Friend WithEvents INSERTA_Borra_Articulo_BitacoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents INSERTA_Borra_Articulo_BitacoraTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.INSERTA_Borra_Articulo_BitacoraTableAdapter
    Friend WithEvents Quita_SEBORROART_SALTECBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Quita_SEBORROART_SALTECTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.Quita_SEBORROART_SALTECTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents DimeSiTieneunaBitacoraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DimeSiTieneunaBitacoraTableAdapter As sofTV.DataSetEdgarRev2TableAdapters.DimeSiTieneunaBitacoraTableAdapter
    Friend WithEvents DataSetLidia As sofTV.DataSetLidia
    Friend WithEvents Revisa_Tempo_OrdBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Revisa_Tempo_OrdTableAdapter As sofTV.DataSetLidiaTableAdapters.Revisa_Tempo_OrdTableAdapter
    Friend WithEvents Clv_Tipo As System.Windows.Forms.TextBox
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Inserta_RelCobraDescBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_RelCobraDescTableAdapter As sofTV.ProcedimientosArnoldo2TableAdapters.Inserta_RelCobraDescTableAdapter
    Friend WithEvents MetrajeFinTxt As System.Windows.Forms.TextBox
    Friend WithEvents MetrajeIniTxt As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Almacen As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
