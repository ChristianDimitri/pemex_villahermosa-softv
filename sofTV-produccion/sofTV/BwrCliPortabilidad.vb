﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System
Imports System.IO.StreamReader
Imports System.IO.File
Public Class BwrCliPortabilidad
    Private customersByCityReport As ReportDocument
    Dim Impresora_Tarjetas As String = Nothing
    Dim Impresora_Contratos As String = Nothing

    Private Sub BwrCliPortabilidad_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        'If Me.rdbPendiente.Checked = True Then
        '    VerPorStatusPortabilidad(2)
        'ElseIf Me.rdbAvtiva.Checked = True Then
        '    VerPorStatusPortabilidad(1)
        'ElseIf Me.rdbAmbas.Checked = True Then
        '    VerPorStatusPortabilidad(3)
        'End If
        'DataGridView1.Refresh()

    End Sub
    Private Sub BwrCliPortabilidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)
        MuestraClientesPortabilidad()

    End Sub
    Private Sub MuestraClientesPortabilidad()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraClientesPortabilidad ")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            
            Me.DataGridView1.DataSource = bindingSource

            Me.DataGridView1.Columns(0).Width = 100
            Me.DataGridView1.Columns(1).Width = 350
            Me.DataGridView1.Columns(2).Width = 100

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub datos()
        Try
            Me.folio.Text = (Me.DataGridView1.Item(0, Me.DataGridView1.CurrentRow.Index).Value)
            Me.contrato.Text = (Me.DataGridView1.Item(2, Me.DataGridView1.CurrentRow.Index).Value)
            Me.nombre.Text = (Me.DataGridView1.Item(1, Me.DataGridView1.CurrentRow.Index).Value)
           
        Catch ex As Exception

        End Try


    End Sub
    Private Sub DataGridView1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        datos()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If IsNumeric(Me.folio.Text) = True Then
            opcionport = "C"
            folioport = CLng(Me.folio.Text)
            contraoport = CLng(Me.contrato.Text)
            FrmDatosPortabilidad.Show()
        Else
            MsgBox("Debe seleccionar un renglon valido", MsgBoxStyle.Information)
        End If

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If IsNumeric(Me.folio.Text) = True Then
            opcionport = "M"
            folioport = CLng(Me.folio.Text)
            contraoport = CLng(Me.contrato.Text)
            FrmDatosPortabilidad.Show()
        Else
            MsgBox("Debe seleccionar un renglon valido", MsgBoxStyle.Information)
        End If

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If IsNumeric(Me.TextBox1.Text) = True Then
            BuscarCliente("", CLng(Me.TextBox1.Text), 0)
        End If
        limpia()
    End Sub

    Private Sub BuscarCliente(ByVal nombre As String, ByVal contrato As Long, ByVal folio As Long)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC BuscarCliente ")
        strSQL.Append("'" & CStr(nombre) & "'" & ",")
        strSQL.Append(CStr(contrato) & ",")
        strSQL.Append(CStr(folio))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable

            Me.DataGridView1.DataSource = bindingSource

            Me.DataGridView1.Columns(0).Width = 100
            Me.DataGridView1.Columns(1).Width = 350
            Me.DataGridView1.Columns(2).Width = 100

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(Me.TextBox2.Text) = True Then
            BuscarCliente("", 0, CLng(Me.TextBox2.Text))
        End If
        limpia()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Len(Me.TextBox3.Text) > 0 Then
            BuscarCliente(Me.TextBox3.Text, 0, 0)
        End If
        limpia()
    End Sub
    Private Sub limpia()
        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
        Me.TextBox3.Clear()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If IsNumeric(Me.folio.Text) = True Then
            folioport = CLng(Me.folio.Text)
            'Selecciona_Impresora_Sucursal(GloClv_Sucursal)
            'ConfigureCrystalReportsPortabilidad(folioport)
            FrmImprimirAux.Show()
        Else
            MsgBox("Debe seleccionar un renglon valido", MsgBoxStyle.Information)
        End If
       
    End Sub
    Private Sub ConfigureCrystalReportsPortabilidad(ByVal FolioS As Long)

        Dim impresora As String = Nothing
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\repotrtePortabilidad.rpt"
        'reportPath = "D:\Exes\Softv\reportes\repotrtePortabilidad.rpt"
        customersByCityReport.Load(reportPath)


        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@Contrato 
        customersByCityReport.SetParameterValue(0, FolioS)
        'CrystalReportViewer1.ReportSource = customersByCityReport
        'CrystalReportViewer1.ShowPrintButton = True

        'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
        Try
            customersByCityReport.PrintOptions.PrinterName = Impresora_Contratos
            customersByCityReport.PrintToPrinter(0, True, 1, 1)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
       


        'customersByCityReport = Nothing


    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub
    Private Sub Selecciona_Impresora_Sucursal(ByVal sucursal As Integer)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Selecciona_Impresora_Sucursal", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_sucursal", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = sucursal
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Impresora_Tarjetas", SqlDbType.VarChar, 50)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Impresora_Contratos", SqlDbType.VarChar, 50)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)

        Try
            con.Open()
            com.ExecuteNonQuery()
            Impresora_Tarjetas = CStr(par2.Value.ToString)
            Impresora_Contratos = CStr(par3.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try

    End Sub
    Public Sub VerPorStatusPortabilidad(ByVal op As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC VerPorStatusPortabilidad ")
        strSQL.Append(CStr(op))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable

            Me.DataGridView1.DataSource = bindingSource

            Me.DataGridView1.Columns(0).Width = 100
            Me.DataGridView1.Columns(1).Width = 350
            Me.DataGridView1.Columns(2).Width = 100

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub rdbPendiente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbPendiente.CheckedChanged
        If Me.rdbPendiente.Checked = True Then
            VerPorStatusPortabilidad(2)
        End If
    End Sub

    Private Sub rdbAvtiva_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbAvtiva.CheckedChanged
        If Me.rdbAvtiva.Checked = True Then
            VerPorStatusPortabilidad(1)
        End If
    End Sub

    Private Sub rdbAmbas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbAmbas.CheckedChanged
        If Me.rdbAmbas.Checked = True Then
            VerPorStatusPortabilidad(3)
        End If
    End Sub
End Class