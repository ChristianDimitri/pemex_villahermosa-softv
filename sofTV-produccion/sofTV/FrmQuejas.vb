
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class FrmQuejas
    Private customersByCityReport As ReportDocument
    Dim op As String = Nothing
    Dim Titulo As String = Nothing

    Public Activado As Boolean = False


    Private Sub ConfigureCrystalReports(ByVal op As Integer, ByVal Titulo As String)
        'Try
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0", Op7 As String = "0"
        Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
        Dim Fec1Ini As Date = "01/01/1900", Fec1Fin As Date = "01/01/1900", Fec2Ini As Date = "01/01/1900", Fec2Fin As Date = "01/01/1900"
        Dim Num1 As String = 0, Num2 As String = 0
        Dim nclv_trabajo As String = "0"
        Dim nClv_colonia As String = "0"

        If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
            StatusPen = "1"
        End If
        If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
            StatusEje = "1"
        End If
        If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
            StatusVis = "1"
        End If

        If Me.CheckBox1.CheckState = CheckState.Checked Then
            Op1 = "1"
        End If
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            Op2 = "1"
        End If
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            Op3 = "1"
        End If
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            Op4 = "1"
        End If
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            Op5 = "1"
        End If
        If Me.CheckBox6.CheckState = CheckState.Checked Then
            Op6 = "1"
        End If
        If Me.CheckBox7.Checked = True Then
            Op7 = "1"
        End If

        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Titulo
        Dim OpOrdenar As String = "0"
        If Me.RadioButton1.Checked = True Then
            OpOrdenar = "0"
        ElseIf Me.RadioButton2.Checked = True Then
            OpOrdenar = "1"
        ElseIf Me.RadioButton4.Checked = True Then
            OpOrdenar = "2"
        End If


        Dim reportPath As String = Nothing
        If op = 0 Then
            reportPath = RutaReportes + "\ReporteQuejasD.rpt"
        ElseIf op = 1 Then
            If IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCabStar.rpt"
            ElseIf IdSistema = "AG" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBueno.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoLogitel.rpt"
            End If
        ElseIf op = 2 Then
            reportPath = RutaReportes + "\ReporteQuejasD_ListadoNuevo.rpt"
        End If
        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        '@Clv_TipSer int
        customersByCityReport.SetParameterValue(0, GloClv_TipSer)
        ',@op1 smallint
        customersByCityReport.SetParameterValue(1, Op1)
        ',@op2 smallint
        customersByCityReport.SetParameterValue(2, Op2)
        ',@op3 smallint
        customersByCityReport.SetParameterValue(3, Op3)
        ',@op4 smallint,
        customersByCityReport.SetParameterValue(4, Op4)
        '@op5 smallint
        customersByCityReport.SetParameterValue(5, Op5)
        '@op6 smallint
        customersByCityReport.SetParameterValue(6, Op6)
        ',@StatusPen bit
        customersByCityReport.SetParameterValue(7, StatusPen)
        ',@StatusEje bit
        customersByCityReport.SetParameterValue(8, StatusEje)
        ',@StatusVis bit,
        customersByCityReport.SetParameterValue(9, StatusVis)
        '@Clv_OrdenIni bigint
        If IsNumeric(Me.NUMINILbl.Text) = True Then Num1 = CStr(Me.NUMINILbl.Text)
        customersByCityReport.SetParameterValue(10, Num1)
        ',@Clv_OrdenFin bigint
        If IsNumeric(Me.NUMFINLBL.Text) = True Then Num2 = CStr(Me.NUMFINLBL.Text)
        customersByCityReport.SetParameterValue(11, Num2)
        ',@Fec1Ini Datetime
        If IsDate(Me.FECSOLINI.Text) = True Then Fec1Ini = Me.FECSOLINI.Text
        customersByCityReport.SetParameterValue(12, Fec1Ini)
        ',@Fec1Fin Datetime,
        If IsDate(Me.FECSOLFIN.Text) = True Then Fec1Fin = Me.FECSOLFIN.Text
        customersByCityReport.SetParameterValue(13, Fec1Fin)
        '@Fec2Ini Datetime
        If IsDate(Me.FECEJEINI.Text) = True Then Fec2Ini = Me.FECEJEINI.Text
        customersByCityReport.SetParameterValue(14, Fec2Ini)
        ',@Fec2Fin Datetime
        If IsDate(Me.FECEJEFIN.Text) = True Then Fec2Fin = Me.FECEJEFIN.Text
        customersByCityReport.SetParameterValue(15, Fec2Fin)
        ',@Clv_Trabajo int
        If IsNumeric(Me.CLV_TRABAJO.Text) = True Then nclv_trabajo = CStr(Me.CLV_TRABAJO.Text)
        customersByCityReport.SetParameterValue(16, nclv_trabajo)
        ',@Clv_Colonia int
        If IsNumeric(Me.CLV_COLONIA.Text) = True Then nClv_colonia = CStr(Me.CLV_COLONIA.Text)
        customersByCityReport.SetParameterValue(17, nClv_colonia)
        ',@OpOrden int
        customersByCityReport.SetParameterValue(18, OpOrdenar)
        '@Clv_Departamento
        'If IsNumeric(LocclvDepto) = False Then LocclvDepto = "0"
        customersByCityReport.SetParameterValue(19, LocclvDepto)
        '@Op7
        customersByCityReport.SetParameterValue(20, Op7)
        '@Contrato
        If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
        customersByCityReport.SetParameterValue(21, Me.TextBox1.Text)

        'Titulos de Reporte
        If op = 0 Or op = 2 Then
            mySelectFormula = "Listado de Quejas"
            customersByCityReport.DataDefinition.FormulaFields("empresa").Text = "'" & LocGloNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
            'customersByCityReport.DataDefinition.FormulaFields("nomservicio").Text = "'" & Me.ComboBox4.Text & "'"
        ElseIf op = 1 Then
            mySelectFormula = "Quejas " + Me.ComboBox1.Text
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
        End If
        CrystalReportViewer1.ReportSource = customersByCityReport


        customersByCityReport = Nothing
        'Catch ex As System.Exception
        'System.Windows.Forms.MessageBox.Show(ex.Message)
        ' End Try
    End Sub

    Private Sub FrmQuejas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBusca_NumQueja = True Then
            GloBusca_NumQueja = False
            Me.NUMFINLBL.Text = GLONUMCLVQUEJAS_FIN
            Me.NUMINILbl.Text = GLONUMCLVQUEJAS_INI
            GLONUMCLVQUEJAS_FIN = 0
            GLONUMCLVQUEJAS_INI = 0
        End If
        If op = "1" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECSOLINI.Text = GloFecha_Ini
            Me.FECSOLFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If op = "2" And GloBndSelFecha = True Then
            GloBndSelFecha = False
            Me.FECEJEINI.Text = GloFecha_Ini
            Me.FECEJEFIN.Text = GloFecha_Fin
            GloFecha_Ini = "01/01/1900"
            GloFecha_Fin = "01/01/1900"
        End If
        If GloBndTrabajo = True Then
            GloBndTrabajo = False
            Me.CLV_TRABAJO.Text = GloNumClv_Trabajo
            Me.NOMTRABAJO.Text = GLONOMTRABAJO
        End If
        If GlobndClv_Colonia = True Then
            GlobndClv_Colonia = False
            Me.CLV_COLONIA.Text = GloNumClv_Colonia
            Me.NOMCOLONIA.Text = GLONOMCOLONIA
        End If
        If LocGlobndDpto = True Then
            LocGlobndDpto = False
            Me.Label8.Text = LocNomDepto
        End If
    End Sub

    Private Sub FrmQuejas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraTipSerPrincipal2TableAdapter.Connection = CON
        Me.MuestraTipSerPrincipal2TableAdapter.Fill(Me.DataSetLidia2.MuestraTipSerPrincipal2)
        CON.Close()
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            GloClv_TipSer = Me.ComboBox1.SelectedValue
            LIMPIA()
        End If
    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub LIMPIA()
        Me.CheckBox1.CheckState = CheckState.Unchecked
        Me.CheckBox2.CheckState = CheckState.Unchecked
        Me.CheckBox3.CheckState = CheckState.Unchecked
        Me.CheckBox5.CheckState = CheckState.Unchecked
        Me.CheckBox4.CheckState = CheckState.Unchecked
        Me.CheckBox6.CheckState = CheckState.Unchecked
        Me.NUMFINLBL.Text = 0
        Me.NUMINILbl.Text = 0
        Me.FECSOLFIN.Text = ""
        Me.FECSOLINI.Text = ""
        Me.FECEJEFIN.Text = ""
        Me.FECEJEINI.Text = ""
        Me.CLV_TRABAJO.Text = 0
        Me.NOMTRABAJO.Text = ""
        Me.CLV_COLONIA.Text = 0
        Me.NOMCOLONIA.Text = ""
        Me.Label8.Text = ""
    End Sub

    'Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
    '    If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
    '        GloClv_TipSer = Me.ComboBox4.SelectedValue
    '        LIMPIA()
    '        '--Me.CatalogodeReportesAreaTecnicaTableAdapter.Fill(Me.NewsoftvDataSet1.CatalogodeReportesAreaTecnica, Me.ComboBox4.SelectedValue, 0)
    '    End If
    'End Sub

    Private Sub PendientesCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PendientesCheckBox.CheckedChanged
        If Me.PendientesCheckBox.CheckState = CheckState.Checked Then
            GloPendientes = 1
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
        Else
            GloPendientes = 0
        End If
    End Sub

    Private Sub EjecutadasCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EjecutadasCheckBox.CheckedChanged
        If Me.EjecutadasCheckBox.CheckState = CheckState.Checked Then
            GloEjecutadas = 1
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
            Me.VisitaCheckBox.CheckState = CheckState.Unchecked
        Else
            GloEjecutadas = 0
        End If
    End Sub

    Private Sub VisitaCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VisitaCheckBox.CheckedChanged
        If Me.VisitaCheckBox.CheckState = CheckState.Checked Then
            GloVisita = 1
            Me.PendientesCheckBox.CheckState = CheckState.Unchecked
            Me.EjecutadasCheckBox.CheckState = CheckState.Unchecked
        Else
            GloVisita = 0
        End If
    End Sub

    'Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
    '    If Me.ComboBox4.SelectedValue = 1 Then
    '        If IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
    '            op = CStr(DataGridView1.SelectedCells(0).Value)
    '            Titulo = CStr(DataGridView1.SelectedCells(1).Value)
    '            GloOpRep = op
    '            If op = "0" Then
    '                FrmSelOrdSer.Show()
    '            ElseIf op = "1" Then
    '                FrmSelFechas.Show()
    '            ElseIf op = "2" Then
    '                FrmSelFechas.Show()
    '            ElseIf op = "3" Then
    '                FrmSelTrabajo.Show()
    '            ElseIf op = "4" Then
    '                FrmSelColonia.Show()
    '            End If
    '        End If
    '    ElseIf Me.ComboBox4.SelectedValue = 2 Then
    '        If IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
    '            op = CStr(DataGridView1.SelectedCells(0).Value)
    '            Titulo = CStr(DataGridView1.SelectedCells(1).Value)
    '            GloOpRep = op
    '            If op = "0" Then
    '                FrmSelOrdSer.Show()
    '            ElseIf op = "1" Then
    '                FrmSelFechas.Show()
    '            ElseIf op = "2" Then
    '                FrmSelFechas.Show()
    '            ElseIf op = "3" Then
    '                FrmSelTrabajo.Show()
    '            ElseIf op = "4" Then
    '                FrmSelColonia.Show()
    '            End If
    '        End If
    '    ElseIf Me.ComboBox4.SelectedValue = 3 Then
    '        If IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
    '            op = CStr(DataGridView1.SelectedCells(0).Value)
    '            Titulo = CStr(DataGridView1.SelectedCells(1).Value)
    '            GloOpRep = op
    '            If op = "0" Then
    '                FrmSelOrdSer.Show()
    '            ElseIf op = "1" Then
    '                FrmSelFechas.Show()
    '            ElseIf op = "2" Then
    '                FrmSelFechas.Show()
    '            ElseIf op = "3" Then
    '                FrmSelTrabajo.Show()
    '            ElseIf op = "4" Then
    '                FrmSelColonia.Show()
    '            End If
    '        End If
    '    End If
    'End Sub

    'Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    'End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            op = "0"
            FrmSelOrdSer.Show()
        Else
            Me.NUMINILbl.Text = 0
            Me.NUMFINLBL.Text = 0
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            op = "1"
            FrmSelFechas.Show()
        Else
            Me.FECSOLFIN.Text = ""
            Me.FECSOLINI.Text = ""
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            op = "2"
            FrmSelFechas.Show()
        Else
            Me.FECEJEINI.Text = ""
            Me.FECEJEFIN.Text = ""
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If Me.CheckBox4.CheckState = CheckState.Checked Then
            op = "3"
            FrmSelTrabajo.Show()
        Else
            Me.CLV_TRABAJO.Text = 0
            Me.NOMTRABAJO.Text = ""
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If Me.CheckBox5.CheckState = CheckState.Checked Then
            op = "4"
            FrmSelColonia.Show()
        Else
            Me.CLV_COLONIA.Text = 0
            Me.NOMCOLONIA.Text = ""
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ConfigureCrystalReports(0, "")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        ConfigureCrystalReports(1, "")
    End Sub

    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox6.CheckedChanged
        If Me.CheckBox6.CheckState = CheckState.Checked Then
            op = "5"
            FrmDepartamento.Show()
        Else
            Me.Label8.Text = ""
        End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        ConfigureCrystalReports(2, "")
    End Sub

    Private Sub CheckBox7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox7.CheckedChanged
        If Me.CheckBox7.Checked = True Then
            Me.TextBox1.Enabled = True

        Else
            Me.TextBox1.Enabled = False

        End If
    End Sub


    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            GloClv_TipSer = Me.ComboBox1.SelectedValue
            LIMPIA()
        End If
    End Sub
End Class