Imports System.Data.SqlClient
Imports System.Text
Public Class BrwCargosEspeciales
    Private SeCobro As Integer = 0
    Private eRes As Integer = 0
    Private eMsg As String = Nothing
    Public eMonto As Boolean = False

    Private Sub Busca(ByVal Op As Integer)
        Me.LabelClave.Text = ""
        Select Case Op
            Case 0
                BuscaCargosEspeciales(0, SeCobro, "", eMonto, Op)

            Case 1
                If IsNumeric(Me.TextBox2.Text) = True Then
                    BuscaCargosEspeciales(CLng(Me.TextBox2.Text), SeCobro, "", eMonto, Op)
                Else
                    BuscaCargosEspeciales(CLng(0), SeCobro, "", eMonto, Op)
                    MsgBox("No Se Puede Buscar Por Ese Contrato", MsgBoxStyle.Information)
                End If
        End Select

    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Busca(1)
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox2, Asc(LCase(e.KeyChar)), "N")))
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.RadioButton1.Checked = True Then
            secobro = 1
        End If
        Busca(0)
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If Me.RadioButton3.Checked = True Then
            secobro = 2
        End If
        Busca(0)
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If Me.RadioButton2.Checked = True Then
            secobro = 0
        End If
        Busca(0)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Busca(2)
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub



    Private Sub BrwCargosEspeciales_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Locbndactualiza = True Then
            Locbndactualiza = False
            Busca(0)
            CambiaSeleccion()
        End If
    End Sub

    Private Sub BrwCargosEspeciales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If eBndCargoEsp = True Then
            eMonto = True
            Me.Text = "Catálogo de Cargos Especiales"
            Me.Label1.Text = "Datos del Cargo Especial"
        ElseIf eBndBonifEsp = True Then
            eMonto = False
            Me.Text = "Catálogo de Bonificaciones Especiales"
            Me.Label1.Text = "Datos de la Bonificación Especial"
        End If

        If IdSistema <> "LO" And IdSistema <> "YU" Then
            Me.RadioButton1.Text = "Cobrados"
            Me.RadioButton2.Text = "No Cobrados "
        End If


        colorea(Me, Me.Name)
        SeCobro = 2
        Busca(0)
        CambiaSeleccion()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        LocOpCargosEsp = "N"
        FrmCargosEspeciales.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        If Me.DataGridView1.Rows.Count = 0 Then
            MsgBox("Selecciona un registro a modificar.", MsgBoxStyle.Information)
            Exit Sub
        End If
        'ValidaCargosEspeciales(Me.LabelClave.Text, eMonto)
        'If eRes = 1 Then
        '    MsgBox(eMsg, MsgBoxStyle.Information)
        'Else
        LocOpCargosEsp = "M"
        Locclv_cobro = CLng(Me.LabelClave.Text)
        eRealizado = Me.LabelSeCobro.Text
        FrmCargosEspeciales.Show()
        'End If


    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.DataGridView1.Rows.Count = 0 Then
            MsgBox("Selecciona un registro a consultar.", MsgBoxStyle.Information)
            Exit Sub
        End If

        LocOpCargosEsp = "C"
        Locclv_cobro = CLng(Me.LabelClave.Text)
        eRealizado = Me.LabelSeCobro.Text
        FrmCargosEspeciales.Show()

    End Sub



    Private Sub BuscaCargosEspeciales(ByVal Contrato As Long, ByVal Se_Cobro As Integer, ByVal Servicio As String, ByVal Monto As Boolean, ByVal Op As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        Me.LabelClave.Text = ""
        strSQL.Append("EXEC busca_cargos_especiales ")
        strSQL.Append(CStr(Contrato) & ", ")
        strSQL.Append(CStr(Se_Cobro) & ", ")
        strSQL.Append("'" & CStr(Servicio) & "', ")
        strSQL.Append(CStr(Monto) & ", ")
        strSQL.Append(CStr(Op))

        Try
            conexion.Open()
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.DataGridView1.DataSource = bindingSource
            conexion.Close()
            CambiaSeleccion()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub ValidaCargosEspeciales(ByVal Clv_Cobro As Long, ByVal Monto As Boolean)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Valida_cargos_especiales2", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Cobro", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Cobro
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Monto", SqlDbType.Bit)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Monto
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = ""
        comando.Parameters.Add(parametro4)

        Try
            eRes = 0
            eMsg = Nothing
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(parametro3.Value.ToString())
            eMsg = parametro4.Value.ToString()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub CambiaSeleccion()
        If Me.DataGridView1.Rows.Count > 0 Then
            Me.LabelClave.Text = Me.DataGridView1.SelectedCells.Item(0).Value.ToString()
            Me.LabelContrato.Text = Me.DataGridView1.SelectedCells.Item(2).Value.ToString()
            Me.LabelMonto.Text = Me.DataGridView1.SelectedCells.Item(3).Value.ToString()
            Me.LabelSeCobro.Text = Me.DataGridView1.SelectedCells.Item(4).Value.ToString()
            Me.LabelFecha.Text = Me.DataGridView1.SelectedCells.Item(5).Value.ToString()
            Me.LabelFechaF.Text = Me.DataGridView1.SelectedCells.Item(7).Value.ToString
        End If
    End Sub



    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        CambiaSeleccion()
    End Sub

    Private Sub LabelSeCobro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LabelSeCobro.Click

    End Sub

    Private Sub Button4_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        If Me.DataGridView1.Rows.Count = 0 Then
            MsgBox("Selecciona un registro a cancelar.", MsgBoxStyle.Information)
        Else
            If UCase(LabelSeCobro.Text) = UCase("No") Then
                cancelarBonificacion(Me.LabelClave.Text)
                Busca(0)
            Else
                MsgBox("Solo se puede cancelar si NO se ha Aplicado", MsgBoxStyle.Information)
            End If
        End If


    End Sub

    Private Sub cancelarBonificacion(ByVal clv_cobro As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("CANCELAR_BONIFICACION", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@CLV_COBRO", SqlDbType.Int, 250)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = clv_cobro
        comando.Parameters.Add(parametro)

        Dim parametro3 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            MsgBox(parametro3.Value.ToString())
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Dispose()
            conexion.Close()
        End Try

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    
End Class