﻿Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Public Class FrmDatosFiscalesMizar
    'Variables bitacora
    Private Razon_social As String = Nothing
    Private RFC As String = Nothing
    Private CURP As String = Nothing
    Private Calle As String = Nothing
    Private Numero As String = Nothing
    Private NumeroInt As String = Nothing
    Private entrecalles As String = Nothing
    Private CodigoPostal As String = Nothing
    Private Colonia As String = Nothing
    Private Ciudad As String = Nothing
    Private Estado As String = Nothing
    Private Pais As String = Nothing
    Private Telefono As String = Nothing
    Private Fax As String = Nothing
    Private Email As String = Nothing

    Private Sub FrmDatosFiscalesMizar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbUsoCFDI.Visible = False
        Label4.Visible = False
        BaseII.limpiaParametros()
        cbCodigoPostal.DataSource = BaseII.ConsultaDT("CodigosPostalesHL")

        'BaseII.limpiaParametros()
        'cbUsoCFDI.DataSource = BaseII.ConsultaDT("UsosCFDIHL")

        If OpcionCli = "C" Then
            Me.Panel1.Enabled = False
            Button1.Enabled = False
            Button2.Enabled = False
        ElseIf OpcionCli = "M" Then
            Me.Panel1.Enabled = True
            Button1.Enabled = True
            Button2.Enabled = True
        ElseIf OpcionCli = "N" Then
            Me.Panel1.Enabled = True
            Button1.Enabled = True
            Button2.Enabled = False
        End If
        If IsNumeric(GloContrato) = True Then
            Me.ContratoTextBox.Text = GloContrato
            Me.IVADESGLOSADOTextBox.Text = 1
        End If
        If OpcionCli = "M" Or OpcionCli = "C" Then
            Busca()
        End If

        ''Variables para la bitacora
        Razon_social = Me.RAZON_SOCIALTextBox.Text
        RFC = Me.RFCTextBox.Text
        Calle = Me.CALLE_RSTextBox.Text
        Numero = Me.NUMERO_RSTextBox.Text
        NumeroInt = Me.NUMERO_intTextBox.Text
        entrecalles = Me.ENTRECALLESTextBox.Text
        CodigoPostal = Me.cbCodigoPostal.SelectedValue
        Colonia = Me.cbColonia.SelectedValue
        Ciudad = Me.cbCiudad.SelectedValue
        Estado = Me.cbEstado.SelectedValue
        Pais = Me.cbPais.SelectedValue
        Telefono = Me.TELEFONO_RSTextBox.Text
        Fax = Me.FAX_RSTextBox.Text
        Email = Me.EMail_TextBox.Text

    End Sub

    Private Sub Busca()

        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContrato)
            DT = BaseII.ConsultaDT("CONDatosFiscales")
            If DT.Rows.Count > 0 Then


                ContratoTextBox.Text = DT.Rows(0)(0).ToString
                IVADESGLOSADOTextBox.Text = DT.Rows(0)(1).ToString
                RAZON_SOCIALTextBox.Text = DT.Rows(0)(2).ToString
                RFCTextBox.Text = DT.Rows(0)(3).ToString
                CALLE_RSTextBox.Text = DT.Rows(0)(4).ToString
                NUMERO_RSTextBox.Text = DT.Rows(0)(5).ToString
                ENTRECALLESTextBox.Text = DT.Rows(0)(6).ToString
                COLONIA_RSTextBox.Text = DT.Rows(0)(7).ToString
                CIUDAD_RSTextBox.Text = DT.Rows(0)(8).ToString
                ESTADO_RSTextBox.Text = DT.Rows(0)(9).ToString
                CP_RSTextBox.Text = DT.Rows(0)(10).ToString
                TELEFONO_RSTextBox.Text = DT.Rows(0)(11).ToString
                FAX_RSTextBox.Text = DT.Rows(0)(12).ToString
                'CURPTextBox.Text = 'DT.Rows(0)(15).ToString
                NUMERO_intTextBox.Text = DT.Rows(0)(16).ToString

                'cbUsoCFDI.SelectedValue = DT.Rows(0)(17).ToString

                cbCodigoPostal.SelectedValue = CP_RSTextBox.Text
                cbColonia.SelectedValue = COLONIA_RSTextBox.Text
            Else
                ContratoTextBox.Text = GloContrato
                IVADESGLOSADOTextBox.Text = "0"
                RAZON_SOCIALTextBox.Text = ""
                RFCTextBox.Text = ""
                CALLE_RSTextBox.Text = ""
                NUMERO_RSTextBox.Text = ""
                ENTRECALLESTextBox.Text = ""
                COLONIA_RSTextBox.Text = ""
                CIUDAD_RSTextBox.Text = ""
                ESTADO_RSTextBox.Text = ""
                CP_RSTextBox.Text = ""
                TELEFONO_RSTextBox.Text = ""
                FAX_RSTextBox.Text = ""
                'CURPTextBox.Text = ""
                NUMERO_intTextBox.Text = ""
            End If



            Consulta_Pais_Fiscal(GloContrato)
        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            'Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try


    End Sub

    Private Sub Consulta_Pais_Fiscal(ByVal contrato As Long)
        Dim CON01 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "Consulta_Pais_Fiscal"
                .CommandTimeout = 0
                .Connection = CON01
                .CommandType = CommandType.StoredProcedure

                Dim PRM As New SqlParameter("@contrato", SqlDbType.BigInt)
                PRM.Direction = ParameterDirection.Input
                PRM.Value = contrato
                .Parameters.Add(PRM)

                Dim reader As SqlDataReader = .ExecuteReader()

                While (reader.Read)
                    TxtPais.Text = reader.GetValue(1)
                    EMail_TextBox.Text = reader.GetValue(2)
                End While
            End With
            CON01.Close()
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            'Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        Dim oIdAsociado As Long = 0
        CON.Open()
        Try
            If Len(RAZON_SOCIALTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar la Razón Social")

                Exit Sub
            ElseIf Len(RFCTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar el RFC")
                Exit Sub
                'ElseIf Len(CURPTextBox.Text) = 0 Then
                '    'MsgBox("Favor de Agregar la CURP")
                '    Exit Sub
            ElseIf Len(CALLE_RSTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar la Calle")
                Exit Sub
            ElseIf Len(NUMERO_RSTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar el número ")
                Exit Sub
            ElseIf Len(cbColonia.Text) = 0 Then
                MsgBox("Favor de Agregar la Colonia ")
                Exit Sub
            ElseIf Len(cbCodigoPostal.Text) = 0 Then
                MsgBox("Favor de Agregar el Codigo Postal ")
                Exit Sub
            ElseIf Len(cbEstado.Text) = 0 Then
                MsgBox("Favor de Agregar el Estado")
                Exit Sub
            ElseIf Len(cbCiudad.Text) = 0 Then
                MsgBox("Favor de Agregar la Ciudad")
                Exit Sub
            ElseIf Len(cbPais.Text) = 0 Then
                MsgBox("Favor de Agregar el País")
                Exit Sub
                'ElseIf Len(cbUsoCFDI.Text) = 0 Then
                '    MsgBox("Favor de Agregar el Uso CFDI")
                '    Exit Sub
            End If
            oIdAsociado = 0
            oIdAsociado = UspEsNuevoRel_Cliente_AsociadoCFD(RFCTextBox.Text, MiConexion)
            'If oIdAsociado = 0 Then
            '    oIdAsociado = ClassCFDI.Nuevo_Asociado("Select Max(id_asociado) from asociados ")
            'End If
            'If oIdAsociado > 0 Then
            'ClassCFDI.Alta_Datosfiscales_NEW(CmbCompania.SelectedValue, Me.RAZON_SOCIALTextBox.Text, Me.RFCTextBox.Text, CALLE_RSTextBox.Text, CStr(oIdAsociado), CP_RSTextBox.Text, COLONIA_RSTextBox.Text, EMail_TextBox.Text, ENTRECALLESTextBox.Text, ESTADO_RSTextBox.Text, CStr(oIdAsociado), CIUDAD_RSTextBox.Text, CIUDAD_RSTextBox.Text, NUMERO_RSTextBox.Text, TxtPais.Text, TELEFONO_RSTextBox.Text, FAX_RSTextBox.Text, CURPTextBox.Text, ClassCFDI.GloContrato, ClassCFDI.MiConexion)
            MZ_SPASOCIOADOSMIZAR(Me.RAZON_SOCIALTextBox.Text, Me.RFCTextBox.Text, CALLE_RSTextBox.Text, CStr(oIdAsociado), cbCodigoPostal.Text, cbColonia.Text, EMail_TextBox.Text, ENTRECALLESTextBox.Text, cbEstado.Text, CStr(oIdAsociado), cbCiudad.Text, cbCiudad.Text, NUMERO_RSTextBox.Text, NUMERO_intTextBox.Text, cbPais.Text, TELEFONO_RSTextBox.Text, FAX_RSTextBox.Text, "", GloContrato, "No Especificado", MiConexion)
            'End If



            Me.Validate()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContrato)
            BaseII.CreateMyParameter("@IVADESGLOSADO", SqlDbType.TinyInt, IVADESGLOSADOTextBox.Text)
            BaseII.CreateMyParameter("@RAZON_SOCIAL", SqlDbType.VarChar, RAZON_SOCIALTextBox.Text, 150)
            BaseII.CreateMyParameter("@RFC", SqlDbType.VarChar, RFCTextBox.Text, 50)
            BaseII.CreateMyParameter("@CALLE_RS", SqlDbType.VarChar, CALLE_RSTextBox.Text, 150)
            BaseII.CreateMyParameter("@NUMERO_RS", SqlDbType.VarChar, NUMERO_RSTextBox.Text, 50)
            BaseII.CreateMyParameter("@ENTRECALLES", SqlDbType.VarChar, ENTRECALLESTextBox.Text, 150)
            BaseII.CreateMyParameter("@COLONIA_RS", SqlDbType.VarChar, cbColonia.Text, 150)
            BaseII.CreateMyParameter("@CIUDAD_RS", SqlDbType.VarChar, cbCiudad.Text, 150)
            BaseII.CreateMyParameter("@ESTADO_RS", SqlDbType.VarChar, cbEstado.Text, 150)
            BaseII.CreateMyParameter("@CP_RS", SqlDbType.VarChar, cbCodigoPostal.Text, 20)
            BaseII.CreateMyParameter("@TELEFONO_RS", SqlDbType.VarChar, TELEFONO_RSTextBox.Text, 50)
            BaseII.CreateMyParameter("@FAX_RS", SqlDbType.VarChar, FAX_RSTextBox.Text, 50)
            BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, "", 1)
            BaseII.CreateMyParameter("@CURP", SqlDbType.VarChar, "", 50)
            BaseII.CreateMyParameter("@NUMEROINT_RS", SqlDbType.VarChar, NUMERO_intTextBox.Text, 50)
            'BaseII.CreateMyParameter("@id_UsoCFDI", SqlDbType.VarChar, cbUsoCFDI.SelectedValue, 50)
            BaseII.Inserta("NUEDatosFiscales")

            Guarda_Rel_DatosFiscales_Pais(GloContrato, Me.cbPais.Text, EMail_TextBox.Text)

            'MsgBox(ClassCFDI.mensaje5)
            guardabitacora(GloContrato)
            Me.Close()

        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            'Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
        CON.Close()
    End Sub

    Public Shared Function UspEsNuevoRel_Cliente_AsociadoCFD(ByVal RFC As String, ByVal LocConexion As String) As Long
        UspEsNuevoRel_Cliente_AsociadoCFD = 0
        Dim CON01 As New SqlConnection(LocConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "UspEsNuevoRel_Cliente_AsociadoCFD"
                .CommandTimeout = 0
                .Connection = CON01
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                Dim PRM As New SqlParameter("@RFC", SqlDbType.VarChar, 50)
                PRM.Direction = ParameterDirection.Input
                PRM.Value = RFC
                .Parameters.Add(PRM)


                Dim reader As SqlDataReader = .ExecuteReader()

                While (reader.Read)
                    UspEsNuevoRel_Cliente_AsociadoCFD = reader.GetValue(0)
                    'EMail_TextBox.Text = reader.GetValue(2)
                End While
            End With

        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            'Llenalog(ex.Source.ToString & " " & ex.Message)
        Finally
            CON01.Close()
        End Try
    End Function

    Public Shared Function MZ_SPASOCIOADOSMIZAR(ByVal oRazonSocial As String,
       ByVal oRFC As String,
       ByVal oCalle As String,
       ByVal oClaveAsociado As String,
       ByVal oCodigoPostal As String,
       ByVal oColonia As String,
       ByVal oEMail As String,
       ByVal oEntreCalles As String,
       ByVal oEstado As String,
       ByVal oIdAsociado As String,
       ByVal oLocalidad As String,
       ByVal oMunicipio As String,
       ByVal oNumeroExterior As String,
       ByVal oNumeroInterior As String,
       ByVal oPais As String,
       ByVal oTel As String,
       ByVal oFax As String,
       ByVal oReferencia As String,
       ByVal oContrato As Long,
       ByVal ometodo_de_pago As String, ByVal locconexion As String) As String
        Try
            '@Contrato Bigint,
            '@BndError int OUTPUT,
            '@BndMsj varchar(800) OUTPUT,
            '@razon_social	varchar(255),
            '@RFC varchar(15),
            '@metodo_de_pago	varchar(30),
            '@calle	varchar(255),
            '@numero_exterior	varchar(15),
            '@numero_interior	varchar(15),
            '@entre_calles	varchar(255),
            '@colonia	varchar(255),
            '@localidad	varchar(255),
            '@referencia	varchar(255),
            '@municipio	varchar(255),
            '@estado	varchar(255),
            '@pais	varchar(255),
            '@codigo_postal	varchar(5),
            '@telefono	varchar(255),
            '@fax	varchar (255),
            '@email	varchar(255)
            Dim oNumError As Integer = 0

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, oContrato)
            'Ouput
            BaseII.CreateMyParameter("@BndError", ParameterDirection.Output, SqlDbType.Int)
            BaseII.CreateMyParameter("@BndMsj", ParameterDirection.Output, SqlDbType.VarChar, 800)

            BaseII.CreateMyParameter("@razon_social", SqlDbType.VarChar, 255, ParameterDirection.Input, oRazonSocial)
            BaseII.CreateMyParameter("@RFC", SqlDbType.VarChar, 15, ParameterDirection.Input, oRFC)
            BaseII.CreateMyParameter("@metodo_de_pago", SqlDbType.VarChar, 30, ParameterDirection.Input, ometodo_de_pago)
            BaseII.CreateMyParameter("@calle", SqlDbType.VarChar, 250, ParameterDirection.Input, oCalle)
            BaseII.CreateMyParameter("@numero_exterior", SqlDbType.VarChar, 250, ParameterDirection.Input, oNumeroExterior)
            BaseII.CreateMyParameter("@numero_interior", SqlDbType.VarChar, 250, ParameterDirection.Input, oNumeroInterior)
            BaseII.CreateMyParameter("@entre_calles", SqlDbType.VarChar, 250, ParameterDirection.Input, oEntreCalles)
            BaseII.CreateMyParameter("@colonia", SqlDbType.VarChar, 250, ParameterDirection.Input, oColonia)
            BaseII.CreateMyParameter("@localidad", SqlDbType.VarChar, 250, ParameterDirection.Input, oLocalidad)
            BaseII.CreateMyParameter("@referencia", SqlDbType.VarChar, 250, ParameterDirection.Input, oReferencia)
            BaseII.CreateMyParameter("@municipio", SqlDbType.VarChar, 250, ParameterDirection.Input, oMunicipio)
            BaseII.CreateMyParameter("@estado", SqlDbType.VarChar, 250, ParameterDirection.Input, oEstado)
            BaseII.CreateMyParameter("@pais", SqlDbType.VarChar, 250, ParameterDirection.Input, oPais)
            BaseII.CreateMyParameter("@codigo_postal", SqlDbType.VarChar, 250, ParameterDirection.Input, oCodigoPostal)
            BaseII.CreateMyParameter("@telefono", SqlDbType.VarChar, 250, ParameterDirection.Input, oTel)
            BaseII.CreateMyParameter("@fax", SqlDbType.VarChar, 250, ParameterDirection.Input, oFax)
            BaseII.CreateMyParameter("@email", SqlDbType.VarChar, 250, ParameterDirection.Input, oEMail)
            BaseII.ProcedimientoOutPut("MZ_SPASOCIOADOSMIZAR")
            oNumError = BaseII.dicoPar("@BndError").ToString()
            If oNumError = 0 Then
                MZ_SPASOCIOADOSMIZAR = ""
            Else
                MZ_SPASOCIOADOSMIZAR = BaseII.dicoPar("@BndMsj").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub Guarda_Rel_DatosFiscales_Pais(ByVal contrato As Long, ByVal Pais As String, ByVal oEmail As String)
        Dim CON01 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "Guarda_Rel_DatosFiscales_Pais"
                .Connection = CON01
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Value = contrato
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                prm = New SqlParameter("@Pais", SqlDbType.VarChar, 150)
                prm.Value = Pais
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                prm = New SqlParameter("@Email", SqlDbType.VarChar, 100)
                prm.Value = oEmail
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                Dim i As Integer = .ExecuteNonQuery()
            End With
            CON01.Close()
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            ' Llenalog(ex.Source.ToString & " " & ex.Message)
            'Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Private Sub guardabitacora(ByVal contrato As Integer)
        Try
            If OpcionCli = "M" Then
                'Razon Social'
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.RAZON_SOCIALTextBox.Name, Razon_social, Me.RAZON_SOCIALTextBox.Text, LocClv_Ciudad)
                'RFC
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.RFCTextBox.Name, RFC, Me.RFCTextBox.Text, LocClv_Ciudad)
                'CURP 
                'bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.CURPTextBox.Name, CURP, Me.CURPTextBox.Text, ClassCFDI.LocClv_Ciudad)
                'Calle
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.CALLE_RSTextBox.Name, Calle, Me.CALLE_RSTextBox.Text, LocClv_Ciudad)
                'Numero ext
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.NUMERO_RSTextBox.Name, Numero, Me.NUMERO_RSTextBox.Text, LocClv_Ciudad)
                'Numero int
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.NUMERO_intTextBox.Name, NumeroInt, Me.NUMERO_intTextBox.Text, LocClv_Ciudad)
                'entrecalles
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.ENTRECALLESTextBox.Name, entrecalles, Me.ENTRECALLESTextBox.Text, LocClv_Ciudad)
                'CodigoPostal
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.cbCodigoPostal.Name, CodigoPostal, Me.cbCodigoPostal.SelectedValue, LocClv_Ciudad)
                'Colonia
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.cbColonia.Name, Colonia, Me.cbColonia.SelectedValue, LocClv_Ciudad)
                'Ciudad
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.cbCiudad.Name, Ciudad, Me.cbCiudad.SelectedValue, LocClv_Ciudad)
                'Estado
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.cbEstado.Name, Estado, Me.cbEstado.SelectedValue, LocClv_Ciudad)
                'Pais
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.cbPais.Name, Pais, Me.cbPais.SelectedValue, LocClv_Ciudad)
                'Telefono
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.TELEFONO_RSTextBox.Name, Telefono, Me.TELEFONO_RSTextBox.Text, LocClv_Ciudad)
                'Fax
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.FAX_RSTextBox.Name, Fax, Me.FAX_RSTextBox.Text, LocClv_Ciudad)
                'Email
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, Me.EMail_TextBox.Name, Email, Me.EMail_TextBox.Text, LocClv_Ciudad)
            ElseIf OpcionCli = "N" Then
                bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, "Se Capturaron Los Datos Fiscales Del Cliente", "", "Se Capturaron Los Datos Fiscales Del Cliente", LocClv_Ciudad)
            End If
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            'Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Private Sub guardabitacoraBorrar(ByVal contrato As Integer)
        Try
            bitsist(GloUsuario, contrato, LocGloSistema, Me.Name, "Se Borraron Los Datos Fiscales Del Cliente", "", "Se Borraron Los Datos Fiscales Del Cliente", LocClv_Ciudad)
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            'Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try


            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, GloContrato)
            BaseII.Inserta("BORDatosFiscales")
            'MsgBox(ClassCFDI.mensaje6)
            UspBorrarDatosFiscalesPais()
            Me.TxtPais.Text = ""
            Borrar_Datosfiscales(RFCTextBox.Text, MiConexion)
            Usp_ED_BorraRel_Cliente_AsociadoCFD(GloContrato, MiConexion)
            guardabitacoraBorrar(GloContrato)
            Me.Close()
        Catch ex As Exception
            'ex.Source.ToString & " " & ex.Message
            'Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Private Sub UspBorrarDatosFiscalesPais()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, GloContrato)
            BaseII.Inserta("UspBorrarDatosFiscalesPais")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Sub Borrar_Datosfiscales(ByVal orfc As String, ByVal Locconexion As String)

        'Modificar
        'Dim DT As New DataTable
        'BaseII.limpiaParametros()
        'DT = BaseII.ConsultaDT("UspDameClvCompañia", Locconexion)

        'Dim oId_AsociadoLlave As Long = 0
        'Dim oIdDireccion As Long = 0
        'oId_AsociadoLlave = UspEsNuevoRel_Cliente_AsociadoCFD(orfc, Locconexion)
        '--oId_AsociadoLlave = Existe_DevuelveValor(" Select id_asociado from asociados  where id_asociado = " + oContrato)
        'oIdDireccion = 1
        'Guardamos la Dirreccion
        'If oId_AsociadoLlave > 0 Then
        '    Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
        '        Dim odirecciones As New MizarCFD.BRL.AsociadosDirecciones
        '        Dim otipo As New MizarCFD.BRL.AsociadosDirecciones.TipoDireccion
        '        odirecciones.Inicializar()
        '        odirecciones.IdDireccion = oIdDireccion
        '        odirecciones.IdAsociado = oId_AsociadoLlave
        '        If Not odirecciones.BorrarPorIdAsociado(Cnx) Then
        '            For Each oMensaje As DAMensajesSistema.RegistroMensaje In odirecciones.MensajesSistema.ListaMensajes
        '                MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
        '                oAlta_Datosfiscales = False
        '            Next
        '            Return
        '        End If
        '    End Using

        '    Dim oAsociados As MizarCFD.BRL.Asociados
        '    Using Cnx As New MizarCFD.DAL.DAConexion("HL", "sa", "sa")
        '        oAsociados = New MizarCFD.BRL.Asociados
        '        oAsociados.Inicializar()
        '        oAsociados.IdAsociado = oId_AsociadoLlave
        '        oAsociados.ClaveAsociado = oId_AsociadoLlave
        '        'oAsociados.IdCompania = CInt(DT.Rows(0)(0).ToString)
        '        'oAsociados.IdCompania = 1
        '        If oAsociados.Validar(Cnx, TipoAfectacionBD.Borrar) = True Then
        '            If Not oAsociados.Borrar(Cnx) Then
        '                For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
        '                    MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
        '                Next
        '                Return
        '            End If
        '        Else
        '            For Each oMensaje As DAMensajesSistema.RegistroMensaje In oAsociados.MensajesSistema.ListaMensajes
        '                MessageBox.Show(oMensaje.TextoMensaje + " " + oMensaje.Contexto)
        '            Next
        '            Return
        '        End If
        '    End Using
        'End If


    End Sub

    Public Shared Sub Usp_ED_BorraRel_Cliente_AsociadoCFD(ByVal oContrato As Long, ByVal LocConexion As String)
        Dim cON_x As New SqlConnection(LocConexion)
        Try
            'Regresa La Session

            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "Usp_ED_BorraRel_Cliente_AsociadoCFD"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                .Connection = cON_x
                Dim prm2 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = oContrato
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Sub

    Private Sub damedatosbitacora()
        Try
            If OpcionCli = "M" Then
                Razon_social = Me.RAZON_SOCIALTextBox.Text
                RFC = Me.RFCTextBox.Text
                CURP = "" 'Me.CURPTextBox.Text
                Calle = Me.CALLE_RSTextBox.Text
                Numero = Me.NUMERO_RSTextBox.Text
                entrecalles = Me.ENTRECALLESTextBox.Text
                Colonia = Me.COLONIA_RSTextBox.Text
                CodigoPostal = Me.CP_RSTextBox.Text
                Estado = Me.ESTADO_RSTextBox.Text
                Ciudad = Me.CIUDAD_RSTextBox.Text
                Telefono = Me.TELEFONO_RSTextBox.Text
                Fax = Me.FAX_RSTextBox.Text
                Pais = Me.TxtPais.Text
            End If
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            'Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub RFCTextBox_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles RFCTextBox.Validating
        If RFCTextBox.Text <> String.Empty Then
            If Regex.IsMatch(RFCTextBox.Text.Trim, "^([A-Z\s]{4})\d{6}([A-Z\w]{3})$") = False Then
                MsgBox("El Registro no es válido. El formato correcto es: LLLL######LL ó LLLL######LLL. L=Letra, #=Número.")
            End If
        End If
    End Sub

    Private Sub cbCodigoPostal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbCodigoPostal.SelectedIndexChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id_CodigoPostal", SqlDbType.VarChar, cbCodigoPostal.SelectedValue)
            cbColonia.DataSource = BaseII.ConsultaDT("ObtieneColoniasHL")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id_CodigoPostal", SqlDbType.VarChar, cbCodigoPostal.SelectedValue)
            cbEstado.DataSource = BaseII.ConsultaDT("ObtieneEstadosHL")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id_CodigoPostal", SqlDbType.VarChar, cbCodigoPostal.SelectedValue)
            BaseII.CreateMyParameter("@id_Estado", SqlDbType.VarChar, cbEstado.SelectedValue)
            cbCiudad.DataSource = BaseII.ConsultaDT("ObtieneMunicipiosHL")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@id_CodigoPostal", SqlDbType.VarChar, cbCodigoPostal.SelectedValue)
            cbPais.DataSource = BaseII.ConsultaDT("ObtienePaisesHL")
        Catch ex As Exception

        End Try
    End Sub
End Class