Imports System.Data.SqlClient
Imports System.Text

Public Class FrmCamServCte
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Dim eClv_ServicioOld As Integer = 0
    Dim eClv_ServicioNew As Integer = 0
    Dim eContratoAux As Long = 0
    Dim eContratoNet As Long = 0
    Dim eClv_TipSer As Integer = 0
    Private Bnd As Boolean = False


    Private Sub FrmCamServCte_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)

        If eContrato > 0 Then

            Me.TextBox1.Text = eContrato
            eContrato = 0
            CON.Open()
            Me.DameClientesActivosTableAdapter.Connection = CON
            Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
            CON.Close()

            If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "SA" Then
                DameServCliente(CLng(Me.TextBox1.Text), CInt(Me.ConceptoComboBox.SelectedValue))
            Else
                BuscarServCte()
            End If


        End If

    End Sub





    Private Sub FrmCamServCte_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)

        If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "SA" Then
            Me.GroupBox1.Visible = False
            Me.GroupBox2.Visible = False
            Me.GroupBoxActual.Visible = True
            Me.GroupBoxPosible.Visible = True
        End If

        colorea(Me, Me.Name)
        If eOpcion = "N" Then
            CON.Open()
            Me.MuestraTipServEricTableAdapter.Connection = CON
            Me.MuestraTipServEricTableAdapter.Fill(Me.DataSetEric.MuestraTipServEric, 0, 0)
            CON.Close()
        End If



    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        eContrato = 0
        BrwSelContrato.Show()
    End Sub

    Private Sub BuscarServCte()
        Dim CON As New SqlConnection(MiConexion)

        If Me.ConceptoComboBox.SelectedValue > 0 Then
            CON.Open()
            eRes = 0
            Me.DameServCteTableAdapter.Connection = CON
            Me.DameServCteTableAdapter.Fill(Me.DataSetEric.DameServCte, Me.TextBox1.Text, Me.ConceptoComboBox.SelectedValue, eRes, eMsg)
            CON.Close()
            If eRes = 1 Then
                MsgBox(eMsg)
            End If
        End If

    End Sub

    Private Sub BuscarServCtePosibles()
        Dim CON As New SqlConnection(MiConexion)
        If eRes = 1 Then
            eContrato = 0
            CON.Open()
            Me.DameServPosiblesCteTableAdapter.Connection = CON
            Me.DameServPosiblesCteTableAdapter.Fill(Me.DataSetEric.DameServPosiblesCte, 0, 0, 0)
            CON.Close()
            MsgBox(eMsg)
        Else
            CON.Open()
            Me.DameServPosiblesCteTableAdapter.Connection = CON
            Me.DameServPosiblesCteTableAdapter.Fill(Me.DataSetEric.DameServPosiblesCte, Me.ContratoNetTextBox.Text, Me.ConceptoComboBox.SelectedValue, Me.Clv_ServicioTextBox.Text)
            CON.Close()
        End If
    End Sub


    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)

        If IsNumeric(Me.TextBox1.Text) = True Then

            If Asc(e.KeyChar) = 13 Then
                eContratoAux = Me.TextBox1.Text
                CON.Open()
                Me.DameClientesActivosTableAdapter.Connection = CON
                Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, eContratoAux, "", "", "", "", 0)
                CON.Close()

                If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "SA" Then
                    DameServCliente(eContratoAux, CInt(Me.ConceptoComboBox.SelectedValue))
                Else
                    BuscarServCte()
                End If

            End If

        End If

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Bnd = True
        Me.Close()
    End Sub


    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Dim CON As New SqlConnection(MiConexion)

        Try
            If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "SA" Then
                If CInt(Me.DataGridViewActual.SelectedCells.Item(3).Value) = 0 Or CInt(Me.DataGridViewPosible.SelectedCells.Item(0).Value) = 0 Then
                    MsgBox("Selecciona el servicio a cambiar.")
                    Exit Sub
                End If
            Else
                If Me.DameServCteDataGridView.RowCount = 0 Or Me.DameServPosiblesCteDataGridView.RowCount = 0 Or IsNumeric(Me.ConceptoComboBox.SelectedValue) = False Then
                    MsgBox("Selecciona el servicio a cambiar.")
                    Exit Sub
                End If
            End If



            If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "SA" Then
                eClv_ServicioOld = CInt(Me.DataGridViewActual.SelectedCells.Item(3).Value)
                eClv_ServicioNew = CInt(Me.DataGridViewPosible.SelectedCells.Item(0).Value)
                eContratoAux = CLng(Me.DataGridViewActual.SelectedCells.Item(0).Value)
                eContratoNet = CLng(Me.DataGridViewActual.SelectedCells.Item(1).Value)
                eClv_TipSer = CInt(Me.DataGridViewActual.SelectedCells.Item(2).Value)
            Else
                eClv_ServicioOld = CInt(Me.Clv_ServicioTextBox.Text)
                eClv_ServicioNew = CInt(Me.Clv_ServicioTextBox1.Text)
                eContratoAux = CLng(Me.ContratoTextBox.Text)
                If IsNumeric(Me.ContratoNetTextBox.Text) = True Then eContratoNet = Me.ContratoNetTextBox.Text Else eContratoNet = 0
                eClv_TipSer = CInt(Me.ConceptoComboBox.SelectedValue)
            End If

            ValidaCambioServCliente(eContratoAux, eClv_TipSer, String.Empty)
            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Information)

                Exit Sub
            End If

            CambiaServCliente(eContratoAux, eContratoNet, eClv_TipSer, eClv_ServicioOld, eClv_ServicioNew)
            bitsist(GloUsuario, eContratoAux, LocGloSistema, Me.Text, "", "Cambio de Servicio de: " + CStr(eClv_ServicioOld) + " a " + CStr(eClv_ServicioNew), GloSucursal, LocClv_Ciudad)



        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    Private Sub ConceptoComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedIndexChanged


    End Sub

    Private Sub ContratoNetTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoNetTextBox.TextChanged
        BuscarServCtePosibles()
    End Sub

    Private Sub Clv_ServicioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ServicioTextBox.TextChanged
        BuscarServCtePosibles()
    End Sub

    Private Sub ValidaCambioServCliente(ByVal Contrato As Long, ByVal Clv_TipSer As Integer, ByVal Mac As String)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaCambioServCliente", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = Clv_TipSer
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Mac", SqlDbType.VarChar, 50)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Mac
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = String.Empty
        comando.Parameters.Add(parametro4)

        Try
            eRes = 0
            eMsg = String.Empty
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            eRes = CInt(parametro3.Value.ToString)
            eMsg = parametro4.Value.ToString

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub DameServCliente(ByVal Contrato As Long, ByVal Clv_TipSer As Integer)

        ValidaCambioServCliente(Contrato, Clv_TipSer, String.Empty)
        



        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC DameServClienteLogitel ")
        strSQL.Append(CStr(Contrato) & ", ")
        strSQL.Append(CStr(Clv_TipSer))

        Try
            conexion.Open()
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.DataGridViewActual.DataSource = bindingSource
            conexion.Close()

            Me.DataGridViewActual.Columns(0).Visible = False
            Me.DataGridViewActual.Columns(1).Visible = False
            Me.DataGridViewActual.Columns(2).Visible = False
            Me.DataGridViewActual.Columns(3).Visible = False
            Me.DataGridViewActual.Columns(4).Visible = False
            Me.DataGridViewActual.Columns(5).Width = 150
            Me.DataGridViewActual.Columns(6).Width = 150
            Me.DataGridViewActual.Columns(7).Width = 100

            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Information)
                DameServPosiblesCte(0, 0, 0)
            Else
                DameServPosiblesCte(Contrato, Clv_TipSer, Me.DataGridViewActual.Rows(0).Cells(3).Value.ToString())
            End If
            


        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub DameServPosiblesCte(ByVal Contrato As Long, ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC DameServPosiblesCteLogitel ")
        strSQL.Append(CStr(Contrato) & ", ")
        strSQL.Append(CStr(Clv_TipSer) & ", ")
        strSQL.Append(CStr(Clv_Servicio))

        Try
            conexion.Open()
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.DataGridViewPosible.DataSource = bindingSource
            conexion.Close()

            If Me.DataGridViewPosible.RowCount > 0 Then
                Me.DataGridViewPosible.Columns(0).Visible = False
                Me.DataGridViewPosible.Columns(1).Width = 150
                Me.DataGridViewPosible.Columns(2).Width = 100
            End If

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub CambiaServCliente(ByVal Contrato As Long, ByVal ContratoNet As Long, ByVal Clv_TipSer As Integer, ByVal Clv_ServOld As Integer, ByVal Clv_ServNew As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("CambiaServCliente", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = ContratoNet
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_TipSer
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_ServOld", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_ServOld
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Clv_ServNew", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Clv_ServNew
        comando.Parameters.Add(parametro5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            eContrato = 0
            MsgBox(mensaje5)
            Me.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub DataGridViewActual_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridViewActual.CellClick
        If Me.DataGridViewActual.Rows.Count > 0 Then
            eContratoAux = CLng(Me.DataGridViewActual.SelectedCells.Item(0).Value)
            eContratoNet = CLng(Me.DataGridViewActual.SelectedCells.Item(1).Value)
            eClv_TipSer = CInt(Me.DataGridViewActual.SelectedCells.Item(2).Value)
            DameServPosiblesCte(Me.TextBox1.Text, eClv_TipSer, CInt(Me.DataGridViewActual.SelectedCells.Item(3).Value))
        End If
    End Sub

    Private Sub DataGridViewActual_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridViewActual.CellContentClick
        If Me.DataGridViewActual.Rows.Count > 0 Then
            eContratoAux = CLng(Me.DataGridViewActual.SelectedCells.Item(0).Value)
            eContratoNet = CLng(Me.DataGridViewActual.SelectedCells.Item(1).Value)
            eClv_TipSer = CInt(Me.DataGridViewActual.SelectedCells.Item(2).Value)
            DameServPosiblesCte(Me.TextBox1.Text, eClv_TipSer, CInt(Me.DataGridViewActual.SelectedCells.Item(3).Value))
        End If
    End Sub



    Private Sub ConceptoComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ConceptoComboBox.SelectedValueChanged

        If Bnd = False Then

            Dim CON As New SqlConnection(MiConexion)

            If IsNumeric(Me.TextBox1.Text) = True Then
                CON.Open()
                Me.DameClientesActivosTableAdapter.Connection = CON
                Me.DameClientesActivosTableAdapter.Fill(Me.DataSetEric.DameClientesActivos, Me.TextBox1.Text, "", "", "", "", 0)
                CON.Close()
                If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "SA" Then
                    DameServCliente(CLng(Me.TextBox1.Text), CInt(Me.ConceptoComboBox.SelectedValue))
                Else
                    BuscarServCte()
                End If

            End If
        End If
    End Sub

    Private Sub TextBox1_ParentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.ParentChanged

    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub
End Class