Imports System.Data.SqlClient
Public Class FrmPaqueteAdicional
    Public eOp As Char = Nothing
    Public eRes As Integer = 0
    Public eMsg As String = Nothing
    Dim err As Integer
    Dim msg1 As String


    Private Sub Modo1()
        Me.Button1.Enabled = False
        Me.Button2.Enabled = False
        Me.Button3.Enabled = True
        Me.Button4.Enabled = False
        Me.Button5.Enabled = True
    End Sub
    Private Sub Modo2()
        Me.Button1.Enabled = True
        Me.Button2.Enabled = True
        Me.Button3.Enabled = False
        Me.Button4.Enabled = True
        Me.Button5.Enabled = False
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Modo1()
        Nuevo()
        eOp = "N"
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Modo1()
        Modificar()
        eOp = "M"
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Modo2()
        Guardar()
        Valida()
        eOp = "G"
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Modo2()
        Eliminar()
        Valida()
        eOp = "E"
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Modo2()
        Cancelar()
        eOp = "C"
    End Sub

    Private Sub Nuevo()
        Try
            Me.Panel2.Enabled = True
            Me.Panel3.Enabled = True
            Me.TextBox1.Text = ""
            Me.TextBox2.Text = ""
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Modificar()
        Try
            Me.Panel2.Enabled = False
            Me.Panel3.Enabled = True
            Me.TextBox1.Text = Me.ContratacionTextBox1.Text
            Me.TextBox2.Text = Me.MensualidadTextBox1.Text
            Me.ConDetPaqueteAdicionalDataGridView.Enabled = False
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Guardar()
        Try

            Me.Panel2.Enabled = False
            Me.Panel3.Enabled = False
            eRes = 0
            eMsg = Nothing

            If Me.TextBox1.Text.Length = 0 Then
                Me.TextBox1.Text = 0
            End If
            If Me.TextBox2.Text.Length = 0 Then
                Me.TextBox2.Text = 0
            End If

            If eOp = "N" Then
                Dim CON5 As New SqlConnection(MiConexion)
                CON5.Open()
                Me.NueDetPaqueteAdicionalTableAdapter.Connection = CON5
                Me.NueDetPaqueteAdicionalTableAdapter.Fill(Me.DataSetEric2.NueDetPaqueteAdicional, eClv_PaqAdi, CInt(Me.DescripcionComboBox1.SelectedValue), Me.TextBox1.Text, Me.TextBox2.Text, eRes, eMsg)
                CON5.Close()
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    eBndPaqAdi = True
                    ConsultarDet()
                End If

            ElseIf eOp = "M" Then
                Dim CON6 As New SqlConnection(MiConexion)
                CON6.Open()
                Me.ModDetPaqueteAdicionalTableAdapter.Connection = CON6
                Me.ModDetPaqueteAdicionalTableAdapter.Fill(Me.DataSetEric2.ModDetPaqueteAdicional, CInt(Me.ClaveTextBox.Text), eClv_PaqAdi, CInt(Me.DescripcionComboBox1.SelectedValue), Me.TextBox1.Text, Me.TextBox2.Text)
                CON6.Close()
                eBndPaqAdi = True
                ConsultarDet()
            End If
            Me.TextBox1.Text = Nothing
            Me.TextBox2.Text = Nothing
            Me.ConDetPaqueteAdicionalDataGridView.Enabled = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Eliminar()
        Try
            Me.Panel2.Enabled = False
            Me.Panel3.Enabled = False
            Dim CON8 As New SqlConnection(MiConexion)
            CON8.Open()
            Me.BorDetPaqueteActivadoTableAdapter.Connection = CON8
            Me.BorDetPaqueteActivadoTableAdapter.Fill(Me.DataSetEric2.BorDetPaqueteActivado, CInt(Me.ClaveTextBox.Text))
            CON8.Close()
            ConsultarDet()
            Me.TextBox1.Text = ""
            Me.TextBox2.Text = ""
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub Cancelar()
        Try
            Me.Panel2.Enabled = False
            Me.Panel3.Enabled = False
            Me.ConDetPaqueteAdicionalDataGridView.Enabled = True
            Me.TextBox1.Text = ""
            Me.TextBox2.Text = ""
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub ConPaqueteAdicionalBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConPaqueteAdicionalBindingNavigatorSaveItem.Click
        Try

            If Me.PaqueteAdicionalTextBox.Text.Length = 0 Then
                MsgBox("Captura el Nombre del Paquete Adicional.", , "Atenci�n")
                Exit Sub
            End If
            If Me.NumeroTextBox.Text.Length = 0 Then
                Me.NumeroTextBox.Text = 0
            End If
            If Me.ContratacionTextBox.Text.Length = 0 Then
                Me.ContratacionTextBox.Text = 0
            End If
            If IsNumeric(Me.TextBox3.Text) = False Then
                Me.TextBox3.Text = 0
            End If

            If eOpcion = "M" Then
                Dim CON2 As New SqlConnection(MiConexion)
                CON2.Open()
                Me.ModPaqueteAdicionalTableAdapter.Connection = CON2
                Me.ModPaqueteAdicionalTableAdapter.Fill(Me.DataSetEric2.ModPaqueteAdicional, eClv_PaqAdi, CInt(Me.DescripcionComboBox.SelectedValue), Me.PaqueteAdicionalTextBox.Text, CInt(Me.NumeroTextBox.Text), Me.TextBox3.Text, Me.NumericUpDown1.Value)
                CON2.Close()
                MsgBox(mensaje5)
                eBndPaqAdi = True
                Me.Close()
            End If

            If eOpcion = "N" Then
                Dim CON2 As New SqlConnection(MiConexion)
                CON2.Open()
                Me.NuePaqueteAdicionalTableAdapter.Connection = CON2
                Me.NuePaqueteAdicionalTableAdapter.Fill(Me.DataSetEric2.NuePaqueteAdicional, CInt(Me.DescripcionComboBox.SelectedValue), Me.PaqueteAdicionalTextBox.Text, CInt(Me.NumeroTextBox.Text), Me.TextBox3.Text, Me.NumericUpDown1.Value, eClv_PaqAdi)
                CON2.Close()
                MsgBox(mensaje5)
                eBndPaqAdi = True
                Me.Clv_PaqAdiTextBox.Text = eClv_PaqAdi
                eOpcion = "M"
                Me.Panel1.Enabled = True
                Me.Panel2.Enabled = False
                Me.Panel3.Enabled = False
                Me.BindingNavigatorDeleteItem.Enabled = True
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Try
            'Dim CON4 As New SqlConnection(MiConexion)
            'CON4.Open()
            'Me.BorPaqueteAdicionalTableAdapter.Connection = CON4
            'Me.BorPaqueteAdicionalTableAdapter.Fill(Me.DataSetEric2.BorPaqueteAdicional, eClv_PaqAdi)
            'CON4.Close()
            BorPaqueteAdicional_1(eClv_PaqAdi)
            If err = 1 Then
                Exit Sub
            End If
            MsgBox(mensaje6)
            eBndPaqAdi = True
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub FrmPaqueteAdicional_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            colorea(Me, Me.Name)
            If eOpcion = "N" Then
                Me.Panel1.Enabled = False
                Me.BindingNavigatorDeleteItem.Enabled = False
                Dim CON7 As New SqlConnection(MiConexion)
                CON7.Open()
                Me.MuestraTipo_Paquetes_AdicionalesTableAdapter.Connection = CON7
                Me.MuestraTipo_Paquetes_AdicionalesTableAdapter.Fill(Me.DataSetEric2.MuestraTipo_Paquetes_Adicionales, 0, 0)
                Me.MuestraServiciosEricTableAdapter.Connection = CON7
                Me.MuestraServiciosEricTableAdapter.Fill(Me.DataSetEric2.MuestraServiciosEric, 5, 0, 0)
                CON7.Close()
            End If
            If eOpcion = "C" Then
                Me.Panel1.Enabled = False
                Me.Panel4.Enabled = False
                Me.ConPaqueteAdicionalBindingNavigator.Enabled = False
                Consultar()
                Valida()
            End If
            If eOpcion = "M" Then
                Me.Panel2.Enabled = False
                Me.Panel3.Enabled = False
                Consultar()
                Valida()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Consultar()
        Try
            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            Me.ConPaqueteAdicionalTableAdapter.Connection = CON3
            Me.ConPaqueteAdicionalTableAdapter.Fill(Me.DataSetEric2.ConPaqueteAdicional, eClv_PaqAdi, 0, "", 3)
            Me.ConDetPaqueteAdicionalTableAdapter.Connection = CON3
            Me.ConDetPaqueteAdicionalTableAdapter.Fill(Me.DataSetEric2.ConDetPaqueteAdicional, eClv_PaqAdi, 0)
            Me.MuestraTipo_Paquetes_AdicionalesTableAdapter.Connection = CON3
            Me.MuestraTipo_Paquetes_AdicionalesTableAdapter.Fill(Me.DataSetEric2.MuestraTipo_Paquetes_Adicionales, CInt(Me.Clv_Tipo_Paquete_AdiocionalTextBox.Text), 1)
            Me.MuestraServiciosEricTableAdapter.Connection = CON3
            Me.MuestraServiciosEricTableAdapter.Fill(Me.DataSetEric2.MuestraServiciosEric, 5, 0, 0)
            CON3.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ConsultarDet()
        Try
            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()

            Me.ConDetPaqueteAdicionalTableAdapter.Connection = CON3
            Me.ConDetPaqueteAdicionalTableAdapter.Fill(Me.DataSetEric2.ConDetPaqueteAdicional, eClv_PaqAdi, 0)

            CON3.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub




    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox2, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub Valida()
        If Me.ConDetPaqueteAdicionalDataGridView.RowCount = 0 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        Else
            Me.Button2.Enabled = True
            Me.Button4.Enabled = True
        End If
    End Sub

    Private Sub Tipo_CobroTextBox_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Tipo_CobroTextBox.TextChanged
        If Tipo_CobroTextBox.Text = 1 Then
            Me.Label1.Text = "Minutos:"
        ElseIf Tipo_CobroTextBox.Text = 2 Then
            Me.Label1.Text = "Eventos:"
        Else
            Me.Label1.Text = "Minutos/Eventos:"
        End If
    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub


    Private Sub DescripcionLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub BorPaqueteAdicional_1(ByVal Clv_PaqAdi As Integer)

        Dim CON As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("BorPaqueteAdicional_1", CON)

        com.CommandType = CommandType.StoredProcedure

        Dim par As New SqlParameter("@Clv_PaqAdi", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_PaqAdi
        com.Parameters.Add(par)

        Dim par2 As New SqlParameter("@ERROR", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@msg", SqlDbType.VarChar, 250)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)

        Try
            CON.Open()
            com.ExecuteNonQuery()

            err = par2.Value
            msg1 = CStr(par3.Value.ToString)

            If err = 1 Then
                MsgBox(msg1)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

    End Sub
End Class