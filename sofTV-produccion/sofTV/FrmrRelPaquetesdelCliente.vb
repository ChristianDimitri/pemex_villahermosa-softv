Imports System.Data.SqlClient

Public Class FrmrRelPaquetesdelCliente
    Private eContratoNet As Long = 0
    Dim MTablas As String = "PaquetesGallery_Tel"
    Dim Mvalues As String = "Id_tel,Clv_Txt, Descripcion, Bnd"
    Dim MValores As String = ""
    Dim MCondicion As String = ""
    Dim MTipMov As Integer = 0
    Dim MBNDIDENTITY As Boolean = False
    Dim MClv_ID As Long = 0
    Dim bndterminar As Boolean = False
    Dim GloClvSerDig As Integer = 0
    Dim GloClvSerDig1 As Integer = 0
    Dim bnd As Long = 0


    Public Sub CREAARBOL()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim mCONTRATONET As Long, MStatus As String, MOp As Integer, MCLV_ORDEN As Long, MCLAVE As Long
            Dim I As Integer = 0
            Dim X As Integer = 0
            Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.Connection = CON
            Me.MUESTRACABLEMODEMSDELCLI_porOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcion, New System.Nullable(Of Long)(CType(Contrato, Long)), "P", 14)
            Dim FilaRow As DataRow
            Dim FilacontNet As DataRow
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcion.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                Me.TreeView1.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                Me.TreeView1.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                Me.TreeView1.Nodes(I).ForeColor = Color.Black

                If GLOTRABAJO = "IPAQU" Or GLOTRABAJO = "IPAQUT" Then
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 1, gloClv_Orden, GloDetClave)

                    mCONTRATONET = Trim(FilaRow("CONTRATONET").ToString())
                    MStatus = ""
                    MOp = 1
                    MCLV_ORDEN = gloClv_Orden
                    MCLAVE = GloDetClave
                ElseIf GLOTRABAJO = "ASDIG" Then
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "C", 50, gloClv_Orden, GloDetClave)
                    ' , I)
                    mCONTRATONET = Trim(FilaRow("CONTRATONET").ToString())
                    MStatus = "C"
                    MOp = 50
                    MCLV_ORDEN = gloClv_Orden
                    MCLAVE = GloDetClave
                ElseIf GLOTRABAJO = "DSDIG" Then
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "I", 50, gloClv_Orden, GloDetClave)
                    mCONTRATONET = Trim(FilaRow("CONTRATONET").ToString())
                    MStatus = "I"
                    MOp = 50
                    MCLV_ORDEN = gloClv_Orden
                    MCLAVE = GloDetClave
                ElseIf GLOTRABAJO = "RSDIG" Then
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "D", 50, gloClv_Orden, GloDetClave)
                    '
                    mCONTRATONET = Trim(FilaRow("CONTRATONET").ToString())
                    MStatus = "D"
                    MOp = 50
                    MCLV_ORDEN = gloClv_Orden
                    MCLAVE = GloDetClave
                ElseIf GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Or GLOTRABAJO = "BPAAD" Or GLOTRABAJO = "BSEDI" Then
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 10, gloClv_Orden, GloDetClave)
                    ', I)
                    mCONTRATONET = Trim(FilaRow("CONTRATONET").ToString())
                    MStatus = ""
                    MOp = 10
                    MCLV_ORDEN = gloClv_Orden
                    MCLAVE = GloDetClave
                ElseIf GLOTRABAJO = "DPAQU" Or GLOTRABAJO = "DPAQT" Then
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 20, gloClv_Orden, GloDetClave)
                    '
                    mCONTRATONET = Trim(FilaRow("CONTRATONET").ToString())
                    MStatus = ""
                    MOp = 20
                    MCLV_ORDEN = gloClv_Orden
                    MCLAVE = GloDetClave
                ElseIf GLOTRABAJO = "RPAQU" Or GLOTRABAJO = "RPAQT" Then
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                    'Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 21, gloClv_Orden, GloDetClave)

                    mCONTRATONET = Trim(FilaRow("CONTRATONET").ToString())
                    MStatus = ""
                    MOp = 21
                    MCLV_ORDEN = gloClv_Orden
                    MCLAVE = GloDetClave
                End If
                'For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion.Rows
                '    Me.TreeView1.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " : " & Trim(FilacontNet("STATUS").ToString()))
                '    Me.TreeView1.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                '    If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                '        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Olive
                '    ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                '        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Navy
                '    Else
                '        Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Red
                '    End If
                '    X += 1
                'Next
                Dim conn As New SqlConnection(MiConexion)
                Try
                    'MUESTRACONTNET_PorOpcion](@CONTRATONET BIGINT,@Status varchar(1),@Op int,@CLV_ORDEN BIGINT,@CLAVE BIGINT)

                    Dim reader As SqlDataReader
                    Dim cmd As New SqlCommand("MUESTRACONTNET_PorOpcion", conn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add("@CONTRATONET", SqlDbType.BigInt).Value = mCONTRATONET
                    cmd.Parameters.Add("@Status", SqlDbType.VarChar, 1).Value = MStatus
                    cmd.Parameters.Add("@Op", SqlDbType.Int).Value = MOp
                    cmd.Parameters.Add("@CLV_ORDEN", SqlDbType.BigInt).Value = MCLV_ORDEN
                    cmd.Parameters.Add("@CLAVE", SqlDbType.BigInt).Value = MCLAVE
                    conn.Open()
                    reader = cmd.ExecuteReader()

                    Using reader
                        While reader.Read
                            'Me.Id_Tel.Text = reader.GetValue(0)
                            'Me.Clave.Text = reader.GetValue(1)
                            'Me.Descripcion.Text = reader.GetValue(2)

                            Me.TreeView1.Nodes(I).Nodes.Add(Trim(reader.GetValue(0).ToString()), Trim(reader.GetValue(1).ToString()) & " : " & Trim(reader.GetValue(2).ToString()))
                            Me.TreeView1.Nodes(I).Nodes(X).Tag = Trim(reader.GetValue(0).ToString()) & "|" & Trim(reader.GetValue(3).ToString())
                            If Trim(reader.GetValue(2).ToString()) = "Suspendido" Then
                                Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Olive
                            ElseIf Trim(reader.GetValue(2).ToString()) = "Instalado" Or Trim(reader.GetValue(2).ToString()) = "Contratado" Then
                                Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Navy
                            Else
                                Me.TreeView1.Nodes(I).Nodes(X).ForeColor = Color.Red
                            End If
                            X += 1


                        End While
                    End Using
                    conn.Close()
                Catch ex As Exception
                    If conn.State <> ConnectionState.Closed Then conn.Close()
                    MsgBox(ex.Message)
                End Try
                I += 1
            Next
            CON.Close()
            Me.TreeView1.ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Sub CREAARBOL2()
        Try
            Dim CON As New SqlConnection(MiConexion)
            Dim mCONTRATONET As Long, MStatus As String, MOp As Integer, MCLV_ORDEN As Long, MCLAVE As Long
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            Me.MUESTRAIPAQU_porSOLTableAdapter.Connection = CON
            Me.MUESTRAIPAQU_porSOLTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAIPAQU_porSOL, GloDetClave, gloClv_Orden, 0)
            Dim FilaRow As DataRow
            Dim FilacontNet As DataRow
            Me.TreeView2.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.MUESTRAIPAQU_porSOL.Rows
                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                Me.TreeView2.Nodes(I).ForeColor = Color.Black

                'Me.MUESTRACONTNET_PorOpcionTableAdapter.Connection = CON
                'Me.MUESTRACONTNET_PorOpcionTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)), "", 2, gloClv_Orden, GloDetClave)
                mCONTRATONET = Trim(FilaRow("CONTRATONET").ToString())
                MStatus = ""
                MOp = 2
                MCLV_ORDEN = gloClv_Orden
                MCLAVE = GloDetClave
                'For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTNET_PorOpcion.Rows
                '    Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & " : " & Trim(FilacontNet("STATUS").ToString()))
                '    Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                '    If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then

                '        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                '    ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                '        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                '    Else
                '        Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                '    End If
                '    X += 1
                'Next
                '''''Prueba
                Dim conn As New SqlConnection(MiConexion)
                Try
                    'MUESTRACONTNET_PorOpcion](@CONTRATONET BIGINT,@Status varchar(1),@Op int,@CLV_ORDEN BIGINT,@CLAVE BIGINT)

                    Dim reader As SqlDataReader
                    Dim cmd As New SqlCommand("MUESTRACONTNET_PorOpcion", conn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add("@CONTRATONET", SqlDbType.BigInt).Value = mCONTRATONET
                    cmd.Parameters.Add("@Status", SqlDbType.VarChar, 1).Value = MStatus
                    cmd.Parameters.Add("@Op", SqlDbType.Int).Value = MOp
                    cmd.Parameters.Add("@CLV_ORDEN", SqlDbType.BigInt).Value = MCLV_ORDEN
                    cmd.Parameters.Add("@CLAVE", SqlDbType.BigInt).Value = MCLAVE
                    conn.Open()
                    reader = cmd.ExecuteReader()

                    Using reader
                        While reader.Read
                            'Me.Id_Tel.Text = reader.GetValue(0)
                            'Me.Clave.Text = reader.GetValue(1)
                            'Me.Descripcion.Text = reader.GetValue(2)

                            Me.TreeView2.Nodes(I).Nodes.Add(Trim(reader.GetValue(0).ToString()), Trim(reader.GetValue(1).ToString()) & " : " & Trim(reader.GetValue(2).ToString()))
                            Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(reader.GetValue(0).ToString()) & "|" & Trim(reader.GetValue(3).ToString())
                            If Trim(reader.GetValue(2).ToString()) = "Suspendido" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                            ElseIf Trim(reader.GetValue(2).ToString()) = "Instalado" Or Trim(reader.GetValue(2).ToString()) = "Contratado" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                            Else
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                            End If
                            X += 1


                        End While
                    End Using
                    conn.Close()
                Catch ex As Exception
                    If conn.State <> ConnectionState.Closed Then conn.Close()
                    MsgBox(ex.Message)
                End Try
                '''''Prueba


                I += 1
            Next
            CON.Close()
            Me.TreeView2.ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If Me.TreeView2.Nodes.Count = 0 Then
            MsgBox("Asigne Primero El Servicio", MsgBoxStyle.Information)
            Exit Sub
        Else
            Me.Close()
        End If
        
    End Sub

    Private Sub FrmrRelPaquetesdelCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
        CON.Close()
        GloBndTrabajo = True
        GloBloqueaDetalle = True
    End Sub

    Private Sub FrmrRelPaquetesdelCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CREAARBOL()
        Me.CREAARBOL2()
        If GLOTRABAJO = "IPAQU" Then
            Me.Text = "Instalación de Servicios de Internet"
            Me.Label3.Text = "Servicios de internet Pendientes de Instalar"
            Me.Label4.Text = "Instalar estos Servicios de Internet"
        ElseIf GLOTRABAJO = "BPAQU" Then
            Me.Text = "Baja de Servicios de Internet"
            Me.Label3.Text = "Servicios de internet Activos"
            Me.Label4.Text = "Pasar a Baja estos Servicios de Internet"
        ElseIf GLOTRABAJO = "BPAQT" Then
            Me.Text = "Baja de Servicios de Telefonia"
            Me.Label3.Text = "Servicios de Telefonia Activos"
            Me.Label4.Text = "Pasar a Baja estos Servicios de Telefonia"
        ElseIf GLOTRABAJO = "IPAQUT" Then
            Me.Text = "Instalación de Servicios de Telefonia"
            Me.Label3.Text = "Servicios de internet Pendientes de Telefonia"
            Me.Label4.Text = "Instalar estos Servicios de Telefonia"
        ElseIf GLOTRABAJO = "RPAQT" Then
            Me.Text = "Reconexión de Servicios de Telefonia"
            Me.Label3.Text = "Servicios de Telefonia Suspendidos"
            Me.Label4.Text = "Activar estos Servicios de Telefonia"
        ElseIf GLOTRABAJO = "RPAQU" Then
            Me.Text = "Reconexión de Servicios de Internet"
            Me.Label3.Text = "Servicios de internet Suspendidos"
            Me.Label4.Text = "Activar estos Servicios de Internet"
        ElseIf GLOTRABAJO = "DPAQT" Then
            Me.Text = "Desconexión de Servicios de Telefonia"
            Me.Label3.Text = "Servicios de Telefonia Activos"
            Me.Label4.Text = "Suspender estos Servicios de Telefonia"
        ElseIf GLOTRABAJO = "DPAQU" Then
            Me.Text = "Desconexión de Servicios de Internet"
            Me.Label3.Text = "Servicios de internet Activos"
            Me.Label4.Text = "Suspender estos Servicios de Internet"
        ElseIf GLOTRABAJO = "DSDIG" Then
            Me.Text = "Desconexión de Servicios Digitales"
            Me.Label3.Text = "Servicios Digitales Activos"
            Me.Label4.Text = "Suspender estos Servicios Digitales"
        ElseIf GLOTRABAJO = "ASDIG" Then
            Me.Text = "Instalación de Servicios de Digitales"
            Me.Label3.Text = "Servicios Digitales Contratados"
            Me.Label4.Text = "Instalar estos Servicios Digitales"
        ElseIf GLOTRABAJO = "RSDIG" Then
            Me.Text = "Reconexión de Servicios de Digitales"
            Me.Label3.Text = "Servicios Digitales Desconectados"
            Me.Label4.Text = "Reconexión de estos Servicios Digitales"
        ElseIf GLOTRABAJO = "BPAAD" Then
            Me.Text = "Baja de Paquetes Adicionales"
            Me.Label3.Text = "Paquetes Adicionales Activos"
            Me.Label4.Text = "Pasar a Baja estos Paquetes Adicionales"
        ElseIf GLOTRABAJO = "BSEDI" Then
            Me.Text = "Baja de Servicios Digitales"
            Me.Label3.Text = "Servicios Digitales Activos"
            Me.Label4.Text = "Pasar a Baja estos Servicios Digitales"
        End If
        If Bloquea = True Or opcion = "M" Then
            Me.Button1.Enabled = False
            Me.Button2.Enabled = False
            Me.Button3.Enabled = False
            Me.Button4.Enabled = False
        End If
    End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        Try
            Dim a As Integer
            Dim b As Integer
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet.Text = e.Node.Tag
                    MacCableModem.Text = e.Node.Text
                    Clv_Unicanet.Text = 0
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))

                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            Else
                If Len(e.Node.Tag) > 0 Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    a = InStr(1, Trim(e.Node.Tag), "|", vbTextCompare)
                    If a > 0 Then
                        b = InStr(a + 1, Trim(e.Node.Tag), "|", vbTextCompare)
                        Clv_Unicanet.Text = Mid(Trim(e.Node.Tag), 1, a - 1)
                        GloClvSerDig = Mid(Trim(e.Node.Tag), a + 1, Len(Trim(Trim(e.Node.Tag))))
                    Else
                        Clv_Unicanet.Text = e.Node.Tag
                    End If

                    'Clv_Unicanet.Text = e.Node.Tag
                    'If IsNumeric(Me.Contratonet.Text) = True Then
                    If eClv_TipSer = 5 Then
                        DameContratoNet2(CLng(Me.Clv_Unicanet.Text), eClv_TipSer)
                        Me.Contratonet.Text = eContratoNet
                    Else
                        Dim CON As New SqlConnection(MiConexion)
                        eContratoNet = 0
                        CON.Open()
                        Me.DameContratoNetTableAdapter.Connection = CON
                        Me.DameContratoNetTableAdapter.Fill(Me.NewSofTvDataSet.DameContratoNet, Me.Clv_Unicanet.Text, eContratoNet)
                        CON.Close()
                        Me.Contratonet.Text = eContratoNet
                    End If
                    'End If

                    'MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub INSERCION(ByVal MClave As Long, ByVal MClv_Orden As Long, ByVal MContratonet As Long, ByVal MClv_Unicanet As Long, ByVal Mop As Integer, ByVal MStatus As String, ByVal MClv_Servicio As Integer)
        Dim conn As New SqlConnection(MiConexion)
        Try
            '@Clave BIGINT, @Clv_Orden BIGINT, @Contratonet BIGINT,@Clv_Unicanet BIGINT,@op int,@Status varchar(1)
            bndterminar = False

            Dim cmd As New SqlCommand("NUEIPAQU_SOL", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim prm1 As New SqlParameter("@Clave", SqlDbType.BigInt)
            prm1.Direction = ParameterDirection.Input
            prm1.Value = MClave
            cmd.Parameters.Add(prm1)

            Dim prm2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
            prm2.Direction = ParameterDirection.Input
            prm2.Value = MClv_Orden
            cmd.Parameters.Add(prm2)

            Dim prm3 As New SqlParameter("@Contratonet", SqlDbType.BigInt)
            prm3.Direction = ParameterDirection.Input
            prm3.Value = MContratonet
            cmd.Parameters.Add(prm3)

            Dim prm4 As New SqlParameter("@Clv_Unicanet", SqlDbType.BigInt)
            prm4.Direction = ParameterDirection.Input
            prm4.Value = MClv_Unicanet
            cmd.Parameters.Add(prm4)

            Dim prm5 As New SqlParameter("@op", SqlDbType.Int)
            prm5.Direction = ParameterDirection.Input
            prm5.Value = Mop
            cmd.Parameters.Add(prm5)

            Dim prm6 As New SqlParameter("@Status", SqlDbType.VarChar, 1)
            prm6.Direction = ParameterDirection.Input

            prm6.Value = MStatus

            cmd.Parameters.Add(prm6)
            Dim prm7 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
            prm7.Direction = ParameterDirection.Input
            prm7.Value = MClv_Servicio
            cmd.Parameters.Add(prm7)


            conn.Open()
            Dim i As Integer = cmd.ExecuteNonQuery
            conn.Close()

        Catch ex As Exception
            bndterminar = True
            If conn.State <> ConnectionState.Closed Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub


    


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open() '''''
            If IsNumeric(Me.Clv_Unicanet.Text) = False Then Me.Clv_Unicanet.Text = 0
            If Me.Clv_Unicanet.Text > 0 Then
                If GLOTRABAJO = "IPAQU" Or GLOTRABAJO = "IPAQUT" Or GLOTRABAJO = "ASDIG" Then
                    'Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    'Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "I")
                    INSERCION(GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "I", GloClvSerDig)
                ElseIf GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Or GLOTRABAJO = "BPAAD" Or GLOTRABAJO = "BSEDI" Then
                    'Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    'Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "B")
                    If GloAuxBaja = 0 Then
                        dameClaveDetordser(gloClv_Orden)
                        INSERCION(bnd, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "B", GloClvSerDig)
                    End If
                    INSERCION(GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "B", GloClvSerDig)
                    Me.GuardaMotivoCanServTableAdapter.Connection = CON
                    Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 2, Me.Contratonet.Text, 0, 0)
                ElseIf GLOTRABAJO = "DPAQU" Or GLOTRABAJO = "DPAQT" Or GLOTRABAJO = "DSDIG" Then
                    'Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    'Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "S")
                    INSERCION(GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "S", GloClvSerDig)
                ElseIf GLOTRABAJO = "RPAQU" Or GLOTRABAJO = "RPAQT" Or GLOTRABAJO = "RSDIG" Then
                    'Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    'Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "I")
                    INSERCION(GloDetClave, gloClv_Orden, Me.Contratonet.Text, Me.Clv_Unicanet.Text, 0, "I", GloClvSerDig)
                End If
            Else
                If IsNumeric(Me.Contratonet.Text) = False Then Me.Contratonet.Text = 0
                If Me.Contratonet.Text > 0 Then
                    If GLOTRABAJO = "IPAQU" Or GLOTRABAJO = "IPAQUT" Or GLOTRABAJO = "ASDIG" Then
                        'Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                        'Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 1, "I")
                        INSERCION(GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 1, "I", GloClvSerDig)
                    ElseIf GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Or GLOTRABAJO = "BPAAD" Or GLOTRABAJO = "BSEDI" Then
                        'Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                        'Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 3, "B")
                        If GloAuxBaja = 0 Then
                            dameClaveDetordser(gloClv_Orden)
                            INSERCION(bnd, gloClv_Orden, Me.Contratonet.Text, 0, 3, "B", GloClvSerDig)
                        End If
                        INSERCION(GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 3, "B", GloClvSerDig)
                        Me.GuardaMotivoCanServTableAdapter.Connection = CON
                        Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 2, Me.Contratonet.Text, 0, 0)
                    ElseIf GLOTRABAJO = "DPAQU" Or GLOTRABAJO = "DPAQT" Or GLOTRABAJO = "DSDIG" Then
                        'Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                        'Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 3, "S")
                        INSERCION(GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 3, "S", GloClvSerDig)
                    ElseIf GLOTRABAJO = "RPAQU" Or GLOTRABAJO = "RPAQT" Or GLOTRABAJO = "RSDIG" Then
                        'Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                        'Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 33, "I")
                        INSERCION(GloDetClave, gloClv_Orden, Me.Contratonet.Text, 0, 33, "I", GloClvSerDig)
                    End If
                End If
            End If
            CON.Close()
            Me.CREAARBOL2()
            Me.Contratonet.Text = 0
            Me.MacCableModem.Text = ""
            Me.Clv_Unicanet.Text = 0
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            If gloClv_Orden > 0 And GloDetClave > 0 Then
                

                If GLOTRABAJO = "IPAQU" Or GLOTRABAJO = "IPAQUT" Or GLOTRABAJO = "ASDIG" Then
                    'Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    'Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, 0, 0, 2, "I")
                    INSERCION(GloDetClave, gloClv_Orden, 0, 0, 4, "I", GloClvSerDig)
                ElseIf GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Or GLOTRABAJO = "BPAAD" Or GLOTRABAJO = "BSEDI" Then
                    'Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    'Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, 0, 0, 4, "B")
                    If GloAuxBaja = 0 Then
                        dameClaveDetordser(gloClv_Orden)
                        INSERCION(bnd, gloClv_Orden, 0, 0, 4, "B", GloClvSerDig)
                    End If
                    INSERCION(GloDetClave, gloClv_Orden, 0, 0, 4, "B", GloClvSerDig)
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.GuardaMotivoCanServTableAdapter.Connection = CON
                    Me.GuardaMotivoCanServTableAdapter.Fill(Me.DataSetEric.GuardaMotivoCanServ, gloClv_Orden, 2, 0, 0, 1)
                    CON.Close()
                ElseIf GLOTRABAJO = "DPAQU" Or GLOTRABAJO = "DPAQT" Or GLOTRABAJO = "DSDIG" Then
                    'Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    'Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, 0, 0, 4, "S")
                    INSERCION(GloDetClave, gloClv_Orden, 0, 0, 4, "S", GloClvSerDig)
                ElseIf GLOTRABAJO = "RPAQU" Or GLOTRABAJO = "RPAQT" Or GLOTRABAJO = "RSDIG" Then
                    'Me.NUEIPAQU_SOLTableAdapter.Connection = CON
                    'Me.NUEIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.NUEIPAQU_SOL, GloDetClave, gloClv_Orden, 0, 0, 44, "I")
                    INSERCION(GloDetClave, gloClv_Orden, 0, 0, 44, "I", GloClvSerDig)
                End If
                'CON.Close()
                Me.CREAARBOL2()
            End If
            Me.Contratonet.Text = 0
            Me.MacCableModem.Text = ""
            Me.Clv_Unicanet.Text = 0
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Public Sub BORRAR(ByVal MClave As Long, ByVal MClv_Orden As Long, ByVal MContratonet As Long, ByVal MClv_Unicanet As Long, ByVal Mop As Integer, ByVal MClv_Servicio As Integer)
        Dim conn As New SqlConnection(MiConexion)
        Try
            '@Clave BIGINT, @Clv_Orden BIGINT, @Contratonet BIGINT,@CLV_UNICANET BIGINT,@op int,@CLV_SERVICIO INT
            bndterminar = False

            Dim cmd As New SqlCommand("BorIPAQU_SOL", conn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim prm1 As New SqlParameter("@Clave", SqlDbType.BigInt)
            prm1.Direction = ParameterDirection.Input
            prm1.Value = MClave
            cmd.Parameters.Add(prm1)

            Dim prm2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
            prm2.Direction = ParameterDirection.Input
            prm2.Value = MClv_Orden
            cmd.Parameters.Add(prm2)

            Dim prm3 As New SqlParameter("@Contratonet", SqlDbType.BigInt)
            prm3.Direction = ParameterDirection.Input
            prm3.Value = MContratonet
            cmd.Parameters.Add(prm3)

            Dim prm4 As New SqlParameter("@Clv_Unicanet", SqlDbType.BigInt)
            prm4.Direction = ParameterDirection.Input
            prm4.Value = MClv_Unicanet
            cmd.Parameters.Add(prm4)

            Dim prm5 As New SqlParameter("@op", SqlDbType.Int)
            prm5.Direction = ParameterDirection.Input
            prm5.Value = Mop
            cmd.Parameters.Add(prm5)

            Dim prm7 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
            prm7.Direction = ParameterDirection.Input
            prm7.Value = MClv_Servicio
            cmd.Parameters.Add(prm7)


            conn.Open()
            Dim i As Integer = cmd.ExecuteNonQuery
            conn.Close()

        Catch ex As Exception
            bndterminar = True
            If conn.State <> ConnectionState.Closed Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.Clv_Unicanet1.Text) = False Then Me.Clv_Unicanet1.Text = 0
            If Me.Clv_Unicanet1.Text > 0 Then
                'Me.BorIPAQU_SOLTableAdapter.Connection = CON
                'Me.BorIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet1.Text, Me.Clv_Unicanet1.Text, 0)
                BORRAR(GloDetClave, gloClv_Orden, Me.Contratonet1.Text, Me.Clv_Unicanet1.Text, 0, GloClvSerDig1)
                If GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Then
                    Me.BorraMotivoCanServTableAdapter.Connection = CON
                    Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, gloClv_Orden, 2, Me.Contratonet1.Text, 0, 0)
                End If
            Else
                If Me.Contratonet1.Text > 0 Then
                    'Me.BorIPAQU_SOLTableAdapter.Connection = CON
                    'Me.BorIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borIPAQU_SOL, GloDetClave, gloClv_Orden, Me.Contratonet1.Text, Me.Clv_Unicanet1.Text, 1)
                    BORRAR(GloDetClave, gloClv_Orden, Me.Contratonet1.Text, Me.Clv_Unicanet1.Text, 1, GloClvSerDig1)
                    If GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Then
                        Me.BorraMotivoCanServTableAdapter.Connection = CON
                        Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, gloClv_Orden, 2, Me.Contratonet1.Text, 0, 0)
                    End If
                End If
            End If
            CON.Close()
            Me.CREAARBOL2()
            Me.Contratonet1.Text = 0
            Me.MacCableModem1.Text = ""
            Me.Clv_Unicanet1.Text = 0
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView2.AfterSelect
        Try
            Dim a As Integer = 0
            Dim b As Integer = 0
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet1.Text = e.Node.Tag
                    MacCableModem1.Text = e.Node.Text
                    Clv_Unicanet1.Text = 0
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))

                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            Else
                If Len(e.Node.Tag) > 0 Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))                    
                    a = InStr(1, Trim(e.Node.Tag), "|", vbTextCompare)
                    If a > 0 Then
                        b = InStr(a + 1, Trim(e.Node.Tag), "|", vbTextCompare)
                        Clv_Unicanet1.Text = Mid(Trim(e.Node.Tag), 1, a - 1)
                        GloClvSerDig1 = Mid(Trim(e.Node.Tag), a + 1, Len(Trim(Trim(e.Node.Tag))))
                    Else
                        Clv_Unicanet1.Text = e.Node.Tag
                    End If

                    'Clv_Unicanet1.Text = e.Node.Tag
                    'If IsNumeric(Me.Contratonet1.Text) = True Then

                    If eClv_TipSer = 5 Then
                        DameContratoNet2(CLng(Me.Clv_Unicanet1.Text), eClv_TipSer)
                        Me.Contratonet1.Text = eContratoNet
                    Else
                        Dim CON As New SqlConnection(MiConexion)
                        eContratoNet = 0
                        CON.Open()
                        Me.DameContratoNetTableAdapter.Connection = CON
                        Me.DameContratoNetTableAdapter.Fill(Me.NewSofTvDataSet.DameContratoNet, Me.Clv_Unicanet1.Text, eContratoNet)
                        CON.Close()
                        Me.Contratonet1.Text = eContratoNet
                    End If

                    'End If
                    'MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'Me.BorIPAQU_SOLTableAdapter.Connection = CON
            'Me.BorIPAQU_SOLTableAdapter.Fill(Me.NewSofTvDataSet.borIPAQU_SOL, GloDetClave, gloClv_Orden, 0, 0, 2)
            BORRAR(GloDetClave, gloClv_Orden, 0, 0, 2, 0)
            If GLOTRABAJO = "BPAQU" Or GLOTRABAJO = "BPAQT" Then
                Me.BorraMotivoCanServTableAdapter.Connection = CON
                Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, gloClv_Orden, 2, 0, 0, 1)
            End If
            CON.Close()
            Me.CREAARBOL2()
            Me.Contratonet1.Text = 0
            Me.MacCableModem1.Text = ""
            Me.Clv_Unicanet1.Text = 0
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub TreeView1_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeView1.NodeMouseClick
        Try
            Dim a As Integer = 0
            Dim b As Integer = 0
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet.Text = e.Node.Tag
                    MacCableModem.Text = e.Node.Text
                    Clv_Unicanet.Text = 0
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))

                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            Else
                If Len(e.Node.Tag) > 0 Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    a = InStr(1, Trim(e.Node.Tag), "|", vbTextCompare)
                    If a > 0 Then
                        b = InStr(a + 1, Trim(e.Node.Tag), "|", vbTextCompare)
                        Clv_Unicanet.Text = Mid(Trim(e.Node.Tag), 1, a - 1)
                        GloClvSerDig = Mid(Trim(e.Node.Tag), a + 1, Len(Trim(Trim(e.Node.Tag))))
                    Else
                        Clv_Unicanet.Text = e.Node.Tag
                    End If
                    'Clv_Unicanet.Text = e.Node.Tag
                    'If IsNumeric(Me.Contratonet.Text) = True Then

                    If eClv_TipSer = 5 Then
                        DameContratoNet2(CLng(Me.Clv_Unicanet.Text), eClv_TipSer)
                        Me.Contratonet.Text = eContratoNet
                    Else
                        Dim CON As New SqlConnection(MiConexion)
                        eContratoNet = 0
                        CON.Open()
                        Me.DameContratoNetTableAdapter.Connection = CON
                        Me.DameContratoNetTableAdapter.Fill(Me.NewSofTvDataSet.DameContratoNet, Me.Clv_Unicanet.Text, eContratoNet)
                        CON.Close()
                        Me.Contratonet.Text = eContratoNet
                    End If

                    'MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If

                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TreeView2_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeView2.NodeMouseClick
        Try
            Dim a As Integer = 0
            Dim b As Integer = 0
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet1.Text = e.Node.Tag
                    MacCableModem1.Text = e.Node.Text
                    Clv_Unicanet1.Text = 0
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))

                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            Else
                If Len(e.Node.Tag) > 0 Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))                    
                    a = InStr(1, Trim(e.Node.Tag), "|", vbTextCompare)
                    If a > 0 Then
                        b = InStr(a + 1, Trim(e.Node.Tag), "|", vbTextCompare)
                        Clv_Unicanet1.Text = Mid(Trim(e.Node.Tag), 1, a - 1)
                        GloClvSerDig1 = Mid(Trim(e.Node.Tag), a + 1, Len(Trim(Trim(e.Node.Tag))))
                    Else
                        Clv_Unicanet1.Text = e.Node.Tag
                    End If
                    'Clv_Unicanet1.Text = e.Node.Tag
                    If IsNumeric(Me.Contratonet1.Text) = True Then

                        If eClv_TipSer = 5 Then
                            DameContratoNet2(CLng(Me.Clv_Unicanet1.Text), eClv_TipSer)
                            Me.Contratonet1.Text = eContratoNet
                        Else
                            Dim CON As New SqlConnection(MiConexion)
                            eContratoNet = 0
                            CON.Open()
                            Me.DameContratoNetTableAdapter.Connection = CON
                            Me.DameContratoNetTableAdapter.Fill(Me.NewSofTvDataSet.DameContratoNet, Me.Clv_Unicanet1.Text, eContratoNet)
                            CON.Close()
                            Me.Contratonet1.Text = eContratoNet
                        End If

                    End If
                    'MacCableModem.Text = e.Node.Text
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DameContratoNet2(ByVal Clv_UnicaNet As Long, ByVal Clv_TipSer As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameContratoNet2", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_UnicaNet
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Contratonet", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            eContratoNet = CLng(parametro3.Value.ToString)
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Private Sub dameClaveDetordser(ByVal clv_orden As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("dameClaveDetordser", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_orden", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = clv_orden
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@clave", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)


        Try
            con.Open()
            com.ExecuteNonQuery()

            bnd = CLng(par2.Value)

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub

End Class