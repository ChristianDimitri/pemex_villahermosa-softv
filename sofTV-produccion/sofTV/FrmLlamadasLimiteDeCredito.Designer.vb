﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLlamadasLimiteDeCredito
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.CONTRATOTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.BusNombreButton = New System.Windows.Forms.Button()
        Me.RegistradasRadio = New System.Windows.Forms.RadioButton()
        Me.NoRegistradasRadio = New System.Windows.Forms.RadioButton()
        Me.TodosRadio = New System.Windows.Forms.RadioButton()
        Me.DatosDataGrid = New System.Windows.Forms.DataGridView()
        Me.Id_mov = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fec_Generado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fec_Llamada = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Usuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RegistrarButton = New System.Windows.Forms.Button()
        Me.ConsultarButton = New System.Windows.Forms.Button()
        Me.ExitButton = New System.Windows.Forms.Button()
        Me.ImprimirButton = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.DatosDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(124, 24)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Buscar Por :"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(144, 9)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(74, 16)
        Me.CMBLabel2.TabIndex = 1
        Me.CMBLabel2.Text = "Contrato :"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(300, 9)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(71, 16)
        Me.CMBLabel3.TabIndex = 2
        Me.CMBLabel3.Text = "Nombre :"
        '
        'CONTRATOTextBox
        '
        Me.CONTRATOTextBox.Location = New System.Drawing.Point(147, 28)
        Me.CONTRATOTextBox.Name = "CONTRATOTextBox"
        Me.CONTRATOTextBox.Size = New System.Drawing.Size(130, 20)
        Me.CONTRATOTextBox.TabIndex = 0
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.Location = New System.Drawing.Point(303, 28)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(394, 20)
        Me.NOMBRETextBox.TabIndex = 2
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(739, 9)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(59, 16)
        Me.CMBLabel4.TabIndex = 5
        Me.CMBLabel4.Text = "Status :"
        '
        'BusNombreButton
        '
        Me.BusNombreButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BusNombreButton.Location = New System.Drawing.Point(610, 54)
        Me.BusNombreButton.Name = "BusNombreButton"
        Me.BusNombreButton.Size = New System.Drawing.Size(87, 23)
        Me.BusNombreButton.TabIndex = 3
        Me.BusNombreButton.Text = "&Buscar"
        Me.BusNombreButton.UseVisualStyleBackColor = True
        '
        'RegistradasRadio
        '
        Me.RegistradasRadio.AutoSize = True
        Me.RegistradasRadio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RegistradasRadio.Location = New System.Drawing.Point(742, 29)
        Me.RegistradasRadio.Name = "RegistradasRadio"
        Me.RegistradasRadio.Size = New System.Drawing.Size(98, 17)
        Me.RegistradasRadio.TabIndex = 4
        Me.RegistradasRadio.TabStop = True
        Me.RegistradasRadio.Text = "Con Llamada"
        Me.RegistradasRadio.UseVisualStyleBackColor = True
        '
        'NoRegistradasRadio
        '
        Me.NoRegistradasRadio.AutoSize = True
        Me.NoRegistradasRadio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NoRegistradasRadio.Location = New System.Drawing.Point(742, 52)
        Me.NoRegistradasRadio.Name = "NoRegistradasRadio"
        Me.NoRegistradasRadio.Size = New System.Drawing.Size(94, 17)
        Me.NoRegistradasRadio.TabIndex = 5
        Me.NoRegistradasRadio.TabStop = True
        Me.NoRegistradasRadio.Text = "Sin Llamada"
        Me.NoRegistradasRadio.UseVisualStyleBackColor = True
        '
        'TodosRadio
        '
        Me.TodosRadio.AutoSize = True
        Me.TodosRadio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TodosRadio.Location = New System.Drawing.Point(742, 75)
        Me.TodosRadio.Name = "TodosRadio"
        Me.TodosRadio.Size = New System.Drawing.Size(60, 17)
        Me.TodosRadio.TabIndex = 6
        Me.TodosRadio.TabStop = True
        Me.TodosRadio.Text = "Todos"
        Me.TodosRadio.UseVisualStyleBackColor = True
        '
        'DatosDataGrid
        '
        Me.DatosDataGrid.AllowUserToAddRows = False
        Me.DatosDataGrid.AllowUserToDeleteRows = False
        Me.DatosDataGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.DatosDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DatosDataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id_mov, Me.Contrato, Me.Nombre, Me.Fec_Generado, Me.Status, Me.Fec_Llamada, Me.Clv_Usuario})
        Me.DatosDataGrid.Location = New System.Drawing.Point(12, 98)
        Me.DatosDataGrid.Name = "DatosDataGrid"
        Me.DatosDataGrid.ReadOnly = True
        Me.DatosDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DatosDataGrid.Size = New System.Drawing.Size(902, 510)
        Me.DatosDataGrid.TabIndex = 0
        '
        'Id_mov
        '
        Me.Id_mov.DataPropertyName = "Id_mov"
        Me.Id_mov.HeaderText = "Id Llamada"
        Me.Id_mov.Name = "Id_mov"
        Me.Id_mov.ReadOnly = True
        Me.Id_mov.Visible = False
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 250
        '
        'Fec_Generado
        '
        Me.Fec_Generado.DataPropertyName = "Fec_Generado"
        Me.Fec_Generado.HeaderText = "Fecha Generación"
        Me.Fec_Generado.Name = "Fec_Generado"
        Me.Fec_Generado.ReadOnly = True
        Me.Fec_Generado.Width = 130
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'Fec_Llamada
        '
        Me.Fec_Llamada.DataPropertyName = "Fec_Llamada"
        Me.Fec_Llamada.HeaderText = "Fecha de Llamada"
        Me.Fec_Llamada.Name = "Fec_Llamada"
        Me.Fec_Llamada.ReadOnly = True
        Me.Fec_Llamada.Width = 127
        '
        'Clv_Usuario
        '
        Me.Clv_Usuario.DataPropertyName = "Usuario"
        Me.Clv_Usuario.HeaderText = "Usuario"
        Me.Clv_Usuario.Name = "Clv_Usuario"
        Me.Clv_Usuario.ReadOnly = True
        Me.Clv_Usuario.Width = 150
        '
        'RegistrarButton
        '
        Me.RegistrarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RegistrarButton.Location = New System.Drawing.Point(12, 611)
        Me.RegistrarButton.Name = "RegistrarButton"
        Me.RegistrarButton.Size = New System.Drawing.Size(149, 48)
        Me.RegistrarButton.TabIndex = 7
        Me.RegistrarButton.Text = "&Registrar Llamada"
        Me.RegistrarButton.UseVisualStyleBackColor = True
        '
        'ConsultarButton
        '
        Me.ConsultarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsultarButton.Location = New System.Drawing.Point(167, 611)
        Me.ConsultarButton.Name = "ConsultarButton"
        Me.ConsultarButton.Size = New System.Drawing.Size(149, 49)
        Me.ConsultarButton.TabIndex = 8
        Me.ConsultarButton.Text = "&Consultar Llamada"
        Me.ConsultarButton.UseVisualStyleBackColor = True
        '
        'ExitButton
        '
        Me.ExitButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExitButton.Location = New System.Drawing.Point(771, 611)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(149, 49)
        Me.ExitButton.TabIndex = 10
        Me.ExitButton.Text = "&Salir"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'ImprimirButton
        '
        Me.ImprimirButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImprimirButton.Location = New System.Drawing.Point(322, 610)
        Me.ImprimirButton.Name = "ImprimirButton"
        Me.ImprimirButton.Size = New System.Drawing.Size(149, 49)
        Me.ImprimirButton.TabIndex = 9
        Me.ImprimirButton.Text = "&Imprimir Reporte"
        Me.ImprimirButton.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'FrmLlamadasLimiteDeCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(926, 664)
        Me.Controls.Add(Me.ImprimirButton)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.ConsultarButton)
        Me.Controls.Add(Me.RegistrarButton)
        Me.Controls.Add(Me.DatosDataGrid)
        Me.Controls.Add(Me.TodosRadio)
        Me.Controls.Add(Me.NoRegistradasRadio)
        Me.Controls.Add(Me.RegistradasRadio)
        Me.Controls.Add(Me.BusNombreButton)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.NOMBRETextBox)
        Me.Controls.Add(Me.CONTRATOTextBox)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FrmLlamadasLimiteDeCredito"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Clientes en Límite De Credito"
        CType(Me.DatosDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CONTRATOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents BusNombreButton As System.Windows.Forms.Button
    Friend WithEvents RegistradasRadio As System.Windows.Forms.RadioButton
    Friend WithEvents NoRegistradasRadio As System.Windows.Forms.RadioButton
    Friend WithEvents TodosRadio As System.Windows.Forms.RadioButton
    Friend WithEvents DatosDataGrid As System.Windows.Forms.DataGridView
    Friend WithEvents RegistrarButton As System.Windows.Forms.Button
    Friend WithEvents ConsultarButton As System.Windows.Forms.Button
    Friend WithEvents ExitButton As System.Windows.Forms.Button
    Friend WithEvents Id_mov As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fec_Generado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fec_Llamada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Usuario As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImprimirButton As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
