﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDatosPortabilidad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAgrega1 = New System.Windows.Forms.Button()
        Me.btnAgregar2 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtTelDel = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtTelAl = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.btnQuitar = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.ListBox3 = New System.Windows.Forms.ListBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.ListBox4 = New System.Windows.Forms.ListBox()
        Me.chkPerFisica = New System.Windows.Forms.RadioButton()
        Me.chkPerMoral = New System.Windows.Forms.RadioButton()
        Me.chkGobierno = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.NombreText = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmbFijo = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cmbMovil = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cmbNoGeo = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmbEmpresaA = New System.Windows.Forms.ComboBox()
        Me.Titulo1 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.btnActiva = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtTelefono
        '
        Me.txtTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelefono.Location = New System.Drawing.Point(77, 61)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(158, 22)
        Me.txtTelefono.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(4, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 15)
        Me.Label3.TabIndex = 633
        Me.Label3.Text = "Telefono: "
        '
        'btnAgrega1
        '
        Me.btnAgrega1.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgrega1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgrega1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgrega1.ForeColor = System.Drawing.Color.Black
        Me.btnAgrega1.Location = New System.Drawing.Point(240, 50)
        Me.btnAgrega1.Name = "btnAgrega1"
        Me.btnAgrega1.Size = New System.Drawing.Size(80, 33)
        Me.btnAgrega1.TabIndex = 634
        Me.btnAgrega1.Text = "&Agregar"
        Me.btnAgrega1.UseVisualStyleBackColor = False
        '
        'btnAgregar2
        '
        Me.btnAgregar2.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgregar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar2.ForeColor = System.Drawing.Color.Black
        Me.btnAgregar2.Location = New System.Drawing.Point(574, 48)
        Me.btnAgregar2.Name = "btnAgregar2"
        Me.btnAgregar2.Size = New System.Drawing.Size(80, 33)
        Me.btnAgregar2.TabIndex = 638
        Me.btnAgregar2.Text = "&Agregar"
        Me.btnAgregar2.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(340, 36)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(142, 15)
        Me.Label4.TabIndex = 637
        Me.Label4.Text = "Intervalo de números"
        '
        'txtTelDel
        '
        Me.txtTelDel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelDel.Location = New System.Drawing.Point(412, 54)
        Me.txtTelDel.Name = "txtTelDel"
        Me.txtTelDel.Size = New System.Drawing.Size(158, 22)
        Me.txtTelDel.TabIndex = 636
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(361, 82)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(23, 15)
        Me.Label9.TabIndex = 640
        Me.Label9.Text = "Al:"
        '
        'txtTelAl
        '
        Me.txtTelAl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelAl.Location = New System.Drawing.Point(412, 82)
        Me.txtTelAl.Name = "txtTelAl"
        Me.txtTelAl.Size = New System.Drawing.Size(158, 22)
        Me.txtTelAl.TabIndex = 639
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(352, 57)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 15)
        Me.Label10.TabIndex = 641
        Me.Label10.Text = "Del: "
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.Color.Black
        Me.btnGuardar.Location = New System.Drawing.Point(335, 723)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(150, 33)
        Me.btnGuardar.TabIndex = 643
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'ListBox2
        '
        Me.ListBox2.DisplayMember = "NumAl"
        Me.ListBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 16
        Me.ListBox2.Location = New System.Drawing.Point(334, 126)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(153, 212)
        Me.ListBox2.TabIndex = 645
        Me.ListBox2.ValueMember = "clave"
        '
        'btnQuitar
        '
        Me.btnQuitar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnQuitar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnQuitar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuitar.ForeColor = System.Drawing.Color.Black
        Me.btnQuitar.Location = New System.Drawing.Point(240, 89)
        Me.btnQuitar.Name = "btnQuitar"
        Me.btnQuitar.Size = New System.Drawing.Size(80, 33)
        Me.btnQuitar.TabIndex = 646
        Me.btnQuitar.Text = "&Borrar"
        Me.btnQuitar.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(574, 89)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(80, 33)
        Me.Button2.TabIndex = 647
        Me.Button2.Text = "&Borrar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'ListBox3
        '
        Me.ListBox3.DisplayMember = "NumDel"
        Me.ListBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox3.FormattingEnabled = True
        Me.ListBox3.ItemHeight = 16
        Me.ListBox3.Location = New System.Drawing.Point(499, 126)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(153, 212)
        Me.ListBox3.TabIndex = 648
        Me.ListBox3.ValueMember = "clave"
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.Maroon
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.ForeColor = System.Drawing.Color.White
        Me.TextBox3.Location = New System.Drawing.Point(45, 3)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(564, 19)
        Me.TextBox3.TabIndex = 638
        Me.TextBox3.TabStop = False
        Me.TextBox3.Text = "Números Telefónicos A Ser Portados"
        Me.TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(505, 723)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(150, 33)
        Me.Button1.TabIndex = 649
        Me.Button1.Text = "&Salir"
        Me.Button1.UseVisualStyleBackColor = False
        Me.Button1.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Button4)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.TextBox3)
        Me.Panel1.Controls.Add(Me.ListBox3)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.btnQuitar)
        Me.Panel1.Controls.Add(Me.ListBox2)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txtTelAl)
        Me.Panel1.Controls.Add(Me.btnAgregar2)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txtTelDel)
        Me.Panel1.Controls.Add(Me.btnAgrega1)
        Me.Panel1.Controls.Add(Me.txtTelefono)
        Me.Panel1.Controls.Add(Me.ListBox4)
        Me.Panel1.Location = New System.Drawing.Point(3, 377)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(656, 353)
        Me.Panel1.TabIndex = 650
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(488, 89)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(80, 33)
        Me.Button4.TabIndex = 650
        Me.Button4.Text = "&Limpiar"
        Me.Button4.UseVisualStyleBackColor = False
        Me.Button4.Visible = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(155, 89)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(80, 33)
        Me.Button3.TabIndex = 649
        Me.Button3.Text = "&Limpiar"
        Me.Button3.UseVisualStyleBackColor = False
        Me.Button3.Visible = False
        '
        'ListBox4
        '
        Me.ListBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox4.FormattingEnabled = True
        Me.ListBox4.ItemHeight = 16
        Me.ListBox4.Location = New System.Drawing.Point(25, 126)
        Me.ListBox4.Name = "ListBox4"
        Me.ListBox4.Size = New System.Drawing.Size(280, 212)
        Me.ListBox4.TabIndex = 651
        '
        'chkPerFisica
        '
        Me.chkPerFisica.AutoSize = True
        Me.chkPerFisica.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPerFisica.Location = New System.Drawing.Point(109, 73)
        Me.chkPerFisica.Name = "chkPerFisica"
        Me.chkPerFisica.Size = New System.Drawing.Size(120, 19)
        Me.chkPerFisica.TabIndex = 6
        Me.chkPerFisica.TabStop = True
        Me.chkPerFisica.Text = "Persona Fisica"
        Me.chkPerFisica.UseVisualStyleBackColor = True
        '
        'chkPerMoral
        '
        Me.chkPerMoral.AutoSize = True
        Me.chkPerMoral.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPerMoral.Location = New System.Drawing.Point(259, 73)
        Me.chkPerMoral.Name = "chkPerMoral"
        Me.chkPerMoral.Size = New System.Drawing.Size(119, 19)
        Me.chkPerMoral.TabIndex = 7
        Me.chkPerMoral.TabStop = True
        Me.chkPerMoral.Text = "Persona Moral"
        Me.chkPerMoral.UseVisualStyleBackColor = True
        '
        'chkGobierno
        '
        Me.chkGobierno.AutoSize = True
        Me.chkGobierno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGobierno.Location = New System.Drawing.Point(391, 73)
        Me.chkGobierno.Name = "chkGobierno"
        Me.chkGobierno.Size = New System.Drawing.Size(157, 19)
        Me.chkGobierno.TabIndex = 8
        Me.chkGobierno.TabStop = True
        Me.chkGobierno.Text = "Entidad de Gobierno"
        Me.chkGobierno.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(53, 73)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 15)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Tipo: "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(32, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 15)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Nombre: "
        '
        'NombreText
        '
        Me.NombreText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreText.Location = New System.Drawing.Point(104, 36)
        Me.NombreText.Name = "NombreText"
        Me.NombreText.Size = New System.Drawing.Size(454, 21)
        Me.NombreText.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(29, 164)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 15)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Fijo: "
        '
        'cmbFijo
        '
        Me.cmbFijo.FormattingEnabled = True
        Me.cmbFijo.Items.AddRange(New Object() {"Postpago", "Prepago"})
        Me.cmbFijo.Location = New System.Drawing.Point(71, 158)
        Me.cmbFijo.Name = "cmbFijo"
        Me.cmbFijo.Size = New System.Drawing.Size(108, 21)
        Me.cmbFijo.TabIndex = 14
        Me.cmbFijo.Text = "Ninguno"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(16, 200)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 15)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Móvil: "
        '
        'cmbMovil
        '
        Me.cmbMovil.FormattingEnabled = True
        Me.cmbMovil.Items.AddRange(New Object() {"Postpago", "Prepago"})
        Me.cmbMovil.Location = New System.Drawing.Point(71, 197)
        Me.cmbMovil.Name = "cmbMovil"
        Me.cmbMovil.Size = New System.Drawing.Size(108, 21)
        Me.cmbMovil.TabIndex = 15
        Me.cmbMovil.Text = "Ninguno"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(207, 158)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(160, 15)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Número no Geográfico: "
        '
        'cmbNoGeo
        '
        Me.cmbNoGeo.FormattingEnabled = True
        Me.cmbNoGeo.Items.AddRange(New Object() {"800 + 7 Díjitos"})
        Me.cmbNoGeo.Location = New System.Drawing.Point(391, 155)
        Me.cmbNoGeo.Name = "cmbNoGeo"
        Me.cmbNoGeo.Size = New System.Drawing.Size(108, 21)
        Me.cmbNoGeo.TabIndex = 18
        Me.cmbNoGeo.Text = "Ninguno"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(15, 309)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(126, 15)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Empresa Anterior: "
        '
        'cmbEmpresaA
        '
        Me.cmbEmpresaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbEmpresaA.FormattingEnabled = True
        Me.cmbEmpresaA.Location = New System.Drawing.Point(147, 306)
        Me.cmbEmpresaA.Name = "cmbEmpresaA"
        Me.cmbEmpresaA.Size = New System.Drawing.Size(411, 23)
        Me.cmbEmpresaA.TabIndex = 20
        Me.cmbEmpresaA.Text = "Ninguno"
        '
        'Titulo1
        '
        Me.Titulo1.BackColor = System.Drawing.Color.Maroon
        Me.Titulo1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Titulo1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Titulo1.ForeColor = System.Drawing.Color.White
        Me.Titulo1.Location = New System.Drawing.Point(9, 4)
        Me.Titulo1.Name = "Titulo1"
        Me.Titulo1.ReadOnly = True
        Me.Titulo1.Size = New System.Drawing.Size(564, 19)
        Me.Titulo1.TabIndex = 635
        Me.Titulo1.TabStop = False
        Me.Titulo1.Text = "Datos Del Suscriptor"
        Me.Titulo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.Maroon
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.White
        Me.TextBox1.Location = New System.Drawing.Point(9, 119)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(564, 19)
        Me.TextBox1.TabIndex = 636
        Me.TextBox1.TabStop = False
        Me.TextBox1.Text = "Tipo de Servicio"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.Maroon
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.White
        Me.TextBox2.Location = New System.Drawing.Point(9, 258)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(564, 19)
        Me.TextBox2.TabIndex = 637
        Me.TextBox2.TabStop = False
        Me.TextBox2.Text = "Proveedor Del Servicio De Telecomunicaciones "
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnActiva
        '
        Me.btnActiva.BackColor = System.Drawing.Color.DarkOrange
        Me.btnActiva.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActiva.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActiva.ForeColor = System.Drawing.Color.Black
        Me.btnActiva.Location = New System.Drawing.Point(158, 723)
        Me.btnActiva.Name = "btnActiva"
        Me.btnActiva.Size = New System.Drawing.Size(150, 33)
        Me.btnActiva.TabIndex = 651
        Me.btnActiva.Text = "&ACTIVAR"
        Me.btnActiva.UseVisualStyleBackColor = False
        Me.btnActiva.Visible = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Titulo1)
        Me.Panel2.Controls.Add(Me.TextBox2)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.NombreText)
        Me.Panel2.Controls.Add(Me.TextBox1)
        Me.Panel2.Controls.Add(Me.cmbFijo)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.cmbMovil)
        Me.Panel2.Controls.Add(Me.chkGobierno)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.cmbEmpresaA)
        Me.Panel2.Controls.Add(Me.chkPerMoral)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.cmbNoGeo)
        Me.Panel2.Controls.Add(Me.chkPerFisica)
        Me.Panel2.Location = New System.Drawing.Point(39, 8)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(583, 369)
        Me.Panel2.TabIndex = 652
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmDatosPortabilidad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(673, 768)
        Me.Controls.Add(Me.btnActiva)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmDatosPortabilidad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Portabilidad"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnAgrega1 As System.Windows.Forms.Button
    Friend WithEvents btnAgregar2 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtTelDel As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtTelAl As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents btnQuitar As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkPerFisica As System.Windows.Forms.RadioButton
    Friend WithEvents chkPerMoral As System.Windows.Forms.RadioButton
    Friend WithEvents chkGobierno As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents NombreText As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbFijo As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbMovil As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmbNoGeo As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmbEmpresaA As System.Windows.Forms.ComboBox
    Friend WithEvents Titulo1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents btnActiva As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents ListBox4 As System.Windows.Forms.ListBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
