﻿
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Collections
Public Class FrmPaqueteAdicionalEntreClasificaciones

    'Declaraciones básicas
    Public diccionario As Dictionary(Of String, Clasificaciones) = New Dictionary(Of String, Clasificaciones)
    Dim listaI As New List(Of Clasificaciones)
    Dim listaD As New List(Of Clasificaciones)
    Dim listaAuxiliar As New List(Of Clasificaciones)
    Dim listaOriginal As New List(Of Clasificaciones)
    Dim clasificaciones As Clasificaciones = Nothing
    Dim arreglo As New ArrayList

    Dim tbl_DetPaqueteAdicional As DataTable = Nothing
    Private TipoClasificacionActual As Integer = 0
    Private ActualizaDetPaqAdic As Boolean = False

    Private Sub FrmPaqueteAdicionalEntreClasificaciones_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        
    End Sub

    Private Sub FrmPaqueteAdicionalEntreClasificaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CargaClasificaciones()
        GUI(OpPaqAdic)
        eBndPaqAdi = False
    End Sub

    Private Sub CargaClasificaciones()

        Dim I As Integer = 0
        Dim Fila2 As DataRow
        Dim conexion As New SqlConnection(Miconexion)
        Dim strSQL As New StringBuilder
        Dim oDataSet As New DataSet


        Try


            Dim cmd As New SqlCommand("CargaClasificacionesTarifasDeLlamada", conexion)
            cmd.CommandType = CommandType.StoredProcedure

            'Declaramos la lista de la clase 
            Dim listaCargos As New List(Of Clasificaciones)

            Dim dr As SqlDataReader
            conexion.Open()
            dr = cmd.ExecuteReader()

            listaI.Clear()

            Using dr

                While dr.Read
                    If diccionario.ContainsKey(dr("ident").ToString().Trim()) Then
                        If Not listaD.Contains(diccionario(dr("ident").ToString().Trim())) Then
                            listaI.Add(diccionario(dr("ident").ToString().Trim()))
                        End If

                    Else

                        clasificaciones = New Clasificaciones()
                        clasificaciones.ident = dr("ident").ToString().Trim()
                        clasificaciones.concepto = dr("concepto").ToString().Trim()
                        clasificaciones.tipo = dr("tipo").ToString().Trim()

                        diccionario.Add(clasificaciones.ident, clasificaciones)
                        listaI.Add(clasificaciones)

                    End If
                End While
            End Using

            Me.ListBox1.DataSource = Nothing
            Me.ListBox1.DataSource = listaI
            Me.ListBox1.DisplayMember = "concepto"
            Me.ListBox1.ValueMember = "ident"

            For Each clasificaciones In listaI
                listaOriginal.Add(clasificaciones)
            Next

           

        Catch ex As Exception

            MsgBox("Ocurrió un error al momento de Cargar las Clasificaciones Generales.", MsgBoxStyle.Critical)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub

        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

        If Me.ListBox1.Items.Count = 0 Then

            MsgBox("Aún no se han creado clasificaciones/Tarifas de Llamadas. Antes de crear un Paquete se deben de crear las Tarifas de Llamadas.", MsgBoxStyle.Information)

            Me.btnGuardarPaqAdic.Enabled = False

            Me.Close()
            Me.Dispose()

        End If

    End Sub


    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click

        Dim TipoACruzar As Integer = 0
        Dim idClasificacionACruzar As Integer = 0
        Dim clasificacion As Clasificaciones

        'SI ES EL PRIMER ELEMENTO A CRUZAR ENTONSES DE ESE TIPO SERÁN LOS DEMÁS
        If Me.ListBox2.Items.Count = 0 Then


            TipoClasificacionActual = 0
            idClasificacionACruzar = 0
            For Each clasificacion In listaOriginal

                If ListBox1.SelectedValue.ToString() = clasificacion.ident Then
                    'Asignamos el tipo que será la restricción
                    TipoClasificacionActual = clasificacion.tipo

                End If

            Next


        End If


        If Me.ListBox1.SelectedValue <> Nothing Then

            For Each clasificacion In listaOriginal

                If ListBox1.SelectedValue.ToString() = clasificacion.ident Then

                    'Obtenemos el tipo de laClasificación que se esta intentando cruzar
                    TipoACruzar = clasificacion.tipo
                    'idClasificacionACruzar = clasificacion.ident
                End If

            Next

        End If


        If TipoACruzar = TipoClasificacionActual Then

            If Me.ListBox1.SelectedValue <> Nothing Then

                If Me.ListBox2.Items.Count = 1 Then
                    Dim idClasificacionAlmacenada As Integer = 0

                    For Each clasificacion In ListBox2.Items

                        idClasificacionAlmacenada = clasificacion.ident

                    Next



                    If (idClasificacionAlmacenada = 4 Or idClasificacionAlmacenada = 5) And (Me.ListBox1.SelectedValue = 4 Or Me.ListBox1.SelectedValue = 5) Then

                        listaD.Add(diccionario(ListBox1.SelectedValue.ToString()))
                        listaI.Remove(diccionario(ListBox1.SelectedValue.ToString()))

                        refreshLists()

                    Else

                        MsgBox("La clasificación no se puede agregar ya que para crear Paquetes Adicionales COMBO solo puede contener clasificaciones de: Larga Distancia Nacional y Larga Distancia E.U.A. Y Canada.", MsgBoxStyle.Information, "NO LA PUEDES AGREGAR")

                    End If

                ElseIf Me.ListBox2.Items.Count > 1 Then

                    MsgBox("La clasificación no se puede agregar ya que para crear Paquetes Adicionales COMBO solo puede contener clasificaciones de: Larga Distancia Nacional y Larga Distancia E.U.A. Y Canada.", MsgBoxStyle.Information, "NO LA PUEDES AGREGAR")
                    
                Else



                    listaD.Add(diccionario(ListBox1.SelectedValue.ToString()))
                    listaI.Remove(diccionario(ListBox1.SelectedValue.ToString()))

                    refreshLists()

                End If


            End If
        Else
            MsgBox("No puedes agregar esta clasificación al paquete ya que el tipo no corresponde al actual.", MsgBoxStyle.Exclamation)
        End If

        If TipoClasificacionActual = 1 Then
            lblTipoClasificacion.Text = "Minutos:"
        ElseIf TipoClasificacionActual = 2 Then
            lblTipoClasificacion.Text = "Eventos:"
        End If

    End Sub

    Private Sub BtnQuitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnQuitar.Click
        If Me.ListBox2.SelectedValue <> Nothing Then


            listaI.Add(diccionario(ListBox2.SelectedValue.ToString()))
            listaD.Remove(diccionario(ListBox2.SelectedValue.ToString()))

            arreglo.Clear()

            For Each clasificaciones In listaI
                arreglo.Add(clasificaciones.ident - 1)
                listaAuxiliar.Add(clasificaciones)

            Next

            listaI.Clear()

            'Barremos los elementos del arreglo para regresarlos a la lista Izquierda ya Ordenados 
            Dim i As Integer
            Dim j As Integer
            j = 0

            For i = 1 To arreglo.Count
                Me.Minimo()
            Next

            listaAuxiliar.Clear()
            refreshLists()

        End If
    End Sub

    Private Sub BtnAgregarTodas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAgregarTodas.Click
        If ListBox1.Items.Count > 0 Then

            For Each clasificaciones In listaI

                listaD.Add(clasificaciones)
            Next

            listaI.Clear()
            refreshLists()

        End If
    End Sub

    Private Sub BtnQuitarTodas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnQuitarTodas.Click
        If ListBox2.Items.Count > 0 Then

            listaI.Clear()

            For Each clasificaciones In listaOriginal

                listaI.Add(clasificaciones)

            Next

            listaD.Clear()
            refreshLists()

        End If
    End Sub

    Public Sub refreshLists()



        ListBox2.DataSource = Nothing
        ListBox2.DataSource = listaD
        ListBox2.DisplayMember = "concepto"
        ListBox2.ValueMember = "ident"

        ListBox1.DataSource = Nothing
        ListBox1.DataSource = listaI
        ListBox1.DisplayMember = "concepto"
        ListBox1.ValueMember = "ident"
        ListBox1.Refresh()


    End Sub

    Public Sub Minimo()

        Dim num_minimo As Integer
        'Dim compara As Integer
        Dim i As Integer
        i = 0
        num_minimo = -1

        For Each clasificaciones In listaAuxiliar

            If num_minimo = -1 Then
                num_minimo = clasificaciones.ident
            End If

            If num_minimo > clasificaciones.ident Then
                num_minimo = clasificaciones.ident
            End If

            If listaAuxiliar.Count = 1 Then
                num_minimo = clasificaciones.ident
            End If
        Next

        Dim IndexList As Integer
        'Veces = listaAuxiliar.Count - 1

        For Each clasificaciones In listaAuxiliar

            If num_minimo = clasificaciones.ident Then
                listaI.Add(clasificaciones)
                IndexList = i
            End If
            i = i + 1
        Next

        listaAuxiliar.RemoveAt(IndexList)

    End Sub

    Public Sub GUI(ByVal OpPaqAdic As String)

        Me.lblClasificacines.ForeColor = Color.Black
        Me.lblTitulo.ForeColor = Color.Black
        Me.lblCostosPlanTarifario.ForeColor = Color.Black

        Select Case OpPaqAdic

            'OPCIÓN DE NUEVO

            Case "N"
                'Habilitamos la Parte superior (Generales del Paquete)
                Me.txtCosto.Enabled = True
                Me.txtNombrePaqAdic.Enabled = True
                Me.NumeroTextBox.Enabled = True
                Me.panelRelPlanTarifario.Enabled = False
                Me.btnGuardarPaqAdic.Enabled = True
                Me.btnEliminaPaqAdic.Enabled = False

                Me.btnGuardar_RelPlanTarifario.Enabled = False
                Me.btnCancela_RelPlanTarifario.Enabled = False

                Me.btnModifica_RelPlanTarifario.Enabled = False
                Me.btnEliminar_RelPlanTarifario.Enabled = False

                Me.lblTipoClasificacion.Text = "Eventos/Minutos"
                Me.btnGuardar_RelPlanTarifario.Text = "Agregar"

                'OPCIÓN DE CONSULTA
            Case Is = "C"
                Me.txtCosto.Enabled = False
                Me.txtNombrePaqAdic.Enabled = False
                Me.NumeroTextBox.Enabled = False
                Me.panelRelPlanTarifario.Enabled = False
                Me.btnGuardarPaqAdic.Enabled = False
                Me.btnEliminaPaqAdic.Enabled = False

                Me.btnGuardar_RelPlanTarifario.Enabled = False
                Me.btnCancela_RelPlanTarifario.Enabled = False

                Me.btnModifica_RelPlanTarifario.Enabled = False
                Me.btnEliminar_RelPlanTarifario.Enabled = False

                'Cargamos los Datos Generales del Paquete Adicional y sus Realaciónes entre las Clasificaciones
                CargaGeneralesPaqAdic(eClv_PaqAdi)

                'OPCIÓN DE CONSULTA
            Case Is = "M"
                Me.txtCosto.Enabled = True
                Me.txtNombrePaqAdic.Enabled = True
                Me.NumeroTextBox.Enabled = True
                Me.panelRelPlanTarifario.Enabled = True
                Me.btnGuardarPaqAdic.Enabled = True

                Me.btnGuardar_RelPlanTarifario.Enabled = False
                Me.btnCancela_RelPlanTarifario.Enabled = False

                Me.btnModifica_RelPlanTarifario.Enabled = True
                Me.btnEliminar_RelPlanTarifario.Enabled = True

                Me.btnGuardar_RelPlanTarifario.Text = "Agregar"

                'Cargamos los Datos Generales del Paquete Adicional y sus Realaciónes entre las Clasificaciones
                CargaGeneralesPaqAdic(eClv_PaqAdi)

        End Select




    End Sub

    Private Function generaXML() As String

        Dim strXML As String = ""

        Try

            Dim sw As New StringWriter()
            Dim w As New XmlTextWriter(sw)
            Dim i As Integer
            Dim serv As Clasificaciones

            w.Formatting = Formatting.Indented

            w.WriteStartElement("Clasificaciones")
            'w.WriteAttributeString("Uno", "Dos")

            For Each serv In listaD

                w.WriteStartElement("Clasificacion")

                w.WriteAttributeString("ident", serv.ident)
                w.WriteAttributeString("concepto", serv.concepto)

                w.WriteEndElement()

            Next

            w.WriteEndElement()
            w.Close()
            strXML = sw.ToString()

        Catch ex As Exception

            MsgBox("Ocurrió un error al momento de seleccionar las Clasificaciones de llamadas a mostrar para la búsqueda.", MsgBoxStyle.Information, "ERROR EN LAS CLASIFICACIONES")
            MsgBox(ex.Message)

        End Try

        Return strXML

    End Function
    

    Private Sub GuardaPaqAdic(ByVal xmlClasificaciones As String)

        Dim conn As New SqlConnection(MiConexion)
        Dim msj As String = ""

        Try

            Dim comando As New SqlClient.SqlCommand("GuardaPaqAdic", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 10

            comando.Parameters.Add("@NombrePaquete", SqlDbType.VarChar, 600)
            comando.Parameters(0).Value = Me.txtNombrePaqAdic.Text

            comando.Parameters.Add("@Numero", SqlDbType.Int)
            comando.Parameters(1).Value = Me.NumeroTextBox.Text

            comando.Parameters.Add("@Costo", SqlDbType.Money)
            comando.Parameters(2).Value = Me.txtCosto.Text

            comando.Parameters.Add("@Minutos", SqlDbType.Int)
            comando.Parameters(3).Value = Me.NumeroTextBox.Text

            comando.Parameters.Add("@xmlClasificaciones", SqlDbType.Xml)
            comando.Parameters(4).Value = xmlClasificaciones

            comando.Parameters.Add("@Clv_PaqAdic", SqlDbType.Int)
            comando.Parameters(5).Direction = ParameterDirection.InputOutput

            comando.Parameters.Add("@Op", SqlDbType.VarChar, 1)
            comando.Parameters(6).Value = OpPaqAdic

            If OpPaqAdic = "N" Then

                comando.Parameters(5).Value = 0
                msj = "El Nuevo Paquete Adicional " & Me.txtNombrePaqAdic.Text & " se ha creado con éxito. Ahora puedes asignarle algún Plan Tarifario."
                OpPaqAdic = "M"

            ElseIf OpPaqAdic = "M" Then

                comando.Parameters(5).Value = eClv_PaqAdi
                msj = "El Paquete Adicional " & Me.txtNombrePaqAdic.Text & " se ha actualizado"

            End If

            conn.Open()
            comando.ExecuteNonQuery()
            
            MsgBox(msj, MsgBoxStyle.Information, "PAQUETE ADICIONAL GUARDADO")

            eClv_PaqAdi = comando.Parameters(5).Value

            CargaGeneralesPaqAdic(eClv_PaqAdi)

            Me.panelRelPlanTarifario.Enabled = True
            Me.btnGuardar_RelPlanTarifario.Enabled = True
            Me.cmbxServicio.Enabled = True

            eBndPaqAdi = True

        Catch ex As Exception

            MsgBox("Ocurrió un error al momento de guardar el Paquete Adicional.", MsgBoxStyle.Information, "ERROR AL GUARDAR")
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            
        Finally

            conn.Close()
            conn.Dispose()

        End Try


    End Sub


    Private Sub GuardaDetPaqAdic()

        Dim conn As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlClient.SqlCommand("GuardaDetPaqAdic", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 5

            comando.Parameters.Add("@Clv_PaqAdic", SqlDbType.Int)
            comando.Parameters(0).Value = eClv_PaqAdi

            comando.Parameters.Add("@Clv_Servicio", SqlDbType.Int)
            comando.Parameters(1).Value = Me.cmbxServicio.SelectedValue

            comando.Parameters.Add("@Contratacion", SqlDbType.Money)
            comando.Parameters(2).Value = Me.txtCostoContratacion.Text

            comando.Parameters.Add("@Mensualidad", SqlDbType.Money)
            comando.Parameters(3).Value = Me.txtCostoMensualidad.Text

            conn.Open()
            comando.ExecuteNonQuery()

            MsgBox("Se haguardado la relación correctamente.", MsgBoxStyle.Information, "SE GUARDÓ CON ÉXITO")

        Catch ex As Exception

            MsgBox(ex.Message, MsgBoxStyle.Exclamation)


        Finally

            conn.Close()
            conn.Dispose()

        End Try


    End Sub
    Private Sub EliminaPaqAdic(ByVal eClv_PaqAdi As Integer)

        Dim conn As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlClient.SqlCommand("EliminaPaqAdic", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 5

            comando.Parameters.Add("@Clv_PaqAdic", SqlDbType.Int)
            comando.Parameters(0).Value = eClv_PaqAdi



            conn.Open()
            comando.ExecuteNonQuery()

            MsgBox("Se ha eliminado correctamente.", MsgBoxStyle.Information, "SE ELIMINÓ CON ÉXITO")
            eBndPaqAdi = True
            Me.Close()
            Me.Dispose()

        Catch ex As Exception

            MsgBox(ex.Message, MsgBoxStyle.Exclamation)


        Finally

            conn.Close()
            conn.Dispose()

        End Try


    End Sub
    Private Sub ActualizaDetPaqAdicSub(ByVal eClv_PaqAdi As Integer, ByVal Clv_Servicio As Integer, ByVal Contratacion As Decimal, ByVal Mensualidad As Decimal)

        Dim conn As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlClient.SqlCommand("ActualizaDetPaqAdic", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 5

            comando.Parameters.Add("@Clv_PaqAdic", SqlDbType.Int)
            comando.Parameters(0).Value = eClv_PaqAdi

            comando.Parameters.Add("@Clv_Servicio", SqlDbType.Int)
            comando.Parameters(1).Value = Clv_Servicio

            comando.Parameters.Add("@Contratacion", SqlDbType.Money)
            comando.Parameters(2).Value = Contratacion

            comando.Parameters.Add("@Mensualidad", SqlDbType.Money)
            comando.Parameters(3).Value = Mensualidad

            conn.Open()
            comando.ExecuteNonQuery()

            MsgBox("Se ha actualizado correctamente.", MsgBoxStyle.Information, "SE ELIMINÓ CON ÉXITO")
            ActualizaDetPaqAdic = False

        Catch ex As Exception

            MsgBox(ex.Message, MsgBoxStyle.Exclamation)


        Finally

            conn.Close()
            conn.Dispose()

        End Try


    End Sub
    Private Sub EliminaDetPaqAdic(ByVal eClv_PaqAdi As Integer, ByVal Clv_Servicio As Integer)

        Dim conn As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlClient.SqlCommand("EliminaDetPaqAdic", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 5

            comando.Parameters.Add("@Clv_PaqAdic", SqlDbType.Int)
            comando.Parameters(0).Value = eClv_PaqAdi

            comando.Parameters.Add("@Clv_Servicio", SqlDbType.Int)
            comando.Parameters(1).Value = Clv_Servicio

            conn.Open()
            comando.ExecuteNonQuery()

            MsgBox("Se ha eliminado correctamente el Plan Tarifario.", MsgBoxStyle.Information, "SE ELIMINÓ CON ÉXITO")
            CargaRelPaqAdicServicios(eClv_PaqAdi)

            'Verificamos GUI
            If Me.cmbxServicio.Items.Count = 0 Then
                Me.btnGuardar_RelPlanTarifario.Enabled = False
                Me.cmbxServicio.Enabled = False
            Else
                Me.btnGuardar_RelPlanTarifario.Enabled = True
                Me.cmbxServicio.Enabled = True
            End If

        Catch ex As Exception

            MsgBox(ex.Message, MsgBoxStyle.Exclamation)


        Finally

            conn.Close()
            conn.Dispose()

        End Try


    End Sub


    Private Sub CargaRelPaqAdicServicios(ByVal clv_paqAdic As Integer)

        Dim conn As New SqlConnection(MiConexion)
        Dim tbl_RelPaqAdicClasificaciones As DataTable


        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("CargaRelPaqAdicServiciosTel", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando


            Adaptador.SelectCommand.Parameters.Add("@Clv_PaqAdic", SqlDbType.Int)
            Adaptador.SelectCommand.Parameters(0).Value = clv_paqAdic



            Dim Dataset As New DataSet
            Dim Bs As New BindingSource
            Adaptador.Fill(Dataset)


            Dataset.Tables(0).TableName = "PaquetesTelefonicosDisponibles"
            Dataset.Tables(1).TableName = "PaquetesTelefonicosAsignados"

            'Paquetes Telefonicos
            Me.cmbxServicio.DataSource = Nothing

            Me.txtCostoContratacion.Text = ""
            Me.txtCostoMensualidad.Text = ""

            Me.cmbxServicio.DisplayMember = "Descripcion"
            Me.cmbxServicio.ValueMember = "Clv_Servicio"
            Me.cmbxServicio.DataSource = Dataset.Tables("PaquetesTelefonicosDisponibles")

            Me.dgvRelServiciosPaqAdic.DataSource = Nothing
            Me.dgvRelServiciosPaqAdic.DataSource = Dataset.Tables("PaquetesTelefonicosAsignados")

            Me.dgvRelServiciosPaqAdic.Columns(1).HeaderText = "Plan Tarifario:"

            Me.dgvRelServiciosPaqAdic.Columns(2).HeaderText = "Contratación:"
            Me.dgvRelServiciosPaqAdic.Columns(3).HeaderText = "Mensualidad:"

            Me.dgvRelServiciosPaqAdic.Columns(0).Visible = False
            Me.dgvRelServiciosPaqAdic.Columns(1).Width = 250
            Me.dgvRelServiciosPaqAdic.Columns(2).Width = 115
            Me.dgvRelServiciosPaqAdic.Columns(3).Width = 115

            'Verificamos si es que existen más servicios que ligar

            If Dataset.Tables(0).Rows.Count > 0 Then

                Me.btnGuardar_RelPlanTarifario.Enabled = True
                Me.cmbxServicio.Enabled = True

                Me.txtCostoContratacion.Enabled = True
                Me.txtCostoMensualidad.Enabled = True
                Me.btnModifica_RelPlanTarifario.Enabled = True


            Else

                Me.btnGuardar_RelPlanTarifario.Enabled = False
                Me.cmbxServicio.Enabled = False

                Me.txtCostoContratacion.Enabled = False
                Me.txtCostoMensualidad.Enabled = False


            End If

            If Me.dgvRelServiciosPaqAdic.RowCount > 0 Then
                Me.btnModifica_RelPlanTarifario.Enabled = True
                Me.btnEliminar_RelPlanTarifario.Enabled = True
                Me.dgvRelServiciosPaqAdic.Enabled = True
            Else
                Me.btnModifica_RelPlanTarifario.Enabled = False
                Me.btnEliminar_RelPlanTarifario.Enabled = False
            End If

            '-----------------------------------------------------

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de enlazar los Datos Principales, los resultados de las búsquedas pueden ser inconsistentes.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub

        Finally

            conn.Close()
            conn.Dispose()

        End Try

    End Sub
    Public Sub CargaGeneralesPaqAdic(ByVal Clv_PaqAdic As Integer)

        Dim conn As New SqlConnection(MiConexion)
        Dim tbl_RelPaqAdicClasificaciones As DataTable



        Try

            conn.Open()

            Dim comando As New SqlClient.SqlCommand("CargaGeneralesPaqAdic", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando


            Adaptador.SelectCommand.Parameters.Add("@Clv_PaqAdic", SqlDbType.Int)
            Adaptador.SelectCommand.Parameters(0).Value = Clv_PaqAdic

            Adaptador.SelectCommand.Parameters.Add("@NombrePaquete", SqlDbType.VarChar, 600)
            Adaptador.SelectCommand.Parameters(1).Value = Me.txtNombrePaqAdic.Text
            Adaptador.SelectCommand.Parameters(1).Direction = ParameterDirection.Output

            Adaptador.SelectCommand.Parameters.Add("@Numero", SqlDbType.Int)
            Adaptador.SelectCommand.Parameters(2).Value = Me.NumeroTextBox.Text
            Adaptador.SelectCommand.Parameters(2).Direction = ParameterDirection.Output

            Adaptador.SelectCommand.Parameters.Add("@Costo", SqlDbType.Money)
            Adaptador.SelectCommand.Parameters(3).Value = Me.txtCosto.Text
            Adaptador.SelectCommand.Parameters(3).Direction = ParameterDirection.Output

            Adaptador.SelectCommand.Parameters.Add("@Minutos", SqlDbType.Int)
            Adaptador.SelectCommand.Parameters(4).Value = Me.NumeroTextBox.Text
            Adaptador.SelectCommand.Parameters(4).Direction = ParameterDirection.Output

            Dim Dataset As New DataSet
            Dim Bs As New BindingSource
            Adaptador.Fill(Dataset)

            'Colocamos los datos generales en los campos correspondientes
            Me.txtNombrePaqAdic.Text = Adaptador.SelectCommand.Parameters(1).Value.ToString
            Me.NumeroTextBox.Text = Adaptador.SelectCommand.Parameters(2).Value.ToString
            Me.txtCosto.Text = CType(Adaptador.SelectCommand.Parameters(3).Value.ToString, Double)
            Me.NumeroTextBox.Text = Adaptador.SelectCommand.Parameters(4).Value.ToString

            Dataset.Tables(0).TableName = "RelPaqAdicClasificaciones"
            Dataset.Tables(1).TableName = "PaquetesTelefonicosDisponibles"
            Dataset.Tables(2).TableName = "PaquetesTelefonicosAsignados"

            tbl_RelPaqAdicClasificaciones = Dataset.Tables("RelPaqAdicClasificaciones")

            'Paquetes Telefonicos
            Me.cmbxServicio.DataSource = Nothing

            Me.txtCostoContratacion.Text = ""
            Me.txtCostoMensualidad.Text = ""

            Me.cmbxServicio.DisplayMember = "Descripcion"
            Me.cmbxServicio.ValueMember = "Clv_Servicio"
            Me.cmbxServicio.DataSource = Dataset.Tables("PaquetesTelefonicosDisponibles")

            Me.dgvRelServiciosPaqAdic.DataSource = Nothing
            Me.dgvRelServiciosPaqAdic.DataSource = Dataset.Tables("PaquetesTelefonicosAsignados")

            Me.dgvRelServiciosPaqAdic.Columns(1).HeaderText = "Plan Tarifario:"

            Me.dgvRelServiciosPaqAdic.Columns(2).HeaderText = "Contratación:"
            Me.dgvRelServiciosPaqAdic.Columns(3).HeaderText = "Mensualidad:"

            Me.dgvRelServiciosPaqAdic.Columns(0).Visible = False
            Me.dgvRelServiciosPaqAdic.Columns(1).Width = 250
            Me.dgvRelServiciosPaqAdic.Columns(2).Width = 115
            Me.dgvRelServiciosPaqAdic.Columns(3).Width = 115

            If Dataset.Tables(0).Rows.Count = 0 Then
                MsgBox("Aún no se han creado las clasificaciones de llamadas. Primero deben de crearse para despues asignarlas con un Paquete Adicional.", MsgBoxStyle.Information, "NO HAY TARIFAS/CLASIFICACIONES CREADAS")

                Me.Close()
                Me.Dispose()

            End If

            If Dataset.Tables(1).Rows.Count > 0 Then

                Me.btnGuardar_RelPlanTarifario.Enabled = True
                Me.cmbxServicio.Enabled = True

                'Me.txtCostoContratacion.Enabled = True
                'Me.txtCostoMensualidad.Enabled = True
                'Me.btnModifica_RelPlanTarifario.Enabled = True


            Else

                Me.btnGuardar_RelPlanTarifario.Enabled = False
                Me.cmbxServicio.Enabled = False

                'Me.txtCostoContratacion.Enabled = False
                'Me.txtCostoMensualidad.Enabled = False
                'Me.btnModifica_RelPlanTarifario.Enabled = False

            End If

        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de enlazar los Datos Principales, los resultados de las búsquedas pueden ser inconsistentes.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub

        Finally

            conn.Close()
            conn.Dispose()

        End Try

        If OpPaqAdic = "C" Or OpPaqAdic = "M" Then
            Try

                If OpPaqAdic = "M" Then
                    listaD.Clear()
                    refreshLists()
                End If

                'Ahora Recorremos la Tabla para ver ordenar las Clasificaciones
                Dim cont As Integer = 0

                For cont = 0 To tbl_RelPaqAdicClasificaciones.Rows.Count - 1

                    For Each clasificaciones In listaOriginal

                        If tbl_RelPaqAdicClasificaciones.Rows(cont).Item(2).ToString = clasificaciones.ident Then

                            listaD.Add(clasificaciones)
                            listaI.Remove(clasificaciones)
                            refreshLists()

                            TipoClasificacionActual = clasificaciones.tipo
                            If TipoClasificacionActual = 1 Then
                                Me.lblTipoClasificacion.Text = "Minutos:"
                            Else
                                Me.lblTipoClasificacion.Text = "Eventos:"
                            End If

                        End If


                    Next

                Next

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Finally

                If OpPaqAdic = "C" Then

                    Me.btnAgregar.Enabled = False
                    Me.BtnQuitar.Enabled = False
                    Me.BtnAgregarTodas.Enabled = False
                    Me.BtnQuitarTodas.Enabled = False

                    Me.btnGuardar_RelPlanTarifario.Enabled = False
                    Me.btnEliminar_RelPlanTarifario.Enabled = False
                    Me.btnModifica_RelPlanTarifario.Enabled = False



                ElseIf OpPaqAdic = "M" Then

                    Me.btnAgregar.Enabled = True
                    Me.BtnQuitar.Enabled = True
                    Me.BtnAgregarTodas.Enabled = True
                    Me.BtnQuitarTodas.Enabled = True

                    Me.btnGuardar_RelPlanTarifario.Enabled = True
                    Me.btnEliminar_RelPlanTarifario.Enabled = True
                    Me.btnModifica_RelPlanTarifario.Enabled = True

                End If


            End Try
        End If


    End Sub

    Private Sub btnGuardarPaqAdic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarPaqAdic.Click

        If listaD.Count > 0 Then

            If Me.NumeroTextBox.Text <> "" And IsNumeric(Me.NumeroTextBox.Text) Then
                Dim msj As String = ""

                If OpPaqAdic = "N" Then
                    msj = "¿Deseas guardar el nuevo paquete: "
                ElseIf OpPaqAdic = "M" Then
                    msj = "¿Deseas actualizar paquete: "
                End If

                Dim Resp = MsgBox(msj & Me.txtNombrePaqAdic.Text & "?", MsgBoxStyle.YesNoCancel, "¿GUARDAR AHORA?")

                If Resp = MsgBoxResult.Yes Then

                    Dim xmlClasificaciones = generaXML()
                    GuardaPaqAdic(xmlClasificaciones)

                End If
            Else
                MsgBox("Se necesita que captures un Costo y un Número de Eventos/Minutos válidos.", MsgBoxStyle.Information, "SE NECESITA UN COSTO Y NÚMERO.")
            End If

            
        Else
            MsgBox("No has seleccionado ninguna clasificación. Antes de guardar el Paquete Adicional debes de elegir por lo menos una.", MsgBoxStyle.Information, "SELECCIONA UNA CLASIFICACIÓN.")
        End If


        

    End Sub
   
    Private Function VerificaTipoClasificacion(ByVal TipoACruzar As Integer) As Boolean
        Dim Permitir As Boolean
        If TipoACruzar = TipoClasificacionActual Then

        End If

    End Function




    Private Sub btnGuardar_RelPlanTarifario_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar_RelPlanTarifario.Click

        If Len(Me.txtCostoContratacion.Text) = 0 Or IsNumeric(Me.txtCostoContratacion.Text) = False Then
            MsgBox("Los costos para contratación y mensulaidad son requeridos.", MsgBoxStyle.Information, "FALTA CONTRATACIÓN Y/O MENSUALIDAD")
            Exit Sub

        End If

        If ActualizaDetPaqAdic = False Then
            If Me.txtCostoContratacion.Text <> "" And IsNumeric(Me.txtCostoContratacion.Text) And Me.txtCostoMensualidad.Text <> "" And IsNumeric(Me.txtCostoMensualidad.Text) Then
                GuardaDetPaqAdic()
            Else
                MsgBox("Necesitas capturar los costos tanto de Contratación como el de Mensualidad.", MsgBoxStyle.Exclamation, "CONTRATACIÓN Y MENSUALIDAD")
            End If

        ElseIf ActualizaDetPaqAdic = True Then
            If Me.txtCostoContratacion.Text <> "" And IsNumeric(Me.txtCostoContratacion.Text) And Me.txtCostoMensualidad.Text <> "" And IsNumeric(Me.txtCostoMensualidad.Text) Then
                ActualizaDetPaqAdicSub(eClv_PaqAdi, Me.dgvRelServiciosPaqAdic.SelectedCells.Item(0).Value, Me.txtCostoContratacion.Text, Me.txtCostoMensualidad.Text)
            Else
                MsgBox("Necesitas capturar los costos tanto de Contratación como el de Mensualidad.", MsgBoxStyle.Exclamation, "CONTRATACIÓN Y MENSUALIDAD")
            End If

        End If
        CargaRelPaqAdicServicios(eClv_PaqAdi)
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Dispose()
        Me.Close()
    End Sub

    
    Private Sub btnEliminar_RelPlanTarifario_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar_RelPlanTarifario.Click
        If Me.dgvRelServiciosPaqAdic.RowCount > 0 Then

            If Me.dgvRelServiciosPaqAdic.SelectedCells.Item(0).Value <> Nothing Then
                Dim Resp = MsgBox("¿Deseas quitar el Paquete Adicional del Plan Tarifario: " & Me.dgvRelServiciosPaqAdic.SelectedCells.Item(1).Value.ToString & "?", MsgBoxStyle.YesNoCancel, "QUITAR PLAN TARIFARIO")

                If Resp = MsgBoxResult.Yes Then
                    EliminaDetPaqAdic(eClv_PaqAdi, Me.dgvRelServiciosPaqAdic.SelectedCells.Item(0).Value)
                End If

            End If

        End If


    End Sub

    Private Sub btnEliminaPaqAdic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminaPaqAdic.Click
        Dim Resp = MsgBox("¿Deseas Eliminar el Paquete Adicional: " & Me.txtNombrePaqAdic.Text.ToString.ToUpper & " ?", MsgBoxStyle.YesNoCancel, "¿BORRAR EL PAQUETE AHORA?")

        If Resp = MsgBoxResult.Yes Then
            EliminaPaqAdic(eClv_PaqAdi)
        End If

    End Sub

    Private Sub btnModifica_RelPlanTarifario_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModifica_RelPlanTarifario.Click
        Me.cmbxServicio.Enabled = False
        Me.cmbxServicio.DataSource = Nothing
        Me.txtCostoContratacion.Text = Me.dgvRelServiciosPaqAdic.SelectedCells.Item(2).Value
        Me.txtCostoMensualidad.Text = Me.dgvRelServiciosPaqAdic.SelectedCells.Item(3).Value

        Me.btnGuardar_RelPlanTarifario.Enabled = True
        Me.btnCancela_RelPlanTarifario.Enabled = True

        Me.btnEliminar_RelPlanTarifario.Enabled = False
        Me.btnModifica_RelPlanTarifario.Enabled = False

        Me.btnGuardar_RelPlanTarifario.Text = "Actualizar"
        ActualizaDetPaqAdic = True

        Me.dgvRelServiciosPaqAdic.Enabled = False
        Me.cmbxServicio.Enabled = True

        Me.txtCostoContratacion.Enabled = True
        Me.txtCostoMensualidad.Enabled = True

    End Sub

    Private Sub btnCancela_RelPlanTarifario_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancela_RelPlanTarifario.Click

        'Me.btnGuardar_RelPlanTarifario.Enabled = False
        Me.btnCancela_RelPlanTarifario.Enabled = False
        Me.btnModifica_RelPlanTarifario.Enabled = True
        Me.btnEliminar_RelPlanTarifario.Enabled = True

        Me.txtCostoContratacion.Text = ""
        Me.txtCostoMensualidad.Text = ""

        Me.btnGuardar_RelPlanTarifario.Text = "Agregar"
        Me.dgvRelServiciosPaqAdic.Enabled = True

    End Sub

    Private Sub pbAyuda01_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbAyuda01.Click
        MsgBox("Los precios que aquí se asignen serán tomados cuando no sean incluidos en el Plan Tarifario.", MsgBoxStyle.Information, "PRECIOS UNITARIOS")
    End Sub
End Class


'Clase base para maninipular las Clasificaciones

Public Class Clasificaciones

    Dim _ident As Integer
    Dim _concepto As String
    Dim _tipo As Integer
    Public Property ident() As Integer
        Get
            Return _ident
        End Get
        Set(ByVal Value As Integer)
            _ident = Value
        End Set
    End Property

    Public Property concepto() As String
        Get
            Return _concepto
        End Get
        Set(ByVal Value As String)
            _concepto = Value
        End Set
    End Property
    Public Property tipo() As Integer
        Get
            Return _tipo
        End Get
        Set(ByVal Value As Integer)
            _tipo = Value
        End Set
    End Property


    Public Sub Clasificaciones(ByVal descripcion As String, ByVal costo As String, ByVal tipo As Integer)

        'Constructor de la Clase
        Me.ident = ident
        Me.concepto = concepto
        Me.tipo = tipo

    End Sub
End Class
