Imports System.Data.SqlClient
Public Class BrwPaqueteAdicional


    Private Sub BrwPaqueteAdicional_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Try
            If eBndPaqAdi = True Then
                eBndPaqAdi = False
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.ConPaqueteAdicionalTableAdapter.Connection = CON
                Me.ConPaqueteAdicionalTableAdapter.Fill(Me.DataSetEric2.ConPaqueteAdicional, 0, 0, "", 0)
                CON.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub BrwPaqueteAdicional_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConPaqueteAdicionalTableAdapter.Connection = CON
            Me.ConPaqueteAdicionalTableAdapter.Fill(Me.DataSetEric2.ConPaqueteAdicional, 0, 0, "", 0)
            CON.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'eOpcion = "N"
        'FrmPaqueteAdicional.Show()
        OpPaqAdic = "N"
        FrmPaqueteAdicionalEntreClasificaciones.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConPaqueteAdicionalDataGridView.RowCount = 0 Then
            MsgBox("No existen Paquetes a Consultar.")
            Exit Sub
        End If
        'eOpcion = "C"
        OpPaqAdic = "C"
        eClv_PaqAdi = Me.ConPaqueteAdicionalDataGridView.SelectedCells.Item(0).Value
        'FrmPaqueteAdicional.Show()
        FrmPaqueteAdicionalEntreClasificaciones.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.ConPaqueteAdicionalDataGridView.RowCount = 0 Then
            MsgBox("No existen Paquetes a Modificar.")
            Exit Sub
        End If
        eOpcion = "M"
        eClv_PaqAdi = Me.ConPaqueteAdicionalDataGridView.SelectedCells.Item(0).Value
        'FrmPaqueteAdicional.Show()
        OpPaqAdic = "M"
        FrmPaqueteAdicionalEntreClasificaciones.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub TipoCobroTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoCobroTextBox.TextChanged
        If TipoCobroTextBox.Text = 1 Then
            Me.Label1.Text = "Minutos:"
        ElseIf TipoCobroTextBox.Text = 2 Then
            Me.Label1.Text = "Eventos:"
        Else
            Me.Label1.Text = "Minutos/Eventos:"
        End If
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try
            If Me.TextBox1.Text.Length > 0 Then
                Dim CON2 As New SqlConnection(MiConexion)
                CON2.Open()
                Me.ConPaqueteAdicionalTableAdapter.Connection = CON2
                Me.ConPaqueteAdicionalTableAdapter.Fill(Me.DataSetEric2.ConPaqueteAdicional, 0, 0, Me.TextBox1.Text, 2)
                CON2.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Try
            If Asc(e.KeyChar) = 13 And Me.TextBox1.Text.Length > 0 Then

                Dim CON2 As New SqlConnection(MiConexion)
                CON2.Open()
                Me.ConPaqueteAdicionalTableAdapter.Connection = CON2
                Me.ConPaqueteAdicionalTableAdapter.Fill(Me.DataSetEric2.ConPaqueteAdicional, 0, 0, Me.TextBox1.Text, 2)
                CON2.Close()

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    
End Class