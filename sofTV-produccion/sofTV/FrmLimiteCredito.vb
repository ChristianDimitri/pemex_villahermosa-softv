﻿
Imports System.Data.SqlClient
Imports System.Text
Public Class FrmLimiteCredito



    Private Sub ConRelClienteLimiteCredito(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelClienteLimiteCredito", conexion)
        Dim CreditoLimitado As Boolean = False
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Contrato
        comando.Parameters.Add(par)

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                CreditoLimitado = reader(0).ToString
                tbCreditoFijo.Text = reader(1).ToString
                tbPagosPuntuales.Text = reader(2).ToString
                tbLimiteCredito.Text = reader(3).ToString
            End While

            If CreditoLimitado = True Then rbCreditoLimitado.Checked = True Else rbCreditoFijo.Checked = True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub NueRelClienteLimiteCredito(ByVal Contrato As Long, ByVal CreditoLimitado As Boolean, ByVal CreditoFijo As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelClienteLimiteCredito", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Contrato
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@CreditoLimitado", SqlDbType.Bit)
        par2.Direction = ParameterDirection.Input
        par2.Value = CreditoLimitado
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@CreditoFijo", SqlDbType.Money)
        par3.Direction = ParameterDirection.Input
        par3.Value = CreditoFijo
        comando.Parameters.Add(par3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            MsgBox(mensaje5, MsgBoxStyle.Information)
            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Private Sub FrmLimiteCredito_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        ConRelClienteLimiteCredito(Contrato)
    End Sub

    Private Sub GuardarToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarToolStripButton3.Click
        Dim CreditoLimitado As Boolean = False

        If rbCreditoLimitado.Checked = True Then CreditoLimitado = True

        If tbLimiteCredito.Text.Length = 0 Then tbCreditoFijo.Text = 0
        If IsNumeric(tbLimiteCredito.Text) = False Then tbCreditoFijo.Text = 0

        NueRelClienteLimiteCredito(Contrato, CreditoLimitado, tbCreditoFijo.Text)
    End Sub

    Private Sub tbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbSalir.Click
        Me.Close()
    End Sub

    Private Sub rbCreditoLimitado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbCreditoLimitado.CheckedChanged
        If rbCreditoLimitado.Checked = True Then
            panelCreditoLimitado.Enabled = True
            panelCreditoFijo.Enabled = False
        Else
            panelCreditoLimitado.Enabled = False
            panelCreditoFijo.Enabled = True
        End If
    End Sub
End Class