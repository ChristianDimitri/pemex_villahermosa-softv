<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImpresionEdoCuenta_DEV
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ImpresionEdoCuenta_DEV))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtPeriodoCobro = New System.Windows.Forms.TextBox
        Me.CMBLblContratoFin = New System.Windows.Forms.Label
        Me.CMBLblContratoIni = New System.Windows.Forms.Label
        Me.TxtContratoIni = New System.Windows.Forms.TextBox
        Me.TxtContratoFin = New System.Windows.Forms.TextBox
        Me.CMBBtnBusca2 = New System.Windows.Forms.Button
        Me.CMBBtnBusca1 = New System.Windows.Forms.Button
        Me.CMBBtnSalir = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.btnCargarPeriodo = New System.Windows.Forms.Button
        Me.PeriodosCobro = New System.Windows.Forms.DataGridView
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker
        Me.GrupoOrdenDeImpresion = New System.Windows.Forms.GroupBox
        Me.cheked3 = New System.Windows.Forms.PictureBox
        Me.cheked2 = New System.Windows.Forms.PictureBox
        Me.cheked1 = New System.Windows.Forms.PictureBox
        Me.CheckBox_ambas = New System.Windows.Forms.CheckBox
        Me.CheckBox_restantes = New System.Windows.Forms.CheckBox
        Me.CheckBox_primeras = New System.Windows.Forms.CheckBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnVisualizarReport = New System.Windows.Forms.Button
        Me.CMBBtnImprimir = New System.Windows.Forms.Button
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PeriodosCobro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GrupoOrdenDeImpresion.SuspendLayout()
        CType(Me.cheked3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cheked2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cheked1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtPeriodoCobro)
        Me.Panel1.Controls.Add(Me.CMBLblContratoFin)
        Me.Panel1.Controls.Add(Me.CMBLblContratoIni)
        Me.Panel1.Controls.Add(Me.TxtContratoIni)
        Me.Panel1.Controls.Add(Me.TxtContratoFin)
        Me.Panel1.Controls.Add(Me.CMBBtnBusca2)
        Me.Panel1.Controls.Add(Me.CMBBtnBusca1)
        Me.Panel1.Location = New System.Drawing.Point(23, 27)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(415, 245)
        Me.Panel1.TabIndex = 9
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(365, 169)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(38, 39)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 15
        Me.PictureBox2.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 169)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 16)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Periodo de Cobro:"
        '
        'txtPeriodoCobro
        '
        Me.txtPeriodoCobro.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeriodoCobro.Location = New System.Drawing.Point(149, 169)
        Me.txtPeriodoCobro.MaxLength = 6
        Me.txtPeriodoCobro.Multiline = True
        Me.txtPeriodoCobro.Name = "txtPeriodoCobro"
        Me.txtPeriodoCobro.ReadOnly = True
        Me.txtPeriodoCobro.Size = New System.Drawing.Size(189, 39)
        Me.txtPeriodoCobro.TabIndex = 6
        Me.txtPeriodoCobro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CMBLblContratoFin
        '
        Me.CMBLblContratoFin.AutoSize = True
        Me.CMBLblContratoFin.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLblContratoFin.Location = New System.Drawing.Point(6, 95)
        Me.CMBLblContratoFin.Name = "CMBLblContratoFin"
        Me.CMBLblContratoFin.Size = New System.Drawing.Size(116, 16)
        Me.CMBLblContratoFin.TabIndex = 5
        Me.CMBLblContratoFin.Text = "Contrato Final:"
        '
        'CMBLblContratoIni
        '
        Me.CMBLblContratoIni.AutoSize = True
        Me.CMBLblContratoIni.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLblContratoIni.Location = New System.Drawing.Point(6, 17)
        Me.CMBLblContratoIni.Name = "CMBLblContratoIni"
        Me.CMBLblContratoIni.Size = New System.Drawing.Size(125, 16)
        Me.CMBLblContratoIni.TabIndex = 4
        Me.CMBLblContratoIni.Text = "Contrato Inicial:"
        '
        'TxtContratoIni
        '
        Me.TxtContratoIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtContratoIni.Location = New System.Drawing.Point(149, 17)
        Me.TxtContratoIni.MaxLength = 6
        Me.TxtContratoIni.Multiline = True
        Me.TxtContratoIni.Name = "TxtContratoIni"
        Me.TxtContratoIni.Size = New System.Drawing.Size(189, 33)
        Me.TxtContratoIni.TabIndex = 1
        Me.TxtContratoIni.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TxtContratoFin
        '
        Me.TxtContratoFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtContratoFin.Location = New System.Drawing.Point(149, 92)
        Me.TxtContratoFin.MaxLength = 6
        Me.TxtContratoFin.Multiline = True
        Me.TxtContratoFin.Name = "TxtContratoFin"
        Me.TxtContratoFin.Size = New System.Drawing.Size(189, 33)
        Me.TxtContratoFin.TabIndex = 2
        Me.TxtContratoFin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CMBBtnBusca2
        '
        Me.CMBBtnBusca2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnBusca2.Location = New System.Drawing.Point(365, 95)
        Me.CMBBtnBusca2.Name = "CMBBtnBusca2"
        Me.CMBBtnBusca2.Size = New System.Drawing.Size(38, 30)
        Me.CMBBtnBusca2.TabIndex = 1
        Me.CMBBtnBusca2.Text = "..."
        Me.CMBBtnBusca2.UseVisualStyleBackColor = True
        '
        'CMBBtnBusca1
        '
        Me.CMBBtnBusca1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnBusca1.Location = New System.Drawing.Point(365, 17)
        Me.CMBBtnBusca1.Name = "CMBBtnBusca1"
        Me.CMBBtnBusca1.Size = New System.Drawing.Size(35, 33)
        Me.CMBBtnBusca1.TabIndex = 0
        Me.CMBBtnBusca1.Text = "..."
        Me.CMBBtnBusca1.UseVisualStyleBackColor = True
        '
        'CMBBtnSalir
        '
        Me.CMBBtnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnSalir.Image = CType(resources.GetObject("CMBBtnSalir.Image"), System.Drawing.Image)
        Me.CMBBtnSalir.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.CMBBtnSalir.Location = New System.Drawing.Point(165, 130)
        Me.CMBBtnSalir.Name = "CMBBtnSalir"
        Me.CMBBtnSalir.Size = New System.Drawing.Size(184, 57)
        Me.CMBBtnSalir.TabIndex = 8
        Me.CMBBtnSalir.Text = "&Salir"
        Me.CMBBtnSalir.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.CMBBtnSalir.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.btnCargarPeriodo)
        Me.GroupBox1.Controls.Add(Me.PeriodosCobro)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(503, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(504, 289)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Selecciona el Periodo de Cobro"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(471, 258)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(27, 25)
        Me.PictureBox1.TabIndex = 14
        Me.PictureBox1.TabStop = False
        '
        'btnCargarPeriodo
        '
        Me.btnCargarPeriodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCargarPeriodo.Image = CType(resources.GetObject("btnCargarPeriodo.Image"), System.Drawing.Image)
        Me.btnCargarPeriodo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCargarPeriodo.Location = New System.Drawing.Point(147, 234)
        Me.btnCargarPeriodo.Name = "btnCargarPeriodo"
        Me.btnCargarPeriodo.Size = New System.Drawing.Size(239, 49)
        Me.btnCargarPeriodo.TabIndex = 13
        Me.btnCargarPeriodo.Text = "Cargar el &Periodo Seleccionado"
        Me.btnCargarPeriodo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCargarPeriodo.UseVisualStyleBackColor = True
        '
        'PeriodosCobro
        '
        Me.PeriodosCobro.AllowUserToAddRows = False
        Me.PeriodosCobro.AllowUserToDeleteRows = False
        Me.PeriodosCobro.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.PeriodosCobro.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.PeriodosCobro.BackgroundColor = System.Drawing.SystemColors.ButtonFace
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.Padding = New System.Windows.Forms.Padding(2)
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.PeriodosCobro.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.PeriodosCobro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.PeriodosCobro.Location = New System.Drawing.Point(39, 25)
        Me.PeriodosCobro.Name = "PeriodosCobro"
        Me.PeriodosCobro.ReadOnly = True
        Me.PeriodosCobro.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.PeriodosCobro.Size = New System.Drawing.Size(430, 201)
        Me.PeriodosCobro.TabIndex = 12
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(189, 117)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(95, 27)
        Me.Button1.TabIndex = 16
        Me.Button1.Text = "Anterior"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(189, 148)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(95, 27)
        Me.Button2.TabIndex = 17
        Me.Button2.Text = "Banners"
        Me.Button2.UseVisualStyleBackColor = True
        Me.Button2.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Panel1)
        Me.GroupBox2.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(467, 289)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Ingresa el rango de contratos"
        '
        'BackgroundWorker1
        '
        '
        'GrupoOrdenDeImpresion
        '
        Me.GrupoOrdenDeImpresion.Controls.Add(Me.cheked3)
        Me.GrupoOrdenDeImpresion.Controls.Add(Me.cheked2)
        Me.GrupoOrdenDeImpresion.Controls.Add(Me.cheked1)
        Me.GrupoOrdenDeImpresion.Controls.Add(Me.CheckBox_ambas)
        Me.GrupoOrdenDeImpresion.Controls.Add(Me.CheckBox_restantes)
        Me.GrupoOrdenDeImpresion.Controls.Add(Me.CheckBox_primeras)
        Me.GrupoOrdenDeImpresion.Controls.Add(Me.Label4)
        Me.GrupoOrdenDeImpresion.Controls.Add(Me.Label3)
        Me.GrupoOrdenDeImpresion.Controls.Add(Me.Label2)
        Me.GrupoOrdenDeImpresion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrupoOrdenDeImpresion.Location = New System.Drawing.Point(12, 307)
        Me.GrupoOrdenDeImpresion.Name = "GrupoOrdenDeImpresion"
        Me.GrupoOrdenDeImpresion.Size = New System.Drawing.Size(467, 207)
        Me.GrupoOrdenDeImpresion.TabIndex = 18
        Me.GrupoOrdenDeImpresion.TabStop = False
        Me.GrupoOrdenDeImpresion.Text = "Selecciona el Orden de Impresión"
        '
        'cheked3
        '
        Me.cheked3.Image = CType(resources.GetObject("cheked3.Image"), System.Drawing.Image)
        Me.cheked3.Location = New System.Drawing.Point(23, 38)
        Me.cheked3.Name = "cheked3"
        Me.cheked3.Size = New System.Drawing.Size(24, 24)
        Me.cheked3.TabIndex = 17
        Me.cheked3.TabStop = False
        '
        'cheked2
        '
        Me.cheked2.Image = CType(resources.GetObject("cheked2.Image"), System.Drawing.Image)
        Me.cheked2.Location = New System.Drawing.Point(23, 176)
        Me.cheked2.Name = "cheked2"
        Me.cheked2.Size = New System.Drawing.Size(24, 24)
        Me.cheked2.TabIndex = 16
        Me.cheked2.TabStop = False
        Me.cheked2.Visible = False
        '
        'cheked1
        '
        Me.cheked1.Image = CType(resources.GetObject("cheked1.Image"), System.Drawing.Image)
        Me.cheked1.Location = New System.Drawing.Point(23, 104)
        Me.cheked1.Name = "cheked1"
        Me.cheked1.Size = New System.Drawing.Size(24, 24)
        Me.cheked1.TabIndex = 15
        Me.cheked1.TabStop = False
        Me.cheked1.Visible = False
        '
        'CheckBox_ambas
        '
        Me.CheckBox_ambas.AutoSize = True
        Me.CheckBox_ambas.Checked = True
        Me.CheckBox_ambas.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox_ambas.Location = New System.Drawing.Point(48, 48)
        Me.CheckBox_ambas.Name = "CheckBox_ambas"
        Me.CheckBox_ambas.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox_ambas.TabIndex = 10
        Me.CheckBox_ambas.UseVisualStyleBackColor = True
        '
        'CheckBox_restantes
        '
        Me.CheckBox_restantes.AutoSize = True
        Me.CheckBox_restantes.Location = New System.Drawing.Point(48, 186)
        Me.CheckBox_restantes.Name = "CheckBox_restantes"
        Me.CheckBox_restantes.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox_restantes.TabIndex = 9
        Me.CheckBox_restantes.UseVisualStyleBackColor = True
        '
        'CheckBox_primeras
        '
        Me.CheckBox_primeras.AutoSize = True
        Me.CheckBox_primeras.Location = New System.Drawing.Point(48, 114)
        Me.CheckBox_primeras.Name = "CheckBox_primeras"
        Me.CheckBox_primeras.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox_primeras.TabIndex = 8
        Me.CheckBox_primeras.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(67, 49)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(347, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Imprimir ambas Hojas (Estado de Cuenta Completo)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(69, 187)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(228, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Imprimir solo las Hojas Restantes"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(69, 114)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(371, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Imprimir solo las Primeras Hojas (Portada Edo. Cuenta)"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnVisualizarReport)
        Me.GroupBox3.Controls.Add(Me.CMBBtnImprimir)
        Me.GroupBox3.Controls.Add(Me.CMBBtnSalir)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(503, 307)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(504, 207)
        Me.GroupBox3.TabIndex = 19
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Imprimir o Visualizar"
        '
        'btnVisualizarReport
        '
        Me.btnVisualizarReport.Enabled = False
        Me.btnVisualizarReport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVisualizarReport.Image = CType(resources.GetObject("btnVisualizarReport.Image"), System.Drawing.Image)
        Me.btnVisualizarReport.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnVisualizarReport.Location = New System.Drawing.Point(293, 38)
        Me.btnVisualizarReport.Name = "btnVisualizarReport"
        Me.btnVisualizarReport.Size = New System.Drawing.Size(205, 57)
        Me.btnVisualizarReport.TabIndex = 9
        Me.btnVisualizarReport.Text = "&Visualizar el Edo. de Cuenta"
        Me.btnVisualizarReport.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnVisualizarReport.UseVisualStyleBackColor = True
        '
        'CMBBtnImprimir
        '
        Me.CMBBtnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnImprimir.Image = CType(resources.GetObject("CMBBtnImprimir.Image"), System.Drawing.Image)
        Me.CMBBtnImprimir.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.CMBBtnImprimir.Location = New System.Drawing.Point(6, 38)
        Me.CMBBtnImprimir.Name = "CMBBtnImprimir"
        Me.CMBBtnImprimir.Size = New System.Drawing.Size(205, 57)
        Me.CMBBtnImprimir.TabIndex = 3
        Me.CMBBtnImprimir.Text = "&Comenzar Impresión"
        Me.CMBBtnImprimir.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.CMBBtnImprimir.UseVisualStyleBackColor = True
        '
        'ImpresionEdoCuenta_DEV
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1014, 524)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GrupoOrdenDeImpresion)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "ImpresionEdoCuenta_DEV"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Impresión de Estados de Cuenta"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PeriodosCobro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GrupoOrdenDeImpresion.ResumeLayout(False)
        Me.GrupoOrdenDeImpresion.PerformLayout()
        CType(Me.cheked3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cheked2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cheked1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBLblContratoFin As System.Windows.Forms.Label
    Friend WithEvents CMBLblContratoIni As System.Windows.Forms.Label
    Friend WithEvents TxtContratoIni As System.Windows.Forms.TextBox
    Friend WithEvents TxtContratoFin As System.Windows.Forms.TextBox
    Friend WithEvents CMBBtnBusca2 As System.Windows.Forms.Button
    Friend WithEvents CMBBtnBusca1 As System.Windows.Forms.Button
    Friend WithEvents CMBBtnSalir As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents PeriodosCobro As System.Windows.Forms.DataGridView
    Friend WithEvents btnCargarPeriodo As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPeriodoCobro As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents GrupoOrdenDeImpresion As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CheckBox_ambas As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox_restantes As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox_primeras As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cheked3 As System.Windows.Forms.PictureBox
    Friend WithEvents cheked2 As System.Windows.Forms.PictureBox
    Friend WithEvents cheked1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents btnVisualizarReport As System.Windows.Forms.Button
    Friend WithEvents CMBBtnImprimir As System.Windows.Forms.Button

End Class
