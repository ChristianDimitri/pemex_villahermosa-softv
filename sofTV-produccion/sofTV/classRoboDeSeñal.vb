﻿Imports System.Data.SqlClient

Public Class classRoboDeSeñal

    'Dim RobaSeñal As New BaseII

    Public Function buscaDT(ByVal prmContrato As Long) As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(prmContrato))
            buscaDT = BaseII.ConsultaDT("ConRoboDeSeñal")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub Actualiza(ByVal prmContrato As Long, ByVal prmDescripcion As String, ByVal Monto As Decimal, ByVal Cobrado As Boolean)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(prmContrato))
            BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, CStr(prmDescripcion))
            BaseII.CreateMyParameter("@Monto", SqlDbType.Money, CDec(Monto))
            BaseII.CreateMyParameter("@Cobrado", SqlDbType.Bit, CByte(Cobrado))

            BaseII.Inserta("NueRoboDeSeñal")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub Elimina(ByVal prmContrato As Long)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(prmContrato))
            BaseII.Inserta("BorRoboDeSeñal")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
