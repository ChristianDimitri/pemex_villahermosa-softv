Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Public Class PruebasEdoCuenta

    Private Sub PruebasEdoCuenta_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Clv_Periodo_Cobro_VisorEdoCta = 0
        Contrato_VisorEdoCta = 0
        
    End Sub

    Private Sub PruebasEdoCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        muestraEdoCuenta()
    End Sub
    Private Sub muestraEdoCuenta()
        Try

            Me.Text = "Estado de Cuenta del Contrato: " & Contrato_VisorEdoCta.ToString & " del Periodo: " & Clv_Periodo_Cobro_VisorEdoCta.ToString

            'customersByCityReport = New EstadoDeCuentaFinal
            Dim customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            Dim reportPath As String = Nothing

            'Si los periodos de cobro son menores  a 12 mostramos los reportes anteriores
            If (Clv_Periodo_Cobro_VisorEdoCta <= 12) Then
                reportPath = RutaReportes & "\EdoCuentaFinal_BannersDinamicos_periodos_menor_12.rpt"
            Else
                reportPath = RutaReportes & "\EdoCuentaFinal_BannersDinamicos.rpt"
            End If


            'Concatenamos la ruta de los reportes


            customersByCityReport.Load(reportPath)

            Dim connection As IConnectionInfo
            'Dim serverName1 As String = GlobalServerName
            'Dim serverName1 As String = "192.168.1.114"
            Dim serverName1 As String = "10.4.247.10"
            'Dim serverName1 As String = "192.168.0.11"

            Dim GlobalServerName As String = "10.4.247.10"
            Dim GlobalUser As String = "sa"
            Dim GlobalPassword As String = "inte@321"

            ' Establecer conexi�n con base de datos al informe principal
            For Each connection In customersByCityReport.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetConnection(GlobalServerName, GloDatabaseName, False)
                        connection.SetLogon(GlobalUser, GlobalPassword)
                End Select
            Next

            ' Establecer conexi�n al subinforme
            Dim subreport As ReportDocument
            For Each subreport In customersByCityReport.Subreports
                For Each connection In subreport.DataSourceConnections
                    connection.SetConnection(GlobalServerName, GloDatabaseName, False)
                    connection.SetLogon(GlobalUser, GlobalPassword)
                Next
            Next


            'miContrato = CInt(Contrato)

            customersByCityReport.SetParameterValue(0, Clv_Periodo_Cobro_VisorEdoCta)   '--------------------------agregada----------------------------
            customersByCityReport.SetParameterValue(1, Contrato_VisorEdoCta)   '--------------------------agregada----------------------------
            '@contrato1
            customersByCityReport.SetParameterValue(2, Contrato_VisorEdoCta)
            'contrato2
            customersByCityReport.SetParameterValue(3, Contrato_VisorEdoCta)
            'Cargamos el Reporte en base al periodo de cobro establecido por el usuario
            'customersByCityReport.SetParameterValue(0, Contrato_VisorEdoCta)
            'customersByCityReport.SetParameterValue(1, Contrato_VisorEdoCta)


            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(100)

        Catch ex As Exception
            MsgBox("Ocurrio un error al cargar el reporte, parece ser que no se encontr� el reporte � la ruta del mismo no existe. Verifica con tu administrador que el reporte exista y se encuentre en su lugar por defecto.", MsgBoxStyle.Critical)
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

End Class