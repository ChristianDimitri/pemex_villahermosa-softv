﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System
Imports System.IO.StreamReader
Imports System.IO.File
Public Class FrmDatosProveedorPortabilidad

    Private Sub FrmDatosProveedorPortabilidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If OPCIONPP = "C" Then
            Me.Panel1.Enabled = False
            Me.Button1.Enabled = False
            datos()
        End If

        If OPCIONPP = "M" Then
            datos()
        End If

        If OPCIONPP = "N" Then
            Me.TextBox1.Text = ""
            Me.TextBox2.Text = ""
            CheckBox1.Checked = False
        End If
    End Sub
    Private Sub datos()
        muestraProvPortabilidad(IdPP)
    End Sub
    Private Sub muestraProvPortabilidad(ByVal id As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("muestraProvPortabilidad", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@id", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = id
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@nombre", SqlDbType.VarChar, 250)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@clave", SqlDbType.BigInt)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@activo", SqlDbType.Bit)
        par4.Direction = ParameterDirection.Output
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()

            Me.TextBox1.Text = CStr(par3.Value.ToString)
            Me.TextBox2.Text = CStr(par2.Value.ToString)

            CheckBox1.Checked = CBool(par4.Value)

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()

        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If Len(Me.TextBox1.Text) = 0 Then
            MsgBox("Debe Introduccir La Clave")
        ElseIf Len(Me.TextBox2.Text) = 0 Then
            MsgBox("Debe Introduccir El Nombre Del Proveedor")
        Else
            modificaDatosProvPorta(IdPP, Me.TextBox2.Text, CLng(Me.TextBox1.Text), Me.CheckBox1.Checked)
            MsgBox("Los Datos Sean Guardado Correctamente")
            Me.Close()
            Me.Dispose()
        End If



    End Sub
    Private Sub modificaDatosProvPorta(ByVal id As Long, ByVal nombre As String, ByVal clave As Long, ByVal activo As Boolean)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("modificaDatosProvPorta", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@id", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = id
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@nombre", SqlDbType.VarChar, 250)
        par2.Direction = ParameterDirection.Input
        par2.Value = nombre
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@clave", SqlDbType.BigInt)
        par3.Direction = ParameterDirection.Input
        par3.Value = clave
        com.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@activo", SqlDbType.Bit)
        par4.Direction = ParameterDirection.Input
        par4.Value = activo
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()


        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
End Class