﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSeleccionaLimite
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.AceptarButton = New System.Windows.Forms.Button()
        Me.CancelarButton = New System.Windows.Forms.Button()
        Me.LimiteNumeric = New System.Windows.Forms.NumericUpDown()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CMBLabel = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ConllamadaCheckBox = New System.Windows.Forms.CheckBox()
        Me.SinLlamadaCheckBox = New System.Windows.Forms.CheckBox()
        Me.ComparacionComboBox = New System.Windows.Forms.ComboBox()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.SucursalComboBox = New System.Windows.Forms.ComboBox()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.LimiteNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'AceptarButton
        '
        Me.AceptarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AceptarButton.Location = New System.Drawing.Point(12, 222)
        Me.AceptarButton.Name = "AceptarButton"
        Me.AceptarButton.Size = New System.Drawing.Size(120, 36)
        Me.AceptarButton.TabIndex = 0
        Me.AceptarButton.Text = "&Aceptar"
        Me.AceptarButton.UseVisualStyleBackColor = True
        '
        'CancelarButton
        '
        Me.CancelarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CancelarButton.Location = New System.Drawing.Point(138, 222)
        Me.CancelarButton.Name = "CancelarButton"
        Me.CancelarButton.Size = New System.Drawing.Size(120, 36)
        Me.CancelarButton.TabIndex = 1
        Me.CancelarButton.Text = "&Cancelar"
        Me.CancelarButton.UseVisualStyleBackColor = True
        '
        'LimiteNumeric
        '
        Me.LimiteNumeric.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LimiteNumeric.Location = New System.Drawing.Point(141, 143)
        Me.LimiteNumeric.Minimum = New Decimal(New Integer() {75, 0, 0, 0})
        Me.LimiteNumeric.Name = "LimiteNumeric"
        Me.LimiteNumeric.Size = New System.Drawing.Size(59, 22)
        Me.LimiteNumeric.TabIndex = 5
        Me.LimiteNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.LimiteNumeric.Value = New Decimal(New Integer() {75, 0, 0, 0})
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(89, 9)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(62, 20)
        Me.CMBLabel1.TabIndex = 6
        Me.CMBLabel1.Text = "Status"
        '
        'CMBLabel
        '
        Me.CMBLabel.AutoSize = True
        Me.CMBLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel.Location = New System.Drawing.Point(94, 85)
        Me.CMBLabel.Name = "CMBLabel"
        Me.CMBLabel.Size = New System.Drawing.Size(57, 20)
        Me.CMBLabel.TabIndex = 7
        Me.CMBLabel.Text = "Límite"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(206, 147)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(22, 18)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "%"
        '
        'ConllamadaCheckBox
        '
        Me.ConllamadaCheckBox.AutoSize = True
        Me.ConllamadaCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConllamadaCheckBox.Location = New System.Drawing.Point(12, 49)
        Me.ConllamadaCheckBox.Name = "ConllamadaCheckBox"
        Me.ConllamadaCheckBox.Size = New System.Drawing.Size(107, 20)
        Me.ConllamadaCheckBox.TabIndex = 9
        Me.ConllamadaCheckBox.Text = "Con Llamada"
        Me.ConllamadaCheckBox.UseVisualStyleBackColor = True
        '
        'SinLlamadaCheckBox
        '
        Me.SinLlamadaCheckBox.AutoSize = True
        Me.SinLlamadaCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SinLlamadaCheckBox.Location = New System.Drawing.Point(152, 49)
        Me.SinLlamadaCheckBox.Name = "SinLlamadaCheckBox"
        Me.SinLlamadaCheckBox.Size = New System.Drawing.Size(102, 20)
        Me.SinLlamadaCheckBox.TabIndex = 10
        Me.SinLlamadaCheckBox.Text = "Sin Llamada"
        Me.SinLlamadaCheckBox.UseVisualStyleBackColor = True
        '
        'ComparacionComboBox
        '
        Me.ComparacionComboBox.DisplayMember = "COMPARACION"
        Me.ComparacionComboBox.FormattingEnabled = True
        Me.ComparacionComboBox.Location = New System.Drawing.Point(141, 116)
        Me.ComparacionComboBox.Name = "ComparacionComboBox"
        Me.ComparacionComboBox.Size = New System.Drawing.Size(59, 21)
        Me.ComparacionComboBox.TabIndex = 11
        Me.ComparacionComboBox.ValueMember = "COMPARACION"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(33, 117)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(89, 16)
        Me.CMBLabel3.TabIndex = 12
        Me.CMBLabel3.Text = "Conidicion :"
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.Location = New System.Drawing.Point(33, 147)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(57, 16)
        Me.CMBLabel4.TabIndex = 13
        Me.CMBLabel4.Text = "Límite :"
        '
        'SucursalComboBox
        '
        Me.SucursalComboBox.DisplayMember = "Ciudad"
        Me.SucursalComboBox.FormattingEnabled = True
        Me.SucursalComboBox.Location = New System.Drawing.Point(84, 184)
        Me.SucursalComboBox.Name = "SucursalComboBox"
        Me.SucursalComboBox.Size = New System.Drawing.Size(170, 21)
        Me.SucursalComboBox.TabIndex = 14
        Me.SucursalComboBox.ValueMember = "Clv_Txt"
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.Location = New System.Drawing.Point(9, 185)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(76, 16)
        Me.CMBLabel5.TabIndex = 15
        Me.CMBLabel5.Text = "Sucursal :"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmSeleccionaLimite
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(266, 264)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Controls.Add(Me.SucursalComboBox)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.ComparacionComboBox)
        Me.Controls.Add(Me.SinLlamadaCheckBox)
        Me.Controls.Add(Me.ConllamadaCheckBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CMBLabel)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.LimiteNumeric)
        Me.Controls.Add(Me.CancelarButton)
        Me.Controls.Add(Me.AceptarButton)
        Me.Name = "FrmSeleccionaLimite"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccion de Limite"
        CType(Me.LimiteNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents AceptarButton As System.Windows.Forms.Button
    Friend WithEvents CancelarButton As System.Windows.Forms.Button
    Friend WithEvents LimiteNumeric As System.Windows.Forms.NumericUpDown
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ConllamadaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SinLlamadaCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ComparacionComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents SucursalComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
