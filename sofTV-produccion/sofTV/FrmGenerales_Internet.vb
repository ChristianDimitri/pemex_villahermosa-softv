Imports System.Data.SqlClient
Public Class FrmGenerales_Internet
    Private IpServerC4 As String = Nothing
    Private Comunidad As String = Nothing
    Private NumeroDeDias As String = Nothing
    Private PaquetesAsignados As String = Nothing
    Dim existe As Integer = 0

    Private Sub BuscaDatosBitacora()
        Try
            IpServerC4 = Me.TextBox2.Text
            Comunidad = Me.TextBox1.Text
            NumeroDeDias = Me.Num_dia_habilitarTextBox.Text
            PaquetesAsignados = Me.Paq_defaultTextBox.Text
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub GuadaDatosBitacora(ByVal Op As String)
        Try
            If Op = 0 Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Ip Server C4", IpServerC4, Me.TextBox2.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Comunidad", Comunidad, Me.TextBox1.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Dias de Paquete de Prueba", NumeroDeDias, Me.Num_dia_habilitarTextBox.Text, LocClv_Ciudad)
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Paquete(s) Asignado(s) Por Default", PaquetesAsignados, Me.Paq_defaultTextBox.Text, LocClv_Ciudad)
            ElseIf Op = 1 Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimin� Generales de Internet", "IpServerC4: " + IpServerC4 + " / Comunidad: " + Comunidad + " / D�as de Paquete de Prueba: " + NumeroDeDias + " / Paquete(s) Asignado(s) Por Default: " + PaquetesAsignados, "", LocClv_Ciudad)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Busca()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONGenerales_InternetTableAdapter.Connection = CON
            Me.CONGenerales_InternetTableAdapter.Fill(Me.DataSetEDGAR.CONGenerales_Internet, 1)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
        BuscaDatosBitacora()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub FrmGenerales_Internet_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Busca()
        If (Me.Clv_IdTextBox.Text = "") Then
            Me.CONGenerales_InternetBindingSource.AddNew()
        End If
        busca_CNR7()

    End Sub
    Private Sub CONGenerales_InternetBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONGenerales_InternetBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONGenerales_InternetBindingSource.EndEdit()
        Me.CONGenerales_InternetTableAdapter.Connection = CON
        Me.CONGenerales_InternetTableAdapter.Update(Me.DataSetEDGAR.CONGenerales_Internet)
        MsgBox(mensaje5)
        'PARA GUARDAR EN LA TABLA Generales_Cnr7

        Dim COMANDO As New SqlCommand("Agrega_Generales_CR7", CON)
        COMANDO.CommandType = CommandType.StoredProcedure

        Dim PARAMETRO1 As New SqlParameter("@Criteria", SqlDbType.VarChar)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = txtcriteria.Text
        COMANDO.Parameters.Add(PARAMETRO1)

        PARAMETRO1 = New SqlParameter("@Complemento", SqlDbType.VarChar)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = txtcom.Text
        COMANDO.Parameters.Add(PARAMETRO1)

        PARAMETRO1 = New SqlParameter("@PolSus", SqlDbType.VarChar)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = txtpolsus.Text
        COMANDO.Parameters.Add(PARAMETRO1)

        PARAMETRO1 = New SqlParameter("@ruta", SqlDbType.VarChar)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = Txtruta.Text
        COMANDO.Parameters.Add(PARAMETRO1)

        PARAMETRO1 = New SqlParameter("@usuario", SqlDbType.VarChar)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = Txtusuario.Text
        COMANDO.Parameters.Add(PARAMETRO1)

        PARAMETRO1 = New SqlParameter("@Password", SqlDbType.VarChar)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = txtpass.Text
        COMANDO.Parameters.Add(PARAMETRO1)

        PARAMETRO1 = New SqlParameter("@correo", SqlDbType.VarChar)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = txtcorreo.Text
        COMANDO.Parameters.Add(PARAMETRO1)

        Try
            COMANDO.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
        GuadaDatosBitacora(0)
        Me.Close()
    End Sub
    Private Sub BindingNavigatorDeleteItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        GuadaDatosBitacora(1)
        CON.Open()

        Me.CONGenerales_InternetTableAdapter.Connection = CON
        Me.CONGenerales_InternetTableAdapter.Delete(Me.Clv_IdTextBox.Text)
        'ELIMINAR DE GENERALES_CNR7
        Dim COMANDO As New SqlCommand("Elimina_Generales_CR7", CON)
        COMANDO.CommandType = CommandType.StoredProcedure

        Try
            COMANDO.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

        MsgBox(mensaje6)
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FolderBrowserDialog1.ShowDialog()
        Txtruta.Text = FolderBrowserDialog1.SelectedPath.ToString()
    End Sub

    Private Sub busca_CNR7()
        'BUSCA SI HAY ALGO EN GENERALES_CNR7
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        Dim COMANDO As New SqlCommand("Busca_Generales_CR7", CON)
        COMANDO.CommandType = CommandType.StoredProcedure

        Dim PARAMETRO1 As New SqlParameter("@Existe", SqlDbType.Int)
        PARAMETRO1.Direction = ParameterDirection.Output
        COMANDO.Parameters.Add(PARAMETRO1)

        Dim PARAMETRO2 As New SqlParameter("@Crit", SqlDbType.VarChar, 8000)
        PARAMETRO2.Direction = ParameterDirection.Output
        COMANDO.Parameters.Add(PARAMETRO2)

        Dim PARAMETRO3 As New SqlParameter("@Com", SqlDbType.VarChar, 8000)
        PARAMETRO3.Direction = ParameterDirection.Output
        COMANDO.Parameters.Add(PARAMETRO3)

        Dim PARAMETRO4 As New SqlParameter("@polsus", SqlDbType.VarChar, 8000)
        PARAMETRO4.Direction = ParameterDirection.Output
        COMANDO.Parameters.Add(PARAMETRO4)

        Dim PARAMETRO5 As New SqlParameter("@ruta", SqlDbType.VarChar, 8000)
        PARAMETRO5.Direction = ParameterDirection.Output
        COMANDO.Parameters.Add(PARAMETRO5)

        Dim PARAMETRO6 As New SqlParameter("@usuario", SqlDbType.VarChar, 8000)
        PARAMETRO6.Direction = ParameterDirection.Output
        COMANDO.Parameters.Add(PARAMETRO6)

        Dim PARAMETRO7 As New SqlParameter("@password", SqlDbType.VarChar, 8000)
        PARAMETRO7.Direction = ParameterDirection.Output
        COMANDO.Parameters.Add(PARAMETRO7)

        Dim PARAMETRO8 As New SqlParameter("@correo", SqlDbType.VarChar, 8000)
        PARAMETRO8.Direction = ParameterDirection.Output
        COMANDO.Parameters.Add(PARAMETRO8)

        Try
            COMANDO.ExecuteNonQuery()
            existe = CInt(PARAMETRO1.Value)
            If existe = 1 Then
                txtpolsus.Text = PARAMETRO4.Value
                Txtruta.Text = PARAMETRO5.Value
                txtcom.Text = PARAMETRO3.Value
                txtcorreo.Text = PARAMETRO8.Value
                txtpass.Text = PARAMETRO7.Value
                txtcriteria.Text = PARAMETRO2.Value
                Txtusuario.Text = PARAMETRO6.Value
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

    End Sub

    Private Sub Num_dia_habilitarTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Num_dia_habilitarTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.Num_dia_habilitarTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub
End Class