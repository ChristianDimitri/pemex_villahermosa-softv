Imports System.Data.SqlClient

Public Class FrmReactivarContrato
    Dim eRes As Integer
    Dim eMsg As String = Nothing
    Public ReactivacionContrato As Boolean = False
    Public ContratoAReactivar As Integer = 0
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub FrmReactivarContrato_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'If pasa = 1 Then
        If tempocontrato > 0 Then
            Me.TextBox1.Text = tempocontrato
            tempocontrato = 0
        Else

        End If
        'ElseIf pasa = 2 Then
        'Me.TextBox1.Clear()
        'End If


    End Sub

    Private Sub FrmReactivarContrato_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        ReactivacionContrato = False
        ContratoAReactivar = 0

    End Sub

    Private Sub FrmReactivarContrato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Reactivar()
    End Sub
    Private Sub Reactivar()

        If IsNumeric(Me.TextBox1.Text) = True And GloClv_Servicio > 0 Then

            GrabaReactivacion()
        ElseIf GloClv_Servicio = 0 And IsNumeric(Me.TextBox1.Text) = True Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameClientesInactivosTableAdapter.Connection = CON
            Me.DameClientesInactivosTableAdapter.Fill(Me.DataSetEric.DameClientesInactivos, Me.TextBox1.Text, "", "", "", "", 0)

            If IsNumeric(Me.TextBox2.Text) = True Then

                Dim resp As MsgBoxResult = MsgBoxResult.Cancel
                resp = MsgBox("El Contrato NO Tiene Asignado un Servicio, � Desea Hacerlo ? ", MsgBoxStyle.YesNo)
                If resp = MsgBoxResult.Yes Then
                    tempocontrato = Me.TextBox1.Text
                    FrmSelPaquete_Reactiva.ShowDialog()
                    If IsNumeric(Me.TextBox1.Text) = True And GloClv_Servicio > 0 Then
                        ContratoAReactivar = Me.TextBox1.Text
                        GrabaReactivacion()
                    End If
                ElseIf resp = MsgBoxResult.No Then
                    Me.TextBox1.Clear()
                End If

            Else
                MsgBox("El Contrato Seleccionado Ya se Encuentra Activado")
            End If
        Else
            MsgBox("Teclee Un Contrato Por Favor", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Reactivar()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        eContratoRec = 0
        BrwSelContratoInactivos.Show()
    End Sub

    'M�todo que Graba la reactivaci�n del Contrato
    Private Sub GrabaReactivacion()



        'Se quitaron los DataSets
        Dim conn As New SqlConnection(MiConexion)

        Try
            ContratoAReactivar = TextBox1.Text

            conn.Open()
            Dim comando As New SqlClient.SqlCommand("ReactivaContrato", conn)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.Add("@Contrato", SqlDbType.Int).Value = ContratoAReactivar
            comando.Parameters.Add("@Res", SqlDbType.Int).Value = eRes
            comando.Parameters.Add("@Msg", SqlDbType.VarChar, 400).Value = eMsg
            comando.Parameters.Add("@clv_servicio", SqlDbType.Int).Value = GloClv_Servicio
            comando.Parameters.Add("@Tipo", SqlDbType.Int).Value = GloClv_TipSer
            comando.Parameters.Add("@seRenta", SqlDbType.Int).Value = eCabModPropio
            comando.Parameters.Add("@TipSerReactivado", SqlDbType.Int).Value = 0

            comando.Parameters("@Msg").Direction = ParameterDirection.Output
            comando.Parameters("@TipSerReactivado").Direction = ParameterDirection.Output

            comando.ExecuteNonQuery()

            If Not IsDBNull(comando.Parameters("@Msg").Value) Then
                eMsg = comando.Parameters("@Msg").Value
            End If

            If Not IsDBNull(comando.Parameters("@TipSerReactivado").Value) Then
                If IsNumeric(comando.Parameters("@TipSerReactivado").Value) Then
                    If (comando.Parameters("@TipSerReactivado").Value) = 1 Then
                        'Mandamos mostrar el n�mero de Televisiones Adiocnales que tendr�
                        ReactivacionContrato = True
                        ContratoAReactivar = CLng(Me.TextBox1.Text)
                        FrmTvAdicionales.ShowDialog()
                        ReactivacionContrato = False
                        ContratoAReactivar = 0
                    End If
                End If

            End If

            'Guardamos la bit�cora de la Reactivaci�n
            bitsist(GloUsuario, Me.TextBox1.Text, LocGloSistema, Me.Text, "", Me.CMBLabel1.Text, GloSucursal, LocClv_Ciudad)
            GloClv_Servicio = 0
            MsgBox(eMsg)
            pasa = 0
            Me.TextBox1.Clear()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Me.TextBox1.Text = 0
            GloClv_Servicio = 0
        Finally
            conn.Close()
            conn.Dispose()
        End Try


    End Sub

End Class