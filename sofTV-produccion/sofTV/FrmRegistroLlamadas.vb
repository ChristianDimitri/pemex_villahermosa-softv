﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualBasic

Public Class FrmRegistroLlamadas

    Private Sub CancelarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelarButton.Click
        Me.Close()
    End Sub

    Private Sub FrmRegistroLlamadas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CONREGISTROLIMITEDECREDITO()
        If TipoRegistro = 1 Then
            Me.LLAMADADateTime.Enabled = False
            Me.RESPUESTATextBox.Enabled = False
            Me.SOLUCIONTextBox.Enabled = False
            Me.AceptarButton.Visible = False
            Me.CancelarButton.Text = "&Salir"
        Else
            Me.LLAMADADateTime.Enabled = True
            Me.RESPUESTATextBox.Enabled = True
            Me.SOLUCIONTextBox.Enabled = True
            Me.AceptarButton.Visible = True
            Me.CancelarButton.Text = "&Cancelar"
        End If
    End Sub

    Private Sub CONREGISTROLIMITEDECREDITO()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("CONREGISTROLIMITEDECREDITO", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@ID_MOV", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = IdRegistroLlamada
        CMD.Parameters.Add(PRM1)

        Dim READER As SqlDataReader

        Try
            CON.Open()
            READER = CMD.ExecuteReader

            While (READER.Read)
                CONTRATOTextBox.Text = READER(0).ToString
                NOMBRETextBox.Text = READER(1).ToString
                GeneradoDateTime.Value = READER(2).ToString
                STATUSTextBox.Text = READER(3).ToString
                USUARIOTextBox.Text = READER(4).ToString
                LLAMADADateTime.Value = READER(5).ToString
                RESPUESTATextBox.Text = READER(6).ToString
                SOLUCIONTextBox.Text = READER(7).ToString
                TelefonoTextBox.Text = READER(8).ToString
                LimiteTextBox.Text = READER(9).ToString
                ConsumoTextBox.Text = READER(10).ToString
                SucursalTextBox.Text = READER(11).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub GUARDAREGISTROLIMITEDECREDITO()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("GUARDAREGISTROLIMITEDECREDITO", CON)
        CMD.CommandType = CommandType.StoredProcedure


        Dim PRM1 As New SqlParameter("@ID_MOV", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = IdRegistroLlamada
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@FEC_LLAMADA", SqlDbType.DateTime)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = Me.LLAMADADateTime.Value
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@CLV_USUARIO", SqlDbType.VarChar, 5)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = GloUsuario
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@RESPUESTA", SqlDbType.VarChar, 8000)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = Me.RESPUESTATextBox.Text
        CMD.Parameters.Add(PRM4)

        Dim PRM5 As New SqlParameter("@SOLUCION", SqlDbType.VarChar, 8000)
        PRM5.Direction = ParameterDirection.Input
        PRM5.Value = Me.SOLUCIONTextBox.Text
        CMD.Parameters.Add(PRM5)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            MsgBox("Cambios Almacenados Exitosamente", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub AceptarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AceptarButton.Click
        GUARDAREGISTROLIMITEDECREDITO()
        Me.Close()
    End Sub

    Private Sub GeneradoDateTime_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneradoDateTime.ValueChanged
        Me.LLAMADADateTime.MinDate = Me.GeneradoDateTime.Value
    End Sub

    Private Sub LLAMADADateTime_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LLAMADADateTime.ValueChanged
        Me.LLAMADADateTime.MaxDate = Today
    End Sub
End Class