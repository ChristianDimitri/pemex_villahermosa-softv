<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Generales_Prosa_Bancomer
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblAfiliacion = New System.Windows.Forms.Label
        Me.LblNomCom = New System.Windows.Forms.Label
        Me.TxtAfiliacion = New System.Windows.Forms.TextBox
        Me.TxtNomComercio = New System.Windows.Forms.TextBox
        Me.CMBBtnGuardar = New System.Windows.Forms.Button
        Me.CMBBtnSalir = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.CMBLblIntod = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'LblAfiliacion
        '
        Me.LblAfiliacion.AutoSize = True
        Me.LblAfiliacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAfiliacion.Location = New System.Drawing.Point(51, 17)
        Me.LblAfiliacion.Name = "LblAfiliacion"
        Me.LblAfiliacion.Size = New System.Drawing.Size(128, 16)
        Me.LblAfiliacion.TabIndex = 0
        Me.LblAfiliacion.Text = "No. De Afiliación:"
        '
        'LblNomCom
        '
        Me.LblNomCom.AutoSize = True
        Me.LblNomCom.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNomCom.Location = New System.Drawing.Point(14, 64)
        Me.LblNomCom.Name = "LblNomCom"
        Me.LblNomCom.Size = New System.Drawing.Size(165, 16)
        Me.LblNomCom.TabIndex = 1
        Me.LblNomCom.Text = "Nombre Del Comercio:"
        '
        'TxtAfiliacion
        '
        Me.TxtAfiliacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtAfiliacion.Location = New System.Drawing.Point(181, 16)
        Me.TxtAfiliacion.MaxLength = 7
        Me.TxtAfiliacion.Name = "TxtAfiliacion"
        Me.TxtAfiliacion.Size = New System.Drawing.Size(172, 21)
        Me.TxtAfiliacion.TabIndex = 2
        '
        'TxtNomComercio
        '
        Me.TxtNomComercio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtNomComercio.Location = New System.Drawing.Point(181, 63)
        Me.TxtNomComercio.MaxLength = 10
        Me.TxtNomComercio.Name = "TxtNomComercio"
        Me.TxtNomComercio.Size = New System.Drawing.Size(172, 21)
        Me.TxtNomComercio.TabIndex = 3
        '
        'CMBBtnGuardar
        '
        Me.CMBBtnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnGuardar.Location = New System.Drawing.Point(155, 154)
        Me.CMBBtnGuardar.Name = "CMBBtnGuardar"
        Me.CMBBtnGuardar.Size = New System.Drawing.Size(111, 32)
        Me.CMBBtnGuardar.TabIndex = 4
        Me.CMBBtnGuardar.Text = "&GUARDAR"
        Me.CMBBtnGuardar.UseVisualStyleBackColor = True
        '
        'CMBBtnSalir
        '
        Me.CMBBtnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnSalir.Location = New System.Drawing.Point(283, 154)
        Me.CMBBtnSalir.Name = "CMBBtnSalir"
        Me.CMBBtnSalir.Size = New System.Drawing.Size(95, 32)
        Me.CMBBtnSalir.TabIndex = 5
        Me.CMBBtnSalir.Text = "&SALIR"
        Me.CMBBtnSalir.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.TxtNomComercio)
        Me.Panel1.Controls.Add(Me.TxtAfiliacion)
        Me.Panel1.Controls.Add(Me.LblNomCom)
        Me.Panel1.Controls.Add(Me.LblAfiliacion)
        Me.Panel1.Location = New System.Drawing.Point(10, 44)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(368, 95)
        Me.Panel1.TabIndex = 6
        '
        'CMBLblIntod
        '
        Me.CMBLblIntod.AutoSize = True
        Me.CMBLblIntod.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLblIntod.Location = New System.Drawing.Point(20, 9)
        Me.CMBLblIntod.Name = "CMBLblIntod"
        Me.CMBLblIntod.Size = New System.Drawing.Size(343, 20)
        Me.CMBLblIntod.TabIndex = 7
        Me.CMBLblIntod.Text = "Datos Bancarios Bancomer Archivo Prosa"
        '
        'Generales_Prosa_Bancomer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(399, 207)
        Me.Controls.Add(Me.CMBLblIntod)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.CMBBtnSalir)
        Me.Controls.Add(Me.CMBBtnGuardar)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Generales_Prosa_Bancomer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generales Prosa Bancomer"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LblAfiliacion As System.Windows.Forms.Label
    Friend WithEvents LblNomCom As System.Windows.Forms.Label
    Friend WithEvents TxtAfiliacion As System.Windows.Forms.TextBox
    Friend WithEvents TxtNomComercio As System.Windows.Forms.TextBox
    Friend WithEvents CMBBtnGuardar As System.Windows.Forms.Button
    Friend WithEvents CMBBtnSalir As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBLblIntod As System.Windows.Forms.Label
End Class
