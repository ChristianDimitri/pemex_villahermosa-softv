<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCorreo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.CheckBox8 = New System.Windows.Forms.CheckBox()
        Me.CheckBox9 = New System.Windows.Forms.CheckBox()
        Me.PortTextBox = New System.Windows.Forms.TextBox()
        Me.ConGeneralCorreoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.HostTextBox = New System.Windows.Forms.TextBox()
        Me.PasswordTextBox = New System.Windows.Forms.TextBox()
        Me.CuentaTextBox = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBox7 = New System.Windows.Forms.CheckBox()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.ConGeneralCorreoTableAdapter = New sofTV.DataSetEricTableAdapters.ConGeneralCorreoTableAdapter()
        Me.MandarCorreoClientesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MandarCorreoClientesTableAdapter = New sofTV.DataSetEricTableAdapters.MandarCorreoClientesTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DataSetEric2 = New sofTV.DataSetEric2()
        Me.InsPreDetBitacoraCorreoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InsPreDetBitacoraCorreoTableAdapter = New sofTV.DataSetEric2TableAdapters.InsPreDetBitacoraCorreoTableAdapter()
        Me.GrabaBitacoraCorreoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GrabaBitacoraCorreoTableAdapter = New sofTV.DataSetEric2TableAdapters.GrabaBitacoraCorreoTableAdapter()
        Me.bnEliminar = New System.Windows.Forms.Button()
        Me.bnAgregar = New System.Windows.Forms.Button()
        Me.panelURL = New System.Windows.Forms.Panel()
        Me.rbLink = New System.Windows.Forms.RadioButton()
        Me.rbImagen = New System.Windows.Forms.RadioButton()
        Me.tbURL = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.bnAceptar = New System.Windows.Forms.Button()
        Me.bnCancelar = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.rbIzquierda = New System.Windows.Forms.RadioButton()
        Me.rbCentrado = New System.Windows.Forms.RadioButton()
        Me.rbDerecha = New System.Windows.Forms.RadioButton()
        Me.panelJus = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.RichTextBox()
        CType(Me.ConGeneralCorreoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.MandarCorreoClientesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InsPreDetBitacoraCorreoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrabaBitacoraCorreoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelURL.SuspendLayout()
        Me.panelJus.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(10, 167)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 15)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Mensaje :"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(637, 104)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(637, 20)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "&ENVIAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'CheckBox8
        '
        Me.CheckBox8.AutoSize = True
        Me.CheckBox8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CheckBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox8.Location = New System.Drawing.Point(140, 20)
        Me.CheckBox8.Name = "CheckBox8"
        Me.CheckBox8.Size = New System.Drawing.Size(101, 19)
        Me.CheckBox8.TabIndex = 0
        Me.CheckBox8.Text = "1er Periodo"
        Me.CheckBox8.UseVisualStyleBackColor = False
        '
        'CheckBox9
        '
        Me.CheckBox9.AutoSize = True
        Me.CheckBox9.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CheckBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox9.Location = New System.Drawing.Point(345, 20)
        Me.CheckBox9.Name = "CheckBox9"
        Me.CheckBox9.Size = New System.Drawing.Size(104, 19)
        Me.CheckBox9.TabIndex = 1
        Me.CheckBox9.Text = "2do Periodo"
        Me.CheckBox9.UseVisualStyleBackColor = False
        '
        'PortTextBox
        '
        Me.PortTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Port", True))
        Me.PortTextBox.Location = New System.Drawing.Point(714, 70)
        Me.PortTextBox.Name = "PortTextBox"
        Me.PortTextBox.ReadOnly = True
        Me.PortTextBox.Size = New System.Drawing.Size(10, 20)
        Me.PortTextBox.TabIndex = 29
        Me.PortTextBox.TabStop = False
        '
        'ConGeneralCorreoBindingSource
        '
        Me.ConGeneralCorreoBindingSource.DataMember = "ConGeneralCorreo"
        Me.ConGeneralCorreoBindingSource.DataSource = Me.DataSetEric
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'HostTextBox
        '
        Me.HostTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Host", True))
        Me.HostTextBox.Location = New System.Drawing.Point(698, 70)
        Me.HostTextBox.Name = "HostTextBox"
        Me.HostTextBox.ReadOnly = True
        Me.HostTextBox.Size = New System.Drawing.Size(10, 20)
        Me.HostTextBox.TabIndex = 27
        Me.HostTextBox.TabStop = False
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Password", True))
        Me.PasswordTextBox.Location = New System.Drawing.Point(654, 70)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.ReadOnly = True
        Me.PasswordTextBox.Size = New System.Drawing.Size(10, 20)
        Me.PasswordTextBox.TabIndex = 25
        Me.PasswordTextBox.TabStop = False
        '
        'CuentaTextBox
        '
        Me.CuentaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConGeneralCorreoBindingSource, "Cuenta", True))
        Me.CuentaTextBox.Location = New System.Drawing.Point(682, 70)
        Me.CuentaTextBox.Name = "CuentaTextBox"
        Me.CuentaTextBox.ReadOnly = True
        Me.CuentaTextBox.Size = New System.Drawing.Size(10, 20)
        Me.CuentaTextBox.TabIndex = 23
        Me.CuentaTextBox.TabStop = False
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(13, 38)
        Me.TextBox2.MaxLength = 250
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(678, 20)
        Me.TextBox2.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(10, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 15)
        Me.Label3.TabIndex = 33
        Me.Label3.Text = "Asunto :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CheckBox7)
        Me.GroupBox1.Controls.Add(Me.CheckBox6)
        Me.GroupBox1.Controls.Add(Me.CheckBox5)
        Me.GroupBox1.Controls.Add(Me.CheckBox4)
        Me.GroupBox1.Controls.Add(Me.CheckBox3)
        Me.GroupBox1.Controls.Add(Me.CheckBox2)
        Me.GroupBox1.Controls.Add(Me.CheckBox1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(556, 80)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Status de Cliente"
        '
        'CheckBox7
        '
        Me.CheckBox7.AutoSize = True
        Me.CheckBox7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CheckBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox7.Location = New System.Drawing.Point(430, 50)
        Me.CheckBox7.Name = "CheckBox7"
        Me.CheckBox7.Size = New System.Drawing.Size(102, 19)
        Me.CheckBox7.TabIndex = 6
        Me.CheckBox7.Text = "Temporales"
        Me.CheckBox7.UseVisualStyleBackColor = False
        '
        'CheckBox6
        '
        Me.CheckBox6.AutoSize = True
        Me.CheckBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox6.Location = New System.Drawing.Point(286, 25)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(116, 19)
        Me.CheckBox6.TabIndex = 5
        Me.CheckBox6.Text = "Fuera de área"
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CheckBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox5.Location = New System.Drawing.Point(140, 25)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(109, 19)
        Me.CheckBox5.TabIndex = 2
        Me.CheckBox5.Text = "Suspendidos"
        Me.CheckBox5.UseVisualStyleBackColor = False
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CheckBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox4.Location = New System.Drawing.Point(140, 50)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(117, 19)
        Me.CheckBox4.TabIndex = 3
        Me.CheckBox4.Text = "Desconectado"
        Me.CheckBox4.UseVisualStyleBackColor = False
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CheckBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.Location = New System.Drawing.Point(13, 25)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(85, 19)
        Me.CheckBox3.TabIndex = 1
        Me.CheckBox3.Text = "Instalado"
        Me.CheckBox3.UseVisualStyleBackColor = False
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CheckBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(286, 50)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(101, 19)
        Me.CheckBox2.TabIndex = 4
        Me.CheckBox2.Text = "Cancelados"
        Me.CheckBox2.UseVisualStyleBackColor = False
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(13, 50)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(96, 19)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "Contratado"
        Me.CheckBox1.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CheckBox8)
        Me.GroupBox2.Controls.Add(Me.CheckBox9)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 98)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(556, 50)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Periodos"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.panelURL)
        Me.GroupBox3.Controls.Add(Me.bnAgregar)
        Me.GroupBox3.Controls.Add(Me.bnEliminar)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.Button4)
        Me.GroupBox3.Controls.Add(Me.ListBox1)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.TextBox2)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.TextBox1)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(12, 154)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(802, 449)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Datos del Correo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(10, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(127, 15)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "Archivos Adjuntos :"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(697, 86)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(85, 23)
        Me.Button4.TabIndex = 35
        Me.Button4.Text = "&Explorar..."
        Me.Button4.UseVisualStyleBackColor = True
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 15
        Me.ListBox1.Location = New System.Drawing.Point(13, 86)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(678, 64)
        Me.ListBox1.TabIndex = 34
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblStatus)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(12, 609)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(802, 52)
        Me.GroupBox4.TabIndex = 30
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Status del Envio"
        '
        'lblStatus
        '
        Me.lblStatus.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblStatus.Location = New System.Drawing.Point(10, 17)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(772, 23)
        Me.lblStatus.TabIndex = 0
        '
        'ConGeneralCorreoTableAdapter
        '
        Me.ConGeneralCorreoTableAdapter.ClearBeforeFill = True
        '
        'MandarCorreoClientesBindingSource
        '
        Me.MandarCorreoClientesBindingSource.DataMember = "MandarCorreoClientes"
        Me.MandarCorreoClientesBindingSource.DataSource = Me.DataSetEric
        '
        'MandarCorreoClientesTableAdapter
        '
        Me.MandarCorreoClientesTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Orange
        Me.Button1.Enabled = False
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(637, 62)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 31
        Me.Button1.Text = "&REPORTE"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'DataSetEric2
        '
        Me.DataSetEric2.DataSetName = "DataSetEric2"
        Me.DataSetEric2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'InsPreDetBitacoraCorreoBindingSource
        '
        Me.InsPreDetBitacoraCorreoBindingSource.DataMember = "InsPreDetBitacoraCorreo"
        Me.InsPreDetBitacoraCorreoBindingSource.DataSource = Me.DataSetEric2
        '
        'InsPreDetBitacoraCorreoTableAdapter
        '
        Me.InsPreDetBitacoraCorreoTableAdapter.ClearBeforeFill = True
        '
        'GrabaBitacoraCorreoBindingSource
        '
        Me.GrabaBitacoraCorreoBindingSource.DataMember = "GrabaBitacoraCorreo"
        Me.GrabaBitacoraCorreoBindingSource.DataSource = Me.DataSetEric2
        '
        'GrabaBitacoraCorreoTableAdapter
        '
        Me.GrabaBitacoraCorreoTableAdapter.ClearBeforeFill = True
        '
        'bnEliminar
        '
        Me.bnEliminar.Location = New System.Drawing.Point(697, 115)
        Me.bnEliminar.Name = "bnEliminar"
        Me.bnEliminar.Size = New System.Drawing.Size(85, 23)
        Me.bnEliminar.TabIndex = 37
        Me.bnEliminar.Text = "&Eliminar"
        Me.bnEliminar.UseVisualStyleBackColor = True
        '
        'bnAgregar
        '
        Me.bnAgregar.Location = New System.Drawing.Point(697, 185)
        Me.bnAgregar.Name = "bnAgregar"
        Me.bnAgregar.Size = New System.Drawing.Size(85, 39)
        Me.bnAgregar.TabIndex = 38
        Me.bnAgregar.Text = "&Agregar URL"
        Me.bnAgregar.UseVisualStyleBackColor = True
        '
        'panelURL
        '
        Me.panelURL.BackColor = System.Drawing.Color.DarkGray
        Me.panelURL.Controls.Add(Me.Label6)
        Me.panelURL.Controls.Add(Me.panelJus)
        Me.panelURL.Controls.Add(Me.Label5)
        Me.panelURL.Controls.Add(Me.bnCancelar)
        Me.panelURL.Controls.Add(Me.bnAceptar)
        Me.panelURL.Controls.Add(Me.Label4)
        Me.panelURL.Controls.Add(Me.tbURL)
        Me.panelURL.Controls.Add(Me.rbImagen)
        Me.panelURL.Controls.Add(Me.rbLink)
        Me.panelURL.Location = New System.Drawing.Point(70, 217)
        Me.panelURL.Name = "panelURL"
        Me.panelURL.Size = New System.Drawing.Size(561, 174)
        Me.panelURL.TabIndex = 39
        Me.panelURL.Visible = False
        '
        'rbLink
        '
        Me.rbLink.AutoSize = True
        Me.rbLink.Checked = True
        Me.rbLink.Location = New System.Drawing.Point(72, 20)
        Me.rbLink.Name = "rbLink"
        Me.rbLink.Size = New System.Drawing.Size(52, 19)
        Me.rbLink.TabIndex = 0
        Me.rbLink.TabStop = True
        Me.rbLink.Text = "Link"
        Me.rbLink.UseVisualStyleBackColor = True
        '
        'rbImagen
        '
        Me.rbImagen.AutoSize = True
        Me.rbImagen.Location = New System.Drawing.Point(72, 45)
        Me.rbImagen.Name = "rbImagen"
        Me.rbImagen.Size = New System.Drawing.Size(73, 19)
        Me.rbImagen.TabIndex = 1
        Me.rbImagen.Text = "Imagen"
        Me.rbImagen.UseVisualStyleBackColor = True
        '
        'tbURL
        '
        Me.tbURL.Location = New System.Drawing.Point(70, 88)
        Me.tbURL.Name = "tbURL"
        Me.tbURL.Size = New System.Drawing.Size(474, 21)
        Me.tbURL.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 94)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 15)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "URL :"
        '
        'bnAceptar
        '
        Me.bnAceptar.Location = New System.Drawing.Point(368, 138)
        Me.bnAceptar.Name = "bnAceptar"
        Me.bnAceptar.Size = New System.Drawing.Size(85, 23)
        Me.bnAceptar.TabIndex = 39
        Me.bnAceptar.Text = "&Aceptar"
        Me.bnAceptar.UseVisualStyleBackColor = True
        '
        'bnCancelar
        '
        Me.bnCancelar.Location = New System.Drawing.Point(459, 138)
        Me.bnCancelar.Name = "bnCancelar"
        Me.bnCancelar.Size = New System.Drawing.Size(85, 23)
        Me.bnCancelar.TabIndex = 40
        Me.bnCancelar.Text = "&Cancelar"
        Me.bnCancelar.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(20, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 15)
        Me.Label5.TabIndex = 41
        Me.Label5.Text = "Tipo : "
        '
        'rbIzquierda
        '
        Me.rbIzquierda.AutoSize = True
        Me.rbIzquierda.Location = New System.Drawing.Point(3, 3)
        Me.rbIzquierda.Name = "rbIzquierda"
        Me.rbIzquierda.Size = New System.Drawing.Size(85, 19)
        Me.rbIzquierda.TabIndex = 42
        Me.rbIzquierda.Text = "Izquierda"
        Me.rbIzquierda.UseVisualStyleBackColor = True
        '
        'rbCentrado
        '
        Me.rbCentrado.AutoSize = True
        Me.rbCentrado.Checked = True
        Me.rbCentrado.Location = New System.Drawing.Point(94, 3)
        Me.rbCentrado.Name = "rbCentrado"
        Me.rbCentrado.Size = New System.Drawing.Size(83, 19)
        Me.rbCentrado.TabIndex = 43
        Me.rbCentrado.TabStop = True
        Me.rbCentrado.Text = "Centrado"
        Me.rbCentrado.UseVisualStyleBackColor = True
        '
        'rbDerecha
        '
        Me.rbDerecha.AutoSize = True
        Me.rbDerecha.Location = New System.Drawing.Point(183, 3)
        Me.rbDerecha.Name = "rbDerecha"
        Me.rbDerecha.Size = New System.Drawing.Size(79, 19)
        Me.rbDerecha.TabIndex = 44
        Me.rbDerecha.Text = "Derecha"
        Me.rbDerecha.UseVisualStyleBackColor = True
        '
        'panelJus
        '
        Me.panelJus.Controls.Add(Me.rbIzquierda)
        Me.panelJus.Controls.Add(Me.rbDerecha)
        Me.panelJus.Controls.Add(Me.rbCentrado)
        Me.panelJus.Enabled = False
        Me.panelJus.Location = New System.Drawing.Point(260, 37)
        Me.panelJus.Name = "panelJus"
        Me.panelJus.Size = New System.Drawing.Size(267, 27)
        Me.panelJus.TabIndex = 45
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(183, 47)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(71, 15)
        Me.Label6.TabIndex = 46
        Me.Label6.Text = "Alineada :"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(13, 185)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(678, 246)
        Me.TextBox1.TabIndex = 40
        Me.TextBox1.Text = ""
        '
        'FrmCorreo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(835, 694)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.CuentaTextBox)
        Me.Controls.Add(Me.PasswordTextBox)
        Me.Controls.Add(Me.HostTextBox)
        Me.Controls.Add(Me.PortTextBox)
        Me.Name = "FrmCorreo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Enviar Correo(s)"
        CType(Me.ConGeneralCorreoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.MandarCorreoClientesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InsPreDetBitacoraCorreoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrabaBitacoraCorreoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelURL.ResumeLayout(False)
        Me.panelURL.PerformLayout()
        Me.panelJus.ResumeLayout(False)
        Me.panelJus.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConGeneralCorreoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConGeneralCorreoTableAdapter As sofTV.DataSetEricTableAdapters.ConGeneralCorreoTableAdapter
    Friend WithEvents CheckBox8 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox9 As System.Windows.Forms.CheckBox
    Friend WithEvents PortTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HostTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CuentaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MandarCorreoClientesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MandarCorreoClientesTableAdapter As sofTV.DataSetEricTableAdapters.MandarCorreoClientesTableAdapter
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox7 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents DataSetEric2 As sofTV.DataSetEric2
    Friend WithEvents InsPreDetBitacoraCorreoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InsPreDetBitacoraCorreoTableAdapter As sofTV.DataSetEric2TableAdapters.InsPreDetBitacoraCorreoTableAdapter
    Friend WithEvents GrabaBitacoraCorreoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GrabaBitacoraCorreoTableAdapter As sofTV.DataSetEric2TableAdapters.GrabaBitacoraCorreoTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents panelURL As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents bnCancelar As System.Windows.Forms.Button
    Friend WithEvents bnAceptar As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbURL As System.Windows.Forms.TextBox
    Friend WithEvents rbImagen As System.Windows.Forms.RadioButton
    Friend WithEvents rbLink As System.Windows.Forms.RadioButton
    Friend WithEvents bnAgregar As System.Windows.Forms.Button
    Friend WithEvents bnEliminar As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents panelJus As System.Windows.Forms.Panel
    Friend WithEvents rbIzquierda As System.Windows.Forms.RadioButton
    Friend WithEvents rbDerecha As System.Windows.Forms.RadioButton
    Friend WithEvents rbCentrado As System.Windows.Forms.RadioButton
    Friend WithEvents TextBox1 As System.Windows.Forms.RichTextBox
End Class
