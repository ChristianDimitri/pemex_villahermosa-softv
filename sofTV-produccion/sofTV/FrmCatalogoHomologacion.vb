﻿Imports System.Data.SqlClient

Public Class FrmCatalogoHomologacion

    Private Sub FrmCatalogoHomologacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        BindingNavigatorDeleteItem.Enabled = True
        Dim CON As New SqlConnection(MiConexion)
        'LLENADO DEL DROPDOWNLIST DE SERVICIOS DIGITALES
        Dim COMANDO As New SqlCommand("Llena_Servicio_Digital", CON)
        Dim dt As New DataTable
        COMANDO.CommandType = CommandType.StoredProcedure
        CON.Open()
        Dim da As New SqlDataAdapter(COMANDO)
        da.SelectCommand = COMANDO
        da.SelectCommand.Connection = CON
        Dim data As New DataSet
        da.Fill(data)
        If (data.Tables(0).Rows.Count > 0) Then
            ServicioDigital.DataSource = data.Tables(0)
            'ServicioDigital.ValueMember = "Clv_Servicio"
            ServicioDigital.ValueMember = data.Tables(0).Columns(0).Caption.ToString()
            ServicioDigital.DisplayMember = data.Tables(0).Columns(1).Caption.ToString()
            ServicioDigital.SelectedIndex = 0
        End If
        COMANDO.ExecuteNonQuery()
        COMANDO.Dispose()

        'LLENADO DEL DROPDOWNLIST DE SERVICIOS ANALOGICOS
        Dim COMANDO2 As New SqlCommand("Llena_Servicio_Analogico", CON)
        Dim dt2 As New DataTable
        COMANDO2.CommandType = CommandType.StoredProcedure
        Dim da2 As New SqlDataAdapter(COMANDO2)
        da.SelectCommand = COMANDO2
        da.SelectCommand.Connection = CON
        Dim data2 As New DataSet
        da.Fill(data2)
        If (data2.Tables(0).Rows.Count > 0) Then
            ServicioAnalogico.DataSource = data2.Tables(0)
            ServicioAnalogico.ValueMember = data2.Tables(0).Columns(0).Caption.ToString()
            ServicioAnalogico.DisplayMember = data2.Tables(0).Columns(1).Caption.ToString()
            ServicioAnalogico.SelectedIndex = 0
        End If
        COMANDO2.ExecuteNonQuery()
        COMANDO2.Dispose()

        'LLENAR DATAGRIT CON LOS DATOS QUE SE ENCUENTRAS YA EN LA TABLA

        Dim strSQL As String
        strSQL = "SELECT S.Descripcion  as 'Servicios Analogicos' ,S2.Descripcion as 'Servicios Digitales', clv_servicio_analogico, clv_servicio_digital FROM  Catalogo_Homologacion inner join Servicios S on S.Clv_Servicio = Catalogo_Homologacion.Clv_Servicio_Analogico  inner join Servicios S2 on S2.Clv_Servicio = Catalogo_Homologacion.Clv_Servicio_Digital"
        Dim COMANDO3 As New SqlCommand
        COMANDO3.CommandText = strSQL
        Dim da3 As New SqlDataAdapter(COMANDO3)
        da.SelectCommand = COMANDO3
        da.SelectCommand.Connection = CON
        Dim dt3 As New DataTable
        Dim carga As DataTable = dt3
        da3.Fill(dt3)
        DataGridView1.DataSource = carga
        DataGridView1.Columns(2).Visible = False
        DataGridView1.Columns(3).Visible = False
        CON.Close()
        CON.Dispose()
    End Sub

    Private Sub CONGenerales_InternetBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONGenerales_InternetBindingNavigatorSaveItem.Click
        If ServicioAnalogico.SelectedValue = -1 Then
            MsgBox("Seleccione un Servicio Analogico", MsgBoxStyle.Critical, "Error")
        Else
            If ServicioDigital.SelectedValue = -1 Then
                MsgBox("Seleccione un Servicio Digital", MsgBoxStyle.Critical, "Error")
            Else
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                'GUARDAR SELECCION DE SERVICIOS
                Dim COMANDO As New SqlCommand("Agrega_Homologacion", CON)
                COMANDO.CommandType = CommandType.StoredProcedure

                Dim PARAMETRO1 As New SqlParameter("@Ser_Ana", SqlDbType.Int)
                PARAMETRO1.Direction = ParameterDirection.Input
                PARAMETRO1.Value = CInt(ServicioAnalogico.SelectedValue)
                COMANDO.Parameters.Add(PARAMETRO1)

                PARAMETRO1 = New SqlParameter("@Ser_Dig", SqlDbType.Int)
                PARAMETRO1.Direction = ParameterDirection.Input
                PARAMETRO1.Value = CInt(ServicioDigital.SelectedValue)
                COMANDO.Parameters.Add(PARAMETRO1)

                Try
                    COMANDO.ExecuteNonQuery()
                    MsgBox("Se Guardo Correctamente", MsgBoxStyle.Information, "Guardado")
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                Finally
                    CON.Close()
                    CON.Dispose()
                End Try
                Me.Close()
            End If
        End If
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim ser_ana As Integer = DataGridView1.Item(2, DataGridView1.CurrentRow.Index).Value
        Dim ser_dig As Integer = DataGridView1.Item(3, DataGridView1.CurrentRow.Index).Value
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'ELIMINA SELECCION DE SERVICIOS
        Dim COMANDO As New SqlCommand("Elimina_Homologacion", CON)
        COMANDO.CommandType = CommandType.StoredProcedure

        Dim PARAMETRO1 As New SqlParameter("@Ser_Ana", SqlDbType.Int)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = ser_ana
        COMANDO.Parameters.Add(PARAMETRO1)

        PARAMETRO1 = New SqlParameter("@Ser_Dig", SqlDbType.Int)
        PARAMETRO1.Direction = ParameterDirection.Input
        PARAMETRO1.Value = ser_dig
        COMANDO.Parameters.Add(PARAMETRO1)

        Try
            COMANDO.ExecuteNonQuery()
            MsgBox("Se Elimino Correctamente", MsgBoxStyle.Information, "Eliminado")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
        Me.Close()
    End Sub
End Class

