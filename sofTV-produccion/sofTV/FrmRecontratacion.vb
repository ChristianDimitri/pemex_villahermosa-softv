﻿Imports System.Text
Imports System.Data.SqlClient
Public Class FrmRecontratacion

   
    Dim eReStatus As Char
    Dim eReCortesia As Boolean = False
    Dim eCombo As String = String.Empty
    Dim eBndTV As Boolean = False
    Dim eBndNET As Boolean = False
    Dim eBndDIG As Boolean = False
    Dim eBndTEL As Boolean = False
    Dim eSerTV As String = String.Empty
    Dim eSerNET As String = String.Empty
    Dim eSerDIG As String = String.Empty
    Dim eSerTEL As String = String.Empty
    Dim Op As Boolean = False


    Function ConReClientesTVTmp(ByVal Clv_Session As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("exec ConReClientesTVTmp " & CStr(Clv_Session))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Try
            dataAdapter.Fill(dataTable)
            Return dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Function

    Function ConReClientesNetTmp(ByVal Clv_TipSer As Integer, ByVal Clv_Session As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("exec ConReClientesNetTmp " & CStr(Clv_TipSer) & ", " & CStr(Clv_Session))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Try
            dataAdapter.Fill(dataTable)
            Return dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Function

    Function ConReContNetTmp(ByVal ContratoNet As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("exec ConReContNetTmp " & CStr(ContratoNet))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Try
            dataAdapter.Fill(dataTable)
            Return dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Function

    Function ConReClientesDigTmp(ByVal Clv_Session As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("exec ConReClientesDigTmp " & CStr(Clv_Session))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Try
            dataAdapter.Fill(dataTable)
            Return dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Function

    Function ConReContDigTmp(ByVal ContratoNet As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("exec ConReContDigTmp " & CStr(ContratoNet))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Try
            dataAdapter.Fill(dataTable)
            Return dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Function

    Function ConReContTelTmp(ByVal ContratoNet As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("exec ConReContTelTmp " & CStr(ContratoNet))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Try
            dataAdapter.Fill(dataTable)
            Return dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Function

    Function ConReContTelAdicTmp(ByVal ContratoNet As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("exec ConReContTelAdicTmp " & CStr(ContratoNet))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Try
            dataAdapter.Fill(dataTable)
            Return dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Function






    Private Sub NueReClientesTVTmp(ByVal Contrato As Long, ByVal Clv_Session As Long, ByVal TvSinPago As Integer, ByVal TvConPago As Integer, ByVal Clv_Servicio As Integer, ByVal Cortesia As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueReClientesTVTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Contrato
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Session
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@TvSinPago", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = TvSinPago
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@TvConPago", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = TvConPago
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par5.Direction = ParameterDirection.Input
        par5.Value = Clv_Servicio
        comando.Parameters.Add(par5)

        Dim par6 As New SqlParameter("@Cortesia", SqlDbType.Bit)
        par6.Direction = ParameterDirection.Input
        par6.Value = Cortesia
        comando.Parameters.Add(par6)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueReClientesNetTmp(ByVal Op As Integer, ByVal Contrato As Long, ByVal Clv_Session As Long, ByVal Clv_TipSer As Integer, ByVal TipoCablemodem As Char, ByVal TipoAdquisicion As Char, ByVal TipoServicio As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueReClientesNetTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Op", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Op
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Contrato
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par3.Direction = ParameterDirection.Input
        par3.Value = Clv_Session
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Clv_TipSer
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@TipoCablemodem", SqlDbType.VarChar, 1)
        par5.Direction = ParameterDirection.Input
        par5.Value = TipoCablemodem
        comando.Parameters.Add(par5)

        Dim par6 As New SqlParameter("@TipoAdquisicion", SqlDbType.VarChar, 1)
        par6.Direction = ParameterDirection.Input
        par6.Value = TipoAdquisicion
        comando.Parameters.Add(par6)

        Dim par7 As New SqlParameter("@TipoServicio", SqlDbType.Int)
        par7.Direction = ParameterDirection.Input
        par7.Value = TipoServicio
        comando.Parameters.Add(par7)

        Dim par8 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        par8.Direction = ParameterDirection.Output
        comando.Parameters.Add(par8)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eContratoNet = CLng(par8.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueReContNetTmp(ByVal Contrato As Long, ByVal ContratoNet As Long, ByVal Clv_Servicio As Integer, ByVal Cortesia As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueReContNetTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Contrato
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = ContratoNet
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = Clv_Servicio
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Cortesia", SqlDbType.Bit)
        par4.Direction = ParameterDirection.Input
        par4.Value = Cortesia
        comando.Parameters.Add(par4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueReClientesDigTmp(ByVal Op As Integer, ByVal Contrato As Long, ByVal Clv_Session As Long, ByVal NumCajas As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueReClientesDigTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Op", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Op
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Contrato
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par3.Direction = ParameterDirection.Input
        par3.Value = Clv_Session
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@NumCajas", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = NumCajas
        comando.Parameters.Add(par4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueReContDigTmp(ByVal Contrato As Long, ByVal Clv_Session As Long, ByVal ContratoNet As Long, ByVal Clv_Servico As Integer, ByVal Cortesia As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueReContDigTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Contrato
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Session
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        par3.Direction = ParameterDirection.Input
        par3.Value = ContratoNet
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Clv_Servico
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@Cortesia", SqlDbType.Bit)
        par5.Direction = ParameterDirection.Input
        par5.Value = Cortesia
        comando.Parameters.Add(par5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Public Sub NueReContTelTmp(ByVal Contrato As Long, ByVal Clv_Session As Long, ByVal ContratoNet As Long, ByVal OpTel As Char, ByVal Clv_Telefono As Long, ByVal Clv_Servicio As Integer, ByVal Cortesia As Boolean)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueReContTelTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Contrato
        comando.Parameters.Add(par)

        Dim par05 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par05.Direction = ParameterDirection.Input
        par05.Value = Clv_Session
        comando.Parameters.Add(par05)

        Dim par2 As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par0 As New SqlParameter("@OpTel", SqlDbType.VarChar, 1)
        par0.Direction = ParameterDirection.Input
        par0.Value = OpTel
        comando.Parameters.Add(par0)

        Dim par3 As New SqlParameter("@Clv_Telefono", SqlDbType.BigInt)
        par3.Direction = ParameterDirection.Input
        par3.Value = Clv_Telefono
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Clv_Servicio
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@Cortesia", SqlDbType.Bit)
        par5.Direction = ParameterDirection.Input
        par5.Value = Cortesia
        comando.Parameters.Add(par5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eContratoNet = par2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueReContTelAdicTmp(ByVal Clv_UnicaNet As Long, ByVal Clv_Servicio As Integer, ByVal Tipo_Servicio As Char)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueReContTelAdicTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_UnicaNet
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Servicio
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Tipo_Servicio", SqlDbType.VarChar, 1)
        par3.Direction = ParameterDirection.Input
        par3.Value = Tipo_Servicio
        comando.Parameters.Add(par3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub







    Private Sub BorReClientesTVTmp(ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorReClientesTVTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_Session
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            eRes = 0
            eMsj = ""
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(par2.Value)
            eMsj = CStr(par3.Value)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BorReClientesNetTmp(ByVal ContratoNet As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorReClientesNetTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = ContratoNet
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            eRes = 0
            eMsj = ""
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(par2.Value)
            eMsj = CStr(par3.Value)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorReContNetTmp(ByVal Clv_UnicaNet As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorReContNetTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_UnicaNet
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            eRes = 0
            eMsj = ""
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(par2.Value)
            eMsj = CStr(par3.Value)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorReClientesDigTmp(ByVal ContratoNet As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorReClientesDigTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@ContratoNet", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = ContratoNet
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            eRes = 0
            eMsj = ""
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(par2.Value)
            eMsj = CStr(par3.Value)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BorReContDigTmp(ByVal Clv_UnicaNet As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorReContDigTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_UnicaNet
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)


        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            eRes = 0
            eMsj = ""
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(par2.Value)
            eMsj = CStr(par3.Value)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorReContTelTmp(ByVal Clv_UnicaNet As Long, ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorReContTelTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_UnicaNet
        comando.Parameters.Add(par)

        Dim par0 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par0.Direction = ParameterDirection.Input
        par0.Value = Clv_Session
        comando.Parameters.Add(par0)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            eRes = 0
            eMsj = ""
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(par2.Value)
            eMsj = CStr(par3.Value)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorReContTelAdicTmp(ByVal Clv_UnicaNet As Long, ByVal Clv_Servicio As Integer, ByVal Tipo_Servicio As Char)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorReContTelAdicTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_UnicaNet
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Servicio
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Tipo_Servicio", SqlDbType.VarChar, 1)
        par3.Direction = ParameterDirection.Input
        par3.Value = Tipo_Servicio
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Res", SqlDbType.Int)
        par4.Direction = ParameterDirection.Output
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@Res", SqlDbType.VarChar, 150)
        par5.Direction = ParameterDirection.Output
        comando.Parameters.Add(par5)

        Try
            eRes = 0
            eMsj = ""
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(par4.Value)
            eMsj = CStr(par5.Value)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub



    Private Sub Limpiar()

        Me.LabelNombre.Text = ""
        Me.LabelCalle.Text = ""
        Me.LabelNumero.Text = ""
        Me.LabelColonia.Text = ""
        Me.LabelCiudad.Text = ""
        Me.LabelTelefono.Text = ""
        Me.LabelCelular.Text = ""
        Me.CheckBoxSoloInternet.Checked = False

        tvTV.Nodes.Clear()
        tvNET.Nodes.Clear()
        tvDIG.Nodes.Clear()
        tvTel.Nodes.Clear()
        lblTV.Text = ""
        lblNET.Text = ""
        lblDIG.Text = ""
        lblTEL.Text = ""
        eContratoNet = 0
        eClv_Servicio = 0
        tcServicios.Enabled = False
        BorRecontratacionSession(eClv_Session)

    End Sub

    Private Sub LimpiarCombo()
        txtSinPagoCombo.Text = 0
        txtConPagoCombo.Text = 0
        txtNumCajas.Text = 0
        radioAutoCombo.Checked = True
        comboTipoCaCombo.DataSource = MuestraTipoCablemodem()
    End Sub



    Private Sub MuestraInfoCliente(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("MuestraInfoCliente", conexion)
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                Me.LabelNombre.Text = reader(0).ToString()
                Me.LabelCalle.Text = reader(1).ToString()
                Me.LabelNumero.Text = reader(2).ToString()
                Me.LabelColonia.Text = reader(3).ToString()
                Me.LabelCiudad.Text = reader(4).ToString()
                Me.LabelTelefono.Text = reader(5).ToString()
                Me.LabelCelular.Text = reader(6).ToString()
                Me.CheckBoxSoloInternet.Checked = reader(7).ToString
                eClv_TipoCliente = reader(8).ToString
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Function MuestraTipoCablemodem() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipoCablemodem")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Private Function MuestraTipoAsignacion() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipoAsignacion")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Private Function MuestraTipSerInternet() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipSerInternet")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable
    End Function

    Private Function MuestraServicioRecon(ByVal ClvSession As Long, ByVal Clv_TipSer As Integer, ByVal ContratoNet As Long) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraServicioRecon " & CStr(ClvSession) & ", " & CStr(Clv_TipSer) & ", " & CStr(ContratoNet))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Private Function MuestraSerTel(ByVal Clv_Unicanet As Long, ByVal Tipo_Servicio As Char) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraSerTel " & CStr(Clv_Unicanet) & ", '" & Tipo_Servicio & "'")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Private Function MuestaCombos(ByVal Op As Integer, ByVal Clv_TipoCliente As Integer) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestaCombos " & CStr(Op) & ", " & CStr(Clv_TipoCliente))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable

    End Function

    Private Function MuestraCatNumTel() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraCatNumTel")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable

        dataAdapter.Fill(dataTable)

        Return dataTable
    End Function


    Private Sub LlenatvTV()
        Try
            Dim dtA As New DataTable
            Dim a As Integer = 0

            dtA = ConReClientesTVTmp(eClv_Session)

            tvTV.Nodes.Clear()

            For Each aparato As DataRow In dtA.Rows
                If a = 0 Then tvTV.Nodes.Add(aparato("Descripcion").ToString()).ForeColor = Color.Blue
                If a > 0 Then tvTV.Nodes.Add(aparato("Descripcion").ToString())
                a += 1
            Next

            'tvTV.Nodes

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub LlenatvNet()
        Try
            Dim dtA As New DataTable
            Dim dtS As New DataTable
            Dim a As Integer = 0
            Dim s As Integer = 0

            dtA = ConReClientesNetTmp(2, eClv_Session)

            tvNET.Nodes.Clear()

            For Each aparato As DataRow In dtA.Rows

                tvNET.Nodes.Add(aparato("Aparato").ToString())
                tvNET.Nodes(a).Tag = aparato("ContratoNet").ToString()
                dtS = ConReContNetTmp(aparato("ContratoNet").ToString())

                For Each servicio As DataRow In dtS.Rows
                    tvNET.Nodes(a).Nodes.Add(servicio("Servicio").ToString()).ForeColor = Color.Blue
                    tvNET.Nodes(a).Nodes(s).Tag = servicio("Clv_Unicanet").ToString()
                    s += 1

                Next

                tvNET.Nodes(a).ExpandAll()
                a += 1
                s = 0
            Next



        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub LlenatvDig()
        Try
            Dim dtA As New DataTable
            Dim dtS As New DataTable
            Dim a As Integer = 0
            Dim s As Integer = 0

            dtA = ConReClientesDigTmp(eClv_Session)

            tvDIG.Nodes.Clear()

            For Each aparato As DataRow In dtA.Rows

                tvDIG.Nodes.Add(aparato("Aparato").ToString())
                tvDIG.Nodes(a).Tag = aparato("ContratoNet").ToString()
                dtS = ConReContDigTmp(aparato("ContratoNet").ToString())

                For Each servicio As DataRow In dtS.Rows
                    tvDIG.Nodes(a).Nodes.Add(servicio("Servicio").ToString()).ForeColor = Color.Blue
                    tvDIG.Nodes(a).Nodes(s).Tag = servicio("Clv_Unicanet").ToString()
                    s += 1

                Next

                tvDIG.Nodes(a).ExpandAll()
                a += 1
                s = 0
            Next


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub LlenatvTEL()
        Try
            Dim dtA As New DataTable
            Dim dtS As New DataTable
            Dim dtX As New DataTable
            Dim a As Integer = 0
            Dim s As Integer = 0
            Dim x As Integer = 0

            dtA = ConReClientesNetTmp(5, eClv_Session)

            tvTel.Nodes.Clear()

            For Each aparato As DataRow In dtA.Rows

                tvTel.Nodes.Add(aparato("Aparato").ToString())
                tvTel.Nodes(a).Tag = aparato("ContratoNet").ToString()
                dtS = ConReContTelTmp(aparato("ContratoNet").ToString())

                For Each servicio As DataRow In dtS.Rows
                    tvTel.Nodes(a).Nodes.Add(servicio("Servicio").ToString()).ForeColor = Color.Blue
                    tvTel.Nodes(a).Nodes(s).Tag = servicio("Clv_Unicanet").ToString()

                    

                    s += 1

                Next

                dtX = ConReContTelAdicTmp(aparato("ContratoNet").ToString())
                For Each digitalAdicional As DataRow In dtX.Rows
                    tvTel.Nodes(a).Nodes(s - 1).Nodes.Add(digitalAdicional("Nombre").ToString())
                    tvTel.Nodes(a).Nodes(s - 1).Nodes(x).Tag = digitalAdicional("IdTelAdic").ToString()
                    x += 1
                Next

                tvTel.Nodes(a).ExpandAll()
                a += 1
                s = 0
            Next


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub


    Private Sub tvNET_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvNET.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            eContratoNet = CLng(e.Node.Tag)
        Else
            eContratoNet = CLng(e.Node.Parent.Tag)
            eClv_UnicaNet = CInt(e.Node.Tag)
        End If
    End Sub

    Private Sub tvDIG_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvDIG.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If
        If e.Node.Level = 0 Then
            eContratoNet = CLng(e.Node.Tag)
        Else
            eContratoNet = CLng(e.Node.Parent.Tag)
            eClv_UnicaNet = CInt(e.Node.Tag)
        End If
    End Sub

    Private Sub tvTel_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvTel.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If

        If e.Node.Level = 0 Then
            eContratoNet = CLng(e.Node.Tag)
        ElseIf e.Node.Level = 1 Then
            eContratoNet = CLng(e.Node.Parent.Tag)
            eClv_UnicaNet = CInt(e.Node.Tag)
        ElseIf e.Node.Level = 2 Then
            DameInfoReContTelAdicTmp(e.Node.Tag)
        End If

    End Sub

    Private Sub DameInfoReContTelAdicTmp(ByVal IdTelAdic As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameInfoReContTelAdicTmp", conexion)
        comando.CommandTimeout = 0
        comando.CommandType = CommandType.StoredProcedure
        Dim reader As SqlDataReader

        Dim par As New SqlParameter("@IdTelAdic", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = IdTelAdic
        comando.Parameters.Add(par)

        Try
            conexion.Open()
            reader = comando.ExecuteReader
            While (reader.Read())
                eClv_UnicaNet = reader(0).ToString
                eClv_Servicio = reader(1).ToString
                eTipo_Servicio = reader(2).ToString
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()

        End Try
    End Sub

    Private Sub ButtonBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBuscar.Click
        GLOCONTRATOSEL = 0
        GloClv_TipSer = 1000
        FrmSelCliente.Show()
    End Sub

    Private Sub TextBoxContrato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxContrato.KeyDown
        If (e.KeyCode <> Keys.Enter) Then
            Exit Sub
        End If
        If IsNumeric(Me.TextBoxContrato.Text) = False Then
            Exit Sub
        End If
        If CInt(Me.TextBoxContrato.Text) <= 0 Then
            Exit Sub
        End If

        MuestraInfoCliente(TextBoxContrato.Text)
        eContrato = TextBoxContrato.Text
        tcServicios.Enabled = True

        If CheckBoxSoloInternet.Checked = True Then
            If tcServicios.TabPages.Count > 1 Then
                tcServicios.TabPages.Remove(tpTV)
                tcServicios.TabPages.Remove(tpDIG)
                tcServicios.TabPages.Remove(tpTEL)
                eClv_TipSer = 2
            End If
        Else
            If tcServicios.TabPages.Count = 1 Then
                tcServicios.TabPages.Remove(tpNET)
                tcServicios.TabPages.Add(tpTV)
                tcServicios.TabPages.Add(tpNET)
                tcServicios.TabPages.Add(tpDIG)
                tcServicios.TabPages.Add(tpTEL)
                eClv_TipSer = 1
            End If
        End If

        DameClv_Session()

        'Clientes de sólo Internet
        If CheckBoxSoloInternet.Checked = True Then
            ValidaServ(eContrato, eClv_TipSer)
            If eRes = 1 Then
                MsgBox(eMsj, MsgBoxStyle.Exclamation)
                Exit Sub
            End If
            NueReClientesNetTmp(0, eContrato, eClv_Session, 0, "", "", 0)
            LlenatvNet()
            Exit Sub
        End If

        'Clientes normales

        ValidaServ(eContrato, eClv_TipSer)
        ValidaSiRecontrataCombo(eContrato)

        If eRes = 1 Then
            MsgBox(eMsj, MsgBoxStyle.Information)
            NueReClientesNetTmp(0, eContrato, eClv_Session, 0, "", "", 0)
            NueReClientesDigTmp(0, eContrato, eClv_Session, 0)
            LlenatvNet()
            LlenatvDig()
            LlenatvTEL()
            Exit Sub
        End If

        Dim res As Integer = 0
        res = MsgBox("Deseas contratar Combo", MsgBoxStyle.YesNo)

        If res = 6 Then 'Si
            comboCombo.DataSource = MuestaCombos(1, eClv_TipoCliente)
            panelSelCombo.Visible = True
        ElseIf res = 7 Then 'No
            NueReClientesNetTmp(0, eContrato, eClv_Session, 0, "", "", 0)
            NueReClientesDigTmp(0, eContrato, eClv_Session, 0)
            LlenatvNet()
            LlenatvDig()
            LlenatvTEL()
        End If

    End Sub

    Private Sub ValidaServ(ByVal Contrato As Long, ByVal Clv_TipSer As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaServReCon", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_TipSer
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = ""
            eRes = parametro3.Value
            eMsj = parametro4.Value

            If eRes > 0 Then
                Op = False
                If eClv_TipSer = 1 Then lblTV.Text = parametro4.Value.ToString
                If eClv_TipSer = 2 Then lblNET.Text = parametro4.Value.ToString
                If eClv_TipSer = 3 Then lblDIG.Text = parametro4.Value.ToString
                If eClv_TipSer = 5 Then lblTEL.Text = parametro4.Value.ToString
            Else
                Op = True
                If eClv_TipSer = 1 Then lblTV.Text = ""
                If eClv_TipSer = 2 Then lblNET.Text = ""
                If eClv_TipSer = 3 Then lblDIG.Text = ""
                If eClv_TipSer = 5 Then lblTEL.Text = ""
            End If

            BloquearBN(Clv_TipSer, Op)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try


    End Sub

    Private Sub ValidaSiRecontrataCombo(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaSiRecontrataCombo", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Contrato
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = ""
            eRes = par2.Value
            eMsj = par3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub TextBoxContrato_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxContrato.KeyPress
        e.KeyChar = Chr(ValidaKey(TextBoxContrato, Asc(LCase(e.KeyChar)), "N"))
    End Sub

    Private Sub TextBoxContrato_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxContrato.TextChanged
        Limpiar()
    End Sub





    Private Sub DameClv_Session()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameClv_Session", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Session = parametro.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub DimeTipSerCombo(ByVal Clv_Descuento As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DimeTipSerCombo", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Clv_Descuento", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_Descuento
        comando.Parameters.Add(par)

        Dim par0 As New SqlParameter("@Combo", SqlDbType.VarChar, 150)
        par0.Direction = ParameterDirection.Output
        comando.Parameters.Add(par0)

        Dim par2 As New SqlParameter("@TV", SqlDbType.Bit)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@NET", SqlDbType.Bit)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@DIG", SqlDbType.Bit)
        par4.Direction = ParameterDirection.Output
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@TEL", SqlDbType.Bit)
        par5.Direction = ParameterDirection.Output
        comando.Parameters.Add(par5)

        Dim par6 As New SqlParameter("@SERTV", SqlDbType.VarChar, 150)
        par6.Direction = ParameterDirection.Output
        comando.Parameters.Add(par6)

        Dim par7 As New SqlParameter("@SERNET", SqlDbType.VarChar, 150)
        par7.Direction = ParameterDirection.Output
        comando.Parameters.Add(par7)

        Dim par8 As New SqlParameter("@SERDIG", SqlDbType.VarChar, 150)
        par8.Direction = ParameterDirection.Output
        comando.Parameters.Add(par8)

        Dim par9 As New SqlParameter("@SERTEL", SqlDbType.VarChar, 150)
        par9.Direction = ParameterDirection.Output
        comando.Parameters.Add(par9)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

            eBndTV = par2.Value
            eBndNET = par3.Value
            eBndDIG = par4.Value
            eBndTEL = par5.Value
            lblCombo.Text = par0.Value
            groupTV.Enabled = par2.Value
            groupNET.Enabled = par3.Value
            groupDIG.Enabled = par4.Value
            groupTEL.Enabled = par5.Value
            lblSerTV.Text = par6.Value
            lblSerNET.Text = par7.Value
            lblSerDIG.Text = par8.Value
            lblSerTEL.Text = par9.Value

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ValidaRecontratacion(ByVal Contrato As Long, ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaRecontratacion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par0 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par0.Direction = ParameterDirection.Input
        par0.Value = Contrato
        comando.Parameters.Add(par0)

        Dim par As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_Session
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = String.Empty
            eRes = CInt(par2.Value)
            eMsj = CStr(par3.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub GrabaRecontratacion(ByVal Contrato As Long, ByVal Clv_Session As Integer, ByVal Clv_Usuario As String, ByVal SoloInternet As Boolean, ByVal Clv_Descuento As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("GrabaRecontratacion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Contrato
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Clv_Session", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Session
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 5)
        par3.Direction = ParameterDirection.Input
        par3.Value = Clv_Usuario
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@SoloInternet", SqlDbType.Bit)
        par4.Direction = ParameterDirection.Input
        par4.Value = SoloInternet
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@Clv_Descuento", SqlDbType.VarChar, 5)
        par5.Direction = ParameterDirection.Input
        par5.Value = Clv_Descuento
        comando.Parameters.Add(par5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            MsgBox(mensaje5)
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Public Sub NueReClienteCombo(ByVal Contrato As Long, ByVal Clv_Session As Long, ByVal Clv_Descuento As Integer, ByVal TVSinPago As Integer, ByVal TVConPago As Integer, ByVal TipoCablemodem As Char, ByVal NumCajas As Integer, ByVal OpTel As Char, ByVal Clv_Telefono As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueReClienteCombo", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Contrato
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Session
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Clv_Descuento", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = Clv_Descuento
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@TVSinPago", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = TVSinPago
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@TVConPago", SqlDbType.Int)
        par5.Direction = ParameterDirection.Input
        par5.Value = TVConPago
        comando.Parameters.Add(par5)

        Dim par6 As New SqlParameter("@TipoCablemodem", SqlDbType.VarChar, 1)
        par6.Direction = ParameterDirection.Input
        par6.Value = TipoCablemodem
        comando.Parameters.Add(par6)

        Dim par7 As New SqlParameter("@NumCajas", SqlDbType.Int)
        par7.Direction = ParameterDirection.Input
        par7.Value = NumCajas
        comando.Parameters.Add(par7)

        Dim par0 As New SqlParameter("@OpTel", SqlDbType.VarChar, 1)
        par0.Direction = ParameterDirection.Input
        par0.Value = OpTel
        comando.Parameters.Add(par0)

        Dim par8 As New SqlParameter("@Clv_Telefono", SqlDbType.Int)
        par8.Direction = ParameterDirection.Input
        par8.Value = Clv_Telefono
        comando.Parameters.Add(par8)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BorRecontratacionSession(ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorRecontratacionSession", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Input
        par.Value = Clv_Session
        comando.Parameters.Add(par)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub FrmRecontratacion_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GLOCONTRATOSEL > 0 Then
            Me.TextBoxContrato.Text = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0
            eContrato = Me.TextBoxContrato.Text

            MuestraInfoCliente(TextBoxContrato.Text)
            eContrato = TextBoxContrato.Text
            tcServicios.Enabled = True

            If CheckBoxSoloInternet.Checked = True Then
                If tcServicios.TabPages.Count > 1 Then
                    tcServicios.TabPages.Remove(tpTV)
                    tcServicios.TabPages.Remove(tpDIG)
                    tcServicios.TabPages.Remove(tpTEL)
                    eClv_TipSer = 2
                End If
            Else
                If tcServicios.TabPages.Count = 1 Then
                    tcServicios.TabPages.Remove(tpNET)
                    tcServicios.TabPages.Add(tpTV)
                    tcServicios.TabPages.Add(tpNET)
                    tcServicios.TabPages.Add(tpDIG)
                    tcServicios.TabPages.Add(tpTEL)
                    eClv_TipSer = 1
                End If
            End If

            DameClv_Session()

            'Clientes de sólo Internet--------------------------------------------------
            If CheckBoxSoloInternet.Checked = True Then
                ValidaServ(eContrato, eClv_TipSer)
                If eRes = 1 Then
                    MsgBox(eMsj, MsgBoxStyle.Exclamation)
                    Exit Sub
                End If
                NueReClientesNetTmp(0, eContrato, eClv_Session, 0, "", "", 0)
                LlenatvNet()
                Exit Sub
            End If
            '--------------------------------------------------------------------------

            'Clientes normales----------------------------------------------------------

            ValidaServ(eContrato, eClv_TipSer)
            ValidaSiRecontrataCombo(eContrato)

            If eRes = 1 Then
                MsgBox(eMsj, MsgBoxStyle.Information)
                NueReClientesNetTmp(0, eContrato, eClv_Session, 0, "", "", 0)
                NueReClientesDigTmp(0, eContrato, eClv_Session, 0)
                LlenatvNet()
                LlenatvDig()
                LlenatvTEL()
                Exit Sub
            End If

            Dim res As Integer = 0
            res = MsgBox("Deseas contratar Combo", MsgBoxStyle.YesNo)

            If res = 6 Then 'Si
                comboCombo.DataSource = MuestaCombos(1, eClv_TipoCliente)
                panelSelCombo.Visible = True
            ElseIf res = 7 Then 'No
                NueReClientesNetTmp(0, eContrato, eClv_Session, 0, "", "", 0)
                NueReClientesDigTmp(0, eContrato, eClv_Session, 0)
                LlenatvNet()
                LlenatvDig()
                LlenatvTEL()
            End If

            '---------------------------------------------------------------------------
        End If

        If eBndRePortabilidad = True Then
            eBndRePortabilidad = False

            ''-------------------Información---------------------
            ''eTipoRe="S" Servicio
            ''eTipoRe="C" Combo

            LlenatvTV()
            LlenatvNet()
            LlenatvDig()
            LlenatvTEL()
            eTipoRe = ""

        End If
    End Sub

    Private Sub FrmRecontratacion_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        limpiar()
    End Sub

    Private Sub FrmRecontratacion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'colorea(Me, Me.Name)
        eClv_Session = 0
        eClv_TipSer = 1
        eContrato = 0
        eContratoNet = 0
        eClv_Servicio = 0
        GLOCONTRATOSEL = 0
        eClv_Descuento = 0
        comboTipoCa.DataSource = MuestraTipoCablemodem()
        comboTipoAs.DataSource = MuestraTipoAsignacion()
        comboTipoSer.DataSource = MuestraTipSerInternet()
        comboSerTV.DataSource = MuestraServicioRecon(eClv_Session, 1, 0)
        comboSerNET.DataSource = MuestraServicioRecon(eClv_Session, 2, 0)
        comboSerDIG.DataSource = MuestraServicioRecon(eClv_Session, 3, 0)
        comboTelPlan.DataSource = MuestraServicioRecon(eClv_Session, 5, 0)
    End Sub



    Private Sub BloquearBN(ByVal Clv_TipSer As Integer, ByVal Op As Boolean)
        If Clv_TipSer = 1 Then
            bnTV.Enabled = Op
        ElseIf Clv_TipSer = 2 Then
            bnNETCablemodem.Enabled = Op
            bnNETServicio.Enabled = Op
        ElseIf Clv_TipSer = 3 Then
            bnDIGTarjeta.Enabled = Op
            bnDIGServicio.Enabled = Op
        ElseIf Clv_TipSer = 5 Then
            bnTelPlan.Enabled = Op
            bnTelSerDig.Enabled = Op
            bnTelPaqAdi.Enabled = Op
        End If

    End Sub



    Private Sub tsbRecon_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbRecon.Click
        ValidaRecontratacion(eContrato, eClv_Session)

        If eRes = 1 Then
            MsgBox(eMsj, MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If eRes = 2 Then
            Dim x As Integer = 0
            x = MsgBox(eMsj, MsgBoxStyle.YesNo)
            If x = 6 Then
                GrabaRecontratacion(eContrato, eClv_Session, eClv_Usuario, True, eClv_Descuento)
            End If
            Exit Sub
        End If

        GrabaRecontratacion(eContrato, eClv_Session, eClv_Usuario, CheckBoxSoloInternet.Checked, eClv_Descuento)

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub


    '--OBJETOS---------------------------------------------------------------------------------------------------------------------

    Private Sub tsbAgregarTV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarTV.Click
        If tvTV.Nodes.Count > 0 Then
            MsgBox("Ya se ha establecido un Servicio de Televisión.", MsgBoxStyle.Information)
            Exit Sub
        End If
        panelTV.Visible = True
    End Sub

    Private Sub tsbEliminarTV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarTV.Click
        If tvTV.Nodes.Count = 0 Then
            MsgBox("Selecciona un Servicio de Televisión.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorReClientesTVTmp(eClv_Session)
        LlenatvTV()
    End Sub

    Private Sub txtSinPago_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSinPago.KeyPress
        e.KeyChar = Chr(ValidaKey(txtSinPago, Asc(LCase(e.KeyChar)), "N"))
    End Sub

    Private Sub txtConPago_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtConPago.KeyPress
        e.KeyChar = Chr(ValidaKey(txtConPago, Asc(LCase(e.KeyChar)), "N"))
    End Sub

    Private Sub btnTVAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTVAceptar.Click
        If txtSinPago.Text.Length = 0 Then txtSinPago.Text = 0
        If txtConPago.Text.Length = 0 Then txtConPago.Text = 0
        NueReClientesTVTmp(eContrato, eClv_Session, txtSinPago.Text, txtConPago.Text, comboSerTV.SelectedValue, checkTV.Checked)
        LlenatvTV()
        panelTV.Visible = False
    End Sub

    Private Sub btnTVCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTVCancelar.Click
        panelTV.Visible = False
    End Sub






    Private Sub tsbAgregarNET_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarNET.Click
        panelCNET.Visible = True
    End Sub

    Private Sub tsbEliminarNET_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarNET.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona un Aparato.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorReClientesNetTmp(eContratoNet)
        LlenatvNet()
    End Sub

    Private Sub btnCNETAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCNETAceptar.Click
        NueReClientesNetTmp(1, eContrato, eClv_Session, 2, comboTipoCa.SelectedValue, comboTipoAs.SelectedValue, comboTipoSer.SelectedValue)
        LlenatvNet()
        panelCNET.Visible = False
    End Sub

    Private Sub btnCNETCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCNETCancelar.Click
        panelCNET.Visible = False
    End Sub

    Private Sub tsbAgregarSNET_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarSNET.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona un Aparato.", MsgBoxStyle.Information)
            Exit Sub
        End If
        comboSerNET.DataSource = MuestraServicioRecon(eClv_Servicio, 2, eContratoNet)
        panelSNET.Visible = True
    End Sub

    Private Sub tsbEliminarSNET_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarSNET.Click
        If eClv_UnicaNet = 0 Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorReContNetTmp(eClv_UnicaNet)
        LlenatvNet()
    End Sub

    Private Sub btnSNETAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSNETAceptar.Click
        If comboSerNET.SelectedValue = 0 Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        NueReContNetTmp(eContrato, eContratoNet, comboSerNET.SelectedValue, checkNET.Checked)
        LlenatvNet()
        eContratoNet = 0
        panelSNET.Visible = False
    End Sub

    Private Sub btnSNETCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSNETCancelar.Click
        panelSNET.Visible = False
    End Sub






    Private Sub tsbAgregarTDIG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarTDIG.Click
        NueReClientesDigTmp(1, eContrato, eClv_Session, 0)
        LlenatvDig()
    End Sub

    Private Sub tsbEliminarTDIG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarTDIG.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona un Aparato.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorReClientesDigTmp(eContratoNet)
        LlenatvDig()
    End Sub

    Private Sub tsbAgregarSDIG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbAgregarSDIG.Click
        If eContratoNet = 0 Then
            MsgBox("Selecciona un Aparato", MsgBoxStyle.Information)
            Exit Sub
        End If

        comboSerDIG.DataSource = MuestraServicioRecon(eClv_Session, 3, eContratoNet)
        panelDIG.Visible = True
    End Sub

    Private Sub tsbEliminarSDIG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminarSDIG.Click
        If eClv_UnicaNet = 0 Then
            MsgBox("Selecciona un Aparato.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorReContDigTmp(eClv_UnicaNet)
        LlenatvDig()
    End Sub

    Private Sub btnDIGAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDIGAceptar.Click
        If comboSerDIG.SelectedValue = 0 Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If
        NueReContDigTmp(eContrato, 0, eContratoNet, comboSerDIG.SelectedValue, checkDIG.Checked)
        eContratoNet = 0
        LlenatvDig()
        panelDIG.Visible = False
    End Sub

    Private Sub btnDIGCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDIGCancelar.Click
        panelDIG.Visible = False
    End Sub


    Private Sub tsbPlanAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbPlanAgregar.Click
        If tvTel.Nodes.Count > 0 Then
            MsgBox("Ya existe un Plan Tarifario.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        comboTelPlan.DataSource = MuestraServicioRecon(eClv_Session, 5, 0)
        panelTelPlan.Visible = True
    End Sub

    Private Sub tsbPlanEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbPlanEliminar.Click
        If eClv_UnicaNet = 0 Then
            MsgBox("Selecciona un Plan Tarifario.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorReContTelTmp(eClv_UnicaNet, eClv_Session)
        LlenatvTEL()
    End Sub

    Private Sub bntTelPlanAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntTelPlanAceptar.Click



        eClv_Telefono = 0
        eOpTel = ""

        If radioAuto.Checked = True Then eOpTel = "A"
        If radioSel.Checked = True Then
            eOpTel = "S"
            eClv_Telefono = comboTel.SelectedValue
        End If
        If radioPor.Checked = True Then eOpTel = "P"



        If eOpTel = "P" Then
            eTipoRe = "S"
            FrmReDatosPortabilidad.Show()
            panelTelPlan.Visible = False
            Exit Sub
        End If


        NueReContTelTmp(eContrato, eClv_Session, 0, eOpTel, eClv_Telefono, comboTelPlan.SelectedValue, checkTEL.Checked)
        LlenatvTEL()
        panelTelPlan.Visible = False
    End Sub

    Private Sub btnTelPlanCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTelPlanCancelar.Click
        panelTelPlan.Visible = False
    End Sub

    Private Sub tsbSerDigAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbSerDigAgregar.Click
        If eClv_UnicaNet = 0 Then
            MsgBox("Selecciona el Plan Tarifario.", MsgBoxStyle.Information)
            Exit Sub
        End If

        comboTelSerDig.DataSource = MuestraSerTel(eClv_UnicaNet, "D")
        panelTelSerDig.Visible = True
    End Sub

    Private Sub tsbSerDigEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbSerDigEliminar.Click
        If eTipo_Servicio <> "D" Then
            MsgBox("Selecciona un Servicio Digital.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorReContTelAdicTmp(eClv_UnicaNet, eClv_Servicio, eTipo_Servicio)
        LlenatvTEL()
    End Sub

    Private Sub btnTelSerDigAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTelSerDigAceptar.Click
        If comboTelSerDig.SelectedValue = 0 Then
            MsgBox("Selecciona un Servicio Digital.", MsgBoxStyle.Information)
            Exit Sub
        End If

        NueReContTelAdicTmp(eClv_UnicaNet, comboTelSerDig.SelectedValue, "D")
        LlenatvTEL()
        panelTelSerDig.Visible = False
    End Sub

    Private Sub btnTelSerDigCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTelSerDigCancelar.Click
        panelTelSerDig.Visible = False
    End Sub

    Private Sub tsbPadAdiAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbPadAdiAgregar.Click
        If eClv_UnicaNet = 0 Then
            MsgBox("Selecciona el Plan Tarifario.", MsgBoxStyle.Information)
            Exit Sub
        End If

        comboTelPaqAdi.DataSource = MuestraSerTel(eClv_UnicaNet, "A")
        panelTelPaqAdi.Visible = True

    End Sub

    Private Sub tsbPadAdiEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbPadAdiEliminar.Click
        If eTipo_Servicio <> "A" Then
            MsgBox("Selecciona un Paquete Adicional.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorReContTelAdicTmp(eClv_UnicaNet, 0, eTipo_Servicio)
        LlenatvTEL()
    End Sub

    Private Sub btnTelPaqAdiAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTelPaqAdiAceptar.Click
        If comboTelPaqAdi.SelectedValue = 0 Then
            MsgBox("Selecciona un Paquete Adicional.", MsgBoxStyle.Information)
        End If

        NueReContTelAdicTmp(eClv_UnicaNet, comboTelPaqAdi.SelectedValue, "A")
        LlenatvTEL()
        panelTelPaqAdi.Visible = False
    End Sub

    Private Sub btnTelPaqAdiCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTelPaqAdiCancelar.Click
        panelTelPaqAdi.Visible = False
    End Sub

    Private Sub tcServicios_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tcServicios.SelectedIndexChanged
        If tcServicios.TabPages.Count = 0 Then
            Exit Sub
        End If
        If tcServicios.SelectedTab.Name = "tpTV" Then
            eClv_TipSer = 1
            ValidaServ(eContrato, eClv_TipSer)
            If eRes = 0 Then LlenatvTV()
        ElseIf tcServicios.SelectedTab.Name = "tpNET" Then
            eClv_TipSer = 2
            ValidaServ(eContrato, eClv_TipSer)
            If eRes = 0 Then LlenatvNet()
        ElseIf tcServicios.SelectedTab.Name = "tpDIG" Then
            eClv_TipSer = 3
            ValidaServ(eContrato, eClv_TipSer)
            If eRes = 0 Then LlenatvDig()
        ElseIf tcServicios.SelectedTab.Name = "tpTEL" Then
            eClv_TipSer = 5
            ValidaServ(eContrato, eClv_TipSer)
            If eRes = 0 Then LlenatvTEL()
        End If

        eContratoNet = 0
        eClv_Servicio = 0
        eClv_UnicaNet = 0
        eTipo_Servicio = ""
        eIdTelAdic = 0

    End Sub

    Private Sub btnAceptarSelCombo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptarSelCombo.Click
        panelSelCombo.Visible = False
        LimpiarCombo()
        eClv_Descuento = comboCombo.SelectedValue
        DimeTipSerCombo(comboCombo.SelectedValue)
        panelCombo.Visible = True
    End Sub

    Private Sub btnCancelarSelCombo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelarSelCombo.Click
        panelSelCombo.Visible = False
        DameClv_Session()
        NueReClientesNetTmp(0, eContrato, eClv_Session, 0, "", "", 0)
        NueReClientesDigTmp(0, eContrato, eClv_Session, 0)
        LlenatvNet()
        LlenatvDig()
        LlenatvTEL()
    End Sub

    Private Sub btnAceptarCombo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptarCombo.Click
        opcionport = "N"
        eClv_Telefono = 0
        eOpTel = ""
        eTipoRe = "C"
        eClv_Descuento = comboCombo.SelectedValue

        If eBndTEL = True Then
            If radioAutoCombo.Checked = True Then eOpTel = "A"
            If radioSelCombo.Checked = True Then
                eOpTel = "S"
                eClv_Telefono = comboTelCombo.SelectedValue
            End If
            If radioPorCombo.Checked = True Then eOpTel = "P"
        End If
        If eBndTV = True Then
            If txtSinPagoCombo.Text.Length = 0 Then txtSinPago.Text = 0
            If txtConPagoCombo.Text.Length = 0 Then txtConPago.Text = 0

        End If
        If eBndNET = True Then
            If comboTipoCaCombo.SelectedValue.ToString.Length = 0 Then
                MsgBox("Selecciona un Tipo de Aparato.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
        End If
        If eBndDIG = True Then
            If txtNumCajas.Text.Length = 0 Then txtNumCajas.Text = 0
            If txtNumCajas.Text = 0 Then
                MsgBox("Teclea un número de Aparatos.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
        End If

        If eOpTel = "P" Then

            FrmReDatosPortabilidad.Show()
            Exit Sub
        End If

        NueReClienteCombo(eContrato, eClv_Session, eClv_Descuento, txtSinPagoCombo.Text, txtConPagoCombo.Text, comboTipoCaCombo.SelectedValue, txtNumCajas.Text, eOpTel, eClv_Telefono)
        LlenatvTV()
        LlenatvNet()
        LlenatvDig()
        LlenatvTEL()
        panelCombo.Visible = False

    End Sub

    Private Sub btnCancelarCombo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelarCombo.Click
        panelCombo.Visible = False
    End Sub

    Private Sub radioSelCombo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radioSelCombo.CheckedChanged
        If radioSelCombo.Checked = True Then
            comboTelCombo.Enabled = True
            comboTelCombo.DataSource = MuestraCatNumTel()
        Else
            comboTelCombo.Enabled = False
        End If
    End Sub


    Private Sub radioSel_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radioSel.CheckedChanged
        If radioSel.Checked = True Then
            comboTel.Enabled = True
            comboTel.DataSource = MuestraCatNumTel()
        Else
            comboTel.Enabled = False
        End If
    End Sub


End Class