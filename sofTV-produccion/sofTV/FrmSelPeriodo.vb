Public Class FrmSelPeriodo


    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            LocPeriodo1 = True
        Else
            LocPeriodo1 = False
        End If


    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            LocPeriodo2 = True
        Else
            LocPeriodo2 = False
        End If
    End Sub

    Private Sub FrmSelPeriodo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LocPeriodo1 = False
        LocPeriodo2 = False
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If Me.CheckBox1.CheckState = CheckState.Unchecked And Me.CheckBox2.CheckState = CheckState.Unchecked Then
            MsgBox("Seleccione al menos un Periodo", MsgBoxStyle.Information)
            Exit Sub
        Else
            Select Case LocOp
                Case 1
                    bndReportC = True
                Case 5
                    bndReport2 = True
                Case 3
                    If bnd_Canc_Sin_Mens = False Then
                        bndfechareport = True
                    ElseIf bnd_Canc_Sin_Mens = True Then
                        bnd_Canc_Sin_Mens = False
                        bnd_Canc_Sin_Mens_buena = True
                    End If
                Case 4
                    bndReport = True
                Case 6
                    GloBndEtiqueta = True
                Case 7
                    If GloOpEtiqueta <> "1" Then
                        If IdSistema = "SA" And (GloOpEtiqueta = "3" Or GloOpEtiqueta = "4" Or GloOpEtiqueta = "1") Then
                            bndAvisos2 = True
                        Else
                            FrmSelRecord.Show()
                        End If
                    ElseIf GloOpEtiqueta = "1" Then
                        FrmTelsi.Show()
                        'Else
                        '    bndAvisos2 = True
                        'End If
                    End If

                Case 8
                    GloBndSelBanco = True
                Case 9
                    bndReportA = True
                Case 10
                    LocServicios = True
                Case 20
                    'If bec_bnd = False Then
                    Locreportcity = True
                    'ElseIf bec_bnd = True Then

                    'End If
                Case 21
                    bndfechareport2 = True
                Case 22
                    GloBndSelBanco = True
                Case 25
                    Locreportcity = True
                Case 30
                    LocBndRepMix = True
                    LocGloOpRep = 2
                    FrmImprimirFac.Show()
                Case 35
                    'Locbndrepcontspago = True
                    FrmSelFechas.Show()
                Case 90
                    'LReportecombo1 = True
                    'FrmImprimirContrato.Show()
                    FrmOpcionRepCombos.Show()
            End Select
            If LEdo_Cuenta = True Or LEdo_Cuenta2 = True Then
                ' bec_bnd = False
                FrmImprimirContrato.Show()
                'LocreportEstado = True
            End If
        End If
        Me.Close()
        
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class