Imports System.Data.SqlClient
Public Class FrmClientesPorRecuperar
    Public eRes As Integer = 0
    Public eMsg As String = Nothing

    Private Sub Nuevo()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONUSUARIOSTableAdapter.Connection = CON
        Me.CONUSUARIOSTableAdapter.Fill(Me.DataSetEric.CONUSUARIOS, eClv_Usuario)
        CON.Close()
        Me.FechaSolicitudLabel2.Text = Today
        Me.BindingNavigatorSaveItem.Text = "&Guardar"
        Me.BindingNavigatorDeleteItem.Enabled = False
    End Sub

    Private Sub Recuperar()
        Me.BindingNavigatorSaveItem.Text = "&Recuperar Cliente"
        Me.ContratoTextBox.Enabled = False
        Me.ConceptoComboBox.Visible = False
        Me.NombreLabel1.Visible = False
    End Sub

    Private Sub Consultar()
        Me.ConceptoComboBox.Visible = False
        Me.NombreLabel1.Visible = False
        Me.BindingNavigator1.Enabled = False
        Me.GroupBox1.Enabled = False
        Me.GroupBox2.Enabled = False
    End Sub

    Private Sub Status()

        If CInt(Me.SeRecuperoTextBox.Text) = 0 Then
            Me.GroupBox2.Text = "Informaci�n del Servicio por Recuperar"
            Me.PanelDias.Visible = True
        ElseIf CInt(Me.SeRecuperoTextBox.Text) = 1 Then
            Me.GroupBox2.Text = "Informaci�n del Servicio Recuperado"
            Me.PanelStatus.Visible = True
            Me.PanelRecuperado.Visible = True
        ElseIf CInt(Me.SeRecuperoTextBox.Text) = 2 Then
            Me.GroupBox2.Text = "Informaci�n del Servicio no Recuperado"
        End If

    End Sub

    Private Sub Muestra()
        MuestraClientePorRecuperar(eClavePorRecuperar)
    End Sub

    Private Sub Guardar()
        If eOpcion = "N" Then
            If CInt(Me.ConceptoComboBox.SelectedValue) = 0 Then
                MsgBox("El cliente no posee servicios para recuperar.", MsgBoxStyle.Information)
                Exit Sub
            End If
            NueClientePorRecuperar(CLng(Me.ContratoTextBox.Text), CInt(Me.ConceptoComboBox.SelectedValue), eClv_Usuario, Me.ObservacionTextBox.Text)
        ElseIf eOpcion = "E" Then
            RecuperarCliente(eClavePorRecuperar, eClv_Usuario, Me.ObservacionTextBox.Text)
            MuestraClientePorRecuperar(eClavePorRecuperar)
        End If
    End Sub

    Private Sub Eliminar()
        BorClientePorRecuperar(eClavePorRecuperar)
    End Sub

    Private Sub BuscarClientePorContrato(ByVal Contrato As Long)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BUSCLIPORCONTRATO3TableAdapter.Connection = CON
        Me.BUSCLIPORCONTRATO3TableAdapter.Fill(Me.DataSetEric2.BUSCLIPORCONTRATO3, Contrato, "", "", "", "", 0)
        CON.Close()
    End Sub

    Private Sub CrearArbol()
        Try
            Dim CON As New SqlConnection(MiConexion)

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow

            CON.Open()
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.DataSetEric2.dameSerDELCli, CLng(Me.ContratoTextBox.Text))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.DataSetEric2.dameSerDELCli, 0)
            End If
            CON.Close()
            Dim pasa As Boolean = False


            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.DataSetEric2.dameSerDELCli.Rows


                X = 0

                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Telefon�a" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    jNet = -1
                    jDig = -1
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next

            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub MuestraServClientePorRecuperar(ByVal Contrato As Long)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraServClientePorRecuperarTableAdapter.Connection = CON
        Me.MuestraServClientePorRecuperarTableAdapter.Fill(Me.DataSetEric2.MuestraServClientePorRecuperar, Contrato)
        CON.Close()
    End Sub

    Private Sub MuestraClientePorRecuperar(ByVal Clave As Long)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraClientePorRecuperarTableAdapter.Connection = CON
        Me.MuestraClientePorRecuperarTableAdapter.Fill(Me.DataSetEric2.MuestraClientePorRecuperar, Clave, 0, 0, 0, Today, 0, 0, Today, 1)
        CON.Close()
    End Sub

    Private Sub NueClientePorRecuperar(ByVal Contrato As Long, ByVal Clv_TipSer As Integer, ByVal Clv_UsuarioSolicito As Integer, ByVal Observacion As String)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueClientePorRecuperar", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        With parametro
            .Direction = ParameterDirection.Input
            .Value = Contrato
        End With
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        With parametro1
            .Direction = ParameterDirection.Input
            .Value = Clv_TipSer
        End With
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Clv_UsuarioSolicito", SqlDbType.Int)
        With parametro2
            .Direction = ParameterDirection.Input
            .Value = Clv_UsuarioSolicito
        End With
        comando.Parameters.Add(parametro2)

        Dim parametro6 As New SqlParameter("@Observacion", SqlDbType.VarChar, 250)
        With parametro6
            .Direction = ParameterDirection.Input
            .Value = Observacion
        End With
        comando.Parameters.Add(parametro6)

        Dim parametro3 As New SqlParameter("@Res", SqlDbType.BigInt)
        With parametro3
            .Direction = ParameterDirection.Output
            .Value = 0
        End With
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        With parametro4
            .Direction = ParameterDirection.Output
            .Value = ""
        End With
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Clave", SqlDbType.Int)
        With parametro5
            .Direction = ParameterDirection.Output
            .Value = 0
        End With
        comando.Parameters.Add(parametro5)

        Try
            eRes = 0
            eMsg = Nothing
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(parametro3.Value.ToString)
            eMsg = parametro4.Value.ToString
            eClavePorRecuperar = CLng(parametro5.Value.ToString)
            conexion.Close()

            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Exclamation)
            Else
                eBndPorRecuperar = True
                MsgBox(mensaje5, MsgBoxStyle.Information)
                Me.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub RecuperarCliente(ByVal Clave As Long, ByVal Clv_UsuarioRecupero As Integer, ByVal Observacion As String)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("RecuperarCliente", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clave", SqlDbType.BigInt)
        With parametro
            .Direction = ParameterDirection.Input
            .Value = Clave
        End With
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Clv_UsuarioRecupero", SqlDbType.Int)
        With parametro1
            .Direction = ParameterDirection.Input
            .Value = Clv_UsuarioRecupero
        End With
        comando.Parameters.Add(parametro1)

        Dim parametro4 As New SqlParameter("@Observacion", SqlDbType.VarChar, 250)
        With parametro4
            .Direction = ParameterDirection.Input
            .Value = Observacion
        End With
        comando.Parameters.Add(parametro4)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        With parametro2
            .Direction = ParameterDirection.Output
            .Value = 0
        End With
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        With parametro3
            .Direction = ParameterDirection.Output
            .Value = ""
        End With
        comando.Parameters.Add(parametro3)

        Try
            eRes = 0
            eMsg = Nothing
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(parametro2.Value.ToString)
            eMsg = parametro3.Value.ToString
            conexion.Close()

            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Exclamation)
            Else
                eBndPorRecuperar = True
                MsgBox("Se Recuper� con �xito.", MsgBoxStyle.Information)
                Me.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub BorClientePorRecuperar(ByVal Clave As Long)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorClientePorRecuperar", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clave", SqlDbType.BigInt)
        With parametro
            .Direction = ParameterDirection.Input
            .Value = Clave
        End With
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Res", SqlDbType.BigInt)
        With parametro1
            .Direction = ParameterDirection.Output
            .Value = 0
        End With
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        With parametro2
            .Direction = ParameterDirection.Output
            .Value = ""
        End With
        comando.Parameters.Add(parametro2)

        Try
            eRes = 0
            eMsg = Nothing
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(parametro1.Value.ToString)
            eMsg = parametro2.Value.ToString
            conexion.Close()

            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Exclamation)
            Else
                eBndPorRecuperar = True
                MsgBox(mensaje6, MsgBoxStyle.Information)
                Me.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged
        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            If CInt(Me.ContratoTextBox.Text) > 0 Then
                BuscarClientePorContrato(CLng(Me.ContratoTextBox.Text))
                CrearArbol()
                MuestraServClientePorRecuperar(CLng(Me.ContratoTextBox.Text))
            End If
        End If
    End Sub

    Private Sub FrmClientesPorRecuperar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        If eOpcion = "N" Then
            Nuevo()
        ElseIf eOpcion = "E" Then
            Muestra()
            Recuperar()
            Status()
        ElseIf eOpcion = "C" Then
            Muestra()
            Consultar()
            Status()
        End If

    End Sub

    Private Sub BindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorSaveItem.Click
        Guardar()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Eliminar()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub
End Class