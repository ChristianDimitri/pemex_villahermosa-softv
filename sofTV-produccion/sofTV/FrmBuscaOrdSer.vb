Imports System.Data.SqlClient
Public Class FrmBuscaOrdSer

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value.ToString) = True Then
            GLOCLV_oRDENbus = Me.DataGridView1.SelectedCells(0).Value.ToString
            Me.Close()
        Else
            MsgBox("Seleccione la Orden ", MsgBoxStyle.Information)
        End If
    End Sub



    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim sTATUS As String = "P"
        Try
            If IsNumeric(GloClv_TipSer) = True Then
                If op = 0 Then 'contrato
                    If IsNumeric(Me.TextBox1.Text) = True Then
                        Me.BusquedaOrdSerDynamicTableAdapter.Connection = CON
                        Me.BusquedaOrdSerDynamicTableAdapter.Fill(Me.NewsoftvDataSet1.BusquedaOrdSerDynamic, 0, GloPendientes, GloEjecutadas, GloVisita, 0, Me.TextBox1.Text, "", "", "", op)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 1 Then
                    If Len(Trim(Me.TextBox2.Text)) > 0 Then
                        Me.BusquedaOrdSerDynamicTableAdapter.Connection = CON
                        Me.BusquedaOrdSerDynamicTableAdapter.Fill(Me.NewsoftvDataSet1.BusquedaOrdSerDynamic, 0, GloPendientes, GloEjecutadas, GloVisita, 0, 0, Me.TextBox2.Text, "", "", op)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                ElseIf op = 2 Then 'Calle y numero
                    Me.BusquedaOrdSerDynamicTableAdapter.Connection = CON
                    Me.BusquedaOrdSerDynamicTableAdapter.Fill(Me.NewsoftvDataSet1.BusquedaOrdSerDynamic, 0, GloPendientes, GloEjecutadas, GloVisita, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, op)
                ElseIf op = 3 Then 'clv_Orden
                    If IsNumeric(Me.TextBox3.Text) = True Then
                        Me.BusquedaOrdSerDynamicTableAdapter.Connection = CON
                        Me.BusquedaOrdSerDynamicTableAdapter.Fill(Me.NewsoftvDataSet1.BusquedaOrdSerDynamic, 0, GloPendientes, GloEjecutadas, GloVisita, Me.TextBox3.Text, 0, "", "", "", op)
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                Else
                    Me.BusquedaOrdSerDynamicTableAdapter.Connection = CON
                    Me.BusquedaOrdSerDynamicTableAdapter.Fill(Me.NewsoftvDataSet1.BusquedaOrdSerDynamic, 0, GloPendientes, GloEjecutadas, GloVisita, 0, 0, Me.TextBox1.Text, "", "", 4)
                End If
                Me.TextBox1.Clear()
                Me.TextBox2.Clear()
                Me.TextBox3.Clear()
                Me.BNUMERO.Clear()
                Me.BCALLE.Clear()
            Else
                MsgBox("Seleccione el Tipo de Servicio")
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub FrmBuscaOrdSer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        CON.Close()
        Busca(4)

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub



    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged

    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(3)
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Clv_calleLabel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub



    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(2)
    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub BCALLE_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BCALLE.TextChanged

    End Sub

    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub BNUMERO_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BNUMERO.TextChanged

    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        If IsNumeric(Me.DataGridView1.SelectedCells(0).Value.ToString) = True Then
            GLOCLV_oRDENbus = Me.DataGridView1.SelectedCells(0).Value.ToString
            Me.Close()
        Else
            MsgBox("Seleccione la Orden ", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub SplitContainer1_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles SplitContainer1.Panel1.Paint

    End Sub
End Class