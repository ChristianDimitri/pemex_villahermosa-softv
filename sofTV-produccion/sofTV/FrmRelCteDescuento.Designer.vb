<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRelCteDescuento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DescuentoLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRelCteDescuento))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DataSetEric = New sofTV.DataSetEric()
        Me.ConRelCteDescuentoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConRelCteDescuentoTableAdapter = New sofTV.DataSetEricTableAdapters.ConRelCteDescuentoTableAdapter()
        Me.ConRelCteDescuentoBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ConRelCteDescuentoBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.DescuentoNumericUpDown = New System.Windows.Forms.NumericUpDown()
        DescuentoLabel = New System.Windows.Forms.Label()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConRelCteDescuentoBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConRelCteDescuentoBindingNavigator.SuspendLayout()
        CType(Me.DescuentoNumericUpDown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DescuentoLabel
        '
        DescuentoLabel.AutoSize = True
        DescuentoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescuentoLabel.Location = New System.Drawing.Point(42, 52)
        DescuentoLabel.Name = "DescuentoLabel"
        DescuentoLabel.Size = New System.Drawing.Size(79, 15)
        DescuentoLabel.TabIndex = 5
        DescuentoLabel.Text = "Descuento:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(214, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "%"
        Me.Label1.Visible = False
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(196, 119)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(105, 31)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ConRelCteDescuentoBindingSource
        '
        Me.ConRelCteDescuentoBindingSource.DataMember = "ConRelCteDescuento"
        Me.ConRelCteDescuentoBindingSource.DataSource = Me.DataSetEric
        '
        'ConRelCteDescuentoTableAdapter
        '
        Me.ConRelCteDescuentoTableAdapter.ClearBeforeFill = True
        '
        'ConRelCteDescuentoBindingNavigator
        '
        Me.ConRelCteDescuentoBindingNavigator.AddNewItem = Nothing
        Me.ConRelCteDescuentoBindingNavigator.BindingSource = Me.ConRelCteDescuentoBindingSource
        Me.ConRelCteDescuentoBindingNavigator.CountItem = Nothing
        Me.ConRelCteDescuentoBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.ConRelCteDescuentoBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConRelCteDescuentoBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.ConRelCteDescuentoBindingNavigatorSaveItem})
        Me.ConRelCteDescuentoBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConRelCteDescuentoBindingNavigator.MoveFirstItem = Nothing
        Me.ConRelCteDescuentoBindingNavigator.MoveLastItem = Nothing
        Me.ConRelCteDescuentoBindingNavigator.MoveNextItem = Nothing
        Me.ConRelCteDescuentoBindingNavigator.MovePreviousItem = Nothing
        Me.ConRelCteDescuentoBindingNavigator.Name = "ConRelCteDescuentoBindingNavigator"
        Me.ConRelCteDescuentoBindingNavigator.PositionItem = Nothing
        Me.ConRelCteDescuentoBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConRelCteDescuentoBindingNavigator.Size = New System.Drawing.Size(321, 25)
        Me.ConRelCteDescuentoBindingNavigator.TabIndex = 4
        Me.ConRelCteDescuentoBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ConRelCteDescuentoBindingNavigatorSaveItem
        '
        Me.ConRelCteDescuentoBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConRelCteDescuentoBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConRelCteDescuentoBindingNavigatorSaveItem.Name = "ConRelCteDescuentoBindingNavigatorSaveItem"
        Me.ConRelCteDescuentoBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.ConRelCteDescuentoBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'DescuentoNumericUpDown
        '
        Me.DescuentoNumericUpDown.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.ConRelCteDescuentoBindingSource, "Descuento", True))
        Me.DescuentoNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescuentoNumericUpDown.Location = New System.Drawing.Point(127, 52)
        Me.DescuentoNumericUpDown.Maximum = New Decimal(New Integer() {1410065407, 2, 0, 0})
        Me.DescuentoNumericUpDown.Name = "DescuentoNumericUpDown"
        Me.DescuentoNumericUpDown.Size = New System.Drawing.Size(131, 21)
        Me.DescuentoNumericUpDown.TabIndex = 6
        '
        'FrmRelCteDescuento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(321, 170)
        Me.Controls.Add(DescuentoLabel)
        Me.Controls.Add(Me.DescuentoNumericUpDown)
        Me.Controls.Add(Me.ConRelCteDescuentoBindingNavigator)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FrmRelCteDescuento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Descuento Cliente"
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelCteDescuentoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConRelCteDescuentoBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConRelCteDescuentoBindingNavigator.ResumeLayout(False)
        Me.ConRelCteDescuentoBindingNavigator.PerformLayout()
        CType(Me.DescuentoNumericUpDown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents ConRelCteDescuentoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConRelCteDescuentoTableAdapter As sofTV.DataSetEricTableAdapters.ConRelCteDescuentoTableAdapter
    Friend WithEvents ConRelCteDescuentoBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ConRelCteDescuentoBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents DescuentoNumericUpDown As System.Windows.Forms.NumericUpDown
End Class
