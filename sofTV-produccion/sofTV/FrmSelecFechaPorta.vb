﻿Public Class FrmSelecFechaPorta
    Dim opcion As Integer
    Private Sub FrmSelecFechaPorta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If Me.DateTimePicker1.Value > Me.DateTimePicker2.Value Then
            MsgBox("La Fecha De Inicio No Puede Ser Mayor A La Fecha Final")
            Exit Sub
        Else
            fechaini = Me.DateTimePicker1.Value.ToString
            fechafin = Me.DateTimePicker2.Value.ToString
            If Me.RadioButton1.Checked = True Then
                opcion = 0
            ElseIf Me.RadioButton2.Checked = True Then
                opcion = 1
            ElseIf Me.RadioButton3.Checked = True Then
                opcion = 2
            End If
            opcionStatus = opcion
            eOpVentas = 110
            FrmImprimirComision.Show()
            Me.Hide()
            Me.Close()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
        Me.Dispose()

    End Sub
End Class