﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCatalogoHomologacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCatalogoHomologacion))
        Me.ServicioAnalogico = New System.Windows.Forms.ComboBox
        Me.ServicioDigital = New System.Windows.Forms.ComboBox
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.CONGenerales_InternetBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.CONGenerales_InternetBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONGenerales_InternetBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONGenerales_InternetBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'ServicioAnalogico
        '
        Me.ServicioAnalogico.FormattingEnabled = True
        Me.ServicioAnalogico.Location = New System.Drawing.Point(12, 35)
        Me.ServicioAnalogico.Name = "ServicioAnalogico"
        Me.ServicioAnalogico.Size = New System.Drawing.Size(246, 21)
        Me.ServicioAnalogico.TabIndex = 0
        '
        'ServicioDigital
        '
        Me.ServicioDigital.FormattingEnabled = True
        Me.ServicioDigital.Location = New System.Drawing.Point(350, 35)
        Me.ServicioDigital.Name = "ServicioDigital"
        Me.ServicioDigital.Size = New System.Drawing.Size(246, 21)
        Me.ServicioDigital.TabIndex = 1
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 72)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(584, 372)
        Me.DataGridView1.TabIndex = 2
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(72, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONGenerales_InternetBindingNavigatorSaveItem
        '
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONGenerales_InternetBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Name = "CONGenerales_InternetBindingNavigatorSaveItem"
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Size = New System.Drawing.Size(108, 22)
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'CONGenerales_InternetBindingNavigator
        '
        Me.CONGenerales_InternetBindingNavigator.AddNewItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.CountItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONGenerales_InternetBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONGenerales_InternetBindingNavigatorSaveItem})
        Me.CONGenerales_InternetBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONGenerales_InternetBindingNavigator.MoveFirstItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.MoveLastItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.MoveNextItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.MovePreviousItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.Name = "CONGenerales_InternetBindingNavigator"
        Me.CONGenerales_InternetBindingNavigator.PositionItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.Size = New System.Drawing.Size(608, 25)
        Me.CONGenerales_InternetBindingNavigator.TabIndex = 5
        Me.CONGenerales_InternetBindingNavigator.TabStop = True
        Me.CONGenerales_InternetBindingNavigator.Text = "BindingNavigator1"
        '
        'FrmCatalogoHomologacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(608, 461)
        Me.Controls.Add(Me.CONGenerales_InternetBindingNavigator)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.ServicioDigital)
        Me.Controls.Add(Me.ServicioAnalogico)
        Me.Name = "FrmCatalogoHomologacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catalogo Homologación"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONGenerales_InternetBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONGenerales_InternetBindingNavigator.ResumeLayout(False)
        Me.CONGenerales_InternetBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ServicioAnalogico As System.Windows.Forms.ComboBox
    Friend WithEvents ServicioDigital As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONGenerales_InternetBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONGenerales_InternetBindingNavigator As System.Windows.Forms.BindingNavigator
End Class
