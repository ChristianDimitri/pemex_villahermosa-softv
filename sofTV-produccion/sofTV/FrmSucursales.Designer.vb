﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSucursales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_SucursalLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim IPLabel As System.Windows.Forms.Label
        Dim ImpresoraLabel As System.Windows.Forms.Label
        Dim Clv_EquivalenteLabel As System.Windows.Forms.Label
        Dim SerieLabel As System.Windows.Forms.Label
        Dim UltimoFolioUsadoLabel As System.Windows.Forms.Label
        Dim Impresora_TarjetasLabel As System.Windows.Forms.Label
        Dim Impresora_ContratosLabel As System.Windows.Forms.Label
        Dim SerieLabel1 As System.Windows.Forms.Label
        Dim No_folioLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSucursales))
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtInterior = New System.Windows.Forms.TextBox()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.txtCP = New System.Windows.Forms.TextBox()
        Me.txtNumero = New System.Windows.Forms.TextBox()
        Me.txtCiudad = New System.Windows.Forms.TextBox()
        Me.txtMunicipio = New System.Windows.Forms.TextBox()
        Me.txtColonia = New System.Windows.Forms.TextBox()
        Me.txtCalle = New System.Windows.Forms.TextBox()
        Me.MatrizChck = New System.Windows.Forms.CheckBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ProcedimientosArnoldo2 = New sofTV.ProcedimientosArnoldo2()
        Me.SerieTextBox1 = New System.Windows.Forms.TextBox()
        Me.Consulta_Generales_FacturasGlobalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetarnoldo = New sofTV.DataSetarnoldo()
        Me.No_folioTextBox = New System.Windows.Forms.TextBox()
        Me.Impresora_TarjetasTextBox = New System.Windows.Forms.TextBox()
        Me.ConsultaImpresoraSucursalBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Impresora_ContratosTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CONSUCURSALESBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONSUCURSALESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.CONSUCURSALESBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_SucursalTextBox = New System.Windows.Forms.TextBox()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.IPTextBox = New System.Windows.Forms.TextBox()
        Me.ImpresoraTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_EquivalenteTextBox = New System.Windows.Forms.TextBox()
        Me.SerieTextBox = New System.Windows.Forms.TextBox()
        Me.UltimoFolioUsadoTextBox = New System.Windows.Forms.TextBox()
        Me.ConsultaImpresoraSucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_Impresora_SucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONSUCURSALESTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONSUCURSALESTableAdapter()
        Me.Consulta_Impresora_SucursalTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Consulta_Impresora_SucursalTableAdapter()
        Me.Inserta_impresora_sucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_impresora_sucursalTableAdapter = New sofTV.DataSetarnoldoTableAdapters.inserta_impresora_sucursalTableAdapter()
        Me.Borra_Impresora_SucursalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Impresora_SucursalesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Borra_Impresora_SucursalesTableAdapter()
        Me.Consulta_Generales_FacturasGlobalesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Consulta_Generales_FacturasGlobalesTableAdapter()
        Me.Inserta_Generales_FacturaGlobalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Generales_FacturaGlobalTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Inserta_Generales_FacturaGlobalTableAdapter()
        Me.Borra_Generales_FacturasGlobalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Generales_FacturasGlobalesTableAdapter = New sofTV.DataSetarnoldoTableAdapters.Borra_Generales_FacturasGlobalesTableAdapter()
        Me.Inserta_impresora_sucursalBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_impresora_sucursalTableAdapter1 = New sofTV.ProcedimientosArnoldo2TableAdapters.inserta_impresora_sucursalTableAdapter()
        Me.Consulta_Impresora_SucursalTableAdapter1 = New sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Impresora_SucursalTableAdapter()
        Me.Ciudadcmb = New System.Windows.Forms.ComboBox()
        Clv_SucursalLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        IPLabel = New System.Windows.Forms.Label()
        ImpresoraLabel = New System.Windows.Forms.Label()
        Clv_EquivalenteLabel = New System.Windows.Forms.Label()
        SerieLabel = New System.Windows.Forms.Label()
        UltimoFolioUsadoLabel = New System.Windows.Forms.Label()
        Impresora_TarjetasLabel = New System.Windows.Forms.Label()
        Impresora_ContratosLabel = New System.Windows.Forms.Label()
        SerieLabel1 = New System.Windows.Forms.Label()
        No_folioLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label13 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Generales_FacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaImpresoraSucursalBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSUCURSALESBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONSUCURSALESBindingNavigator.SuspendLayout()
        CType(Me.CONSUCURSALESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConsultaImpresoraSucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_impresora_sucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Impresora_SucursalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Generales_FacturaGlobalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Generales_FacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_impresora_sucursalBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_SucursalLabel
        '
        Clv_SucursalLabel.AutoSize = True
        Clv_SucursalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_SucursalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_SucursalLabel.Location = New System.Drawing.Point(153, 45)
        Clv_SucursalLabel.Name = "Clv_SucursalLabel"
        Clv_SucursalLabel.Size = New System.Drawing.Size(50, 15)
        Clv_SucursalLabel.TabIndex = 0
        Clv_SucursalLabel.Text = "Clave :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(112, 72)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(91, 15)
        NombreLabel.TabIndex = 2
        NombreLabel.Text = "Descripción :"
        '
        'IPLabel
        '
        IPLabel.AutoSize = True
        IPLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        IPLabel.ForeColor = System.Drawing.Color.LightSlateGray
        IPLabel.Location = New System.Drawing.Point(179, 99)
        IPLabel.Name = "IPLabel"
        IPLabel.Size = New System.Drawing.Size(24, 15)
        IPLabel.TabIndex = 4
        IPLabel.Text = "IP:"
        '
        'ImpresoraLabel
        '
        ImpresoraLabel.AutoSize = True
        ImpresoraLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImpresoraLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ImpresoraLabel.Location = New System.Drawing.Point(11, 126)
        ImpresoraLabel.Name = "ImpresoraLabel"
        ImpresoraLabel.Size = New System.Drawing.Size(192, 15)
        ImpresoraLabel.TabIndex = 6
        ImpresoraLabel.Text = "Impresora Facturas Fiscales:"
        '
        'Clv_EquivalenteLabel
        '
        Clv_EquivalenteLabel.AutoSize = True
        Clv_EquivalenteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_EquivalenteLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_EquivalenteLabel.Location = New System.Drawing.Point(78, 153)
        Clv_EquivalenteLabel.Name = "Clv_EquivalenteLabel"
        Clv_EquivalenteLabel.Size = New System.Drawing.Size(125, 15)
        Clv_EquivalenteLabel.TabIndex = 8
        Clv_EquivalenteLabel.Text = "Clave Equivalente:"
        '
        'SerieLabel
        '
        SerieLabel.AutoSize = True
        SerieLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SerieLabel.ForeColor = System.Drawing.Color.LightSlateGray
        SerieLabel.Location = New System.Drawing.Point(158, 180)
        SerieLabel.Name = "SerieLabel"
        SerieLabel.Size = New System.Drawing.Size(45, 15)
        SerieLabel.TabIndex = 10
        SerieLabel.Text = "Serie:"
        '
        'UltimoFolioUsadoLabel
        '
        UltimoFolioUsadoLabel.AutoSize = True
        UltimoFolioUsadoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        UltimoFolioUsadoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        UltimoFolioUsadoLabel.Location = New System.Drawing.Point(72, 207)
        UltimoFolioUsadoLabel.Name = "UltimoFolioUsadoLabel"
        UltimoFolioUsadoLabel.Size = New System.Drawing.Size(134, 15)
        UltimoFolioUsadoLabel.TabIndex = 12
        UltimoFolioUsadoLabel.Text = "Ultimo Folio Usado:"
        '
        'Impresora_TarjetasLabel
        '
        Impresora_TarjetasLabel.AutoSize = True
        Impresora_TarjetasLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Impresora_TarjetasLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Impresora_TarjetasLabel.Location = New System.Drawing.Point(71, 233)
        Impresora_TarjetasLabel.Name = "Impresora_TarjetasLabel"
        Impresora_TarjetasLabel.Size = New System.Drawing.Size(132, 15)
        Impresora_TarjetasLabel.TabIndex = 22
        Impresora_TarjetasLabel.Text = "Impresora Tarjetas:"
        '
        'Impresora_ContratosLabel
        '
        Impresora_ContratosLabel.AutoSize = True
        Impresora_ContratosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Impresora_ContratosLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Impresora_ContratosLabel.Location = New System.Drawing.Point(62, 257)
        Impresora_ContratosLabel.Name = "Impresora_ContratosLabel"
        Impresora_ContratosLabel.Size = New System.Drawing.Size(141, 15)
        Impresora_ContratosLabel.TabIndex = 24
        Impresora_ContratosLabel.Text = "Impresora Contratos:"
        '
        'SerieLabel1
        '
        SerieLabel1.AutoSize = True
        SerieLabel1.Cursor = System.Windows.Forms.Cursors.Default
        SerieLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        SerieLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        SerieLabel1.Location = New System.Drawing.Point(60, 313)
        SerieLabel1.Name = "SerieLabel1"
        SerieLabel1.Size = New System.Drawing.Size(143, 15)
        SerieLabel1.TabIndex = 25
        SerieLabel1.Text = "Serie Factura Global:"
        '
        'No_folioLabel
        '
        No_folioLabel.AutoSize = True
        No_folioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        No_folioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        No_folioLabel.Location = New System.Drawing.Point(40, 342)
        No_folioLabel.Name = "No_folioLabel"
        No_folioLabel.Size = New System.Drawing.Size(163, 15)
        No_folioLabel.TabIndex = 27
        No_folioLabel.Text = "No Folio Factura Global:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(76, 287)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(125, 15)
        Label2.TabIndex = 29
        Label2.Text = "Impresora Tickets:"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Cursor = System.Windows.Forms.Cursors.Default
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(13, 61)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(79, 15)
        Label7.TabIndex = 43
        Label7.Text = "No Interior:"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Cursor = System.Windows.Forms.Cursors.Default
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(337, 147)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(88, 15)
        Label14.TabIndex = 39
        Label14.Text = "Telefono(s) :"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Cursor = System.Windows.Forms.Cursors.Default
        Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Label13.Location = New System.Drawing.Point(371, 103)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(41, 15)
        Label13.TabIndex = 37
        Label13.Text = "C.P. :"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Cursor = System.Windows.Forms.Cursors.Default
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Label12.Location = New System.Drawing.Point(371, 17)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(29, 15)
        Label12.TabIndex = 35
        Label12.Text = "No:"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Cursor = System.Windows.Forms.Cursors.Default
        Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Label11.Location = New System.Drawing.Point(13, 146)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(55, 15)
        Label11.TabIndex = 33
        Label11.Text = "Estado:"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Cursor = System.Windows.Forms.Cursors.Default
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Label10.Location = New System.Drawing.Point(13, 104)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(56, 15)
        Label10.TabIndex = 31
        Label10.Text = "Ciudad:"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Cursor = System.Windows.Forms.Cursors.Default
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Label9.Location = New System.Drawing.Point(153, 62)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(60, 15)
        Label9.TabIndex = 29
        Label9.Text = "Colonia:"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Cursor = System.Windows.Forms.Cursors.Default
        Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Label8.Location = New System.Drawing.Point(13, 18)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(44, 15)
        Label8.TabIndex = 27
        Label8.Text = "Calle:"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(392, 704)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 11
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.MatrizChck)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Label2)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(SerieLabel1)
        Me.Panel1.Controls.Add(Me.SerieTextBox1)
        Me.Panel1.Controls.Add(No_folioLabel)
        Me.Panel1.Controls.Add(Me.No_folioTextBox)
        Me.Panel1.Controls.Add(Impresora_TarjetasLabel)
        Me.Panel1.Controls.Add(Me.Impresora_TarjetasTextBox)
        Me.Panel1.Controls.Add(Impresora_ContratosLabel)
        Me.Panel1.Controls.Add(Me.Impresora_ContratosTextBox)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.CONSUCURSALESBindingNavigator)
        Me.Panel1.Controls.Add(Clv_SucursalLabel)
        Me.Panel1.Controls.Add(Me.Clv_SucursalTextBox)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.NombreTextBox)
        Me.Panel1.Controls.Add(IPLabel)
        Me.Panel1.Controls.Add(Me.IPTextBox)
        Me.Panel1.Controls.Add(ImpresoraLabel)
        Me.Panel1.Controls.Add(Me.ImpresoraTextBox)
        Me.Panel1.Controls.Add(Clv_EquivalenteLabel)
        Me.Panel1.Controls.Add(Me.Clv_EquivalenteTextBox)
        Me.Panel1.Controls.Add(SerieLabel)
        Me.Panel1.Controls.Add(Me.SerieTextBox)
        Me.Panel1.Controls.Add(UltimoFolioUsadoLabel)
        Me.Panel1.Controls.Add(Me.UltimoFolioUsadoTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(541, 686)
        Me.Panel1.TabIndex = 19
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Label7)
        Me.GroupBox1.Controls.Add(Me.txtInterior)
        Me.GroupBox1.Controls.Add(Label14)
        Me.GroupBox1.Controls.Add(Me.txtTelefono)
        Me.GroupBox1.Controls.Add(Label13)
        Me.GroupBox1.Controls.Add(Me.txtCP)
        Me.GroupBox1.Controls.Add(Label12)
        Me.GroupBox1.Controls.Add(Me.txtNumero)
        Me.GroupBox1.Controls.Add(Label11)
        Me.GroupBox1.Controls.Add(Me.txtCiudad)
        Me.GroupBox1.Controls.Add(Label10)
        Me.GroupBox1.Controls.Add(Me.txtMunicipio)
        Me.GroupBox1.Controls.Add(Label9)
        Me.GroupBox1.Controls.Add(Me.txtColonia)
        Me.GroupBox1.Controls.Add(Label8)
        Me.GroupBox1.Controls.Add(Me.txtCalle)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.GroupBox1.Location = New System.Drawing.Point(14, 455)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(520, 210)
        Me.GroupBox1.TabIndex = 41
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Dirección Sucursal"
        '
        'txtInterior
        '
        Me.txtInterior.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtInterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtInterior.Location = New System.Drawing.Point(16, 80)
        Me.txtInterior.Name = "txtInterior"
        Me.txtInterior.Size = New System.Drawing.Size(123, 21)
        Me.txtInterior.TabIndex = 42
        '
        'txtTelefono
        '
        Me.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtTelefono.Location = New System.Drawing.Point(340, 165)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(157, 21)
        Me.txtTelefono.TabIndex = 5
        '
        'txtCP
        '
        Me.txtCP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtCP.Location = New System.Drawing.Point(374, 121)
        Me.txtCP.Name = "txtCP"
        Me.txtCP.Size = New System.Drawing.Size(123, 21)
        Me.txtCP.TabIndex = 3
        '
        'txtNumero
        '
        Me.txtNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtNumero.Location = New System.Drawing.Point(374, 36)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.Size = New System.Drawing.Size(123, 21)
        Me.txtNumero.TabIndex = 1
        '
        'txtCiudad
        '
        Me.txtCiudad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtCiudad.Location = New System.Drawing.Point(16, 164)
        Me.txtCiudad.Name = "txtCiudad"
        Me.txtCiudad.Size = New System.Drawing.Size(308, 21)
        Me.txtCiudad.TabIndex = 6
        '
        'txtMunicipio
        '
        Me.txtMunicipio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMunicipio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtMunicipio.Location = New System.Drawing.Point(16, 122)
        Me.txtMunicipio.Name = "txtMunicipio"
        Me.txtMunicipio.Size = New System.Drawing.Size(339, 21)
        Me.txtMunicipio.TabIndex = 4
        '
        'txtColonia
        '
        Me.txtColonia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtColonia.Location = New System.Drawing.Point(156, 80)
        Me.txtColonia.Name = "txtColonia"
        Me.txtColonia.Size = New System.Drawing.Size(339, 21)
        Me.txtColonia.TabIndex = 2
        '
        'txtCalle
        '
        Me.txtCalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtCalle.Location = New System.Drawing.Point(16, 36)
        Me.txtCalle.Name = "txtCalle"
        Me.txtCalle.Size = New System.Drawing.Size(339, 21)
        Me.txtCalle.TabIndex = 0
        '
        'MatrizChck
        '
        Me.MatrizChck.AutoSize = True
        Me.MatrizChck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MatrizChck.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MatrizChck.ForeColor = System.Drawing.Color.Blue
        Me.MatrizChck.Location = New System.Drawing.Point(208, 414)
        Me.MatrizChck.Name = "MatrizChck"
        Me.MatrizChck.Size = New System.Drawing.Size(99, 22)
        Me.MatrizChck.TabIndex = 38
        Me.MatrizChck.Text = "Es Matriz"
        Me.MatrizChck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MatrizChck.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label6.Location = New System.Drawing.Point(49, 367)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(151, 15)
        Me.Label6.TabIndex = 39
        Me.Label6.Text = "Serie y Folio de Mizar:"
        Me.Label6.Visible = False
        '
        'ComboBox1
        '
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(209, 364)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(298, 23)
        Me.ComboBox1.TabIndex = 37
        Me.ComboBox1.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProcedimientosArnoldo2, "Consulta_Impresora_Sucursal.Impresora_Tickets", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TextBox1.Location = New System.Drawing.Point(209, 281)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(294, 21)
        Me.TextBox1.TabIndex = 28
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SerieTextBox1
        '
        Me.SerieTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SerieTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Generales_FacturasGlobalesBindingSource, "serie", True))
        Me.SerieTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.SerieTextBox1.Location = New System.Drawing.Point(209, 311)
        Me.SerieTextBox1.Name = "SerieTextBox1"
        Me.SerieTextBox1.Size = New System.Drawing.Size(100, 21)
        Me.SerieTextBox1.TabIndex = 8
        '
        'Consulta_Generales_FacturasGlobalesBindingSource
        '
        Me.Consulta_Generales_FacturasGlobalesBindingSource.DataMember = "Consulta_Generales_FacturasGlobales"
        Me.Consulta_Generales_FacturasGlobalesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'DataSetarnoldo
        '
        Me.DataSetarnoldo.DataSetName = "DataSetarnoldo"
        Me.DataSetarnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'No_folioTextBox
        '
        Me.No_folioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.No_folioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_Generales_FacturasGlobalesBindingSource, "no_folio", True))
        Me.No_folioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.No_folioTextBox.Location = New System.Drawing.Point(209, 337)
        Me.No_folioTextBox.Name = "No_folioTextBox"
        Me.No_folioTextBox.Size = New System.Drawing.Size(100, 21)
        Me.No_folioTextBox.TabIndex = 9
        '
        'Impresora_TarjetasTextBox
        '
        Me.Impresora_TarjetasTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Impresora_TarjetasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultaImpresoraSucursalBindingSource1, "Impresora_Tarjetas", True))
        Me.Impresora_TarjetasTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Impresora_TarjetasTextBox.Location = New System.Drawing.Point(209, 232)
        Me.Impresora_TarjetasTextBox.Name = "Impresora_TarjetasTextBox"
        Me.Impresora_TarjetasTextBox.Size = New System.Drawing.Size(294, 21)
        Me.Impresora_TarjetasTextBox.TabIndex = 6
        '
        'ConsultaImpresoraSucursalBindingSource1
        '
        Me.ConsultaImpresoraSucursalBindingSource1.DataMember = "Consulta_Impresora_Sucursal"
        Me.ConsultaImpresoraSucursalBindingSource1.DataSource = Me.ProcedimientosArnoldo2
        '
        'Impresora_ContratosTextBox
        '
        Me.Impresora_ContratosTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Impresora_ContratosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ProcedimientosArnoldo2, "Consulta_Impresora_Sucursal.Impresora_Contratos", True))
        Me.Impresora_ContratosTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Impresora_ContratosTextBox.Location = New System.Drawing.Point(209, 256)
        Me.Impresora_ContratosTextBox.Name = "Impresora_ContratosTextBox"
        Me.Impresora_ContratosTextBox.Size = New System.Drawing.Size(294, 21)
        Me.Impresora_ContratosTextBox.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(315, 101)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(179, 13)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = " Nota : Ejemplo 192.168.(""  60 "") .25"
        '
        'CONSUCURSALESBindingNavigator
        '
        Me.CONSUCURSALESBindingNavigator.AddNewItem = Nothing
        Me.CONSUCURSALESBindingNavigator.BindingSource = Me.CONSUCURSALESBindingSource
        Me.CONSUCURSALESBindingNavigator.CountItem = Nothing
        Me.CONSUCURSALESBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONSUCURSALESBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONSUCURSALESBindingNavigatorSaveItem})
        Me.CONSUCURSALESBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONSUCURSALESBindingNavigator.MoveFirstItem = Nothing
        Me.CONSUCURSALESBindingNavigator.MoveLastItem = Nothing
        Me.CONSUCURSALESBindingNavigator.MoveNextItem = Nothing
        Me.CONSUCURSALESBindingNavigator.MovePreviousItem = Nothing
        Me.CONSUCURSALESBindingNavigator.Name = "CONSUCURSALESBindingNavigator"
        Me.CONSUCURSALESBindingNavigator.PositionItem = Nothing
        Me.CONSUCURSALESBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONSUCURSALESBindingNavigator.Size = New System.Drawing.Size(541, 25)
        Me.CONSUCURSALESBindingNavigator.TabIndex = 10
        Me.CONSUCURSALESBindingNavigator.TabStop = True
        Me.CONSUCURSALESBindingNavigator.Text = "BindingNavigator1"
        '
        'CONSUCURSALESBindingSource
        '
        Me.CONSUCURSALESBindingSource.DataMember = "CONSUCURSALES"
        Me.CONSUCURSALESBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONSUCURSALESBindingNavigatorSaveItem
        '
        Me.CONSUCURSALESBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONSUCURSALESBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONSUCURSALESBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONSUCURSALESBindingNavigatorSaveItem.Name = "CONSUCURSALESBindingNavigatorSaveItem"
        Me.CONSUCURSALESBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONSUCURSALESBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'Clv_SucursalTextBox
        '
        Me.Clv_SucursalTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_SucursalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Clv_Sucursal", True))
        Me.Clv_SucursalTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_SucursalTextBox.Location = New System.Drawing.Point(209, 43)
        Me.Clv_SucursalTextBox.Name = "Clv_SucursalTextBox"
        Me.Clv_SucursalTextBox.ReadOnly = True
        Me.Clv_SucursalTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_SucursalTextBox.TabIndex = 10
        Me.Clv_SucursalTextBox.TabStop = False
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(209, 70)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(294, 21)
        Me.NombreTextBox.TabIndex = 0
        '
        'IPTextBox
        '
        Me.IPTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.IPTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.IPTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "IP", True))
        Me.IPTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IPTextBox.Location = New System.Drawing.Point(209, 97)
        Me.IPTextBox.Name = "IPTextBox"
        Me.IPTextBox.Size = New System.Drawing.Size(100, 21)
        Me.IPTextBox.TabIndex = 1
        '
        'ImpresoraTextBox
        '
        Me.ImpresoraTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ImpresoraTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ImpresoraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Impresora", True))
        Me.ImpresoraTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImpresoraTextBox.Location = New System.Drawing.Point(209, 124)
        Me.ImpresoraTextBox.Name = "ImpresoraTextBox"
        Me.ImpresoraTextBox.Size = New System.Drawing.Size(294, 21)
        Me.ImpresoraTextBox.TabIndex = 2
        '
        'Clv_EquivalenteTextBox
        '
        Me.Clv_EquivalenteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_EquivalenteTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_EquivalenteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Clv_Equivalente", True))
        Me.Clv_EquivalenteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_EquivalenteTextBox.Location = New System.Drawing.Point(209, 151)
        Me.Clv_EquivalenteTextBox.Name = "Clv_EquivalenteTextBox"
        Me.Clv_EquivalenteTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_EquivalenteTextBox.TabIndex = 3
        '
        'SerieTextBox
        '
        Me.SerieTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SerieTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.SerieTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "Serie", True))
        Me.SerieTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieTextBox.Location = New System.Drawing.Point(209, 178)
        Me.SerieTextBox.Name = "SerieTextBox"
        Me.SerieTextBox.Size = New System.Drawing.Size(100, 21)
        Me.SerieTextBox.TabIndex = 4
        '
        'UltimoFolioUsadoTextBox
        '
        Me.UltimoFolioUsadoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.UltimoFolioUsadoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSUCURSALESBindingSource, "UltimoFolioUsado", True))
        Me.UltimoFolioUsadoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltimoFolioUsadoTextBox.Location = New System.Drawing.Point(209, 205)
        Me.UltimoFolioUsadoTextBox.Name = "UltimoFolioUsadoTextBox"
        Me.UltimoFolioUsadoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.UltimoFolioUsadoTextBox.TabIndex = 5
        '
        'ConsultaImpresoraSucursalBindingSource
        '
        Me.ConsultaImpresoraSucursalBindingSource.DataMember = "Consulta_Impresora_Sucursal"
        Me.ConsultaImpresoraSucursalBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'Consulta_Impresora_SucursalBindingSource
        '
        Me.Consulta_Impresora_SucursalBindingSource.DataMember = "Consulta_Impresora_Sucursal"
        Me.Consulta_Impresora_SucursalBindingSource.DataSource = Me.DataSetarnoldo
        '
        'CONSUCURSALESTableAdapter
        '
        Me.CONSUCURSALESTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Impresora_SucursalTableAdapter
        '
        Me.Consulta_Impresora_SucursalTableAdapter.ClearBeforeFill = True
        '
        'Inserta_impresora_sucursalBindingSource
        '
        Me.Inserta_impresora_sucursalBindingSource.DataMember = "inserta_impresora_sucursal"
        Me.Inserta_impresora_sucursalBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_impresora_sucursalTableAdapter
        '
        Me.Inserta_impresora_sucursalTableAdapter.ClearBeforeFill = True
        '
        'Borra_Impresora_SucursalesBindingSource
        '
        Me.Borra_Impresora_SucursalesBindingSource.DataMember = "Borra_Impresora_Sucursales"
        Me.Borra_Impresora_SucursalesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Borra_Impresora_SucursalesTableAdapter
        '
        Me.Borra_Impresora_SucursalesTableAdapter.ClearBeforeFill = True
        '
        'Consulta_Generales_FacturasGlobalesTableAdapter
        '
        Me.Consulta_Generales_FacturasGlobalesTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Generales_FacturaGlobalBindingSource
        '
        Me.Inserta_Generales_FacturaGlobalBindingSource.DataMember = "Inserta_Generales_FacturaGlobal"
        Me.Inserta_Generales_FacturaGlobalBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Inserta_Generales_FacturaGlobalTableAdapter
        '
        Me.Inserta_Generales_FacturaGlobalTableAdapter.ClearBeforeFill = True
        '
        'Borra_Generales_FacturasGlobalesBindingSource
        '
        Me.Borra_Generales_FacturasGlobalesBindingSource.DataMember = "Borra_Generales_FacturasGlobales"
        Me.Borra_Generales_FacturasGlobalesBindingSource.DataSource = Me.DataSetarnoldo
        '
        'Borra_Generales_FacturasGlobalesTableAdapter
        '
        Me.Borra_Generales_FacturasGlobalesTableAdapter.ClearBeforeFill = True
        '
        'Inserta_impresora_sucursalBindingSource1
        '
        Me.Inserta_impresora_sucursalBindingSource1.DataMember = "inserta_impresora_sucursal"
        Me.Inserta_impresora_sucursalBindingSource1.DataSource = Me.ProcedimientosArnoldo2
        '
        'Inserta_impresora_sucursalTableAdapter1
        '
        Me.Inserta_impresora_sucursalTableAdapter1.ClearBeforeFill = True
        '
        'Consulta_Impresora_SucursalTableAdapter1
        '
        Me.Consulta_Impresora_SucursalTableAdapter1.ClearBeforeFill = True
        '
        'Ciudadcmb
        '
        Me.Ciudadcmb.DisplayMember = "Clv_ciudad"
        Me.Ciudadcmb.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ciudadcmb.FormattingEnabled = True
        Me.Ciudadcmb.Location = New System.Drawing.Point(64, 714)
        Me.Ciudadcmb.Name = "Ciudadcmb"
        Me.Ciudadcmb.Size = New System.Drawing.Size(310, 23)
        Me.Ciudadcmb.TabIndex = 20
        Me.Ciudadcmb.Visible = False
        '
        'FrmSucursales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(565, 749)
        Me.Controls.Add(Me.Ciudadcmb)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Name = "FrmSucursales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Sucursales"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Generales_FacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetarnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaImpresoraSucursalBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSUCURSALESBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONSUCURSALESBindingNavigator.ResumeLayout(False)
        Me.CONSUCURSALESBindingNavigator.PerformLayout()
        CType(Me.CONSUCURSALESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConsultaImpresoraSucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_impresora_sucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Impresora_SucursalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Generales_FacturaGlobalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Generales_FacturasGlobalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_impresora_sucursalBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONSUCURSALESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSUCURSALESTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONSUCURSALESTableAdapter
    Friend WithEvents Clv_SucursalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IPTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImpresoraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_EquivalenteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SerieTextBox As System.Windows.Forms.TextBox
    Friend WithEvents UltimoFolioUsadoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONSUCURSALESBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONSUCURSALESBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataSetarnoldo As sofTV.DataSetarnoldo
    Friend WithEvents Consulta_Impresora_SucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Impresora_SucursalTableAdapter As sofTV.DataSetarnoldoTableAdapters.Consulta_Impresora_SucursalTableAdapter
    Friend WithEvents Impresora_TarjetasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Impresora_ContratosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Inserta_impresora_sucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_impresora_sucursalTableAdapter As sofTV.DataSetarnoldoTableAdapters.inserta_impresora_sucursalTableAdapter
    Friend WithEvents Borra_Impresora_SucursalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Impresora_SucursalesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Borra_Impresora_SucursalesTableAdapter
    Friend WithEvents Consulta_Generales_FacturasGlobalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Generales_FacturasGlobalesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Consulta_Generales_FacturasGlobalesTableAdapter
    Friend WithEvents SerieTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents No_folioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Inserta_Generales_FacturaGlobalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Generales_FacturaGlobalTableAdapter As sofTV.DataSetarnoldoTableAdapters.Inserta_Generales_FacturaGlobalTableAdapter
    Friend WithEvents Borra_Generales_FacturasGlobalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Generales_FacturasGlobalesTableAdapter As sofTV.DataSetarnoldoTableAdapters.Borra_Generales_FacturasGlobalesTableAdapter
    Friend WithEvents ProcedimientosArnoldo2 As sofTV.ProcedimientosArnoldo2
    Friend WithEvents Inserta_impresora_sucursalBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_impresora_sucursalTableAdapter1 As sofTV.ProcedimientosArnoldo2TableAdapters.inserta_impresora_sucursalTableAdapter
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ConsultaImpresoraSucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Impresora_SucursalTableAdapter1 As sofTV.ProcedimientosArnoldo2TableAdapters.Consulta_Impresora_SucursalTableAdapter
    Friend WithEvents ConsultaImpresoraSucursalBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtInterior As System.Windows.Forms.TextBox
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents txtCP As System.Windows.Forms.TextBox
    Friend WithEvents txtNumero As System.Windows.Forms.TextBox
    Friend WithEvents txtCiudad As System.Windows.Forms.TextBox
    Friend WithEvents txtMunicipio As System.Windows.Forms.TextBox
    Friend WithEvents txtColonia As System.Windows.Forms.TextBox
    Friend WithEvents txtCalle As System.Windows.Forms.TextBox
    Friend WithEvents MatrizChck As System.Windows.Forms.CheckBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Ciudadcmb As System.Windows.Forms.ComboBox
End Class
