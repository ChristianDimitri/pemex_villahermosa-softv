﻿Imports System.Data.SqlClient
Imports System.Text
Public Class FrmNumerosTelefonicos
    Dim numeroDeTelefon As Integer = 0

    Private Sub FrmNumerosTelefonicos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        MuestraTelefonosCliente(Clv_UnicaTel)
    End Sub
    Private Sub FrmNumerosTelefonicos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        MuestraTelefonosCliente(Clv_UnicaTel)
    End Sub
    Private Sub MuestraTelefonosCliente(ByVal contratoNet As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTelefonosCliente ")
        strSQL.Append(CStr(contratoNet))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable

            Me.DataGridView1.Refresh()
            Me.DataGridView1.DataSource = bindingSource

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        numeroDeTelefon = checaLineasCliente()

        If numeroDeTelefon = 1 Then
            optTel = 5
            FrmSeleccionaTel.Show()
        Else
            MsgBox("No Se Puede Agregar Otro Numero Telefonico", MsgBoxStyle.Information)
        End If

    End Sub
    Private Function checaLineasCliente() As Integer
        Dim ConCheck As New SqlConnection(MiConexion)
        Dim Cmd As New SqlCommand
        ConCheck.Open()
        With Cmd
            .CommandText = "checaLineasCliente" ' (@clv_servicio bigint,@lineas int output)"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = ConCheck
            Dim pmt As New SqlParameter("@clv_unicanet", SqlDbType.Int)
            Dim pmt3 As New SqlParameter("@error", SqlDbType.Int)

            pmt.Direction = ParameterDirection.Input
            pmt3.Direction = ParameterDirection.Output

            pmt.Value = Clv_UnicaTel
            .Parameters.Add(pmt)
            .Parameters.Add(pmt3)

            .ExecuteNonQuery()
            checaLineasCliente = pmt3.Value

        End With
        ConCheck.Close()
    End Function
End Class