﻿Imports System.Data.SqlClient
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Public Class FrmAparatosDisponibles
    Dim instance As DataGridViewCheckBoxColumn
    Dim value As Boolean
    Private customersByCityReport As ReportDocument
    Dim Impresora_Tarjetas As String = Nothing
    Dim Impresora_Contratos As String = Nothing
    Dim clv_cablemodems As String = Nothing
    Dim tipo_aparato As New Collection
    Dim lista As New Collection
    Dim LocGlo_clv_Cablemodem As Long
    Dim LocGlo_serie As String = Nothing
    Dim LocGlo_tipo As String = Nothing
    Dim LocGlo_status As String = Nothing
    Dim LocGlo_clv_articulo As String = Nothing
    Private Sub Form3_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
       
    End Sub

    Private Sub FrmAparatosDisponibles_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("DELETE FROM tbl_tmpSeries", con)

        Dim com2 As New SqlCommand("DELETE FROM tbl_tmpSeriesRecepcion", con)

        con.Open()
        com.ExecuteNonQuery()
        con.Close()

        con.Open()
        com2.ExecuteNonQuery()
        con.Close()
    End Sub

    Private Sub Form3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Panel3.Visible = False
        muestraTipoAparato()
        MuestraAparatosRecepcion(CInt(Me.TipoAparatoCmbx.SelectedValue))
        'muestraStatusAparatos()
    End Sub

    Private Sub MuestraAparatosRecepcion(ByVal clave_tipo As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraAparatosRecepcion ")
        strSQL.Append(CStr(clave_tipo))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable

            Me.DataGridView1.Refresh()
            Me.DataGridView1.DataSource = bindingSource

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    'Private Sub muestraStatusAparatos()

    '    Dim column As DataGridViewComboBoxColumn = DirectCast(DataGridView1.Columns(0), DataGridViewComboBoxColumn)

    '    Dim conexion As New SqlConnection(MiConexion)
    '    Dim strSQL As New StringBuilder
    '    strSQL.Append("EXEC muestraStatusAparatos ")

    '    Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
    '    Dim dataTable As New DataTable
    '    Dim bindingSource As New BindingSource

    '    Try
    '        conexion.Open()
    '        dataAdapter.Fill(dataTable)
    '        bindingSource.DataSource = dataTable

    '        With column

    '            .DisplayMember = "descripcion"
    '            .ValueMember = "status"
    '            .DataSource = bindingSource

    '        End With

    '        'DataGridView1.Columns.Add(column)
    '        'DataGridView1.Rows.Add("Status", column)
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    '    Finally
    '        conexion.Close()
    '        conexion.Dispose()
    '    End Try

    'End Sub




 
    Private Sub DataGridView1_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

        If DataGridView1.IsCurrentCellDirty Then
            DataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If


        value = Me.DataGridView1.Rows(e.RowIndex).Cells("Seleccion").Value
        LocGlo_clv_Cablemodem = Me.DataGridView1.Rows(e.RowIndex).Cells("Clave").Value
        If value = True Then

            If Me.DataGridView1.Rows(e.RowIndex).Cells("Almacen").Value.ToString = "Almacen" Then
                LocGlo_serie = Me.DataGridView1.Rows(e.RowIndex).Cells("Serie").Value.ToString
                LocGlo_status = Me.DataGridView1.Rows(e.RowIndex).Cells("Status").Value.ToString

                clv_cablemodems = dameTipoAparato(Me.DataGridView1.Rows(e.RowIndex).Cells("Clave").Value)
                Panel3.Visible = True
                Panel1.Enabled = False
                Panel2.Enabled = False
                Me.DataGridView1.Enabled = False
                Me.txtClasificacionMat.Text = clv_cablemodems
                muestradescripcionArticulo(clv_cablemodems)
                LocGlo_clv_articulo = Me.cmbMarca.SelectedValue
            End If
        End If
        If value = False Then
            borrarTmpSerie(LocGlo_clv_Cablemodem)
        End If



    End Sub
    Private Sub muestraTipoAparato()


        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC muestraTipoAparato ")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable



            Me.TipoAparatoCmbx.DisplayMember = "Concepto"
            Me.TipoAparatoCmbx.ValueMember = "Clave"
            Me.TipoAparatoCmbx.DataSource = bindingSource



        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub TipoAparatoCmbx_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoAparatoCmbx.SelectedIndexChanged
        If IsNumeric(Me.TipoAparatoCmbx.SelectedValue) = True Then
            MuestraAparatosRecepcion(CInt(Me.TipoAparatoCmbx.SelectedValue))
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress

        Dim i As Integer
        Dim bnd As Integer
        i = 0
        bnd = 0
        If Asc(e.KeyChar) = 13 Then

            For i = 0 To Me.DataGridView1.Rows.Count - 1

                If Me.DataGridView1.Rows(i).Cells("Serie").Value.ToString.ToLower = Me.TextBox1.Text.ToLower Then
                    Me.DataGridView1.Rows(i).Cells("Seleccion").Value = True
                    bnd = 1
                    Exit For
                Else
                    bnd = 0
                End If
            Next

            If bnd = 0 Then
                MsgBox("El Aparato No Existe En El Catalogo")
                Exit Sub
            End If


        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim i As Integer = 0
        Dim clv_cablemodem As Long = 0
        Dim serie As String = ""
        Dim tipo As String = ""
        Dim status As String = ""
        Dim cont As Integer = 0
        Dim aux As Integer = 0
        Dim bandera As Integer = 0

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("DELETE FROM TMPRelAparatoRecepcionAlmacen", con)

        lista = New Collection
        lista.Clear()
        tipo_aparato = New Collection
        tipo_aparato.Clear()

        con.Open()
        com.ExecuteNonQuery()
        con.Close()

        For i = 0 To DataGridView1.Rows.Count - 1

            If Me.DataGridView1.Item("Seleccion", i).Value = False Then

                cont = cont + 1

            End If

        Next

        If cont = (DataGridView1.Rows.Count) Then
            MsgBox("No Se A Seleccionado Ningun Equipo", MsgBoxStyle.Information)
            Exit Sub
        End If

        For i = 0 To DataGridView1.Rows.Count - 1

            If Me.DataGridView1.Item("Seleccion", i).Value = True Then
                status = CStr(Me.DataGridView1.Item("Status", i).Value)
                If Len(status) = 0 Then
                    'If Len(Me.DataGridView1.Item(3, i).Value.ToString) = 0 Then

                    MsgBox("Debe Seleccionar El Status Del Aparato", MsgBoxStyle.Information)

                    Exit Sub
                End If
            End If
        Next

        For i = 0 To DataGridView1.Rows.Count - 1
            aux = i + 1
            If aux <= DataGridView1.Rows.Count - 1 Then
                If Me.DataGridView1.Item("Seleccion", i).Value = True Then
                    If Len(Me.DataGridView1.Item(3, i).Value.ToString) > 0 Then

                        lista.Add(CStr(Me.DataGridView1.Item("Almacen", i).Value))
                        tipo = CStr(Me.DataGridView1.Item("Almacen", i).Value)
                        If Me.DataGridView1.Item("Seleccion", aux).Value = True Then
                            If tipo <> CStr(Me.DataGridView1.Item("Almacen", aux).Value) Then
                                MsgBox("La Recepcion Debe Ser Del Mismo Almacen", MsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If

                    End If
                End If
            End If
        Next

        For i = 1 To lista.Count
            If lista.Item(1) <> lista.Item(i) Then
                MsgBox("La Recepcion Debe Ser Del Mismo Almacen", MsgBoxStyle.Information)
                Exit Sub
            End If
        Next



        For i = 0 To DataGridView1.Rows.Count - 1
            If Me.DataGridView1.Item("Seleccion", i).Value = True Then
                If Len(Me.DataGridView1.Item(3, i).Value.ToString) > 0 Then

                    clv_cablemodem = CLng(Me.DataGridView1.Item("Clave", i).Value)
                    serie = CStr(Me.DataGridView1.Item("Serie", i).Value)
                    tipo = CStr(Me.DataGridView1.Item("Almacen", i).Value)
                    status = CStr(Me.DataGridView1.Item("Status", i).Value)

                    'tipo_aparato.Add(dameTipoAparato(clv_cablemodem))

                    InsertarAparatosRecepcion(clv_cablemodem, serie, tipo, status)
                End If
            End If
        Next

        'For i = 1 To tipo_aparato.Count
        '    If tipo_aparato.Item(1) <> tipo_aparato.Item(i) Then
        '        MsgBox("Los aparatos deben ser del mismo tipo", MsgBoxStyle.Information)
        '        Exit Sub
        '    End If
        'Next

        'If lista.Item(1) = "Almacen" Then
        '    Panel3.Visible = True
        '    Panel1.Enabled = False
        '    Panel2.Enabled = False
        '    Me.DataGridView1.Enabled = False
        '    Me.txtClasificacionMat.Text = tipo_aparato.Item(1)
        '    muestradescripcionArticulo(tipo_aparato.Item(1))
        '    bandera = 1
        'End If

        If bandera = 0 Then
            GloOpcionRecepcion = 0
            GloFolioRecepcion = 0
            'eOpVentas = 120
            'FrmImprimirComision.Show()
            'ConfigureCrystalReportsPortabilidad(0, 0)

            RecepcionAparaosSoftv()
            MsgBox("La Recepcion Se Completo De Manera Exitosa")
            Me.Dispose()
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim i As Integer
        Dim bnd As Integer
        i = 0
        bnd = 0
        For i = 0 To Me.DataGridView1.Rows.Count - 1

            If Me.DataGridView1.Rows(i).Cells("Serie").Value.ToString.ToLower = Me.TextBox1.Text.ToLower Then
                Me.DataGridView1.Rows(i).Cells("Seleccion").Value = True

                If Me.DataGridView1.Rows(i).Cells("Almacen").Value.ToString = "Almacen" Then
                    LocGlo_serie = Me.DataGridView1.Rows(i).Cells("Serie").Value.ToString
                    LocGlo_status = Me.DataGridView1.Rows(i).Cells("Status").Value.ToString

                    clv_cablemodems = dameTipoAparato(Me.DataGridView1.Rows(i).Cells("Clave").Value)
                    Panel3.Visible = True
                    Panel1.Enabled = False
                    Panel2.Enabled = False
                    Me.DataGridView1.Enabled = False
                    Me.txtClasificacionMat.Text = clv_cablemodems
                    muestradescripcionArticulo(clv_cablemodems)
                    LocGlo_clv_articulo = Me.cmbMarca.SelectedValue
                End If

                bnd = 1
                Exit For
            Else
                bnd = 0
            End If
        Next

        If bnd = 0 Then
            MsgBox("El Aparato No Existe En El Catalogo")
            Exit Sub
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("DELETE FROM tbl_tmpSeries", con)

        Dim com2 As New SqlCommand("DELETE FROM tbl_tmpSeriesRecepcion", con)

        con.Open()
        com.ExecuteNonQuery()
        con.Close()

        con.Open()
        com2.ExecuteNonQuery()
        con.Close()

        Me.Dispose()
        Me.Close()
    End Sub
    Private Sub InsertarAparatosRecepcion(ByVal clv_cablemode As Long, ByVal serie As String, ByVal tipo As String, ByVal status As String)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("InsertarAparatosRecepcion", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_cablemode", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = clv_cablemode
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@serie", SqlDbType.VarChar, 30)
        par2.Direction = ParameterDirection.Input
        par2.Value = serie
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@tipo", SqlDbType.VarChar, 100)
        par3.Direction = ParameterDirection.Input
        par3.Value = tipo
        com.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@status_aparato", SqlDbType.VarChar, 1)
        par4.Direction = ParameterDirection.Input
        par4.Value = status
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()

        End Try
    End Sub
    Private Sub RecepcionAparaosSoftv()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("RecepcionAparaosSoftv", con)

        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@usuario", SqlDbType.VarChar, 250)
        par1.Direction = ParameterDirection.Input
        par1.Value = GloUsuario
        com.Parameters.Add(par1)


        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsPortabilidad(ByVal opcion As Integer, ByVal folio As Long)
        Dim impresora As String = Nothing
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\RptRecepcionSoftv.rpt"
        'reportPath = "D:\Exes\Softv\reportes\repotrtePortabilidad.rpt"
        customersByCityReport.Load(reportPath)


        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@Contrato 
        customersByCityReport.SetParameterValue(0, opcion)
        customersByCityReport.SetParameterValue(0, folio)
        'CrystalReportViewer1.ReportSource = customersByCityReport
        'CrystalReportViewer1.ShowPrintButton = True

        'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)


        customersByCityReport.PrintOptions.PrinterName = Impresora_Contratos
        customersByCityReport.PrintToPrinter(0, True, 1, 1)


        'customersByCityReport = Nothing


    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub
    Private Sub Selecciona_Impresora_Sucursal(ByVal sucursal As Integer)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Selecciona_Impresora_Sucursal", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_sucursal", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = sucursal
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Impresora_Tarjetas", SqlDbType.VarChar, 50)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Impresora_Contratos", SqlDbType.VarChar, 50)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)

        Try
            con.Open()
            com.ExecuteNonQuery()
            Impresora_Tarjetas = CStr(par2.Value.ToString)
            Impresora_Contratos = CStr(par3.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        cancelar()
    End Sub
    Function dameTipoAparato(ByVal clv_cablemodem As Long) As String


        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("dameTipoAparato", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_cablemodem", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = clv_cablemodem
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@tipo", SqlDbType.VarChar, 50)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)

        Try
            con.Open()
            com.ExecuteNonQuery()
            Return CStr(par2.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try

    End Function
    Private Sub muestradescripcionArticulo(ByVal tipoAparato As String)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC muestradescripcionArticulo ")
        strSQL.Append("'" & tipoAparato & "'")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable



            Me.cmbClaveArt.DisplayMember = "clv_articulo"
            Me.cmbClaveArt.ValueMember = "NoArticulo"
            Me.cmbClaveArt.DataSource = bindingSource

            Me.cmbMarca.DisplayMember = "Descripcion"
            Me.cmbMarca.ValueMember = "NoArticulo"
            Me.cmbMarca.DataSource = bindingSource

     

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        llenaTemporalesSerie(LocGlo_serie, clv_cablemodems, LocGlo_status, LocGlo_clv_articulo, LocGlo_clv_Cablemodem)
        Panel3.Visible = False
        Panel1.Enabled = True
        Panel2.Enabled = True
        Me.DataGridView1.Enabled = True
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        cancelar()

    End Sub
    Private Sub cancelar()
        Panel3.Visible = False
        Panel1.Enabled = True
        Panel2.Enabled = True
        Me.DataGridView1.Enabled = True
        Dim i As Integer = 0

        For i = 0 To Me.DataGridView1.Rows.Count - 1

            If Me.DataGridView1.Rows(i).Cells("Clave").Value = LocGlo_clv_Cablemodem Then
                Me.DataGridView1.Rows(i).Cells("Seleccion").Value = False
            End If
        Next
    End Sub
    Private Sub llenaTemporalesSerie(ByVal SERIE As String, ByVal TIPO As String, ByVal STATUS As String, ByVal CLV_ARTICULO As Long, ByVal clv_cablemodem As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("llenaTemporalesSerie", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@serie", SqlDbType.VarChar, 50)
        par1.Direction = ParameterDirection.Input
        par1.Value = SERIE
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@tipo", SqlDbType.VarChar, 50)
        par2.Direction = ParameterDirection.Input
        par2.Value = TIPO
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@status", SqlDbType.VarChar, 20)
        par3.Direction = ParameterDirection.Input
        par3.Value = STATUS
        com.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@clv_articulo", SqlDbType.BigInt)
        par4.Direction = ParameterDirection.Input
        par4.Value = CLV_ARTICULO
        com.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@clv_cablemodem", SqlDbType.BigInt)
        par5.Direction = ParameterDirection.Input
        par5.Value = clv_cablemodem
        com.Parameters.Add(par5)



        Try
            con.Open()
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
    Private Sub borrarTmpSerie(ByVal cablemodem As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("DELETE FROM tbl_tmpSeries where clv_cablemodem =" & cablemodem, con)

        Dim com2 As New SqlCommand("DELETE FROM tbl_tmpSeriesRecepcion where clv_cablemodem =" & cablemodem, con)

        con.Open()
        com.ExecuteNonQuery()
        con.Close()

        con.Open()
        com2.ExecuteNonQuery()
        con.Close()
    End Sub

  
 
    Private Sub cmbMarca_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMarca.SelectedIndexChanged
        LocGlo_clv_articulo = Me.cmbMarca.SelectedValue
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub
End Class