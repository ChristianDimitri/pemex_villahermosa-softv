Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.Net.NetworkInformation
Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Public Class FrmCorreo
    Dim Con, Ins, Sus, Des, Baj, Tem, Fue, PorPagar, Per1, Per2 As Integer
    Dim eCuenta As String = Nothing
    Dim ePass As String = Nothing
    Dim eHost As String = Nothing
    Dim ePort As Integer = 0


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Try
            Dim CONE As New SqlConnection(MiConexion)
            Dim CON1 As New SqlConnection(MiConexion)
            Dim i As Integer
            CON1.Open()
            Me.ConGeneralCorreoTableAdapter.Connection = CON1
            Me.ConGeneralCorreoTableAdapter.Fill(Me.DataSetEric.ConGeneralCorreo)
            CON1.Close()


            eCuenta = Me.CuentaTextBox.Text
            ePass = Me.PasswordTextBox.Text
            eHost = Me.HostTextBox.Text
            If IsNumeric(Me.PortTextBox.Text) = True Then
                ePort = CType(Me.PortTextBox.Text, Integer)
            Else
                ePort = 0
            End If

            If eCuenta.Length = 0 Or ePass.Length = 0 Or eHost.Length = 0 Or ePort.ToString.Length = 0 Then
                MsgBox("No se Puede realizar esta Operaci�n debido a que carece de los datos Necearios (Correo,Password,Host,Port)para enviar el Correo. Consulta Generales del Sistema.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            If Me.CheckBox1.Checked = False And Me.CheckBox3.Checked = False And Me.CheckBox6.Checked = False And Me.CheckBox4.Checked = False And Me.CheckBox5.Checked = False And Me.CheckBox2.Checked = False And Me.CheckBox7.Checked = False Then
                MsgBox("Selecciona al menos un Status.", MsgBoxStyle.Information)
                Exit Sub
            End If

            If Me.TextBox2.Text.Length <= 0 Then
                MsgBox("Captura el Asunto.", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Me.TextBox1.Text.Length <= 0 Then
                MsgBox("Captura el Mensaje.", MsgBoxStyle.Information)
                Exit Sub
            End If

            Me.lblStatus.Text = "Enviando... "
            Me.lblStatus.Refresh()

            Me.GroupBox1.Enabled = False
            Me.GroupBox2.Enabled = False
            Me.GroupBox3.Enabled = False
            Me.Button2.Enabled = False

            If Me.CheckBox1.Checked = True Then
                Con = 1
            Else
                Con = 0
            End If

            If Me.CheckBox3.Checked = True Then
                Ins = 1
            Else
                Ins = 0
            End If

            If Me.CheckBox4.Checked = True Then
                Des = 1
            Else
                Des = 0
            End If

            If Me.CheckBox5.Checked = True Then
                Sus = 1
            Else
                Sus = 0
            End If

            If Me.CheckBox2.Checked = True Then
                Baj = 1
            Else
                Baj = 0
            End If

            If Me.CheckBox7.Checked = True Then
                Tem = 1
            Else
                Tem = 0
            End If

            If Me.CheckBox6.Checked = True Then
                Fue = 1
            Else
                Fue = 0
            End If

            'If Me.CheckBox10.Checked = True Then
            '    PorPagar = 1
            'Else
            '    PorPagar = 0
            'End If

            If Me.CheckBox8.Checked = True Then
                Per1 = 1
            Else
                Per1 = 0
            End If

            If Me.CheckBox9.Checked = True Then
                Per2 = 1
            Else
                Per2 = 0
            End If


            Dim eEmail As String = Nothing
            Dim eContrato As Long = 0
            Dim eTotal As Integer = 0
            Dim ContT As Integer = 0
            Dim ContS As Integer = 0
            Dim ContN As Integer = 0


            CONE.Open()
            Dim comando As SqlClient.SqlCommand
            Dim reader As SqlDataReader
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CONE
                .CommandText = "EXEC MandarCorreoClientes " & CType(LocClv_session, String) & "," & CType(Con, String) & "," & CType(Ins, String) & "," & CType(Sus, String) & "," & CType(Des, String) & "," & CType(Baj, String) & "," & CType(Fue, String) & "," & CType(Tem, String) & "," & CType(Per1, String) & "," & CType(Per2, String)
                .CommandType = CommandType.Text
                .CommandTimeout = 0
            End With 'FIN WITH
            reader = comando.ExecuteReader(CommandBehavior.CloseConnection)
            'Using reader

            While reader.Read
                ContT = ContT + 1
                eContrato = reader.Item(1)
                eEmail = reader.GetValue(2)
                eTotal = reader.Item(3)

                Me.lblStatus.Text = "Enviando... " & CStr(ContT) & " de " & CStr(eTotal) & " correos."
                Me.lblStatus.Refresh()



                Try
                    Dim miEmail As New System.Net.Mail.MailMessage
                    Dim file As Attachment

                    With miEmail
                        .From = New System.Net.Mail.MailAddress(eCuenta)
                        .To.Add(eEmail)
                        .Subject = Me.TextBox2.Text
                        .Body = Me.TextBox1.Text
                        .IsBodyHtml = True
                        For i = 0 To Me.ListBox1.Items.Count - 1
                            file = New Attachment(Me.ListBox1.Items.Item(i).ToString)
                            .Attachments.Add(file)
                        Next
                    End With

                    Dim miSMTP As New System.Net.Mail.SmtpClient
                    miSMTP.UseDefaultCredentials = False
                    miSMTP.Credentials = New System.Net.NetworkCredential(eCuenta, ePass)
                    miSMTP.Port = ePort
                    miSMTP.Host = eHost
                    miSMTP.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network
                    miSMTP.EnableSsl = True
                    miSMTP.Send(miEmail)


                    CON1.Open()
                    Me.InsPreDetBitacoraCorreoTableAdapter.Connection = CON1
                    Me.InsPreDetBitacoraCorreoTableAdapter.Fill(Me.DataSetEric2.InsPreDetBitacoraCorreo, LocClv_session, eContrato, eEmail, 0, "Se envi� correctamente.")
                    CON1.Close()
                    ContS = ContS + 1

                Catch eX As Exception

                    CON1.Open()
                    Me.InsPreDetBitacoraCorreoTableAdapter.Connection = CON1
                    Me.InsPreDetBitacoraCorreoTableAdapter.Fill(Me.DataSetEric2.InsPreDetBitacoraCorreo, LocClv_session, eContrato, eEmail, 1, eX.Message)
                    CON1.Close()
                    ContN = ContN + 1

                End Try 'FIN DEL TRY QUE MANDA CORREOS



            End While 'FIN DEL WHILE QUE TRAE LA CONSULTA
            ' End Using 'FIN DE USING


            Me.lblStatus.Text = "Correos enviados con �xito: " & CStr(ContS) & ". Correos enviados fallidos: " & CStr(ContN)
            CON1.Open()
            Me.GrabaBitacoraCorreoTableAdapter.Connection = CON1
            Me.GrabaBitacoraCorreoTableAdapter.Fill(Me.DataSetEric2.GrabaBitacoraCorreo, LocClv_session, eClv_Usuario, "Asunto: " & Me.TextBox2.Text & ", Mensaje: " & Me.TextBox1.Text, eClaveCorreo)
            CON1.Close()
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Mandaron Correos", "Asunto:" + Me.TextBox2.Text, "Mensaje: " + Me.TextBox1.Text, LocClv_Ciudad)
            CONE.Close()
            Me.Button1.Enabled = True
        Catch ex As System.Exception
            Me.Button1.Enabled = False
            System.Windows.Forms.MessageBox.Show(ex.Message + " " + ex.ToString)
        End Try 'FIN DEL PRIMER TRY


    End Sub

    Private Sub FrmCorreo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CONE As New SqlConnection(MiConexion)
        CONE.Open()
        Me.ConGeneralCorreoTableAdapter.Connection = CONE
        Me.ConGeneralCorreoTableAdapter.Fill(Me.DataSetEric.ConGeneralCorreo)
        CONE.Close()
    End Sub

    
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpVentas = 57
        eOpCorreo = 1
        FrmImprimirComision.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        Dim openFileDialog As New OpenFileDialog

        openFileDialog.InitialDirectory = "c:\"

        If openFileDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                Me.ListBox1.Items.Add(openFileDialog.FileName)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub bnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnEliminar.Click
        If ListBox1.Items.Count = 0 Then
            MsgBox("Selecciona un archivo adjunto.", MsgBoxStyle.Information)
            Exit Sub
        End If

        ListBox1.Items.Remove(ListBox1.SelectedValue)

    End Sub

    Private Sub bnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAgregar.Click
        panelURL.Visible = True
        bnAgregar.Enabled = False
        tbURL.Clear()
    End Sub

    Private Sub bnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAceptar.Click

        Dim alineacion As String

        panelURL.Visible = False
        bnAgregar.Enabled = True

        If tbURL.Text.Length = 0 Then
            Exit Sub
        End If

        If rbLink.Checked = True Then
            TextBox1.Text = TextBox1.Text + " <a href=""" + tbURL.Text + """>" + tbURL.Text + "</a> "
        Else

            If rbIzquierda.Checked = True Then alineacion = "style=text-aling:left;"
            If rbCentrado.Checked = True Then alineacion = "style=text-aling:center;"
            If rbDerecha.Checked = True Then alineacion = "style=text-aling:right;"

            TextBox1.Text = TextBox1.Text + " <p " + alineacion + " > <img src=""" + tbURL.Text + """ ></p> "
        End If

    End Sub

    Private Sub bnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnCancelar.Click
        panelURL.Visible = False
        bnAgregar.Enabled = True
    End Sub

    Private Sub rbLink_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbLink.CheckedChanged
        If rbLink.Checked = True Then
            panelJus.Enabled = False
        End If
    End Sub

    Private Sub rbImagen_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbImagen.CheckedChanged
        If rbImagen.Checked = True Then
            panelJus.Enabled = True
        End If
    End Sub
End Class