﻿Imports System.Data.SqlClient
Public Class FrmTiposServicios
    Dim concepto As String = Nothing
    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                concepto = Me.ConceptoTextBox1.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora()
        Try
            ' bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.TRABAJOTextBox.Name + ": " + Me.TRABAJOTextBox.Text, clave, Me.TRABAJOTextBox.Text, LocClv_Ciudad)
            If opcion = "M" Then
                'concepto = Me.ConceptoTextBox1.Text
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.ConceptoTextBox1.Name + ": " + Me.Clv_TipSerTextBox.Text, concepto, Me.ConceptoTextBox1.Text, LocClv_Ciudad)
            ElseIf opcion = "N" Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Guardo Un Nuevo Tipo De Servicio", "", "Se Guardo Un Nuevo Tipo De Servicio: " + Me.ConceptoTextBox1.Text, LocClv_Ciudad)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub CONTipServBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub BUSCA(ByVal CLV_TIPSER As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONTipServTableAdapter.Connection = CON
            Me.CONTipServTableAdapter.Fill(Me.NewSofTvDataSet.CONTipServ, New System.Nullable(Of Integer)(CType(CLV_TIPSER, Integer)))
            damedatosbitacora()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmTiposServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            Me.CONTipServBindingSource.AddNew()
            Panel1.Enabled = True
            'CON_IEPS()
            DimeSiAplicaIEPS(1, CInt(Me.Clv_TipSerTextBox.Text), 0, 0)
            Me.IEPSCheckBox.Visible = VISIBLE_IEPS
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(GloClv_TipoServicio)
            ConsultaIepsServicios()
            DimeSiAplicaIEPS(1, CInt(Me.Clv_TipSerTextBox.Text), 0, 0)
            Me.IEPSCheckBox.Visible = VISIBLE_IEPS
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(GloClv_TipoServicio)
            ConsultaIepsServicios()
            'CON_IEPS()
            DimeSiAplicaIEPS(1, CInt(Me.Clv_TipSerTextBox.Text), 0, 0)
            Me.IEPSCheckBox.Visible = VISIBLE_IEPS
        End If
    End Sub

    Private Sub Clv_TipSerTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_TipSerTextBox.TextChanged
        GloClv_TipoServicio = Me.Clv_TipSerTextBox.Text
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Me.Close()
    End Sub

    Private Sub ConceptoTextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ConceptoTextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(ConceptoTextBox1, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub ConceptoTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConceptoTextBox1.TextChanged

    End Sub

    Private Sub CONTipServBindingNavigatorSaveItem_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTipServBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONTipServBindingSource.EndEdit()
        Me.CONTipServTableAdapter.Connection = CON
        Me.CONTipServTableAdapter.Update(Me.NewSofTvDataSet.CONTipServ)
        GuardaSerImpuestos()
        MsgBox("Se ha Guardado con Exíto")
        guardabitacora()
        GloBnd = True
        CON.Close()
        Me.Close()
    End Sub


    Private Sub BindingNavigatorDeleteItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Me.CONTipServBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub GuardaSerImpuestos()
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("GuardaSerImpuestos", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = CInt(Me.Clv_TipSerTextBox.Text)
        command.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Aplica_Ieps", SqlDbType.Bit)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Me.IEPSCheckBox.CheckState
        command.Parameters.Add(parametro2)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ConsultaIepsServicios()
        Dim conexion As New SqlConnection(MiConexion)
        Dim command As New SqlCommand("ConSerImpuestos", conexion)
        command.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = GloClv_TipoServicio
        command.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@APLICA_IEPS", SqlDbType.Bit)
        parametro2.Direction = ParameterDirection.Output
        command.Parameters.Add(parametro2)

        Try
            conexion.Open()
            command.ExecuteNonQuery()
            Me.IEPSCheckBox.Checked = parametro2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
End Class