Imports System.Data.SqlClient

Public Class FrmRangos
    Private RangoInf As String = Nothing
    Private RangoSup As String = Nothing

    Private Sub DameDatosBitacora()
        Try
            If eOpcion = "M" Then
                RangoInf = Me.RangoInferiorTextBox.Text
                RangoSup = Me.RangoSuperiorTextBox.Text
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub GuardaDatosBitacora(ByVal Op As Integer)

        Try
            If Op = 0 Then
                If eOpcion = "M" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Rango Inferior", RangoInf, Me.RangoInferiorTextBox.Text, LocClv_Ciudad)
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Rango Superior", RangoSup, Me.RangoSuperiorTextBox.Text, LocClv_Ciudad)
                ElseIf eOpcion = "N" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Nuevo Rango", "", "Rango: " + Me.RangoInferiorTextBox.Text + " - " + Me.RangoSuperiorTextBox.Text, LocClv_Ciudad)
                End If
            ElseIf Op = 1 Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Text, "Se Elimin� Rango", "Rango: " + Me.RangoInferiorTextBox.Text + " - " + Me.RangoSuperiorTextBox.Text, "", LocClv_Ciudad)
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub ConCatalogoDeRangosBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConCatalogoDeRangosBindingNavigatorSaveItem.Click
        'VALIDO SI NO PASAN PAR�METROS
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        If Me.RangoInferiorTextBox.Text = "" Then
            Me.RangoInferiorTextBox.Text = 0
        End If
        If Me.RangoSuperiorTextBox.Text = "" Then
            Me.RangoSuperiorTextBox.Text = 0
        End If

        eRInferior = Me.RangoInferiorTextBox.Text
        eRSuperior = Me.RangoSuperiorTextBox.Text

        'VALIDO QUE EL RANGO SUPERIOR SIEMPRE SEA MAYOR AL INFERIOR
        If eRSuperior > eRInferior Then

            If eOpcion = "N" Then
                Me.ConCatalogoDeRangosTableAdapter.Connection = CON
                Me.ConCatalogoDeRangosTableAdapter.Insert(Me.RangoInferiorTextBox.Text, Me.RangoSuperiorTextBox.Text)
                MsgBox(mensaje5)
                GuardaDatosBitacora(0)
                Me.Close()
            End If

            If eOpcion = "M" Then

                'NO SE PUEDEN MODIFICAR RANGOS PREVIAMENTE ASIGNADOS A UNA COMISION
                Me.ValidaRangosAEliminarTableAdapter.Connection = CON
                Me.ValidaRangosAEliminarTableAdapter.Fill(Me.DataSetEDGAR.ValidaRangosAEliminar, eCveRango, eValidaRango)

                If eValidaRango = 0 Then


                    Try
                        Me.Validate()
                        Me.ConCatalogoDeRangosBindingSource.EndEdit()
                        Me.ConCatalogoDeRangosTableAdapter.Connection = CON
                        Me.ConCatalogoDeRangosTableAdapter.Update(eCveRango, Me.RangoInferiorTextBox.Text, Me.RangoSuperiorTextBox.Text)
                        MsgBox(mensaje5)
                        GuardaDatosBitacora(0)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                    Me.Close()

                Else
                    MsgBox("El Rango no se puede Modificar debido a que est� siendo Usado en una Comisi�n. Inserta un Nuevo Rango.", , "Error")
                    Me.Close()
                End If


            End If
        Else
            MsgBox("El Rango Superior No puede ser Menor o Igual al Rango Inferior.", , "Atenci�n")
        End If
        CON.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ValidaRangosAEliminarTableAdapter.Connection = CON
            Me.ValidaRangosAEliminarTableAdapter.Fill(Me.DataSetEDGAR.ValidaRangosAEliminar, eCveRango, eValidaRango)
            If eValidaRango = 0 Then
                GuardaDatosBitacora(1)
                Me.ConCatalogoDeRangosTableAdapter.Connection = CON
                Me.ConCatalogoDeRangosTableAdapter.Delete(eCveRango)
                MsgBox(mensaje6)
            Else
                MsgBox("El Rango no se puede Eliminar debido a que est� siendo Usado en una Comisi�n.", , "Error")
            End If
            CON.Close()
            Me.Close()


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub FrmRangos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        If eOpcion = "N" Then
            Me.ConCatalogoDeRangosBindingSource.AddNew()
            Me.BindingNavigatorDeleteItem.Enabled = False
        End If

        If eOpcion = "M" Then
            Me.BindingNavigatorDeleteItem.Enabled = True
            Me.CveRangoTextBox.Text = eCveRango
            Me.RangoInferiorTextBox.Text = eRangoInferior
            Me.RangoSuperiorTextBox.Text = eRangoSuperior
        End If
        DameDatosBitacora()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub RangoInferiorTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RangoInferiorTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.RangoInferiorTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub RangoSuperiorTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RangoSuperiorTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.RangoSuperiorTextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub RangoInferiorTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RangoInferiorTextBox.TextChanged

    End Sub
End Class