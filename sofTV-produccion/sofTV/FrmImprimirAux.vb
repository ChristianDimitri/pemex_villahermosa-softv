﻿
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System
Imports System.IO.StreamReader
Imports System.IO.File
Public Class FrmImprimirAux

    Private customersByCityReport As ReportDocument

    Private Sub ReportsPortabilidad(ByVal FolioS As Long)
        Dim impresora As String = Nothing


        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\repotrtePortabilidad.rpt"

        customersByCityReport.Load(reportPath)


        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@FolioS 
        customersByCityReport.SetParameterValue(0, FolioS)
        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.ShowPrintButton = True

        'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
        'customersByCityReport.PrintOptions.PrinterName = Impresora_Contratos
        'customersByCityReport.PrintToPrinter(0, True, 1, 1)
        'customersByCityReport = Nothing


    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub FrmImprimirAux_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ReportsPortabilidad(folioport)
    End Sub
End Class