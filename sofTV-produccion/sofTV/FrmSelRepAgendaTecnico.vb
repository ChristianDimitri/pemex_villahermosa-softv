Imports System.Data.SqlClient
Public Class FrmSelRepAgendaTecnico

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmSelRepAgendaTecnico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Me.Muestra_Tecnicos_AlmacenTableAdapter.Connection = CON
        Me.Muestra_Tecnicos_AlmacenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Tecnicos_Almacen, 0)
        Me.ComboBox1.Text = ""
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
       
        LocFecha1Agnd = Me.DateTimePicker1.Text
        LocFecha2Agnd = Me.DateTimePicker2.Text
        If Me.ComboBox1.Text = "" Then
            LocClv_tecnico = 0
            LocopAgnd = 0
        ElseIf Me.ComboBox1.Text <> "" Then
            LocClv_tecnico = Me.ComboBox1.SelectedValue
            LocopAgnd = 1
        End If
        Locbndagnd = True
        FrmImprimirContrato.Show()
        Me.Close()
    End Sub
End Class