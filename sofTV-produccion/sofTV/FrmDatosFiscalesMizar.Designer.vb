﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDatosFiscalesMizar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RAZON_SOCIALLabel = New System.Windows.Forms.Label()
        Me.RFCLabel = New System.Windows.Forms.Label()
        Me.CALLE_RSLabel = New System.Windows.Forms.Label()
        Me.NUMERO_RSLabel = New System.Windows.Forms.Label()
        Me.ENTRECALLESLabel = New System.Windows.Forms.Label()
        Me.COLONIA_RSLabel = New System.Windows.Forms.Label()
        Me.CIUDAD_RSLabel = New System.Windows.Forms.Label()
        Me.ESTADO_RSLabel = New System.Windows.Forms.Label()
        Me.CP_RSLabel = New System.Windows.Forms.Label()
        Me.TELEFONO_RSLabel = New System.Windows.Forms.Label()
        Me.FAX_RSLabel = New System.Windows.Forms.Label()
        Me.CURPLabel = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.IVADESGLOSADOTextBox = New System.Windows.Forms.TextBox()
        Me.RFCTextBox = New System.Windows.Forms.TextBox()
        Me.CALLE_RSTextBox = New System.Windows.Forms.TextBox()
        Me.NUMERO_RSTextBox = New System.Windows.Forms.TextBox()
        Me.ENTRECALLESTextBox = New System.Windows.Forms.TextBox()
        Me.COLONIA_RSTextBox = New System.Windows.Forms.TextBox()
        Me.CIUDAD_RSTextBox = New System.Windows.Forms.TextBox()
        Me.ESTADO_RSTextBox = New System.Windows.Forms.TextBox()
        Me.CP_RSTextBox = New System.Windows.Forms.TextBox()
        Me.TELEFONO_RSTextBox = New System.Windows.Forms.TextBox()
        Me.FAX_RSTextBox = New System.Windows.Forms.TextBox()
        Me.CURPTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbUsoCFDI = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbPais = New System.Windows.Forms.ComboBox()
        Me.cbCiudad = New System.Windows.Forms.ComboBox()
        Me.cbEstado = New System.Windows.Forms.ComboBox()
        Me.cbColonia = New System.Windows.Forms.ComboBox()
        Me.cbCodigoPostal = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.NUMERO_intTextBox = New System.Windows.Forms.TextBox()
        Me.RAZON_SOCIALTextBox = New System.Windows.Forms.TextBox()
        Me.CmbCompania = New System.Windows.Forms.ComboBox()
        Me.EMail_TextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LblPais = New System.Windows.Forms.Label()
        Me.TxtPais = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'RAZON_SOCIALLabel
        '
        Me.RAZON_SOCIALLabel.AutoSize = True
        Me.RAZON_SOCIALLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RAZON_SOCIALLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.RAZON_SOCIALLabel.Location = New System.Drawing.Point(20, 15)
        Me.RAZON_SOCIALLabel.Name = "RAZON_SOCIALLabel"
        Me.RAZON_SOCIALLabel.Size = New System.Drawing.Size(96, 15)
        Me.RAZON_SOCIALLabel.TabIndex = 6
        Me.RAZON_SOCIALLabel.Text = "Razon Social:"
        '
        'RFCLabel
        '
        Me.RFCLabel.AutoSize = True
        Me.RFCLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RFCLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.RFCLabel.Location = New System.Drawing.Point(78, 66)
        Me.RFCLabel.Name = "RFCLabel"
        Me.RFCLabel.Size = New System.Drawing.Size(38, 15)
        Me.RFCLabel.TabIndex = 8
        Me.RFCLabel.Text = "RFC:"
        '
        'CALLE_RSLabel
        '
        Me.CALLE_RSLabel.AutoSize = True
        Me.CALLE_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLE_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CALLE_RSLabel.Location = New System.Drawing.Point(68, 94)
        Me.CALLE_RSLabel.Name = "CALLE_RSLabel"
        Me.CALLE_RSLabel.Size = New System.Drawing.Size(48, 15)
        Me.CALLE_RSLabel.TabIndex = 10
        Me.CALLE_RSLabel.Text = "Calle :"
        '
        'NUMERO_RSLabel
        '
        Me.NUMERO_RSLabel.AutoSize = True
        Me.NUMERO_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMERO_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.NUMERO_RSLabel.Location = New System.Drawing.Point(25, 145)
        Me.NUMERO_RSLabel.Name = "NUMERO_RSLabel"
        Me.NUMERO_RSLabel.Size = New System.Drawing.Size(94, 15)
        Me.NUMERO_RSLabel.TabIndex = 12
        Me.NUMERO_RSLabel.Text = "Numero Ext. :"
        '
        'ENTRECALLESLabel
        '
        Me.ENTRECALLESLabel.AutoSize = True
        Me.ENTRECALLESLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ENTRECALLESLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.ENTRECALLESLabel.Location = New System.Drawing.Point(25, 198)
        Me.ENTRECALLESLabel.Name = "ENTRECALLESLabel"
        Me.ENTRECALLESLabel.Size = New System.Drawing.Size(91, 15)
        Me.ENTRECALLESLabel.TabIndex = 14
        Me.ENTRECALLESLabel.Text = "Entre calles :"
        '
        'COLONIA_RSLabel
        '
        Me.COLONIA_RSLabel.AutoSize = True
        Me.COLONIA_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIA_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.COLONIA_RSLabel.Location = New System.Drawing.Point(52, 251)
        Me.COLONIA_RSLabel.Name = "COLONIA_RSLabel"
        Me.COLONIA_RSLabel.Size = New System.Drawing.Size(64, 15)
        Me.COLONIA_RSLabel.TabIndex = 16
        Me.COLONIA_RSLabel.Text = "Colonia :"
        '
        'CIUDAD_RSLabel
        '
        Me.CIUDAD_RSLabel.AutoSize = True
        Me.CIUDAD_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDAD_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CIUDAD_RSLabel.Location = New System.Drawing.Point(56, 275)
        Me.CIUDAD_RSLabel.Name = "CIUDAD_RSLabel"
        Me.CIUDAD_RSLabel.Size = New System.Drawing.Size(60, 15)
        Me.CIUDAD_RSLabel.TabIndex = 18
        Me.CIUDAD_RSLabel.Text = "Ciudad :"
        '
        'ESTADO_RSLabel
        '
        Me.ESTADO_RSLabel.AutoSize = True
        Me.ESTADO_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ESTADO_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.ESTADO_RSLabel.Location = New System.Drawing.Point(57, 301)
        Me.ESTADO_RSLabel.Name = "ESTADO_RSLabel"
        Me.ESTADO_RSLabel.Size = New System.Drawing.Size(59, 15)
        Me.ESTADO_RSLabel.TabIndex = 20
        Me.ESTADO_RSLabel.Text = "Estado :"
        '
        'CP_RSLabel
        '
        Me.CP_RSLabel.AutoSize = True
        Me.CP_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CP_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CP_RSLabel.Location = New System.Drawing.Point(12, 225)
        Me.CP_RSLabel.Name = "CP_RSLabel"
        Me.CP_RSLabel.Size = New System.Drawing.Size(104, 15)
        Me.CP_RSLabel.TabIndex = 22
        Me.CP_RSLabel.Text = "Codigo Postal :"
        '
        'TELEFONO_RSLabel
        '
        Me.TELEFONO_RSLabel.AutoSize = True
        Me.TELEFONO_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TELEFONO_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.TELEFONO_RSLabel.Location = New System.Drawing.Point(45, 357)
        Me.TELEFONO_RSLabel.Name = "TELEFONO_RSLabel"
        Me.TELEFONO_RSLabel.Size = New System.Drawing.Size(71, 15)
        Me.TELEFONO_RSLabel.TabIndex = 24
        Me.TELEFONO_RSLabel.Text = "Teléfono :"
        '
        'FAX_RSLabel
        '
        Me.FAX_RSLabel.AutoSize = True
        Me.FAX_RSLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FAX_RSLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.FAX_RSLabel.Location = New System.Drawing.Point(78, 383)
        Me.FAX_RSLabel.Name = "FAX_RSLabel"
        Me.FAX_RSLabel.Size = New System.Drawing.Size(38, 15)
        Me.FAX_RSLabel.TabIndex = 26
        Me.FAX_RSLabel.Text = "Fax :"
        '
        'CURPLabel
        '
        Me.CURPLabel.AutoSize = True
        Me.CURPLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CURPLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CURPLabel.Location = New System.Drawing.Point(202, 480)
        Me.CURPLabel.Name = "CURPLabel"
        Me.CURPLabel.Size = New System.Drawing.Size(49, 15)
        Me.CURPLabel.TabIndex = 32
        Me.CURPLabel.Text = "CURP:"
        Me.CURPLabel.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(273, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 15)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "Compañia:"
        Me.Label2.Visible = False
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.Location = New System.Drawing.Point(249, 35)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ContratoTextBox.TabIndex = 300
        Me.ContratoTextBox.TabStop = False
        '
        'IVADESGLOSADOTextBox
        '
        Me.IVADESGLOSADOTextBox.Location = New System.Drawing.Point(175, 19)
        Me.IVADESGLOSADOTextBox.Name = "IVADESGLOSADOTextBox"
        Me.IVADESGLOSADOTextBox.Size = New System.Drawing.Size(100, 20)
        Me.IVADESGLOSADOTextBox.TabIndex = 50
        Me.IVADESGLOSADOTextBox.TabStop = False
        Me.IVADESGLOSADOTextBox.Text = "1"
        '
        'RFCTextBox
        '
        Me.RFCTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RFCTextBox.CausesValidation = False
        Me.RFCTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RFCTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RFCTextBox.Location = New System.Drawing.Point(122, 65)
        Me.RFCTextBox.MaxLength = 15
        Me.RFCTextBox.Name = "RFCTextBox"
        Me.RFCTextBox.Size = New System.Drawing.Size(128, 21)
        Me.RFCTextBox.TabIndex = 1
        '
        'CALLE_RSTextBox
        '
        Me.CALLE_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CALLE_RSTextBox.CausesValidation = False
        Me.CALLE_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLE_RSTextBox.Location = New System.Drawing.Point(122, 92)
        Me.CALLE_RSTextBox.MaxLength = 150
        Me.CALLE_RSTextBox.Multiline = True
        Me.CALLE_RSTextBox.Name = "CALLE_RSTextBox"
        Me.CALLE_RSTextBox.Size = New System.Drawing.Size(516, 45)
        Me.CALLE_RSTextBox.TabIndex = 2
        '
        'NUMERO_RSTextBox
        '
        Me.NUMERO_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NUMERO_RSTextBox.CausesValidation = False
        Me.NUMERO_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMERO_RSTextBox.Location = New System.Drawing.Point(122, 143)
        Me.NUMERO_RSTextBox.MaxLength = 50
        Me.NUMERO_RSTextBox.Name = "NUMERO_RSTextBox"
        Me.NUMERO_RSTextBox.Size = New System.Drawing.Size(304, 21)
        Me.NUMERO_RSTextBox.TabIndex = 3
        '
        'ENTRECALLESTextBox
        '
        Me.ENTRECALLESTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ENTRECALLESTextBox.CausesValidation = False
        Me.ENTRECALLESTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ENTRECALLESTextBox.Location = New System.Drawing.Point(122, 197)
        Me.ENTRECALLESTextBox.MaxLength = 150
        Me.ENTRECALLESTextBox.Name = "ENTRECALLESTextBox"
        Me.ENTRECALLESTextBox.Size = New System.Drawing.Size(516, 21)
        Me.ENTRECALLESTextBox.TabIndex = 5
        '
        'COLONIA_RSTextBox
        '
        Me.COLONIA_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.COLONIA_RSTextBox.CausesValidation = False
        Me.COLONIA_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIA_RSTextBox.Location = New System.Drawing.Point(466, 355)
        Me.COLONIA_RSTextBox.MaxLength = 150
        Me.COLONIA_RSTextBox.Name = "COLONIA_RSTextBox"
        Me.COLONIA_RSTextBox.Size = New System.Drawing.Size(392, 21)
        Me.COLONIA_RSTextBox.TabIndex = 6
        Me.COLONIA_RSTextBox.Visible = False
        '
        'CIUDAD_RSTextBox
        '
        Me.CIUDAD_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CIUDAD_RSTextBox.CausesValidation = False
        Me.CIUDAD_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDAD_RSTextBox.Location = New System.Drawing.Point(466, 430)
        Me.CIUDAD_RSTextBox.MaxLength = 150
        Me.CIUDAD_RSTextBox.Name = "CIUDAD_RSTextBox"
        Me.CIUDAD_RSTextBox.Size = New System.Drawing.Size(292, 21)
        Me.CIUDAD_RSTextBox.TabIndex = 9
        Me.CIUDAD_RSTextBox.Visible = False
        '
        'ESTADO_RSTextBox
        '
        Me.ESTADO_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ESTADO_RSTextBox.CausesValidation = False
        Me.ESTADO_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ESTADO_RSTextBox.Location = New System.Drawing.Point(466, 379)
        Me.ESTADO_RSTextBox.MaxLength = 150
        Me.ESTADO_RSTextBox.Name = "ESTADO_RSTextBox"
        Me.ESTADO_RSTextBox.Size = New System.Drawing.Size(292, 21)
        Me.ESTADO_RSTextBox.TabIndex = 8
        Me.ESTADO_RSTextBox.Visible = False
        '
        'CP_RSTextBox
        '
        Me.CP_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CP_RSTextBox.CausesValidation = False
        Me.CP_RSTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CP_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CP_RSTextBox.Location = New System.Drawing.Point(466, 324)
        Me.CP_RSTextBox.MaxLength = 20
        Me.CP_RSTextBox.Name = "CP_RSTextBox"
        Me.CP_RSTextBox.Size = New System.Drawing.Size(100, 21)
        Me.CP_RSTextBox.TabIndex = 7
        Me.CP_RSTextBox.Visible = False
        '
        'TELEFONO_RSTextBox
        '
        Me.TELEFONO_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TELEFONO_RSTextBox.CausesValidation = False
        Me.TELEFONO_RSTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TELEFONO_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TELEFONO_RSTextBox.Location = New System.Drawing.Point(122, 356)
        Me.TELEFONO_RSTextBox.MaxLength = 30
        Me.TELEFONO_RSTextBox.Name = "TELEFONO_RSTextBox"
        Me.TELEFONO_RSTextBox.Size = New System.Drawing.Size(128, 21)
        Me.TELEFONO_RSTextBox.TabIndex = 11
        '
        'FAX_RSTextBox
        '
        Me.FAX_RSTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FAX_RSTextBox.CausesValidation = False
        Me.FAX_RSTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.FAX_RSTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FAX_RSTextBox.Location = New System.Drawing.Point(122, 383)
        Me.FAX_RSTextBox.MaxLength = 30
        Me.FAX_RSTextBox.Name = "FAX_RSTextBox"
        Me.FAX_RSTextBox.Size = New System.Drawing.Size(128, 21)
        Me.FAX_RSTextBox.TabIndex = 12
        '
        'CURPTextBox
        '
        Me.CURPTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CURPTextBox.CausesValidation = False
        Me.CURPTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CURPTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CURPTextBox.Location = New System.Drawing.Point(318, 474)
        Me.CURPTextBox.MaxLength = 30
        Me.CURPTextBox.Name = "CURPTextBox"
        Me.CURPTextBox.Size = New System.Drawing.Size(241, 21)
        Me.CURPTextBox.TabIndex = 3
        Me.CURPTextBox.Text = "0"
        Me.CURPTextBox.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(574, 508)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cbUsoCFDI)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.cbPais)
        Me.Panel1.Controls.Add(Me.cbCiudad)
        Me.Panel1.Controls.Add(Me.cbEstado)
        Me.Panel1.Controls.Add(Me.cbColonia)
        Me.Panel1.Controls.Add(Me.cbCodigoPostal)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.NUMERO_intTextBox)
        Me.Panel1.Controls.Add(Me.RAZON_SOCIALTextBox)
        Me.Panel1.Controls.Add(Me.CmbCompania)
        Me.Panel1.Controls.Add(Me.EMail_TextBox)
        Me.Panel1.Controls.Add(Me.ContratoTextBox)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.IVADESGLOSADOTextBox)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.LblPais)
        Me.Panel1.Controls.Add(Me.TxtPais)
        Me.Panel1.Controls.Add(Me.RAZON_SOCIALLabel)
        Me.Panel1.Controls.Add(Me.RFCLabel)
        Me.Panel1.Controls.Add(Me.CALLE_RSLabel)
        Me.Panel1.Controls.Add(Me.NUMERO_RSLabel)
        Me.Panel1.Controls.Add(Me.ENTRECALLESLabel)
        Me.Panel1.Controls.Add(Me.FAX_RSTextBox)
        Me.Panel1.Controls.Add(Me.COLONIA_RSLabel)
        Me.Panel1.Controls.Add(Me.CIUDAD_RSLabel)
        Me.Panel1.Controls.Add(Me.ESTADO_RSLabel)
        Me.Panel1.Controls.Add(Me.CP_RSLabel)
        Me.Panel1.Controls.Add(Me.TELEFONO_RSLabel)
        Me.Panel1.Controls.Add(Me.FAX_RSLabel)
        Me.Panel1.Controls.Add(Me.RFCTextBox)
        Me.Panel1.Controls.Add(Me.CALLE_RSTextBox)
        Me.Panel1.Controls.Add(Me.NUMERO_RSTextBox)
        Me.Panel1.Controls.Add(Me.ENTRECALLESTextBox)
        Me.Panel1.Controls.Add(Me.COLONIA_RSTextBox)
        Me.Panel1.Controls.Add(Me.CIUDAD_RSTextBox)
        Me.Panel1.Controls.Add(Me.ESTADO_RSTextBox)
        Me.Panel1.Controls.Add(Me.CP_RSTextBox)
        Me.Panel1.Controls.Add(Me.TELEFONO_RSTextBox)
        Me.Panel1.Location = New System.Drawing.Point(29, 17)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(664, 485)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'cbUsoCFDI
        '
        Me.cbUsoCFDI.DisplayMember = "Descripcion"
        Me.cbUsoCFDI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.cbUsoCFDI.FormattingEnabled = True
        Me.cbUsoCFDI.Location = New System.Drawing.Point(122, 437)
        Me.cbUsoCFDI.Name = "cbUsoCFDI"
        Me.cbUsoCFDI.Size = New System.Drawing.Size(304, 23)
        Me.cbUsoCFDI.TabIndex = 309
        Me.cbUsoCFDI.ValueMember = "id_UsoCFDI"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(45, 438)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 15)
        Me.Label4.TabIndex = 308
        Me.Label4.Text = "Uso CFDI:"
        '
        'cbPais
        '
        Me.cbPais.DisplayMember = "Descripcion"
        Me.cbPais.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.cbPais.FormattingEnabled = True
        Me.cbPais.Location = New System.Drawing.Point(122, 328)
        Me.cbPais.Name = "cbPais"
        Me.cbPais.Size = New System.Drawing.Size(304, 23)
        Me.cbPais.TabIndex = 307
        Me.cbPais.ValueMember = "id_Pais"
        '
        'cbCiudad
        '
        Me.cbCiudad.DisplayMember = "Descripcion"
        Me.cbCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.cbCiudad.FormattingEnabled = True
        Me.cbCiudad.Location = New System.Drawing.Point(122, 275)
        Me.cbCiudad.Name = "cbCiudad"
        Me.cbCiudad.Size = New System.Drawing.Size(304, 23)
        Me.cbCiudad.TabIndex = 306
        Me.cbCiudad.ValueMember = "id_Municipio"
        '
        'cbEstado
        '
        Me.cbEstado.DisplayMember = "Descripcion"
        Me.cbEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.cbEstado.FormattingEnabled = True
        Me.cbEstado.Location = New System.Drawing.Point(122, 300)
        Me.cbEstado.Name = "cbEstado"
        Me.cbEstado.Size = New System.Drawing.Size(304, 23)
        Me.cbEstado.TabIndex = 305
        Me.cbEstado.ValueMember = "id_Estado"
        '
        'cbColonia
        '
        Me.cbColonia.DisplayMember = "Descripcion"
        Me.cbColonia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.cbColonia.FormattingEnabled = True
        Me.cbColonia.Location = New System.Drawing.Point(122, 248)
        Me.cbColonia.Name = "cbColonia"
        Me.cbColonia.Size = New System.Drawing.Size(304, 23)
        Me.cbColonia.TabIndex = 304
        Me.cbColonia.ValueMember = "Descripcion"
        '
        'cbCodigoPostal
        '
        Me.cbCodigoPostal.DisplayMember = "id_CodigoPostal"
        Me.cbCodigoPostal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.cbCodigoPostal.FormattingEnabled = True
        Me.cbCodigoPostal.Location = New System.Drawing.Point(122, 222)
        Me.cbCodigoPostal.Name = "cbCodigoPostal"
        Me.cbCodigoPostal.Size = New System.Drawing.Size(128, 23)
        Me.cbCodigoPostal.TabIndex = 303
        Me.cbCodigoPostal.ValueMember = "id_CodigoPostal"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(26, 172)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 15)
        Me.Label3.TabIndex = 302
        Me.Label3.Text = "Numero Int. :"
        '
        'NUMERO_intTextBox
        '
        Me.NUMERO_intTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NUMERO_intTextBox.CausesValidation = False
        Me.NUMERO_intTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMERO_intTextBox.Location = New System.Drawing.Point(122, 170)
        Me.NUMERO_intTextBox.MaxLength = 50
        Me.NUMERO_intTextBox.Name = "NUMERO_intTextBox"
        Me.NUMERO_intTextBox.Size = New System.Drawing.Size(304, 21)
        Me.NUMERO_intTextBox.TabIndex = 4
        '
        'RAZON_SOCIALTextBox
        '
        Me.RAZON_SOCIALTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RAZON_SOCIALTextBox.CausesValidation = False
        Me.RAZON_SOCIALTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RAZON_SOCIALTextBox.Location = New System.Drawing.Point(122, 14)
        Me.RAZON_SOCIALTextBox.MaxLength = 150
        Me.RAZON_SOCIALTextBox.Multiline = True
        Me.RAZON_SOCIALTextBox.Name = "RAZON_SOCIALTextBox"
        Me.RAZON_SOCIALTextBox.Size = New System.Drawing.Size(516, 45)
        Me.RAZON_SOCIALTextBox.TabIndex = 0
        '
        'CmbCompania
        '
        Me.CmbCompania.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmbCompania.FormattingEnabled = True
        Me.CmbCompania.Location = New System.Drawing.Point(200, 15)
        Me.CmbCompania.Name = "CmbCompania"
        Me.CmbCompania.Size = New System.Drawing.Size(61, 24)
        Me.CmbCompania.TabIndex = 0
        Me.CmbCompania.Visible = False
        '
        'EMail_TextBox
        '
        Me.EMail_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EMail_TextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.EMail_TextBox.Location = New System.Drawing.Point(122, 410)
        Me.EMail_TextBox.MaxLength = 100
        Me.EMail_TextBox.Name = "EMail_TextBox"
        Me.EMail_TextBox.Size = New System.Drawing.Size(292, 21)
        Me.EMail_TextBox.TabIndex = 13
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(64, 410)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 15)
        Me.Label1.TabIndex = 41
        Me.Label1.Text = "EMail :"
        '
        'LblPais
        '
        Me.LblPais.AutoSize = True
        Me.LblPais.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.LblPais.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LblPais.Location = New System.Drawing.Point(74, 329)
        Me.LblPais.Name = "LblPais"
        Me.LblPais.Size = New System.Drawing.Size(39, 15)
        Me.LblPais.TabIndex = 34
        Me.LblPais.Text = "Pais:"
        '
        'TxtPais
        '
        Me.TxtPais.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TxtPais.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TxtPais.Location = New System.Drawing.Point(466, 403)
        Me.TxtPais.MaxLength = 150
        Me.TxtPais.Name = "TxtPais"
        Me.TxtPais.Size = New System.Drawing.Size(292, 21)
        Me.TxtPais.TabIndex = 10
        Me.TxtPais.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(12, 508)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 33)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "&GUARDAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(154, 508)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 33)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "&BORRAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'FrmDatosFiscalesMizar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(722, 553)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.CURPTextBox)
        Me.Controls.Add(Me.CURPLabel)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmDatosFiscalesMizar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos Fiscales del Cliente"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CONDatosFiscalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONDatosFiscalesBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONDatosFiscalesBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IVADESGLOSADOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RFCTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CALLE_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NUMERO_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ENTRECALLESTextBox As System.Windows.Forms.TextBox
    Friend WithEvents COLONIA_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CIUDAD_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ESTADO_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CP_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TELEFONO_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FAX_RSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CURPTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LblPais As System.Windows.Forms.Label
    Friend WithEvents TxtPais As System.Windows.Forms.TextBox
    Friend WithEvents EMail_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CmbCompania As System.Windows.Forms.ComboBox
    Friend WithEvents RAZON_SOCIALTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RAZON_SOCIALLabel As System.Windows.Forms.Label
    Friend WithEvents RFCLabel As System.Windows.Forms.Label
    Friend WithEvents CALLE_RSLabel As System.Windows.Forms.Label
    Friend WithEvents NUMERO_RSLabel As System.Windows.Forms.Label
    Friend WithEvents ENTRECALLESLabel As System.Windows.Forms.Label
    Friend WithEvents COLONIA_RSLabel As System.Windows.Forms.Label
    Friend WithEvents CIUDAD_RSLabel As System.Windows.Forms.Label
    Friend WithEvents ESTADO_RSLabel As System.Windows.Forms.Label
    Friend WithEvents CP_RSLabel As System.Windows.Forms.Label
    Friend WithEvents TELEFONO_RSLabel As System.Windows.Forms.Label
    Friend WithEvents FAX_RSLabel As System.Windows.Forms.Label
    Friend WithEvents CURPLabel As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents NUMERO_intTextBox As System.Windows.Forms.TextBox
    Friend WithEvents cbPais As System.Windows.Forms.ComboBox
    Friend WithEvents cbCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents cbEstado As System.Windows.Forms.ComboBox
    Friend WithEvents cbColonia As System.Windows.Forms.ComboBox
    Friend WithEvents cbCodigoPostal As System.Windows.Forms.ComboBox
    Friend WithEvents cbUsoCFDI As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label

End Class
