Imports System.Data.SqlClient
Public Class FrmServiciosDigitales
    Dim ID_eliminando As Integer = 0
    Dim MTablas As String = "PaquetesGallery_Tel"
    Dim Mvalues As String = "Id_Tel, Descripcion"
    Dim MValores As String = ""
    Dim MCondicion As String = ""
    Dim MTipMov As Integer = 0
    Dim MBNDIDENTITY As Boolean = False
    Dim MClv_ID As Long = 0

    Private Sub Consulta_ServiciosDigitalesBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consulta_ServiciosDigitalesBindingNavigatorSaveItem.Click
        If opcion = "N" Then
            Guardar(1)
        ElseIf opcion = "M" Then
            Guardar(2)
        End If
    End Sub

    Public Sub Desplegar(ByVal Tablas As String, ByVal values As String, ByVal Valores As String, ByVal Condicion As String, ByVal TipMov As Integer, ByVal BNDIDENTITY As Boolean)
        'MiConexion = "Data Source=TeamEdgar-PC;Initial Catalog=Sahuayo ;Persist Security Info=True;User ID=sa;Password=06011975;Connect Timeout=0"
        Dim conn As New SqlConnection(MiConexion)
        Try

            Dim reader As SqlDataReader
            Dim cmd As New SqlCommand("Select Id_Tel, Descripcion from PaquetesGallery_Tel ORder by Descripcion ", conn)
            cmd.CommandType = CommandType.Text
            'cmd.Parameters.Add("@Tablas", SqlDbType.VarChar, 150).Value = Tablas
            'cmd.Parameters.Add("@values", SqlDbType.VarChar, 250).Value = values
            'cmd.Parameters.Add("@Valores", SqlDbType.VarChar, 250).Value = Valores
            'cmd.Parameters.Add("@Condicion", SqlDbType.VarChar, 250).Value = Condicion
            'cmd.Parameters.Add("@TipMov", SqlDbType.Int).Value = TipMov
            'cmd.Parameters.Add("@BNDIDENTITY", SqlDbType.Bit).Value = BNDIDENTITY
            'cmd.Parameters.Add("@CLV_ID", SqlDbType.BigInt).Value = 0
            conn.Open()
            reader = cmd.ExecuteReader()

            Using reader
                While reader.Read
                    Me.ComboBox1.Items.Add(reader.GetValue(0))
                    Me.ComboBox2.Items.Add(reader.GetValue(1))
                    'Me.Clave.Text = reader.GetValue(1)
                    'Me.Descripcion.Text = reader.GetValue(2)
                End While
            End Using
            conn.Close()
        Catch ex As Exception
            If conn.State <> ConnectionState.Closed Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub


    Public Sub Desplegar_Existente(ByVal Tablas As String, ByVal values As String, ByVal Valores As String, ByVal Condicion As String, ByVal TipMov As Integer, ByVal BNDIDENTITY As Boolean)
        'MiConexion = "Data Source=TeamEdgar-PC;Initial Catalog=Sahuayo ;Persist Security Info=True;User ID=sa;Password=06011975;Connect Timeout=0"
        Dim conn As New SqlConnection(MiConexion)
        Try

            Dim reader As SqlDataReader
            Dim cmd As New SqlCommand("UP_PROCESA_TODO", conn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@Tablas", SqlDbType.VarChar, 150).Value = Tablas
            cmd.Parameters.Add("@values", SqlDbType.VarChar, 250).Value = values
            cmd.Parameters.Add("@Valores", SqlDbType.VarChar, 250).Value = Valores
            cmd.Parameters.Add("@Condicion", SqlDbType.VarChar, 250).Value = Condicion
            cmd.Parameters.Add("@TipMov", SqlDbType.Int).Value = TipMov
            cmd.Parameters.Add("@BNDIDENTITY", SqlDbType.Bit).Value = BNDIDENTITY
            cmd.Parameters.Add("@CLV_ID", SqlDbType.BigInt).Value = 0
            conn.Open()
            reader = cmd.ExecuteReader()

            Using reader
                While reader.Read
                    Clv_ServicioDigitalTextBox.Text = reader.GetValue(0)
                    NombreTextBox.Text = reader.GetValue(1)
                    Precio_UnitarioTextBox.Text = reader.GetValue(2)
                    Me.ComboBox1.Text = (reader.GetValue(3))
                    Me.ComboBox1.FindString((reader.GetValue(3)))
                    If (reader.GetValue(5)) = 0 Then
                        Me.RadioButton2.Checked = False
                        Me.RadioButton1.Checked = True
                    Else
                        Me.RadioButton1.Checked = False
                        Me.RadioButton2.Checked = True
                    End If
                    'Me.Clave.Text = reader.GetValue(1)
                    'Me.Descripcion.Text = reader.GetValue(2)
                End While
            End Using
            conn.Close()
        Catch ex As Exception
            If conn.State <> ConnectionState.Closed Then conn.Close()
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Guardar(ByVal opt As Integer)
        Dim cone As New SqlConnection(MiConexion)
        Dim Cmd As New SqlCommand
        Dim nombre As String = Nothing
        If opt = 1 Then
            nombre = "Nuevo_ServiciosDigitales"
        ElseIf opt = 2 Then
            nombre = "Modifica_ServiciosDigitales"
        End If
        cone.Open()
        With Cmd
            .CommandText = nombre
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = cone
            Dim prm As New SqlParameter("@Clv_ServicioDigital", SqlDbType.Int)
            Dim prm2 As New SqlParameter("@Nombre", SqlDbType.VarChar, 450)
            Dim prm3 As New SqlParameter("@Precio", SqlDbType.Money)
            Dim prm4 As New SqlParameter("@Id_Tel", SqlDbType.Int)
            Dim prm5 As New SqlParameter("@TipoCobro", SqlDbType.Int)
            If opt = 1 Then
                prm.Direction = ParameterDirection.Output
            ElseIf opt = 2 Then
                prm.Direction = ParameterDirection.Input
            End If
            prm.Value = Me.Clv_ServicioDigitalTextBox.Text
            prm2.Direction = ParameterDirection.Input
            prm3.Direction = ParameterDirection.Input
            prm4.Direction = ParameterDirection.Input
            prm2.Value = Me.NombreTextBox.Text
            If Me.Precio_UnitarioTextBox.Text = "" Then
                prm3.Value = 0
            Else
                prm3.Value = Me.Precio_UnitarioTextBox.Text
            End If
            If IsNumeric(Me.ComboBox1.Text) = True Then
                prm4.Value = Me.ComboBox1.Text
            Else
                prm4.Value = 0
            End If
            If RadioButton1.Checked = True Then
                prm5.Value = 0
            Else
                prm5.Value = 1
            End If


            .Parameters.Add(prm)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            .Parameters.Add(prm4)
            .Parameters.Add(prm5)



            Dim i As Integer = .ExecuteNonQuery
            If opt = 1 Then
                Me.Clv_ServicioDigitalTextBox.Text = prm.Value
            End If
        End With
        cone.Close()
        MsgBox("Se Ha Guardado con �xito", MsgBoxStyle.Information)
        bec_bnd = True
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        elimina()
        bec_bnd = True
        MsgBox("Se Ha Eliminado con �xito", MsgBoxStyle.Information)
        Me.Close()
    End Sub
    Private Sub elimina()
        Dim cone2 As New SqlConnection(MiConexion)
        Dim Cmd2 As New SqlCommand

        cone2.Open()
        With Cmd2
            .CommandText = "Borrar_ServiciosDigitales"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = cone2
            Dim prm As New SqlParameter("@Clv_ServicioDigital", SqlDbType.Int)
            prm.Direction = ParameterDirection.Input
            prm.Value = Me.Clv_ServicioDigitalTextBox.Text
            .Parameters.Add(prm)
            Dim i As Integer = .ExecuteNonQuery

        End With
        cone2.Close()
    End Sub

    Private Sub FrmServiciosDigitales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim cone4 As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        If opcion = "N" Then
            MCondicion = ""
            MTipMov = 0
            Desplegar(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
        ElseIf opcion = "C" Or opcion = "M" Then

            MCondicion = ""
            MTipMov = 0
            Desplegar(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
            MTablas = "VistaServicios_Digitales_PaquetesGallery"
            Mvalues = "Clv_ServicioDigital,Nombre,	Precio_Unitario,	Id_tel,	Descripcion,TipoCobro"
            MCondicion = "Clv_ServicioDigital = " & gloclv_servicioDigital
            MTipMov = 0
            Desplegar_Existente(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
            'cone4.Open()
            'Me.Consulta_ServiciosDigitalesTableAdapter.Connection = cone4
            'Me.Consulta_ServiciosDigitalesTableAdapter.Fill(Me.DataSetLidia2.Consulta_ServiciosDigitales, gloclv_servicioDigital)
            'cone4.Close()
            If opcion = "C" Then
                Me.Consulta_ServiciosDigitalesBindingNavigator.Enabled = False
                Me.NombreTextBox.ReadOnly = True
                Me.Precio_UnitarioTextBox.ReadOnly = True
            End If
            'MCondicion = ""
            'MTipMov = 0
            'Desplegar(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If Me.ComboBox1.SelectedIndex >= 0 Then
            Me.ComboBox2.SelectedIndex = Me.ComboBox1.SelectedIndex
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged, ComboBox3.SelectedIndexChanged
        If Me.ComboBox2.SelectedIndex >= 0 Then
            Me.ComboBox1.SelectedIndex = Me.ComboBox2.SelectedIndex
        End If
    End Sub
End Class