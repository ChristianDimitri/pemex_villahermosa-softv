Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Public Class FrmImprimirComision

    Private customersByCityReport As ReportDocument
    Private op As String = Nothing
    Private Titulo As String = Nothing
    Delegate Sub Reporte()
    Delegate Sub Forms(ByVal Mycontrol As FrmImprimirComision, ByVal Text As String)

    Private Sub DelegadoForm(ByVal MyControl As FrmImprimirComision, ByVal text As String)
        MyControl.Text = text
    End Sub


    Private Sub ConfigureCrystalReports()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            eTituloComision = ""


            If eOpVentas = 1 Then

                Titulo = "Reporte de Comisiones de " & eGrupo
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Comisiones de " & eGrupo
                End If
                reportPath = RutaReportes + "\ReportComisiones.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@Op
                customersByCityReport.SetParameterValue(2, eOp)
                '@Clv_Session
                customersByCityReport.SetParameterValue(3, eClv_Session)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If


            If eOpVentas = 2 Then
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de N�mero de Ventas"
                End If


                reportPath = RutaReportes + "\ReportNumeroVentas.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@Op
                customersByCityReport.SetParameterValue(2, eOp)
                '@Clv_Session
                customersByCityReport.SetParameterValue(3, eClv_Session)
                '@ServicioPadre
                customersByCityReport.SetParameterValue(4, eTipSer)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Servicio").Text = "'" & eServicio & "'"
            End If


            If eOpVentas = 3 Then
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Status de Ventas"
                End If



                reportPath = RutaReportes + "\ReportStatusVentas.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@Op
                customersByCityReport.SetParameterValue(2, eOp)
                '@ServicioPadre
                customersByCityReport.SetParameterValue(3, eTipSer)
                '@Contratado
                customersByCityReport.SetParameterValue(4, eCont)
                '@Instalado
                customersByCityReport.SetParameterValue(5, eInst)
                '@Desconectado
                customersByCityReport.SetParameterValue(6, eDesc)
                '@Suspendido
                customersByCityReport.SetParameterValue(7, eSusp)
                '@Baja
                customersByCityReport.SetParameterValue(8, eBaja)
                '@Fuera de Area
                customersByCityReport.SetParameterValue(9, eFuera)
                '@Clv_SessionVendedores
                customersByCityReport.SetParameterValue(10, eClv_Session)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Servicio").Text = "'" & eServicio & "'"
                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eTituloComision & "'"
            End If

            If eOpVentas = 4 Then
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Folios Faltantes"
                End If



                reportPath = RutaReportes + "\ReportFolios.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                '@FechaInicial
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@FechaFinal
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eTituloComision & "'"


            End If

            If eOpVentas = 11 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - Consolidado de Ventas"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica Consolidado de Ventas de Servicio " & eServicio
                Else
                    eServicio = "Gr�fica Consolidado de Ventas"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_1.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(2, eFechaFin)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 12 Then


                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - De Sucursales"
                End If


                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica De Sucursales de Servicio  " & eServicio
                Else
                    eServicio = "Gr�fica De Sucursales de Varios Tipos de Servicio"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_2.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 13 Then


                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - De Sucursales"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica De Sucursales Por Servicios de " & eServicio
                Else
                    eServicio = "Gr�fica De Sucursales Por Varios Servicios"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_3.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 14 Then


                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - Del Depto. de Ventas"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica Del Depto. de Ventas Por Vendedores y por Servicio de " & eServicio
                Else
                    If eBndOpVentas = True Then
                        eBndOpVentas = False
                        eServicio = "Gr�fica Del Depto. de Ventas Por Vendedores y por Varios Tipos de Servicio"
                    Else
                        eServicio = "Gr�fica Del Depto. de Ventas Por Vendedores y por Varios Servicios"
                    End If
                End If
                reportPath = RutaReportes + "\ReporteGrafica_4.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If
            If eOpVentas = 15 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - Del Depto. de Ventas"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica Del Depto. de Ventas de Servicios de " & eServicio
                Else
                    eServicio = "Gr�fica Del Depto. de Ventas de Varios Servicios"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_5.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 16 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica - De Sucursales"
                End If

                If eServicio <> "Todos" Then
                    eServicio = "Gr�fica De Sucursales con Servicios de" & eServicio
                Else
                    eServicio = "Gr�fica De Sucursales con Varios Servicios"
                End If
                reportPath = RutaReportes + "\ReporteGrafica_6.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eServicio & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 20 Then

                Titulo = "Detalle de Vendedores del Servicio " & eServicio

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte - " & Titulo
                End If
                reportPath = RutaReportes + "\ReportMetasVen.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 21 Then
                Titulo = "Detalle de Sucursales del Servicio " & eServicio

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte - " & Titulo
                End If

                reportPath = RutaReportes + "\ReportMetasSuc.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 22 Then
                Titulo = "Gr�fica de Medidores de Vendedores del Servicio" & eServicio

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores de Vendedores"
                End If

                reportPath = RutaReportes + "\ReportMetasVenGraf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 23 Then
                Titulo = "Gr�fica de Medidores de Sucursales del Servicio" & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores de Sucursales"
                End If

                reportPath = RutaReportes + "\ReportMetasSucGraf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 30 Then
                Titulo = "Medidores de Ingresos"

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Medidores de Ingresos"
                End If


                reportPath = RutaReportes + "\ReportMetasIng.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 31 Then
                Titulo = "Gr�fica Medidores de Ingresos del Servicio de " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica Medidores de Ingresos"
                End If

                reportPath = RutaReportes + "\ReportMetasIngGraf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 32 Then
                Titulo = "Reporte de Ingresos Por Punto de Cobro"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Ingresos Por Punto de Cobro"
                End If

                reportPath = RutaReportes + "\ReportMetasIngSuc.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 33 Then
                Titulo = "Gr�fica de Ingresos Por Punto de Cobro del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Ingresos Por Punto de Cobro"
                End If

                reportPath = RutaReportes + "\ReportMetasIngSucGraf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(2, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(3, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(4, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(5, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 34 Then
                Titulo = "Reporte Medidores de Cartera"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Medidores de Cartera"
                End If

                reportPath = RutaReportes + "\ReportMetasCartera.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)


                '@MesIni
                customersByCityReport.SetParameterValue(0, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(1, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(2, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(3, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 40 Then
                Titulo = "Clientes en Baja de Contrato Forzoso del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Listado de Clientes en Baja de Contrato Forzoso"
                End If

                reportPath = RutaReportes + "\ReportContratoF.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Orden
                customersByCityReport.SetParameterValue(1, eOpContratoF)
                ''@MesIni
                'customersByCityReport.SetParameterValue(2, eMesIni)
                ''@AnioIni
                'customersByCityReport.SetParameterValue(3, eAnioIni)
                ''@MesFin
                'customersByCityReport.SetParameterValue(4, eMesFin)
                ''@AnioFin
                'customersByCityReport.SetParameterValue(5, eAnioFin)

                'eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 41 Then
                Titulo = "Detalle de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Medidor de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo1.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(3, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(4, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(5, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(6, eAnioFin)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(7, "")

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 42 Then

                Titulo = "Detalle de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Medidor de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo2.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(3, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(4, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(5, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(6, eAnioFin)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(7, "")

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            End If

            If eOpVentas = 43 Then

                Titulo = "Detalle de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Medidor de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupoN.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(3, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(4, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(5, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(6, eAnioFin)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(7, "")

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            End If

            If eOpVentas = 44 Then

                Titulo = "Gr�fica de Medidores de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores"
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo1Graf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(3, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(4, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(5, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(6, eAnioFin)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(7, "")

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            End If

            If eOpVentas = 45 Then

                Titulo = "Gr�fica de Medidores de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores"
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo2Graf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(3, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(4, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(5, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(6, eAnioFin)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(7, "")

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            End If

            If eOpVentas = 46 Then

                Titulo = "Gr�fica de Medidores de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Medidores"
                End If

                reportPath = RutaReportes + "\ReportMetasGrupoNGraf.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@MesIni
                customersByCityReport.SetParameterValue(3, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(4, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(5, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(6, eAnioFin)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(7, "")

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            End If

            If eOpVentas = 47 Then

                Titulo = "Detallado de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Detallado de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo1Det.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(3, "")
                '@FechaIni
                customersByCityReport.SetParameterValue(4, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(5, eFechaFin)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            End If

            If eOpVentas = 48 Then

                Titulo = "Detallado de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Detallado de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupoNDet.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_TipSer
                customersByCityReport.SetParameterValue(1, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(2, eClv_Session)
                '@TipoGrupo
                customersByCityReport.SetParameterValue(3, "")
                '@FechaIni
                customersByCityReport.SetParameterValue(4, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(5, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            End If

            If eOpVentas = 49 Then
                Titulo = "Reporte de Comisiones de " & eGrupo
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Comisiones de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportComisionesN.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@FechaIni
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 50 Then
                Titulo = "Gr�fica de Atenci�n Telef�nica por Servicios al Cliente de " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Atenci�n Telef�nica"
                End If

                reportPath = RutaReportes + "\ReportAtenTelGraf50.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@FechaIni
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 51 Then
                Titulo = "Gr�fica de Atenci�n Telef�nica por Usuarios y Servicios al Cliente de " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Atenci�n Telef�nica"
                End If

                reportPath = RutaReportes + "\ReportAtenTelGraf51.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@FechaIni
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 52 Then
                Titulo = "Gr�fica de Llamadas de Atenci�n Telef�nica del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Gr�fica de Atenci�n Telef�nica"
                End If

                reportPath = RutaReportes + "\ReportAtenTelGraf52.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@FechaIni
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 53 Then
                Titulo = "Reporte de Status de Ventas de " & eGrupo
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Status de Ventas"
                End If


                reportPath = RutaReportes + "\ReportStatusVentasN.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                'Clv_Grupo
                customersByCityReport.SetParameterValue(0, eClv_Grupo)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(2, eFechaFin)
                '@ServicioPadre
                customersByCityReport.SetParameterValue(3, eTipSer)
                '@Contratado
                customersByCityReport.SetParameterValue(4, eCont)
                '@Instalado
                customersByCityReport.SetParameterValue(5, eInst)
                '@Desconectado
                customersByCityReport.SetParameterValue(6, eDesc)
                '@Suspendido
                customersByCityReport.SetParameterValue(7, eSusp)
                '@Baja
                customersByCityReport.SetParameterValue(8, eBaja)
                '@Fuera de Area
                customersByCityReport.SetParameterValue(9, eFuera)
                '@Temporal
                customersByCityReport.SetParameterValue(10, eTempo)
                '@Clv_SessionVendedores
                customersByCityReport.SetParameterValue(11, eClv_Session)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If


            If eOpVentas = 54 Then
                Titulo = "Reporte de PPV"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de PPV"
                End If


                reportPath = RutaReportes + "\ReportPPV.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)
                'Contrato
                customersByCityReport.SetParameterValue(0, eContrato)
                '@FechaIni
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(2, eFechaFin)
                '@Res
                customersByCityReport.SetParameterValue(3, 0)
                '@OP1
                customersByCityReport.SetParameterValue(4, eOP1)
                '@OP2
                customersByCityReport.SetParameterValue(5, eOP2)
                '@OP3
                customersByCityReport.SetParameterValue(6, eOP3)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 55 Then
                Titulo = "Reporte de Servicios Contratados"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Servicios Contratados"
                End If


                reportPath = RutaReportes + "\ReportVentas.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@FechaIni
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 56 Then
                Titulo = "Reporte de Ventas en Baja de todos los Grupos de Ventas"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Ventas en Baja"
                End If


                reportPath = RutaReportes + "\ReportVentasBaja.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@FechaIni
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 57 Then
                Titulo = "Bit�cora de Correo(s)"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Bit�cora de Correo(s)"
                End If


                reportPath = RutaReportes + "\ReportCorreo.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clave
                customersByCityReport.SetParameterValue(0, eClaveCorreo)
                '@FechaIni
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(2, eFechaFin)
                '@Op
                customersByCityReport.SetParameterValue(3, eOpCorreo)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 58 Then
                Titulo = "Reporte de Comisiones de " & eGrupo
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Comisiones de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportComisiones0.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_Session
                customersByCityReport.SetParameterValue(0, eClv_Session)
                '@FechaIni
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(2, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 59 Then
                Titulo = "Detallado de " & eGrupo & " del Servicio " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Detallado de " & eGrupo
                End If

                reportPath = RutaReportes + "\ReportMetasGrupo0Det.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)


                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Clv_Session
                customersByCityReport.SetParameterValue(1, eClv_Session)
                '@FechaIni
                customersByCityReport.SetParameterValue(2, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(3, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            End If

            If eOpVentas = 60 Then
                Titulo = "Reporte de Status de Ventas de " & eGrupo

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Status de Ventas"
                End If


                reportPath = RutaReportes + "\ReportStatusVentas0.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@ServicioPadre
                customersByCityReport.SetParameterValue(2, eTipSer)
                '@Contratado
                customersByCityReport.SetParameterValue(3, eCont)
                '@Instalado
                customersByCityReport.SetParameterValue(4, eInst)
                '@Desconectado
                customersByCityReport.SetParameterValue(5, eDesc)
                '@Suspendido
                customersByCityReport.SetParameterValue(6, eSusp)
                '@Baja
                customersByCityReport.SetParameterValue(7, eBaja)
                '@Fuera de Area
                customersByCityReport.SetParameterValue(8, eFuera)
                '@Temporal
                customersByCityReport.SetParameterValue(9, eTempo)
                '@Clv_SessionVendedores
                customersByCityReport.SetParameterValue(10, eClv_Session)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 61 Then
                Titulo = "Reporte Ventas Totales de " & eServicio

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Ventas Totales"
                End If


                reportPath = RutaReportes + "\ReportVentasTotales.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@ServicioPadre
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(2, eFechaFin)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 62 Then
                Titulo = "Reporte de Interfaz " & eServicio
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Interfaz"
                End If


                reportPath = RutaReportes + "\ReportCNR_CNRDig.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@ServicioPadre
                customersByCityReport.SetParameterValue(0, eOpCNRCNRDIG)
                '@Fecha_Ini
                customersByCityReport.SetParameterValue(1, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(2, eFechaFin)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 63 Then
                Titulo = "Reporte de Resumen de Clientes"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Resumen de Clientes"
                End If


                reportPath = RutaReportes + "\ReportResumenDeClientes.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)


                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 64 Then
                Titulo = "Reporte Auxiliar"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Auxiliar"
                End If


                reportPath = RutaReportes + "\ReportTelefonia.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 65 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Hoteles con Fecha de �ltimo Pago"
                End If


                reportPath = RutaReportes + "\ReportHoteles.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 66 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte De Ordenes"
                End If


                reportPath = RutaReportes + "\ReportOrdenes.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)
                '@Op
                customersByCityReport.SetParameterValue(2, eOp)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 67 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Gerencial"
                End If

                reportPath = RutaReportes + "\ReportGerencial.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If


            If eOpVentas = 68 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Resumen de Cancelaciones"
                End If

                reportPath = RutaReportes + "\ReportCancelaciones.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_Ini
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Fin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 69 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Permanencia de " & eServicio
                End If

                reportPath = RutaReportes + "\ReportPermanencia.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@MesIni
                customersByCityReport.SetParameterValue(1, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(2, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(3, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(4, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            End If


            If eOpVentas = 70 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Resumen de Ventas por Status de " & eServicio
                End If

                reportPath = RutaReportes + "\ReportVentasPorStatus.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Clv_TipSer
                customersByCityReport.SetParameterValue(0, eTipSer)
                '@MesIni
                customersByCityReport.SetParameterValue(1, eMesIni)
                '@AnioIni
                customersByCityReport.SetParameterValue(2, eAnioIni)
                '@MesFin
                customersByCityReport.SetParameterValue(3, eMesFin)
                '@AnioFin
                customersByCityReport.SetParameterValue(4, eAnioFin)

                eTituloComision = "De " & eStrMesIni & "/" & CStr(eAnioIni) & " a " & eStrMesFin & "/" & CStr(eAnioFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"

            End If

            If eOpVentas = 77 Then

                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Recontrataciones"
                End If


                reportPath = RutaReportes + "\ReportRecontratacion.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@FechaIni
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@FechaFin
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            'Yahve -------------------------------
            If eOpVentas = 100 Then
                Titulo = "Resumen de Puntos de Antig�edad"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte de Resumen de Puntos de Antig�edad "
                End If


                reportPath = RutaReportes + "\RepAntiguedadResumen.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)

                '@Fecha_inicial
                customersByCityReport.SetParameterValue(0, eFechaIni)
                '@Fecha_Final
                customersByCityReport.SetParameterValue(1, eFechaFin)

                eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & eTituloComision & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 110 Then
                Titulo = "Clientes De Portabilidad"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Clientes Portabilidad "
                End If


                reportPath = RutaReportes + "\RptClientesPortabilidad.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)


                customersByCityReport.SetParameterValue(0, opcionStatus)
                '@Fecha_inicial
                customersByCityReport.SetParameterValue(1, fechaini)
                '@Fecha_Final
                customersByCityReport.SetParameterValue(2, fechafin)

                'eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                'customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & eTituloComision & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If

            If eOpVentas = 120 Then
                Titulo = "Reporte Rececpcion"
                If Me.InvokeRequired Then
                    Dim d As New Forms(AddressOf DelegadoForm)
                    Me.Invoke(d, New Object() {Me, ""})
                Else
                    Me.Text = "Reporte Rececpcion "
                End If


                reportPath = RutaReportes + "\RptRecepcionSoftv.rpt"
                customersByCityReport.Load(reportPath)
                SetDBLogonForReport(connectionInfo, customersByCityReport)


                customersByCityReport.SetParameterValue(0, GloOpcionRecepcion)
                customersByCityReport.SetParameterValue(1, GloFolioRecepcion)

                'eTituloComision = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
                ''customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & eTituloComision & "'"
                'customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            End If


            ''If eOpVentas = 11 Then
            ''    Me.Text = "Gr�fica - Consolidado de Ventas"
            ''    reportPath = RutaReportes + "\ReporteGrafica_1.rpt"
            ''    customersByCityReport.Load(reportPath)
            ''    SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''    '@Fecha_Ini
            ''    customersByCityReport.SetParameterValue(0, eFechaIni)
            ''    '@Fecha_Fin
            ''    customersByCityReport.SetParameterValue(1, eFechaFin)

            ''    eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
            ''    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            ''End If

            ''If eOpVentas = 12 Then
            ''    Me.Text = "Gr�fica - Oficinas"
            ''    reportPath = RutaReportes + "\ReporteGrafica_2.rpt"
            ''    customersByCityReport.Load(reportPath)
            ''    SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''    '@Fecha_Ini
            ''    customersByCityReport.SetParameterValue(0, eFechaIni)
            ''    '@Fecha_Fin
            ''    customersByCityReport.SetParameterValue(1, eFechaFin)

            ''    eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
            ''    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            ''End If
            ''If eOpVentas = 13 Then
            ''    Me.Text = "Gr�fica - Departamento de Ventas"
            ''    reportPath = RutaReportes + "\ReporteGrafica_3.rpt"
            ''    customersByCityReport.Load(reportPath)
            ''    SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''    '@Fecha_Ini
            ''    customersByCityReport.SetParameterValue(0, eFechaIni)
            ''    '@Fecha_Fin
            ''    customersByCityReport.SetParameterValue(1, eFechaFin)

            ''    eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
            ''    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            ''End If
            ''If eOpVentas = 14 Then
            ''    Me.Text = "Gr�fica - Por Sucursal"
            ''    Titulo = ""
            ''    Titulo = "Gr�fica de Ventas de: " + eNombre
            ''    reportPath = RutaReportes + "\ReporteGrafica_4.rpt"
            ''    customersByCityReport.Load(reportPath)
            ''    SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''    '@Fecha_Ini
            ''    customersByCityReport.SetParameterValue(0, eFechaIni)
            ''    '@Fecha_Fin
            ''    customersByCityReport.SetParameterValue(1, eFechaFin)
            ''    '@Sucursal
            ''    customersByCityReport.SetParameterValue(2, eClv_Sucursal)

            ''    eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
            ''    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            ''End If
            ''If eOpVentas = 15 Then
            ''    Me.Text = "Gr�fica - Por Vendedor"
            ''    Titulo = ""
            ''    Titulo = "Gr�fica de Ventas de: " + eNombre
            ''    reportPath = RutaReportes + "\ReporteGrafica_5.rpt"
            ''    customersByCityReport.Load(reportPath)
            ''    SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''    '@Fecha_Ini
            ''    customersByCityReport.SetParameterValue(0, eFechaIni)
            ''    '@Fecha_Fin
            ''    customersByCityReport.SetParameterValue(1, eFechaFin)
            ''    '@Vendedor
            ''    customersByCityReport.SetParameterValue(2, eClv_Vendedor)

            ''    eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
            ''    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            ''End If
            ''If eOpVentas = 16 Then
            ''    Me.Text = "Gr�fica - Resumen de Vendedores"
            ''    reportPath = RutaReportes + "\ReporteGrafica_6.rpt"
            ''    customersByCityReport.Load(reportPath)
            ''    SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''    '@Fecha_Ini
            ''    customersByCityReport.SetParameterValue(0, eFechaIni)
            ''    '@Fecha_Fin
            ''    customersByCityReport.SetParameterValue(1, eFechaFin)
            ''    '@Session
            ''    customersByCityReport.SetParameterValue(2, eClv_Session)

            ''    eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
            ''    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            ''End If
            ''If eOpVentas = 17 Then
            ''    Me.Text = "Gr�fica - Por Paquetes"
            ''    reportPath = RutaReportes + "\ReporteGrafica_7.rpt"
            ''    customersByCityReport.Load(reportPath)
            ''    SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''    '@Fecha_Ini
            ''    customersByCityReport.SetParameterValue(0, eFechaIni)
            ''    '@Fecha_Fin
            ''    customersByCityReport.SetParameterValue(1, eFechaFin)
            ''    '@Session
            ''    customersByCityReport.SetParameterValue(2, eClv_Session)

            ''    eTituloComision = "Del " & eFechaIni & " al " & eFechaFin
            ''    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eTituloComision & "'"
            ''    customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
            ''End If

            If eBndGraf = True Or eOpVentas = 20 Or eOpVentas = 21 Or eOpVentas = 22 Or eOpVentas = 23 Or eOpVentas = 31 Or eOpVentas = 33 Or eOpVentas = 57 Then
                eBndGraf = False
                eBndVen = True
                Me.customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            End If
            If eOpVentas <> 0 Then
                If Me.CrystalReportViewer1.InvokeRequired Then
                    Dim d As New Reporte(AddressOf ConfigureCrystalReports)
                    Me.Invoke(d, New Object() {})
                Else
                    CrystalReportViewer1.ReportSource = customersByCityReport
                End If
                'Me.CrystalReportViewer1.ReportSource = customersByCityReport
            End If
            customersByCityReport = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
            'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub FrmImprimirComision_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PantallaProcesando.Show()
        Me.BackgroundWorker1.RunWorkerAsync()
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        ConfigureCrystalReports()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        PantallaProcesando.Close()
    End Sub
End Class