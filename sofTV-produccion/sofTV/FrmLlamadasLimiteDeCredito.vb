﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualBasic

Public Class FrmLlamadasLimiteDeCredito

    Private Sub FrmLlamadasLimiteDeCredito_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        CONDATOSLIMITECREDITO(1, 0, String.Empty, String.Empty)
    End Sub


    Private Sub FrmLlamadasLimiteDeCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        CONDATOSLIMITECREDITO(1, 0, String.Empty, String.Empty)
    End Sub
    Private Sub CONDATOSLIMITECREDITO(ByVal OP As Integer, ByVal CONTRATO As Integer, ByVal NOMBRE As String, ByVal STATUS As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim StrSql As New StringBuilder

        StrSql.Append("EXEC CONDATOSLIMITECREDITO ")
        StrSql.Append(CStr(OP) & ", ")
        StrSql.Append(CStr(CONTRATO) & ", ")
        StrSql.Append("'" & CStr(NOMBRE) & "', ")
        StrSql.Append("'" & CStr(STATUS) & "'")

        Dim DA As New SqlDataAdapter(StrSql.ToString, CON)
        Dim DT As New DataTable
        Dim BS As New BindingSource

        Try
            CON.Open()
            DA.Fill(DT)
            BS.DataSource = DT
            Me.DatosDataGrid.DataSource = BS.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub BusNombreButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BusNombreButton.Click
        If Len(Me.CONTRATOTextBox.Text) > 0 Then
            CONDATOSLIMITECREDITO(2, Me.CONTRATOTextBox.Text, String.Empty, String.Empty)
            Me.CONTRATOTextBox.Clear()
        ElseIf Len(Me.NOMBRETextBox.Text) > 0 Then
            CONDATOSLIMITECREDITO(3, 0, NOMBRETextBox.Text, String.Empty)
            Me.NOMBRETextBox.Clear()
        Else
            CONDATOSLIMITECREDITO(1, 0, String.Empty, String.Empty)
        End If
    End Sub

    Private Sub RegistradasRadio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegistradasRadio.CheckedChanged
        CONDATOSLIMITECREDITO(4, 0, String.Empty, "L")
    End Sub

    Private Sub NoRegistradasRadio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoRegistradasRadio.CheckedChanged
        CONDATOSLIMITECREDITO(4, 0, String.Empty, "P")
    End Sub

    Private Sub TodosRadio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TodosRadio.CheckedChanged
        CONDATOSLIMITECREDITO(1, 0, String.Empty, String.Empty)
    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        Me.Close()
    End Sub

    Private Sub RegistrarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegistrarButton.Click
        If Me.DatosDataGrid.RowCount = 0 Then
            MsgBox("Debe seleccionar al menos un Contrato para realizar ésta operación", MsgBoxStyle.Information)
            Exit Sub
        End If
        IdRegistroLlamada = DatosDataGrid.SelectedCells.Item(0).Value()
        TipoRegistro = 0
        FrmRegistroLlamadas.Show()
    End Sub

    Private Sub ConsultarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultarButton.Click
        If Me.DatosDataGrid.RowCount = 0 Then
            MsgBox("Debe seleccionar al menos un Contrato para realizar ésta operación", MsgBoxStyle.Information)
            Exit Sub
        End If
        IdRegistroLlamada = DatosDataGrid.SelectedCells.Item(0).Value()
        TipoRegistro = 1
        FrmRegistroLlamadas.Show()
    End Sub

    Private Sub CONTRATOTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONTRATOTextBox.TextChanged
        Me.NOMBRETextBox.Clear()
    End Sub

    Private Sub NOMBRETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NOMBRETextBox.TextChanged
        Me.CONTRATOTextBox.Clear()
    End Sub

    Private Sub ImprimirButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImprimirButton.Click
        FrmSeleccionaLimite.Show()
    End Sub
End Class