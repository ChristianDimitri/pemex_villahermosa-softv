﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRecontratacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRecontratacion))
        Me.tcServicios = New System.Windows.Forms.TabControl()
        Me.tpTV = New System.Windows.Forms.TabPage()
        Me.lblTV = New System.Windows.Forms.Label()
        Me.panelTV = New System.Windows.Forms.Panel()
        Me.checkTV = New System.Windows.Forms.CheckBox()
        Me.btnTVCancelar = New System.Windows.Forms.Button()
        Me.btnTVAceptar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.comboSerTV = New System.Windows.Forms.ComboBox()
        Me.txtConPago = New System.Windows.Forms.TextBox()
        Me.txtSinPago = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.bnTV = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarTV = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarTV = New System.Windows.Forms.ToolStripButton()
        Me.tvTV = New System.Windows.Forms.TreeView()
        Me.tpNET = New System.Windows.Forms.TabPage()
        Me.lblNET = New System.Windows.Forms.Label()
        Me.panelCNET = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.comboTipoSer = New System.Windows.Forms.ComboBox()
        Me.btnCNETCancelar = New System.Windows.Forms.Button()
        Me.btnCNETAceptar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.comboTipoAs = New System.Windows.Forms.ComboBox()
        Me.comboTipoCa = New System.Windows.Forms.ComboBox()
        Me.panelSNET = New System.Windows.Forms.Panel()
        Me.checkNET = New System.Windows.Forms.CheckBox()
        Me.btnSNETCancelar = New System.Windows.Forms.Button()
        Me.btnSNETAceptar = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.comboSerNET = New System.Windows.Forms.ComboBox()
        Me.bnNETServicio = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarSNET = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarSNET = New System.Windows.Forms.ToolStripButton()
        Me.bnNETCablemodem = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarNET = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarNET = New System.Windows.Forms.ToolStripButton()
        Me.tvNET = New System.Windows.Forms.TreeView()
        Me.tpDIG = New System.Windows.Forms.TabPage()
        Me.lblDIG = New System.Windows.Forms.Label()
        Me.panelDIG = New System.Windows.Forms.Panel()
        Me.checkDIG = New System.Windows.Forms.CheckBox()
        Me.btnDIGCancelar = New System.Windows.Forms.Button()
        Me.btnDIGAceptar = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.comboSerDIG = New System.Windows.Forms.ComboBox()
        Me.bnDIGServicio = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarSDIG = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarSDIG = New System.Windows.Forms.ToolStripButton()
        Me.bnDIGTarjeta = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminarTDIG = New System.Windows.Forms.ToolStripButton()
        Me.tsbAgregarTDIG = New System.Windows.Forms.ToolStripButton()
        Me.tvDIG = New System.Windows.Forms.TreeView()
        Me.tpTEL = New System.Windows.Forms.TabPage()
        Me.lblTEL = New System.Windows.Forms.Label()
        Me.panelTelPaqAdi = New System.Windows.Forms.Panel()
        Me.btnTelPaqAdiCancelar = New System.Windows.Forms.Button()
        Me.btnTelPaqAdiAceptar = New System.Windows.Forms.Button()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.comboTelPaqAdi = New System.Windows.Forms.ComboBox()
        Me.panelTelSerDig = New System.Windows.Forms.Panel()
        Me.btnTelSerDigCancelar = New System.Windows.Forms.Button()
        Me.btnTelSerDigAceptar = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.comboTelSerDig = New System.Windows.Forms.ComboBox()
        Me.panelTelPlan = New System.Windows.Forms.Panel()
        Me.grupoTel = New System.Windows.Forms.GroupBox()
        Me.comboTel = New System.Windows.Forms.ComboBox()
        Me.radioPor = New System.Windows.Forms.RadioButton()
        Me.radioAuto = New System.Windows.Forms.RadioButton()
        Me.radioSel = New System.Windows.Forms.RadioButton()
        Me.checkTEL = New System.Windows.Forms.CheckBox()
        Me.btnTelPlanCancelar = New System.Windows.Forms.Button()
        Me.bntTelPlanAceptar = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.comboTelPlan = New System.Windows.Forms.ComboBox()
        Me.tvTel = New System.Windows.Forms.TreeView()
        Me.bnTelPaqAdi = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbPadAdiEliminar = New System.Windows.Forms.ToolStripButton()
        Me.tsbPadAdiAgregar = New System.Windows.Forms.ToolStripButton()
        Me.bnTelSerDig = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbSerDigEliminar = New System.Windows.Forms.ToolStripButton()
        Me.tsbSerDigAgregar = New System.Windows.Forms.ToolStripButton()
        Me.bnTelPlan = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbPlanEliminar = New System.Windows.Forms.ToolStripButton()
        Me.tsbPlanAgregar = New System.Windows.Forms.ToolStripButton()
        Me.btnNETCancelar = New System.Windows.Forms.Button()
        Me.btnNETAceptar = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.comboSerNET2023 = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxSoloInternet = New System.Windows.Forms.CheckBox()
        Me.LabelCelular = New System.Windows.Forms.Label()
        Me.LabelTelefono = New System.Windows.Forms.Label()
        Me.LabelCiudad = New System.Windows.Forms.Label()
        Me.LabelColonia = New System.Windows.Forms.Label()
        Me.LabelNumero = New System.Windows.Forms.Label()
        Me.LabelCalle = New System.Windows.Forms.Label()
        Me.LabelNombre = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBoxContrato = New System.Windows.Forms.TextBox()
        Me.ButtonBuscar = New System.Windows.Forms.Button()
        Me.bnRecontratar = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbRecon = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.panelCombo = New System.Windows.Forms.Panel()
        Me.lblCombo = New System.Windows.Forms.Label()
        Me.btnCancelarCombo = New System.Windows.Forms.Button()
        Me.btnAceptarCombo = New System.Windows.Forms.Button()
        Me.groupTEL = New System.Windows.Forms.GroupBox()
        Me.lblSerTEL = New System.Windows.Forms.Label()
        Me.comboTelCombo = New System.Windows.Forms.ComboBox()
        Me.radioPorCombo = New System.Windows.Forms.RadioButton()
        Me.radioAutoCombo = New System.Windows.Forms.RadioButton()
        Me.radioSelCombo = New System.Windows.Forms.RadioButton()
        Me.groupDIG = New System.Windows.Forms.GroupBox()
        Me.lblSerDIG = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtNumCajas = New System.Windows.Forms.TextBox()
        Me.groupNET = New System.Windows.Forms.GroupBox()
        Me.lblSerNET = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.comboTipoCaCombo = New System.Windows.Forms.ComboBox()
        Me.groupTV = New System.Windows.Forms.GroupBox()
        Me.lblSerTV = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtSinPagoCombo = New System.Windows.Forms.TextBox()
        Me.txtConPagoCombo = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.panelSelCombo = New System.Windows.Forms.Panel()
        Me.btnCancelarSelCombo = New System.Windows.Forms.Button()
        Me.btnAceptarSelCombo = New System.Windows.Forms.Button()
        Me.comboCombo = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.tcServicios.SuspendLayout()
        Me.tpTV.SuspendLayout()
        Me.panelTV.SuspendLayout()
        CType(Me.bnTV, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTV.SuspendLayout()
        Me.tpNET.SuspendLayout()
        Me.panelCNET.SuspendLayout()
        Me.panelSNET.SuspendLayout()
        CType(Me.bnNETServicio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnNETServicio.SuspendLayout()
        CType(Me.bnNETCablemodem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnNETCablemodem.SuspendLayout()
        Me.tpDIG.SuspendLayout()
        Me.panelDIG.SuspendLayout()
        CType(Me.bnDIGServicio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnDIGServicio.SuspendLayout()
        CType(Me.bnDIGTarjeta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnDIGTarjeta.SuspendLayout()
        Me.tpTEL.SuspendLayout()
        Me.panelTelPaqAdi.SuspendLayout()
        Me.panelTelSerDig.SuspendLayout()
        Me.panelTelPlan.SuspendLayout()
        Me.grupoTel.SuspendLayout()
        CType(Me.bnTelPaqAdi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTelPaqAdi.SuspendLayout()
        CType(Me.bnTelSerDig, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTelSerDig.SuspendLayout()
        CType(Me.bnTelPlan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTelPlan.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.bnRecontratar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnRecontratar.SuspendLayout()
        Me.panelCombo.SuspendLayout()
        Me.groupTEL.SuspendLayout()
        Me.groupDIG.SuspendLayout()
        Me.groupNET.SuspendLayout()
        Me.groupTV.SuspendLayout()
        Me.panelSelCombo.SuspendLayout()
        Me.SuspendLayout()
        '
        'tcServicios
        '
        Me.tcServicios.Controls.Add(Me.tpTV)
        Me.tcServicios.Controls.Add(Me.tpNET)
        Me.tcServicios.Controls.Add(Me.tpDIG)
        Me.tcServicios.Controls.Add(Me.tpTEL)
        Me.tcServicios.Enabled = False
        Me.tcServicios.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tcServicios.Location = New System.Drawing.Point(24, 270)
        Me.tcServicios.Name = "tcServicios"
        Me.tcServicios.SelectedIndex = 0
        Me.tcServicios.Size = New System.Drawing.Size(680, 368)
        Me.tcServicios.TabIndex = 0
        '
        'tpTV
        '
        Me.tpTV.Controls.Add(Me.lblTV)
        Me.tpTV.Controls.Add(Me.panelTV)
        Me.tpTV.Controls.Add(Me.bnTV)
        Me.tpTV.Controls.Add(Me.tvTV)
        Me.tpTV.Location = New System.Drawing.Point(4, 24)
        Me.tpTV.Name = "tpTV"
        Me.tpTV.Padding = New System.Windows.Forms.Padding(3)
        Me.tpTV.Size = New System.Drawing.Size(672, 340)
        Me.tpTV.TabIndex = 0
        Me.tpTV.Text = "Televisión"
        Me.tpTV.UseVisualStyleBackColor = True
        '
        'lblTV
        '
        Me.lblTV.ForeColor = System.Drawing.Color.Red
        Me.lblTV.Location = New System.Drawing.Point(3, 313)
        Me.lblTV.Name = "lblTV"
        Me.lblTV.Size = New System.Drawing.Size(666, 24)
        Me.lblTV.TabIndex = 18
        '
        'panelTV
        '
        Me.panelTV.BackColor = System.Drawing.Color.WhiteSmoke
        Me.panelTV.Controls.Add(Me.checkTV)
        Me.panelTV.Controls.Add(Me.btnTVCancelar)
        Me.panelTV.Controls.Add(Me.btnTVAceptar)
        Me.panelTV.Controls.Add(Me.Label3)
        Me.panelTV.Controls.Add(Me.comboSerTV)
        Me.panelTV.Controls.Add(Me.txtConPago)
        Me.panelTV.Controls.Add(Me.txtSinPago)
        Me.panelTV.Controls.Add(Me.Label2)
        Me.panelTV.Controls.Add(Me.Label1)
        Me.panelTV.Location = New System.Drawing.Point(97, 89)
        Me.panelTV.Name = "panelTV"
        Me.panelTV.Size = New System.Drawing.Size(472, 178)
        Me.panelTV.TabIndex = 14
        Me.panelTV.Visible = False
        '
        'checkTV
        '
        Me.checkTV.AutoSize = True
        Me.checkTV.Location = New System.Drawing.Point(136, 99)
        Me.checkTV.Name = "checkTV"
        Me.checkTV.Size = New System.Drawing.Size(79, 19)
        Me.checkTV.TabIndex = 8
        Me.checkTV.Text = "Cortesía"
        Me.checkTV.UseVisualStyleBackColor = True
        Me.checkTV.Visible = False
        '
        'btnTVCancelar
        '
        Me.btnTVCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnTVCancelar.Name = "btnTVCancelar"
        Me.btnTVCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnTVCancelar.TabIndex = 7
        Me.btnTVCancelar.Text = "&Cancelar"
        Me.btnTVCancelar.UseVisualStyleBackColor = True
        '
        'btnTVAceptar
        '
        Me.btnTVAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnTVAceptar.Name = "btnTVAceptar"
        Me.btnTVAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnTVAceptar.TabIndex = 6
        Me.btnTVAceptar.Text = "&Aceptar"
        Me.btnTVAceptar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(64, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 15)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Servicio: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboSerTV
        '
        Me.comboSerTV.DisplayMember = "Descripcion"
        Me.comboSerTV.FormattingEnabled = True
        Me.comboSerTV.Location = New System.Drawing.Point(136, 16)
        Me.comboSerTV.Name = "comboSerTV"
        Me.comboSerTV.Size = New System.Drawing.Size(276, 23)
        Me.comboSerTV.TabIndex = 4
        Me.comboSerTV.ValueMember = "Clv_Servicio"
        '
        'txtConPago
        '
        Me.txtConPago.Location = New System.Drawing.Point(136, 72)
        Me.txtConPago.Name = "txtConPago"
        Me.txtConPago.Size = New System.Drawing.Size(100, 21)
        Me.txtConPago.TabIndex = 3
        '
        'txtSinPago
        '
        Me.txtSinPago.Location = New System.Drawing.Point(136, 45)
        Me.txtSinPago.Name = "txtSinPago"
        Me.txtSinPago.Size = New System.Drawing.Size(100, 21)
        Me.txtSinPago.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(40, 78)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "TV con pago:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(44, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "TV sin pago:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'bnTV
        '
        Me.bnTV.AddNewItem = Nothing
        Me.bnTV.CountItem = Nothing
        Me.bnTV.DeleteItem = Nothing
        Me.bnTV.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnTV.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarTV, Me.tsbAgregarTV})
        Me.bnTV.Location = New System.Drawing.Point(3, 3)
        Me.bnTV.MoveFirstItem = Nothing
        Me.bnTV.MoveLastItem = Nothing
        Me.bnTV.MoveNextItem = Nothing
        Me.bnTV.MovePreviousItem = Nothing
        Me.bnTV.Name = "bnTV"
        Me.bnTV.PositionItem = Nothing
        Me.bnTV.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnTV.Size = New System.Drawing.Size(666, 25)
        Me.bnTV.TabIndex = 13
        Me.bnTV.Text = "BindingNavigator1"
        '
        'tsbEliminarTV
        '
        Me.tsbEliminarTV.Image = CType(resources.GetObject("tsbEliminarTV.Image"), System.Drawing.Image)
        Me.tsbEliminarTV.Name = "tsbEliminarTV"
        Me.tsbEliminarTV.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarTV.Size = New System.Drawing.Size(125, 22)
        Me.tsbEliminarTV.Text = "Eliminar Servicio"
        '
        'tsbAgregarTV
        '
        Me.tsbAgregarTV.Image = CType(resources.GetObject("tsbAgregarTV.Image"), System.Drawing.Image)
        Me.tsbAgregarTV.Name = "tsbAgregarTV"
        Me.tsbAgregarTV.Size = New System.Drawing.Size(127, 22)
        Me.tsbAgregarTV.Text = "Agregar Servicio"
        '
        'tvTV
        '
        Me.tvTV.Location = New System.Drawing.Point(73, 64)
        Me.tvTV.Name = "tvTV"
        Me.tvTV.Size = New System.Drawing.Size(521, 235)
        Me.tvTV.TabIndex = 4
        '
        'tpNET
        '
        Me.tpNET.Controls.Add(Me.lblNET)
        Me.tpNET.Controls.Add(Me.panelCNET)
        Me.tpNET.Controls.Add(Me.panelSNET)
        Me.tpNET.Controls.Add(Me.bnNETServicio)
        Me.tpNET.Controls.Add(Me.bnNETCablemodem)
        Me.tpNET.Controls.Add(Me.tvNET)
        Me.tpNET.Location = New System.Drawing.Point(4, 24)
        Me.tpNET.Name = "tpNET"
        Me.tpNET.Padding = New System.Windows.Forms.Padding(3)
        Me.tpNET.Size = New System.Drawing.Size(672, 340)
        Me.tpNET.TabIndex = 1
        Me.tpNET.Text = "Internet"
        Me.tpNET.UseVisualStyleBackColor = True
        '
        'lblNET
        '
        Me.lblNET.ForeColor = System.Drawing.Color.Red
        Me.lblNET.Location = New System.Drawing.Point(3, 313)
        Me.lblNET.Name = "lblNET"
        Me.lblNET.Size = New System.Drawing.Size(666, 24)
        Me.lblNET.TabIndex = 17
        '
        'panelCNET
        '
        Me.panelCNET.BackColor = System.Drawing.SystemColors.Control
        Me.panelCNET.Controls.Add(Me.Label17)
        Me.panelCNET.Controls.Add(Me.comboTipoSer)
        Me.panelCNET.Controls.Add(Me.btnCNETCancelar)
        Me.panelCNET.Controls.Add(Me.btnCNETAceptar)
        Me.panelCNET.Controls.Add(Me.Label7)
        Me.panelCNET.Controls.Add(Me.Label6)
        Me.panelCNET.Controls.Add(Me.comboTipoAs)
        Me.panelCNET.Controls.Add(Me.comboTipoCa)
        Me.panelCNET.Location = New System.Drawing.Point(95, 93)
        Me.panelCNET.Name = "panelCNET"
        Me.panelCNET.Size = New System.Drawing.Size(472, 178)
        Me.panelCNET.TabIndex = 15
        Me.panelCNET.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(80, 84)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(94, 15)
        Me.Label17.TabIndex = 15
        Me.Label17.Text = "Tipo Servicio:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboTipoSer
        '
        Me.comboTipoSer.DisplayMember = "Concepto"
        Me.comboTipoSer.Enabled = False
        Me.comboTipoSer.FormattingEnabled = True
        Me.comboTipoSer.Location = New System.Drawing.Point(196, 76)
        Me.comboTipoSer.Name = "comboTipoSer"
        Me.comboTipoSer.Size = New System.Drawing.Size(202, 23)
        Me.comboTipoSer.TabIndex = 14
        Me.comboTipoSer.ValueMember = "Clv_TipSerInternet"
        '
        'btnCNETCancelar
        '
        Me.btnCNETCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnCNETCancelar.Name = "btnCNETCancelar"
        Me.btnCNETCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnCNETCancelar.TabIndex = 13
        Me.btnCNETCancelar.Text = "&Cancelar"
        Me.btnCNETCancelar.UseVisualStyleBackColor = True
        '
        'btnCNETAceptar
        '
        Me.btnCNETAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnCNETAceptar.Name = "btnCNETAceptar"
        Me.btnCNETAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnCNETAceptar.TabIndex = 12
        Me.btnCNETAceptar.Text = "&Aceptar"
        Me.btnCNETAceptar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(80, 53)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(113, 15)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Tipo Asignación:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(65, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(128, 15)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Tipo Cablemodem:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboTipoAs
        '
        Me.comboTipoAs.DisplayMember = "Tipo"
        Me.comboTipoAs.Enabled = False
        Me.comboTipoAs.FormattingEnabled = True
        Me.comboTipoAs.Location = New System.Drawing.Point(196, 47)
        Me.comboTipoAs.Name = "comboTipoAs"
        Me.comboTipoAs.Size = New System.Drawing.Size(202, 23)
        Me.comboTipoAs.TabIndex = 9
        Me.comboTipoAs.ValueMember = "Id"
        '
        'comboTipoCa
        '
        Me.comboTipoCa.DisplayMember = "Tipo"
        Me.comboTipoCa.FormattingEnabled = True
        Me.comboTipoCa.Location = New System.Drawing.Point(196, 16)
        Me.comboTipoCa.Name = "comboTipoCa"
        Me.comboTipoCa.Size = New System.Drawing.Size(202, 23)
        Me.comboTipoCa.TabIndex = 8
        Me.comboTipoCa.ValueMember = "ID"
        '
        'panelSNET
        '
        Me.panelSNET.BackColor = System.Drawing.SystemColors.Control
        Me.panelSNET.Controls.Add(Me.checkNET)
        Me.panelSNET.Controls.Add(Me.btnSNETCancelar)
        Me.panelSNET.Controls.Add(Me.btnSNETAceptar)
        Me.panelSNET.Controls.Add(Me.Label11)
        Me.panelSNET.Controls.Add(Me.comboSerNET)
        Me.panelSNET.Location = New System.Drawing.Point(98, 96)
        Me.panelSNET.Name = "panelSNET"
        Me.panelSNET.Size = New System.Drawing.Size(472, 178)
        Me.panelSNET.TabIndex = 16
        Me.panelSNET.Visible = False
        '
        'checkNET
        '
        Me.checkNET.AutoSize = True
        Me.checkNET.Location = New System.Drawing.Point(116, 74)
        Me.checkNET.Name = "checkNET"
        Me.checkNET.Size = New System.Drawing.Size(79, 19)
        Me.checkNET.TabIndex = 16
        Me.checkNET.Text = "Cortesía"
        Me.checkNET.UseVisualStyleBackColor = True
        Me.checkNET.Visible = False
        '
        'btnSNETCancelar
        '
        Me.btnSNETCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnSNETCancelar.Name = "btnSNETCancelar"
        Me.btnSNETCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnSNETCancelar.TabIndex = 13
        Me.btnSNETCancelar.Text = "&Cancelar"
        Me.btnSNETCancelar.UseVisualStyleBackColor = True
        '
        'btnSNETAceptar
        '
        Me.btnSNETAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnSNETAceptar.Name = "btnSNETAceptar"
        Me.btnSNETAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnSNETAceptar.TabIndex = 12
        Me.btnSNETAceptar.Text = "&Aceptar"
        Me.btnSNETAceptar.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(44, 50)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(66, 15)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "Servicio: "
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'comboSerNET
        '
        Me.comboSerNET.DisplayMember = "Descripcion"
        Me.comboSerNET.FormattingEnabled = True
        Me.comboSerNET.Location = New System.Drawing.Point(116, 45)
        Me.comboSerNET.Name = "comboSerNET"
        Me.comboSerNET.Size = New System.Drawing.Size(316, 23)
        Me.comboSerNET.TabIndex = 6
        Me.comboSerNET.ValueMember = "Clv_Servicio"
        '
        'bnNETServicio
        '
        Me.bnNETServicio.AddNewItem = Nothing
        Me.bnNETServicio.CountItem = Nothing
        Me.bnNETServicio.DeleteItem = Nothing
        Me.bnNETServicio.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnNETServicio.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarSNET, Me.tsbAgregarSNET})
        Me.bnNETServicio.Location = New System.Drawing.Point(3, 28)
        Me.bnNETServicio.MoveFirstItem = Nothing
        Me.bnNETServicio.MoveLastItem = Nothing
        Me.bnNETServicio.MoveNextItem = Nothing
        Me.bnNETServicio.MovePreviousItem = Nothing
        Me.bnNETServicio.Name = "bnNETServicio"
        Me.bnNETServicio.PositionItem = Nothing
        Me.bnNETServicio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnNETServicio.Size = New System.Drawing.Size(666, 25)
        Me.bnNETServicio.TabIndex = 16
        Me.bnNETServicio.Text = "BindingNavigator1"
        '
        'tsbEliminarSNET
        '
        Me.tsbEliminarSNET.Image = CType(resources.GetObject("tsbEliminarSNET.Image"), System.Drawing.Image)
        Me.tsbEliminarSNET.Name = "tsbEliminarSNET"
        Me.tsbEliminarSNET.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarSNET.Size = New System.Drawing.Size(125, 22)
        Me.tsbEliminarSNET.Text = "Eliminar Servicio"
        '
        'tsbAgregarSNET
        '
        Me.tsbAgregarSNET.Image = CType(resources.GetObject("tsbAgregarSNET.Image"), System.Drawing.Image)
        Me.tsbAgregarSNET.Name = "tsbAgregarSNET"
        Me.tsbAgregarSNET.Size = New System.Drawing.Size(127, 22)
        Me.tsbAgregarSNET.Text = "Agregar Servicio"
        '
        'bnNETCablemodem
        '
        Me.bnNETCablemodem.AddNewItem = Nothing
        Me.bnNETCablemodem.CountItem = Nothing
        Me.bnNETCablemodem.DeleteItem = Nothing
        Me.bnNETCablemodem.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnNETCablemodem.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarNET, Me.tsbAgregarNET})
        Me.bnNETCablemodem.Location = New System.Drawing.Point(3, 3)
        Me.bnNETCablemodem.MoveFirstItem = Nothing
        Me.bnNETCablemodem.MoveLastItem = Nothing
        Me.bnNETCablemodem.MoveNextItem = Nothing
        Me.bnNETCablemodem.MovePreviousItem = Nothing
        Me.bnNETCablemodem.Name = "bnNETCablemodem"
        Me.bnNETCablemodem.PositionItem = Nothing
        Me.bnNETCablemodem.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnNETCablemodem.Size = New System.Drawing.Size(666, 25)
        Me.bnNETCablemodem.TabIndex = 14
        Me.bnNETCablemodem.Text = "BindingNavigator1"
        '
        'tsbEliminarNET
        '
        Me.tsbEliminarNET.Image = CType(resources.GetObject("tsbEliminarNET.Image"), System.Drawing.Image)
        Me.tsbEliminarNET.Name = "tsbEliminarNET"
        Me.tsbEliminarNET.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarNET.Size = New System.Drawing.Size(156, 22)
        Me.tsbEliminarNET.Text = "Eliminar Cablemodem"
        '
        'tsbAgregarNET
        '
        Me.tsbAgregarNET.Image = CType(resources.GetObject("tsbAgregarNET.Image"), System.Drawing.Image)
        Me.tsbAgregarNET.Name = "tsbAgregarNET"
        Me.tsbAgregarNET.Size = New System.Drawing.Size(158, 22)
        Me.tsbAgregarNET.Text = "Agregar Cablemodem"
        '
        'tvNET
        '
        Me.tvNET.Location = New System.Drawing.Point(73, 64)
        Me.tvNET.Name = "tvNET"
        Me.tvNET.Size = New System.Drawing.Size(521, 235)
        Me.tvNET.TabIndex = 0
        '
        'tpDIG
        '
        Me.tpDIG.Controls.Add(Me.lblDIG)
        Me.tpDIG.Controls.Add(Me.panelDIG)
        Me.tpDIG.Controls.Add(Me.bnDIGServicio)
        Me.tpDIG.Controls.Add(Me.bnDIGTarjeta)
        Me.tpDIG.Controls.Add(Me.tvDIG)
        Me.tpDIG.Location = New System.Drawing.Point(4, 24)
        Me.tpDIG.Name = "tpDIG"
        Me.tpDIG.Padding = New System.Windows.Forms.Padding(3)
        Me.tpDIG.Size = New System.Drawing.Size(672, 340)
        Me.tpDIG.TabIndex = 2
        Me.tpDIG.Text = "Digital"
        Me.tpDIG.UseVisualStyleBackColor = True
        '
        'lblDIG
        '
        Me.lblDIG.ForeColor = System.Drawing.Color.Red
        Me.lblDIG.Location = New System.Drawing.Point(3, 313)
        Me.lblDIG.Name = "lblDIG"
        Me.lblDIG.Size = New System.Drawing.Size(666, 24)
        Me.lblDIG.TabIndex = 18
        '
        'panelDIG
        '
        Me.panelDIG.BackColor = System.Drawing.SystemColors.Control
        Me.panelDIG.Controls.Add(Me.checkDIG)
        Me.panelDIG.Controls.Add(Me.btnDIGCancelar)
        Me.panelDIG.Controls.Add(Me.btnDIGAceptar)
        Me.panelDIG.Controls.Add(Me.Label9)
        Me.panelDIG.Controls.Add(Me.comboSerDIG)
        Me.panelDIG.Location = New System.Drawing.Point(97, 89)
        Me.panelDIG.Name = "panelDIG"
        Me.panelDIG.Size = New System.Drawing.Size(472, 178)
        Me.panelDIG.TabIndex = 16
        Me.panelDIG.Visible = False
        '
        'checkDIG
        '
        Me.checkDIG.AutoSize = True
        Me.checkDIG.Location = New System.Drawing.Point(137, 83)
        Me.checkDIG.Name = "checkDIG"
        Me.checkDIG.Size = New System.Drawing.Size(79, 19)
        Me.checkDIG.TabIndex = 10
        Me.checkDIG.Text = "Cortesía"
        Me.checkDIG.UseVisualStyleBackColor = True
        Me.checkDIG.Visible = False
        '
        'btnDIGCancelar
        '
        Me.btnDIGCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnDIGCancelar.Name = "btnDIGCancelar"
        Me.btnDIGCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnDIGCancelar.TabIndex = 9
        Me.btnDIGCancelar.Text = "&Cancelar"
        Me.btnDIGCancelar.UseVisualStyleBackColor = True
        '
        'btnDIGAceptar
        '
        Me.btnDIGAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnDIGAceptar.Name = "btnDIGAceptar"
        Me.btnDIGAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnDIGAceptar.TabIndex = 8
        Me.btnDIGAceptar.Text = "&Aceptar"
        Me.btnDIGAceptar.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(65, 62)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(66, 15)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Servicio: "
        '
        'comboSerDIG
        '
        Me.comboSerDIG.DisplayMember = "Descripcion"
        Me.comboSerDIG.FormattingEnabled = True
        Me.comboSerDIG.Location = New System.Drawing.Point(137, 54)
        Me.comboSerDIG.Name = "comboSerDIG"
        Me.comboSerDIG.Size = New System.Drawing.Size(295, 23)
        Me.comboSerDIG.TabIndex = 6
        Me.comboSerDIG.ValueMember = "Clv_Servicio"
        '
        'bnDIGServicio
        '
        Me.bnDIGServicio.AddNewItem = Nothing
        Me.bnDIGServicio.CountItem = Nothing
        Me.bnDIGServicio.DeleteItem = Nothing
        Me.bnDIGServicio.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnDIGServicio.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarSDIG, Me.tsbAgregarSDIG})
        Me.bnDIGServicio.Location = New System.Drawing.Point(3, 28)
        Me.bnDIGServicio.MoveFirstItem = Nothing
        Me.bnDIGServicio.MoveLastItem = Nothing
        Me.bnDIGServicio.MoveNextItem = Nothing
        Me.bnDIGServicio.MovePreviousItem = Nothing
        Me.bnDIGServicio.Name = "bnDIGServicio"
        Me.bnDIGServicio.PositionItem = Nothing
        Me.bnDIGServicio.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnDIGServicio.Size = New System.Drawing.Size(666, 25)
        Me.bnDIGServicio.TabIndex = 15
        Me.bnDIGServicio.Text = "BindingNavigator1"
        '
        'tsbEliminarSDIG
        '
        Me.tsbEliminarSDIG.Image = CType(resources.GetObject("tsbEliminarSDIG.Image"), System.Drawing.Image)
        Me.tsbEliminarSDIG.Name = "tsbEliminarSDIG"
        Me.tsbEliminarSDIG.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarSDIG.Size = New System.Drawing.Size(125, 22)
        Me.tsbEliminarSDIG.Text = "Eliminar Servicio"
        '
        'tsbAgregarSDIG
        '
        Me.tsbAgregarSDIG.Image = CType(resources.GetObject("tsbAgregarSDIG.Image"), System.Drawing.Image)
        Me.tsbAgregarSDIG.Name = "tsbAgregarSDIG"
        Me.tsbAgregarSDIG.Size = New System.Drawing.Size(127, 22)
        Me.tsbAgregarSDIG.Text = "Agregar Servicio"
        '
        'bnDIGTarjeta
        '
        Me.bnDIGTarjeta.AddNewItem = Nothing
        Me.bnDIGTarjeta.CountItem = Nothing
        Me.bnDIGTarjeta.DeleteItem = Nothing
        Me.bnDIGTarjeta.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnDIGTarjeta.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminarTDIG, Me.tsbAgregarTDIG})
        Me.bnDIGTarjeta.Location = New System.Drawing.Point(3, 3)
        Me.bnDIGTarjeta.MoveFirstItem = Nothing
        Me.bnDIGTarjeta.MoveLastItem = Nothing
        Me.bnDIGTarjeta.MoveNextItem = Nothing
        Me.bnDIGTarjeta.MovePreviousItem = Nothing
        Me.bnDIGTarjeta.Name = "bnDIGTarjeta"
        Me.bnDIGTarjeta.PositionItem = Nothing
        Me.bnDIGTarjeta.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnDIGTarjeta.Size = New System.Drawing.Size(666, 25)
        Me.bnDIGTarjeta.TabIndex = 14
        Me.bnDIGTarjeta.Text = "BindingNavigator1"
        '
        'tsbEliminarTDIG
        '
        Me.tsbEliminarTDIG.Image = CType(resources.GetObject("tsbEliminarTDIG.Image"), System.Drawing.Image)
        Me.tsbEliminarTDIG.Name = "tsbEliminarTDIG"
        Me.tsbEliminarTDIG.RightToLeftAutoMirrorImage = True
        Me.tsbEliminarTDIG.Size = New System.Drawing.Size(121, 22)
        Me.tsbEliminarTDIG.Text = "Eliminar Tarjeta"
        '
        'tsbAgregarTDIG
        '
        Me.tsbAgregarTDIG.Image = CType(resources.GetObject("tsbAgregarTDIG.Image"), System.Drawing.Image)
        Me.tsbAgregarTDIG.Name = "tsbAgregarTDIG"
        Me.tsbAgregarTDIG.Size = New System.Drawing.Size(123, 22)
        Me.tsbAgregarTDIG.Text = "Agregar Tarjeta"
        '
        'tvDIG
        '
        Me.tvDIG.Location = New System.Drawing.Point(73, 64)
        Me.tvDIG.Name = "tvDIG"
        Me.tvDIG.Size = New System.Drawing.Size(521, 235)
        Me.tvDIG.TabIndex = 0
        '
        'tpTEL
        '
        Me.tpTEL.Controls.Add(Me.lblTEL)
        Me.tpTEL.Controls.Add(Me.panelTelPaqAdi)
        Me.tpTEL.Controls.Add(Me.panelTelSerDig)
        Me.tpTEL.Controls.Add(Me.panelTelPlan)
        Me.tpTEL.Controls.Add(Me.tvTel)
        Me.tpTEL.Controls.Add(Me.bnTelPaqAdi)
        Me.tpTEL.Controls.Add(Me.bnTelSerDig)
        Me.tpTEL.Controls.Add(Me.bnTelPlan)
        Me.tpTEL.Location = New System.Drawing.Point(4, 24)
        Me.tpTEL.Name = "tpTEL"
        Me.tpTEL.Size = New System.Drawing.Size(672, 340)
        Me.tpTEL.TabIndex = 3
        Me.tpTEL.Text = "Telefonía"
        Me.tpTEL.UseVisualStyleBackColor = True
        '
        'lblTEL
        '
        Me.lblTEL.ForeColor = System.Drawing.Color.Red
        Me.lblTEL.Location = New System.Drawing.Point(3, 316)
        Me.lblTEL.Name = "lblTEL"
        Me.lblTEL.Size = New System.Drawing.Size(666, 24)
        Me.lblTEL.TabIndex = 21
        '
        'panelTelPaqAdi
        '
        Me.panelTelPaqAdi.BackColor = System.Drawing.SystemColors.Control
        Me.panelTelPaqAdi.Controls.Add(Me.btnTelPaqAdiCancelar)
        Me.panelTelPaqAdi.Controls.Add(Me.btnTelPaqAdiAceptar)
        Me.panelTelPaqAdi.Controls.Add(Me.Label20)
        Me.panelTelPaqAdi.Controls.Add(Me.comboTelPaqAdi)
        Me.panelTelPaqAdi.Location = New System.Drawing.Point(95, 108)
        Me.panelTelPaqAdi.Name = "panelTelPaqAdi"
        Me.panelTelPaqAdi.Size = New System.Drawing.Size(472, 178)
        Me.panelTelPaqAdi.TabIndex = 20
        Me.panelTelPaqAdi.Visible = False
        '
        'btnTelPaqAdiCancelar
        '
        Me.btnTelPaqAdiCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnTelPaqAdiCancelar.Name = "btnTelPaqAdiCancelar"
        Me.btnTelPaqAdiCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnTelPaqAdiCancelar.TabIndex = 9
        Me.btnTelPaqAdiCancelar.Text = "&Cancelar"
        Me.btnTelPaqAdiCancelar.UseVisualStyleBackColor = True
        '
        'btnTelPaqAdiAceptar
        '
        Me.btnTelPaqAdiAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnTelPaqAdiAceptar.Name = "btnTelPaqAdiAceptar"
        Me.btnTelPaqAdiAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnTelPaqAdiAceptar.TabIndex = 8
        Me.btnTelPaqAdiAceptar.Text = "&Aceptar"
        Me.btnTelPaqAdiAceptar.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(17, 70)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(127, 15)
        Me.Label20.TabIndex = 7
        Me.Label20.Text = "Paquete Adicional:"
        '
        'comboTelPaqAdi
        '
        Me.comboTelPaqAdi.DisplayMember = "Descripcion"
        Me.comboTelPaqAdi.FormattingEnabled = True
        Me.comboTelPaqAdi.Location = New System.Drawing.Point(149, 65)
        Me.comboTelPaqAdi.Name = "comboTelPaqAdi"
        Me.comboTelPaqAdi.Size = New System.Drawing.Size(289, 23)
        Me.comboTelPaqAdi.TabIndex = 6
        Me.comboTelPaqAdi.ValueMember = "Clv_Servicio"
        '
        'panelTelSerDig
        '
        Me.panelTelSerDig.BackColor = System.Drawing.SystemColors.Control
        Me.panelTelSerDig.Controls.Add(Me.btnTelSerDigCancelar)
        Me.panelTelSerDig.Controls.Add(Me.btnTelSerDigAceptar)
        Me.panelTelSerDig.Controls.Add(Me.Label19)
        Me.panelTelSerDig.Controls.Add(Me.comboTelSerDig)
        Me.panelTelSerDig.Location = New System.Drawing.Point(98, 111)
        Me.panelTelSerDig.Name = "panelTelSerDig"
        Me.panelTelSerDig.Size = New System.Drawing.Size(472, 178)
        Me.panelTelSerDig.TabIndex = 19
        Me.panelTelSerDig.Visible = False
        '
        'btnTelSerDigCancelar
        '
        Me.btnTelSerDigCancelar.Location = New System.Drawing.Point(333, 131)
        Me.btnTelSerDigCancelar.Name = "btnTelSerDigCancelar"
        Me.btnTelSerDigCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnTelSerDigCancelar.TabIndex = 9
        Me.btnTelSerDigCancelar.Text = "&Cancelar"
        Me.btnTelSerDigCancelar.UseVisualStyleBackColor = True
        '
        'btnTelSerDigAceptar
        '
        Me.btnTelSerDigAceptar.Location = New System.Drawing.Point(228, 131)
        Me.btnTelSerDigAceptar.Name = "btnTelSerDigAceptar"
        Me.btnTelSerDigAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnTelSerDigAceptar.TabIndex = 8
        Me.btnTelSerDigAceptar.Text = "&Aceptar"
        Me.btnTelSerDigAceptar.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(14, 67)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(108, 15)
        Me.Label19.TabIndex = 7
        Me.Label19.Text = "Servicio Digital:"
        '
        'comboTelSerDig
        '
        Me.comboTelSerDig.DisplayMember = "Descripcion"
        Me.comboTelSerDig.FormattingEnabled = True
        Me.comboTelSerDig.Location = New System.Drawing.Point(125, 64)
        Me.comboTelSerDig.Name = "comboTelSerDig"
        Me.comboTelSerDig.Size = New System.Drawing.Size(325, 23)
        Me.comboTelSerDig.TabIndex = 6
        Me.comboTelSerDig.ValueMember = "Clv_Servicio"
        '
        'panelTelPlan
        '
        Me.panelTelPlan.BackColor = System.Drawing.SystemColors.Control
        Me.panelTelPlan.Controls.Add(Me.grupoTel)
        Me.panelTelPlan.Controls.Add(Me.checkTEL)
        Me.panelTelPlan.Controls.Add(Me.btnTelPlanCancelar)
        Me.panelTelPlan.Controls.Add(Me.bntTelPlanAceptar)
        Me.panelTelPlan.Controls.Add(Me.Label18)
        Me.panelTelPlan.Controls.Add(Me.comboTelPlan)
        Me.panelTelPlan.Location = New System.Drawing.Point(101, 95)
        Me.panelTelPlan.Name = "panelTelPlan"
        Me.panelTelPlan.Size = New System.Drawing.Size(472, 197)
        Me.panelTelPlan.TabIndex = 18
        Me.panelTelPlan.Visible = False
        '
        'grupoTel
        '
        Me.grupoTel.Controls.Add(Me.comboTel)
        Me.grupoTel.Controls.Add(Me.radioPor)
        Me.grupoTel.Controls.Add(Me.radioAuto)
        Me.grupoTel.Controls.Add(Me.radioSel)
        Me.grupoTel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grupoTel.Location = New System.Drawing.Point(24, 51)
        Me.grupoTel.Name = "grupoTel"
        Me.grupoTel.Size = New System.Drawing.Size(426, 99)
        Me.grupoTel.TabIndex = 11
        Me.grupoTel.TabStop = False
        Me.grupoTel.Text = "Teléfono"
        '
        'comboTel
        '
        Me.comboTel.DisplayMember = "numero_tel"
        Me.comboTel.Enabled = False
        Me.comboTel.FormattingEnabled = True
        Me.comboTel.Location = New System.Drawing.Point(118, 42)
        Me.comboTel.Name = "comboTel"
        Me.comboTel.Size = New System.Drawing.Size(276, 23)
        Me.comboTel.TabIndex = 9
        Me.comboTel.ValueMember = "Clave"
        '
        'radioPor
        '
        Me.radioPor.AutoSize = True
        Me.radioPor.Location = New System.Drawing.Point(11, 71)
        Me.radioPor.Name = "radioPor"
        Me.radioPor.Size = New System.Drawing.Size(103, 19)
        Me.radioPor.TabIndex = 9
        Me.radioPor.Text = "Portabilidad"
        Me.radioPor.UseVisualStyleBackColor = True
        '
        'radioAuto
        '
        Me.radioAuto.AutoSize = True
        Me.radioAuto.Checked = True
        Me.radioAuto.Location = New System.Drawing.Point(11, 21)
        Me.radioAuto.Name = "radioAuto"
        Me.radioAuto.Size = New System.Drawing.Size(96, 19)
        Me.radioAuto.TabIndex = 7
        Me.radioAuto.TabStop = True
        Me.radioAuto.Text = "Automático"
        Me.radioAuto.UseVisualStyleBackColor = True
        '
        'radioSel
        '
        Me.radioSel.AutoSize = True
        Me.radioSel.Location = New System.Drawing.Point(11, 46)
        Me.radioSel.Name = "radioSel"
        Me.radioSel.Size = New System.Drawing.Size(101, 19)
        Me.radioSel.TabIndex = 8
        Me.radioSel.Text = "Seleccionar"
        Me.radioSel.UseVisualStyleBackColor = True
        '
        'checkTEL
        '
        Me.checkTEL.AutoSize = True
        Me.checkTEL.Location = New System.Drawing.Point(333, 106)
        Me.checkTEL.Name = "checkTEL"
        Me.checkTEL.Size = New System.Drawing.Size(79, 19)
        Me.checkTEL.TabIndex = 10
        Me.checkTEL.Text = "Cortesía"
        Me.checkTEL.UseVisualStyleBackColor = True
        Me.checkTEL.Visible = False
        '
        'btnTelPlanCancelar
        '
        Me.btnTelPlanCancelar.Location = New System.Drawing.Point(328, 161)
        Me.btnTelPlanCancelar.Name = "btnTelPlanCancelar"
        Me.btnTelPlanCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnTelPlanCancelar.TabIndex = 9
        Me.btnTelPlanCancelar.Text = "&Cancelar"
        Me.btnTelPlanCancelar.UseVisualStyleBackColor = True
        '
        'bntTelPlanAceptar
        '
        Me.bntTelPlanAceptar.Location = New System.Drawing.Point(223, 161)
        Me.bntTelPlanAceptar.Name = "bntTelPlanAceptar"
        Me.bntTelPlanAceptar.Size = New System.Drawing.Size(99, 27)
        Me.bntTelPlanAceptar.TabIndex = 8
        Me.bntTelPlanAceptar.Text = "&Aceptar"
        Me.bntTelPlanAceptar.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(21, 22)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(98, 15)
        Me.Label18.TabIndex = 7
        Me.Label18.Text = "Plan Tarifario:"
        '
        'comboTelPlan
        '
        Me.comboTelPlan.DisplayMember = "Descripcion"
        Me.comboTelPlan.FormattingEnabled = True
        Me.comboTelPlan.Location = New System.Drawing.Point(125, 14)
        Me.comboTelPlan.Name = "comboTelPlan"
        Me.comboTelPlan.Size = New System.Drawing.Size(325, 23)
        Me.comboTelPlan.TabIndex = 6
        Me.comboTelPlan.ValueMember = "Clv_Servicio"
        '
        'tvTel
        '
        Me.tvTel.Location = New System.Drawing.Point(73, 89)
        Me.tvTel.Name = "tvTel"
        Me.tvTel.Size = New System.Drawing.Size(521, 209)
        Me.tvTel.TabIndex = 17
        '
        'bnTelPaqAdi
        '
        Me.bnTelPaqAdi.AddNewItem = Nothing
        Me.bnTelPaqAdi.CountItem = Nothing
        Me.bnTelPaqAdi.DeleteItem = Nothing
        Me.bnTelPaqAdi.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnTelPaqAdi.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbPadAdiEliminar, Me.tsbPadAdiAgregar})
        Me.bnTelPaqAdi.Location = New System.Drawing.Point(0, 50)
        Me.bnTelPaqAdi.MoveFirstItem = Nothing
        Me.bnTelPaqAdi.MoveLastItem = Nothing
        Me.bnTelPaqAdi.MoveNextItem = Nothing
        Me.bnTelPaqAdi.MovePreviousItem = Nothing
        Me.bnTelPaqAdi.Name = "bnTelPaqAdi"
        Me.bnTelPaqAdi.PositionItem = Nothing
        Me.bnTelPaqAdi.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnTelPaqAdi.Size = New System.Drawing.Size(672, 25)
        Me.bnTelPaqAdi.TabIndex = 16
        Me.bnTelPaqAdi.Text = "BindingNavigator1"
        '
        'tsbPadAdiEliminar
        '
        Me.tsbPadAdiEliminar.Image = CType(resources.GetObject("tsbPadAdiEliminar.Image"), System.Drawing.Image)
        Me.tsbPadAdiEliminar.Name = "tsbPadAdiEliminar"
        Me.tsbPadAdiEliminar.RightToLeftAutoMirrorImage = True
        Me.tsbPadAdiEliminar.Size = New System.Drawing.Size(188, 22)
        Me.tsbPadAdiEliminar.Text = "Eliminar Paquete Adicional"
        '
        'tsbPadAdiAgregar
        '
        Me.tsbPadAdiAgregar.Image = CType(resources.GetObject("tsbPadAdiAgregar.Image"), System.Drawing.Image)
        Me.tsbPadAdiAgregar.Name = "tsbPadAdiAgregar"
        Me.tsbPadAdiAgregar.Size = New System.Drawing.Size(190, 22)
        Me.tsbPadAdiAgregar.Text = "Agregar Paquete Adicional"
        '
        'bnTelSerDig
        '
        Me.bnTelSerDig.AddNewItem = Nothing
        Me.bnTelSerDig.CountItem = Nothing
        Me.bnTelSerDig.DeleteItem = Nothing
        Me.bnTelSerDig.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnTelSerDig.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbSerDigEliminar, Me.tsbSerDigAgregar})
        Me.bnTelSerDig.Location = New System.Drawing.Point(0, 25)
        Me.bnTelSerDig.MoveFirstItem = Nothing
        Me.bnTelSerDig.MoveLastItem = Nothing
        Me.bnTelSerDig.MoveNextItem = Nothing
        Me.bnTelSerDig.MovePreviousItem = Nothing
        Me.bnTelSerDig.Name = "bnTelSerDig"
        Me.bnTelSerDig.PositionItem = Nothing
        Me.bnTelSerDig.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnTelSerDig.Size = New System.Drawing.Size(672, 25)
        Me.bnTelSerDig.TabIndex = 15
        Me.bnTelSerDig.Text = "BindingNavigator1"
        '
        'tsbSerDigEliminar
        '
        Me.tsbSerDigEliminar.Image = CType(resources.GetObject("tsbSerDigEliminar.Image"), System.Drawing.Image)
        Me.tsbSerDigEliminar.Name = "tsbSerDigEliminar"
        Me.tsbSerDigEliminar.RightToLeftAutoMirrorImage = True
        Me.tsbSerDigEliminar.Size = New System.Drawing.Size(168, 22)
        Me.tsbSerDigEliminar.Text = "Eliminar Servicio Digital"
        '
        'tsbSerDigAgregar
        '
        Me.tsbSerDigAgregar.Image = CType(resources.GetObject("tsbSerDigAgregar.Image"), System.Drawing.Image)
        Me.tsbSerDigAgregar.Name = "tsbSerDigAgregar"
        Me.tsbSerDigAgregar.Size = New System.Drawing.Size(170, 22)
        Me.tsbSerDigAgregar.Text = "Agregar Servicio Digital"
        '
        'bnTelPlan
        '
        Me.bnTelPlan.AddNewItem = Nothing
        Me.bnTelPlan.CountItem = Nothing
        Me.bnTelPlan.DeleteItem = Nothing
        Me.bnTelPlan.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnTelPlan.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbPlanEliminar, Me.tsbPlanAgregar})
        Me.bnTelPlan.Location = New System.Drawing.Point(0, 0)
        Me.bnTelPlan.MoveFirstItem = Nothing
        Me.bnTelPlan.MoveLastItem = Nothing
        Me.bnTelPlan.MoveNextItem = Nothing
        Me.bnTelPlan.MovePreviousItem = Nothing
        Me.bnTelPlan.Name = "bnTelPlan"
        Me.bnTelPlan.PositionItem = Nothing
        Me.bnTelPlan.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnTelPlan.Size = New System.Drawing.Size(672, 25)
        Me.bnTelPlan.TabIndex = 14
        Me.bnTelPlan.Text = "BindingNavigator1"
        '
        'tsbPlanEliminar
        '
        Me.tsbPlanEliminar.Image = CType(resources.GetObject("tsbPlanEliminar.Image"), System.Drawing.Image)
        Me.tsbPlanEliminar.Name = "tsbPlanEliminar"
        Me.tsbPlanEliminar.RightToLeftAutoMirrorImage = True
        Me.tsbPlanEliminar.Size = New System.Drawing.Size(158, 22)
        Me.tsbPlanEliminar.Text = "Eliminar Plan Tarifario"
        '
        'tsbPlanAgregar
        '
        Me.tsbPlanAgregar.Image = CType(resources.GetObject("tsbPlanAgregar.Image"), System.Drawing.Image)
        Me.tsbPlanAgregar.Name = "tsbPlanAgregar"
        Me.tsbPlanAgregar.Size = New System.Drawing.Size(160, 22)
        Me.tsbPlanAgregar.Text = "Agregar Plan Tarifario"
        '
        'btnNETCancelar
        '
        Me.btnNETCancelar.Location = New System.Drawing.Point(319, 174)
        Me.btnNETCancelar.Name = "btnNETCancelar"
        Me.btnNETCancelar.Size = New System.Drawing.Size(99, 27)
        Me.btnNETCancelar.TabIndex = 9
        Me.btnNETCancelar.Text = "&Cancelar"
        Me.btnNETCancelar.UseVisualStyleBackColor = True
        '
        'btnNETAceptar
        '
        Me.btnNETAceptar.Location = New System.Drawing.Point(214, 174)
        Me.btnNETAceptar.Name = "btnNETAceptar"
        Me.btnNETAceptar.Size = New System.Drawing.Size(99, 27)
        Me.btnNETAceptar.TabIndex = 8
        Me.btnNETAceptar.Text = "&Aceptar"
        Me.btnNETAceptar.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(133, 75)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Servicio: "
        '
        'comboSerNET2023
        '
        Me.comboSerNET2023.FormattingEnabled = True
        Me.comboSerNET2023.Location = New System.Drawing.Point(0, 0)
        Me.comboSerNET2023.Name = "comboSerNET2023"
        Me.comboSerNET2023.Size = New System.Drawing.Size(500, 21)
        Me.comboSerNET2023.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(38, 40)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 23)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Contrato:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CheckBoxSoloInternet)
        Me.GroupBox1.Controls.Add(Me.LabelCelular)
        Me.GroupBox1.Controls.Add(Me.LabelTelefono)
        Me.GroupBox1.Controls.Add(Me.LabelCiudad)
        Me.GroupBox1.Controls.Add(Me.LabelColonia)
        Me.GroupBox1.Controls.Add(Me.LabelNumero)
        Me.GroupBox1.Controls.Add(Me.LabelCalle)
        Me.GroupBox1.Controls.Add(Me.LabelNombre)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(24, 78)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(673, 174)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'CheckBoxSoloInternet
        '
        Me.CheckBoxSoloInternet.AutoSize = True
        Me.CheckBoxSoloInternet.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxSoloInternet.Enabled = False
        Me.CheckBoxSoloInternet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBoxSoloInternet.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CheckBoxSoloInternet.Location = New System.Drawing.Point(469, 20)
        Me.CheckBoxSoloInternet.Name = "CheckBoxSoloInternet"
        Me.CheckBoxSoloInternet.Size = New System.Drawing.Size(108, 19)
        Me.CheckBoxSoloInternet.TabIndex = 16
        Me.CheckBoxSoloInternet.Text = "Sólo Internet"
        Me.CheckBoxSoloInternet.UseVisualStyleBackColor = True
        '
        'LabelCelular
        '
        Me.LabelCelular.Location = New System.Drawing.Point(150, 115)
        Me.LabelCelular.Name = "LabelCelular"
        Me.LabelCelular.Size = New System.Drawing.Size(300, 19)
        Me.LabelCelular.TabIndex = 15
        Me.LabelCelular.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelTelefono
        '
        Me.LabelTelefono.Location = New System.Drawing.Point(150, 134)
        Me.LabelTelefono.Name = "LabelTelefono"
        Me.LabelTelefono.Size = New System.Drawing.Size(300, 19)
        Me.LabelTelefono.TabIndex = 14
        Me.LabelTelefono.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCiudad
        '
        Me.LabelCiudad.Location = New System.Drawing.Point(150, 96)
        Me.LabelCiudad.Name = "LabelCiudad"
        Me.LabelCiudad.Size = New System.Drawing.Size(300, 19)
        Me.LabelCiudad.TabIndex = 13
        Me.LabelCiudad.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelColonia
        '
        Me.LabelColonia.Location = New System.Drawing.Point(150, 77)
        Me.LabelColonia.Name = "LabelColonia"
        Me.LabelColonia.Size = New System.Drawing.Size(300, 19)
        Me.LabelColonia.TabIndex = 12
        Me.LabelColonia.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNumero
        '
        Me.LabelNumero.Location = New System.Drawing.Point(150, 58)
        Me.LabelNumero.Name = "LabelNumero"
        Me.LabelNumero.Size = New System.Drawing.Size(300, 19)
        Me.LabelNumero.TabIndex = 11
        Me.LabelNumero.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelCalle
        '
        Me.LabelCalle.Location = New System.Drawing.Point(150, 39)
        Me.LabelCalle.Name = "LabelCalle"
        Me.LabelCalle.Size = New System.Drawing.Size(300, 19)
        Me.LabelCalle.TabIndex = 10
        Me.LabelCalle.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'LabelNombre
        '
        Me.LabelNombre.Location = New System.Drawing.Point(150, 20)
        Me.LabelNombre.Name = "LabelNombre"
        Me.LabelNombre.Size = New System.Drawing.Size(300, 19)
        Me.LabelNombre.TabIndex = 9
        Me.LabelNombre.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(51, 115)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 19)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Celular: "
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label10.Location = New System.Drawing.Point(51, 134)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(100, 19)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Teléfono: "
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label12.Location = New System.Drawing.Point(51, 96)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 19)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Ciudad: "
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label13.Location = New System.Drawing.Point(51, 77)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 19)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "Colonia: "
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label14.Location = New System.Drawing.Point(51, 58)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(101, 19)
        Me.Label14.TabIndex = 3
        Me.Label14.Text = "#: "
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label15.Location = New System.Drawing.Point(51, 39)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 19)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Calle: "
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label16.Location = New System.Drawing.Point(51, 20)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 19)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Nombre: "
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'TextBoxContrato
        '
        Me.TextBoxContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxContrato.Location = New System.Drawing.Point(124, 42)
        Me.TextBoxContrato.Name = "TextBoxContrato"
        Me.TextBoxContrato.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxContrato.TabIndex = 19
        '
        'ButtonBuscar
        '
        Me.ButtonBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBuscar.Location = New System.Drawing.Point(230, 40)
        Me.ButtonBuscar.Name = "ButtonBuscar"
        Me.ButtonBuscar.Size = New System.Drawing.Size(43, 23)
        Me.ButtonBuscar.TabIndex = 18
        Me.ButtonBuscar.Text = "..."
        Me.ButtonBuscar.UseVisualStyleBackColor = True
        '
        'bnRecontratar
        '
        Me.bnRecontratar.AddNewItem = Nothing
        Me.bnRecontratar.CountItem = Nothing
        Me.bnRecontratar.DeleteItem = Nothing
        Me.bnRecontratar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnRecontratar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbRecon})
        Me.bnRecontratar.Location = New System.Drawing.Point(0, 0)
        Me.bnRecontratar.MoveFirstItem = Nothing
        Me.bnRecontratar.MoveLastItem = Nothing
        Me.bnRecontratar.MoveNextItem = Nothing
        Me.bnRecontratar.MovePreviousItem = Nothing
        Me.bnRecontratar.Name = "bnRecontratar"
        Me.bnRecontratar.PositionItem = Nothing
        Me.bnRecontratar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnRecontratar.Size = New System.Drawing.Size(731, 25)
        Me.bnRecontratar.TabIndex = 22
        Me.bnRecontratar.Text = "BindingNavigator1"
        '
        'tsbRecon
        '
        Me.tsbRecon.Image = CType(resources.GetObject("tsbRecon.Image"), System.Drawing.Image)
        Me.tsbRecon.Name = "tsbRecon"
        Me.tsbRecon.Size = New System.Drawing.Size(101, 22)
        Me.tsbRecon.Text = "Recontratar"
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(579, 656)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 23
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'panelCombo
        '
        Me.panelCombo.BackColor = System.Drawing.Color.WhiteSmoke
        Me.panelCombo.Controls.Add(Me.lblCombo)
        Me.panelCombo.Controls.Add(Me.btnCancelarCombo)
        Me.panelCombo.Controls.Add(Me.btnAceptarCombo)
        Me.panelCombo.Controls.Add(Me.groupTEL)
        Me.panelCombo.Controls.Add(Me.groupDIG)
        Me.panelCombo.Controls.Add(Me.groupNET)
        Me.panelCombo.Controls.Add(Me.groupTV)
        Me.panelCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panelCombo.Location = New System.Drawing.Point(141, 72)
        Me.panelCombo.Name = "panelCombo"
        Me.panelCombo.Size = New System.Drawing.Size(453, 578)
        Me.panelCombo.TabIndex = 24
        Me.panelCombo.Visible = False
        '
        'lblCombo
        '
        Me.lblCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCombo.ForeColor = System.Drawing.Color.Red
        Me.lblCombo.Location = New System.Drawing.Point(9, 10)
        Me.lblCombo.Name = "lblCombo"
        Me.lblCombo.Size = New System.Drawing.Size(434, 37)
        Me.lblCombo.TabIndex = 11
        Me.lblCombo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnCancelarCombo
        '
        Me.btnCancelarCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelarCombo.Location = New System.Drawing.Point(307, 526)
        Me.btnCancelarCombo.Name = "btnCancelarCombo"
        Me.btnCancelarCombo.Size = New System.Drawing.Size(136, 36)
        Me.btnCancelarCombo.TabIndex = 6
        Me.btnCancelarCombo.Text = "&CANCELAR"
        Me.btnCancelarCombo.UseVisualStyleBackColor = True
        '
        'btnAceptarCombo
        '
        Me.btnAceptarCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptarCombo.Location = New System.Drawing.Point(165, 526)
        Me.btnAceptarCombo.Name = "btnAceptarCombo"
        Me.btnAceptarCombo.Size = New System.Drawing.Size(136, 36)
        Me.btnAceptarCombo.TabIndex = 4
        Me.btnAceptarCombo.Text = "&ACEPTAR"
        Me.btnAceptarCombo.UseVisualStyleBackColor = True
        '
        'groupTEL
        '
        Me.groupTEL.Controls.Add(Me.lblSerTEL)
        Me.groupTEL.Controls.Add(Me.comboTelCombo)
        Me.groupTEL.Controls.Add(Me.radioPorCombo)
        Me.groupTEL.Controls.Add(Me.radioAutoCombo)
        Me.groupTEL.Controls.Add(Me.radioSelCombo)
        Me.groupTEL.Location = New System.Drawing.Point(24, 360)
        Me.groupTEL.Name = "groupTEL"
        Me.groupTEL.Size = New System.Drawing.Size(402, 134)
        Me.groupTEL.TabIndex = 3
        Me.groupTEL.TabStop = False
        Me.groupTEL.Text = "Telefonía"
        '
        'lblSerTEL
        '
        Me.lblSerTEL.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerTEL.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblSerTEL.Location = New System.Drawing.Point(10, 17)
        Me.lblSerTEL.Name = "lblSerTEL"
        Me.lblSerTEL.Size = New System.Drawing.Size(386, 22)
        Me.lblSerTEL.TabIndex = 13
        '
        'comboTelCombo
        '
        Me.comboTelCombo.DisplayMember = "numero_tel"
        Me.comboTelCombo.Enabled = False
        Me.comboTelCombo.FormattingEnabled = True
        Me.comboTelCombo.Location = New System.Drawing.Point(150, 62)
        Me.comboTelCombo.Name = "comboTelCombo"
        Me.comboTelCombo.Size = New System.Drawing.Size(222, 23)
        Me.comboTelCombo.TabIndex = 9
        Me.comboTelCombo.ValueMember = "clave"
        '
        'radioPorCombo
        '
        Me.radioPorCombo.AutoSize = True
        Me.radioPorCombo.Location = New System.Drawing.Point(38, 91)
        Me.radioPorCombo.Name = "radioPorCombo"
        Me.radioPorCombo.Size = New System.Drawing.Size(103, 19)
        Me.radioPorCombo.TabIndex = 9
        Me.radioPorCombo.Text = "Portabilidad"
        Me.radioPorCombo.UseVisualStyleBackColor = True
        '
        'radioAutoCombo
        '
        Me.radioAutoCombo.AutoSize = True
        Me.radioAutoCombo.Checked = True
        Me.radioAutoCombo.Location = New System.Drawing.Point(38, 41)
        Me.radioAutoCombo.Name = "radioAutoCombo"
        Me.radioAutoCombo.Size = New System.Drawing.Size(96, 19)
        Me.radioAutoCombo.TabIndex = 7
        Me.radioAutoCombo.TabStop = True
        Me.radioAutoCombo.Text = "Automático"
        Me.radioAutoCombo.UseVisualStyleBackColor = True
        '
        'radioSelCombo
        '
        Me.radioSelCombo.AutoSize = True
        Me.radioSelCombo.Location = New System.Drawing.Point(38, 66)
        Me.radioSelCombo.Name = "radioSelCombo"
        Me.radioSelCombo.Size = New System.Drawing.Size(101, 19)
        Me.radioSelCombo.TabIndex = 8
        Me.radioSelCombo.Text = "Seleccionar"
        Me.radioSelCombo.UseVisualStyleBackColor = True
        '
        'groupDIG
        '
        Me.groupDIG.Controls.Add(Me.lblSerDIG)
        Me.groupDIG.Controls.Add(Me.Label22)
        Me.groupDIG.Controls.Add(Me.txtNumCajas)
        Me.groupDIG.Location = New System.Drawing.Point(24, 280)
        Me.groupDIG.Name = "groupDIG"
        Me.groupDIG.Size = New System.Drawing.Size(402, 74)
        Me.groupDIG.TabIndex = 2
        Me.groupDIG.TabStop = False
        Me.groupDIG.Text = "Premium"
        '
        'lblSerDIG
        '
        Me.lblSerDIG.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerDIG.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblSerDIG.Location = New System.Drawing.Point(6, 17)
        Me.lblSerDIG.Name = "lblSerDIG"
        Me.lblSerDIG.Size = New System.Drawing.Size(386, 22)
        Me.lblSerDIG.TabIndex = 12
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(35, 46)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(146, 15)
        Me.Label22.TabIndex = 7
        Me.Label22.Text = "Número de Aparatos: "
        '
        'txtNumCajas
        '
        Me.txtNumCajas.Location = New System.Drawing.Point(187, 39)
        Me.txtNumCajas.Name = "txtNumCajas"
        Me.txtNumCajas.Size = New System.Drawing.Size(100, 21)
        Me.txtNumCajas.TabIndex = 10
        '
        'groupNET
        '
        Me.groupNET.Controls.Add(Me.lblSerNET)
        Me.groupNET.Controls.Add(Me.Label23)
        Me.groupNET.Controls.Add(Me.comboTipoCaCombo)
        Me.groupNET.Location = New System.Drawing.Point(24, 177)
        Me.groupNET.Name = "groupNET"
        Me.groupNET.Size = New System.Drawing.Size(402, 97)
        Me.groupNET.TabIndex = 1
        Me.groupNET.TabStop = False
        Me.groupNET.Text = "Internet"
        '
        'lblSerNET
        '
        Me.lblSerNET.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerNET.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblSerNET.Location = New System.Drawing.Point(6, 18)
        Me.lblSerNET.Name = "lblSerNET"
        Me.lblSerNET.Size = New System.Drawing.Size(386, 22)
        Me.lblSerNET.TabIndex = 11
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(48, 38)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(132, 15)
        Me.Label23.TabIndex = 8
        Me.Label23.Text = "Tipo Cablemodem: "
        '
        'comboTipoCaCombo
        '
        Me.comboTipoCaCombo.DisplayMember = "Tipo"
        Me.comboTipoCaCombo.FormattingEnabled = True
        Me.comboTipoCaCombo.Location = New System.Drawing.Point(51, 56)
        Me.comboTipoCaCombo.Name = "comboTipoCaCombo"
        Me.comboTipoCaCombo.Size = New System.Drawing.Size(292, 23)
        Me.comboTipoCaCombo.TabIndex = 8
        Me.comboTipoCaCombo.ValueMember = "Id"
        '
        'groupTV
        '
        Me.groupTV.Controls.Add(Me.lblSerTV)
        Me.groupTV.Controls.Add(Me.Label24)
        Me.groupTV.Controls.Add(Me.txtSinPagoCombo)
        Me.groupTV.Controls.Add(Me.txtConPagoCombo)
        Me.groupTV.Controls.Add(Me.Label21)
        Me.groupTV.Location = New System.Drawing.Point(24, 67)
        Me.groupTV.Name = "groupTV"
        Me.groupTV.Size = New System.Drawing.Size(402, 104)
        Me.groupTV.TabIndex = 0
        Me.groupTV.TabStop = False
        Me.groupTV.Text = "Televisión"
        '
        'lblSerTV
        '
        Me.lblSerTV.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerTV.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblSerTV.Location = New System.Drawing.Point(10, 17)
        Me.lblSerTV.Name = "lblSerTV"
        Me.lblSerTV.Size = New System.Drawing.Size(386, 22)
        Me.lblSerTV.TabIndex = 10
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(88, 48)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(91, 15)
        Me.Label24.TabIndex = 9
        Me.Label24.Text = "Tv Sin Pago: "
        '
        'txtSinPagoCombo
        '
        Me.txtSinPagoCombo.Location = New System.Drawing.Point(187, 42)
        Me.txtSinPagoCombo.Name = "txtSinPagoCombo"
        Me.txtSinPagoCombo.Size = New System.Drawing.Size(100, 21)
        Me.txtSinPagoCombo.TabIndex = 9
        '
        'txtConPagoCombo
        '
        Me.txtConPagoCombo.Location = New System.Drawing.Point(187, 68)
        Me.txtConPagoCombo.Name = "txtConPagoCombo"
        Me.txtConPagoCombo.Size = New System.Drawing.Size(100, 21)
        Me.txtConPagoCombo.TabIndex = 7
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(86, 74)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(95, 15)
        Me.Label21.TabIndex = 5
        Me.Label21.Text = "Tv Con Pago: "
        '
        'panelSelCombo
        '
        Me.panelSelCombo.BackColor = System.Drawing.Color.WhiteSmoke
        Me.panelSelCombo.Controls.Add(Me.btnCancelarSelCombo)
        Me.panelSelCombo.Controls.Add(Me.btnAceptarSelCombo)
        Me.panelSelCombo.Controls.Add(Me.comboCombo)
        Me.panelSelCombo.Controls.Add(Me.Label25)
        Me.panelSelCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panelSelCombo.Location = New System.Drawing.Point(118, 256)
        Me.panelSelCombo.Name = "panelSelCombo"
        Me.panelSelCombo.Size = New System.Drawing.Size(496, 173)
        Me.panelSelCombo.TabIndex = 25
        Me.panelSelCombo.Visible = False
        '
        'btnCancelarSelCombo
        '
        Me.btnCancelarSelCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelarSelCombo.Location = New System.Drawing.Point(348, 125)
        Me.btnCancelarSelCombo.Name = "btnCancelarSelCombo"
        Me.btnCancelarSelCombo.Size = New System.Drawing.Size(136, 36)
        Me.btnCancelarSelCombo.TabIndex = 3
        Me.btnCancelarSelCombo.Text = "&CANCELAR"
        Me.btnCancelarSelCombo.UseVisualStyleBackColor = True
        '
        'btnAceptarSelCombo
        '
        Me.btnAceptarSelCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptarSelCombo.Location = New System.Drawing.Point(206, 125)
        Me.btnAceptarSelCombo.Name = "btnAceptarSelCombo"
        Me.btnAceptarSelCombo.Size = New System.Drawing.Size(136, 36)
        Me.btnAceptarSelCombo.TabIndex = 2
        Me.btnAceptarSelCombo.Text = "&ACEPTAR"
        Me.btnAceptarSelCombo.UseVisualStyleBackColor = True
        '
        'comboCombo
        '
        Me.comboCombo.DisplayMember = "Descripcion"
        Me.comboCombo.FormattingEnabled = True
        Me.comboCombo.Location = New System.Drawing.Point(54, 56)
        Me.comboCombo.Name = "comboCombo"
        Me.comboCombo.Size = New System.Drawing.Size(393, 23)
        Me.comboCombo.TabIndex = 1
        Me.comboCombo.ValueMember = "Clv_Descuento"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(51, 36)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(127, 15)
        Me.Label25.TabIndex = 0
        Me.Label25.Text = "Selecciona Combo"
        '
        'FrmRecontratacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(731, 704)
        Me.Controls.Add(Me.panelSelCombo)
        Me.Controls.Add(Me.panelCombo)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.bnRecontratar)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TextBoxContrato)
        Me.Controls.Add(Me.ButtonBuscar)
        Me.Controls.Add(Me.tcServicios)
        Me.Name = "FrmRecontratacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Recontratación"
        Me.tcServicios.ResumeLayout(False)
        Me.tpTV.ResumeLayout(False)
        Me.tpTV.PerformLayout()
        Me.panelTV.ResumeLayout(False)
        Me.panelTV.PerformLayout()
        CType(Me.bnTV, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTV.ResumeLayout(False)
        Me.bnTV.PerformLayout()
        Me.tpNET.ResumeLayout(False)
        Me.tpNET.PerformLayout()
        Me.panelCNET.ResumeLayout(False)
        Me.panelCNET.PerformLayout()
        Me.panelSNET.ResumeLayout(False)
        Me.panelSNET.PerformLayout()
        CType(Me.bnNETServicio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnNETServicio.ResumeLayout(False)
        Me.bnNETServicio.PerformLayout()
        CType(Me.bnNETCablemodem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnNETCablemodem.ResumeLayout(False)
        Me.bnNETCablemodem.PerformLayout()
        Me.tpDIG.ResumeLayout(False)
        Me.tpDIG.PerformLayout()
        Me.panelDIG.ResumeLayout(False)
        Me.panelDIG.PerformLayout()
        CType(Me.bnDIGServicio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnDIGServicio.ResumeLayout(False)
        Me.bnDIGServicio.PerformLayout()
        CType(Me.bnDIGTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnDIGTarjeta.ResumeLayout(False)
        Me.bnDIGTarjeta.PerformLayout()
        Me.tpTEL.ResumeLayout(False)
        Me.tpTEL.PerformLayout()
        Me.panelTelPaqAdi.ResumeLayout(False)
        Me.panelTelPaqAdi.PerformLayout()
        Me.panelTelSerDig.ResumeLayout(False)
        Me.panelTelSerDig.PerformLayout()
        Me.panelTelPlan.ResumeLayout(False)
        Me.panelTelPlan.PerformLayout()
        Me.grupoTel.ResumeLayout(False)
        Me.grupoTel.PerformLayout()
        CType(Me.bnTelPaqAdi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTelPaqAdi.ResumeLayout(False)
        Me.bnTelPaqAdi.PerformLayout()
        CType(Me.bnTelSerDig, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTelSerDig.ResumeLayout(False)
        Me.bnTelSerDig.PerformLayout()
        CType(Me.bnTelPlan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTelPlan.ResumeLayout(False)
        Me.bnTelPlan.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.bnRecontratar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnRecontratar.ResumeLayout(False)
        Me.bnRecontratar.PerformLayout()
        Me.panelCombo.ResumeLayout(False)
        Me.groupTEL.ResumeLayout(False)
        Me.groupTEL.PerformLayout()
        Me.groupDIG.ResumeLayout(False)
        Me.groupDIG.PerformLayout()
        Me.groupNET.ResumeLayout(False)
        Me.groupNET.PerformLayout()
        Me.groupTV.ResumeLayout(False)
        Me.groupTV.PerformLayout()
        Me.panelSelCombo.ResumeLayout(False)
        Me.panelSelCombo.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tcServicios As System.Windows.Forms.TabControl
    Friend WithEvents tpTV As System.Windows.Forms.TabPage
    Friend WithEvents tpNET As System.Windows.Forms.TabPage
    Friend WithEvents tpDIG As System.Windows.Forms.TabPage
    Friend WithEvents tvTV As System.Windows.Forms.TreeView
    Friend WithEvents tvNET As System.Windows.Forms.TreeView
    Friend WithEvents tvDIG As System.Windows.Forms.TreeView
    Friend WithEvents bnTV As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarTV As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarTV As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnNETCablemodem As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarNET As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarNET As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnDIGServicio As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarSDIG As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarSDIG As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnDIGTarjeta As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarTDIG As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarTDIG As System.Windows.Forms.ToolStripButton
    Friend WithEvents panelTV As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents comboSerTV As System.Windows.Forms.ComboBox
    Friend WithEvents txtConPago As System.Windows.Forms.TextBox
    Friend WithEvents txtSinPago As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnTVCancelar As System.Windows.Forms.Button
    Friend WithEvents btnTVAceptar As System.Windows.Forms.Button
    Friend WithEvents panelCNET As System.Windows.Forms.Panel
    Friend WithEvents btnCNETCancelar As System.Windows.Forms.Button
    Friend WithEvents btnCNETAceptar As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents comboTipoAs As System.Windows.Forms.ComboBox
    Friend WithEvents comboTipoCa As System.Windows.Forms.ComboBox
    Friend WithEvents panelDIG As System.Windows.Forms.Panel
    Friend WithEvents btnDIGCancelar As System.Windows.Forms.Button
    Friend WithEvents btnDIGAceptar As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents comboSerDIG As System.Windows.Forms.ComboBox
    Friend WithEvents btnNETCancelar As System.Windows.Forms.Button
    Friend WithEvents btnNETAceptar As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents comboSerNET2023 As System.Windows.Forms.ComboBox
    Friend WithEvents bnNETServicio As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminarSNET As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbAgregarSNET As System.Windows.Forms.ToolStripButton
    Friend WithEvents panelSNET As System.Windows.Forms.Panel
    Friend WithEvents btnSNETCancelar As System.Windows.Forms.Button
    Friend WithEvents btnSNETAceptar As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents comboSerNET As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxSoloInternet As System.Windows.Forms.CheckBox
    Friend WithEvents LabelCelular As System.Windows.Forms.Label
    Friend WithEvents LabelTelefono As System.Windows.Forms.Label
    Friend WithEvents LabelCiudad As System.Windows.Forms.Label
    Friend WithEvents LabelColonia As System.Windows.Forms.Label
    Friend WithEvents LabelNumero As System.Windows.Forms.Label
    Friend WithEvents LabelCalle As System.Windows.Forms.Label
    Friend WithEvents LabelNombre As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextBoxContrato As System.Windows.Forms.TextBox
    Friend WithEvents ButtonBuscar As System.Windows.Forms.Button
    Friend WithEvents lblTV As System.Windows.Forms.Label
    Friend WithEvents lblNET As System.Windows.Forms.Label
    Friend WithEvents lblDIG As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents comboTipoSer As System.Windows.Forms.ComboBox
    Friend WithEvents bnRecontratar As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbRecon As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents tpTEL As System.Windows.Forms.TabPage
    Friend WithEvents panelTelPaqAdi As System.Windows.Forms.Panel
    Friend WithEvents btnTelPaqAdiCancelar As System.Windows.Forms.Button
    Friend WithEvents btnTelPaqAdiAceptar As System.Windows.Forms.Button
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents comboTelPaqAdi As System.Windows.Forms.ComboBox
    Friend WithEvents panelTelSerDig As System.Windows.Forms.Panel
    Friend WithEvents btnTelSerDigCancelar As System.Windows.Forms.Button
    Friend WithEvents btnTelSerDigAceptar As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents comboTelSerDig As System.Windows.Forms.ComboBox
    Friend WithEvents panelTelPlan As System.Windows.Forms.Panel
    Friend WithEvents btnTelPlanCancelar As System.Windows.Forms.Button
    Friend WithEvents bntTelPlanAceptar As System.Windows.Forms.Button
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents comboTelPlan As System.Windows.Forms.ComboBox
    Friend WithEvents tvTel As System.Windows.Forms.TreeView
    Friend WithEvents bnTelPaqAdi As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbPadAdiEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbPadAdiAgregar As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnTelSerDig As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbSerDigEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbSerDigAgregar As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnTelPlan As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbPlanEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbPlanAgregar As System.Windows.Forms.ToolStripButton
    Friend WithEvents checkTV As System.Windows.Forms.CheckBox
    Friend WithEvents checkNET As System.Windows.Forms.CheckBox
    Friend WithEvents checkDIG As System.Windows.Forms.CheckBox
    Friend WithEvents checkTEL As System.Windows.Forms.CheckBox
    Friend WithEvents lblTEL As System.Windows.Forms.Label
    Friend WithEvents panelCombo As System.Windows.Forms.Panel
    Friend WithEvents btnCancelarCombo As System.Windows.Forms.Button
    Friend WithEvents btnAceptarCombo As System.Windows.Forms.Button
    Friend WithEvents groupTEL As System.Windows.Forms.GroupBox
    Friend WithEvents comboTelCombo As System.Windows.Forms.ComboBox
    Friend WithEvents radioPorCombo As System.Windows.Forms.RadioButton
    Friend WithEvents radioAutoCombo As System.Windows.Forms.RadioButton
    Friend WithEvents radioSelCombo As System.Windows.Forms.RadioButton
    Friend WithEvents groupDIG As System.Windows.Forms.GroupBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtNumCajas As System.Windows.Forms.TextBox
    Friend WithEvents groupNET As System.Windows.Forms.GroupBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents comboTipoCaCombo As System.Windows.Forms.ComboBox
    Friend WithEvents groupTV As System.Windows.Forms.GroupBox
    Friend WithEvents lblSerTV As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtSinPagoCombo As System.Windows.Forms.TextBox
    Friend WithEvents txtConPagoCombo As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents lblCombo As System.Windows.Forms.Label
    Friend WithEvents lblSerTEL As System.Windows.Forms.Label
    Friend WithEvents lblSerDIG As System.Windows.Forms.Label
    Friend WithEvents lblSerNET As System.Windows.Forms.Label
    Friend WithEvents panelSelCombo As System.Windows.Forms.Panel
    Friend WithEvents btnCancelarSelCombo As System.Windows.Forms.Button
    Friend WithEvents btnAceptarSelCombo As System.Windows.Forms.Button
    Friend WithEvents comboCombo As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents grupoTel As System.Windows.Forms.GroupBox
    Friend WithEvents comboTel As System.Windows.Forms.ComboBox
    Friend WithEvents radioPor As System.Windows.Forms.RadioButton
    Friend WithEvents radioAuto As System.Windows.Forms.RadioButton
    Friend WithEvents radioSel As System.Windows.Forms.RadioButton
End Class
