Imports System.Data.SqlClient
Public Class FrmSelTecnico_Rep

    Private Sub FrmSelTecnico_Rep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.Muestra_Seleccion_Tecnicostmp_NuevoTableAdapter.Connection = CON2
        Me.Muestra_Seleccion_Tecnicostmp_NuevoTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Seleccion_Tecnicostmp_Nuevo, LocClv_session)
        Me.Muestra_Seleccion_Tecnicostmp_ConsultaTableAdapter.Connection = CON2
        Me.Muestra_Seleccion_Tecnicostmp_ConsultaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Seleccion_Tecnicostmp_Consulta, LocClv_session)
        CON2.Close()



    End Sub
    Private Sub Muestra(ByVal clv_session As Long)
        Dim CON1 As New SqlConnection(MiConexion)
        CON1.Open()
        Me.Muestra_Seleccion_Tecnicos_ConsultaTableAdapter.Connection = CON1
        Me.Muestra_Seleccion_Tecnicos_ConsultaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Seleccion_Tecnicos_Consulta, LocClv_session)
        Me.Muestra_Seleccion_Tecnicostmp_ConsultaTableAdapter.Connection = CON1
        Me.Muestra_Seleccion_Tecnicostmp_ConsultaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Seleccion_Tecnicostmp_Consulta, LocClv_session)
        CON1.Close()
    End Sub




    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.InsertaTOSeleccion_TecnicoTableAdapter.Connection = CON
        Me.InsertaTOSeleccion_TecnicoTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaTOSeleccion_Tecnico, LocClv_session)
        CON.Close()
        Muestra(LocClv_session)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON3 As New SqlConnection(MiConexion)
        CON3.Open()
        Me.Insertauno_Seleccion_TecnicoTableAdapter.Connection = CON3
        Me.Insertauno_Seleccion_TecnicoTableAdapter.Fill(Me.ProcedimientosArnoldo2.Insertauno_Seleccion_Tecnico, LocClv_session, CLng(Me.ListBox1.SelectedValue))
        CON3.Close()
        Muestra(LocClv_session)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON4 As New SqlConnection(MiConexion)
        CON4.Open()
        Me.Insertauno_Seleccion_Tecnico_tmpTableAdapter.Connection = CON4
        Me.Insertauno_Seleccion_Tecnico_tmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.Insertauno_Seleccion_Tecnico_tmp, LocClv_session, CLng(Me.ListBox2.SelectedValue))
        CON4.Close()
        Muestra(LocClv_session)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON5 As New SqlConnection(MiConexion)
        CON5.Open()
        Me.InsertaTOSeleccion_Tecnico_tmpTableAdapter.Connection = CON5
        Me.InsertaTOSeleccion_Tecnico_tmpTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaTOSeleccion_Tecnico_tmp, LocClv_session)
        CON5.Close()
        Muestra(LocClv_session)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim cont As Integer = 0
        cont = Me.ListBox2.Items.Count()
        If cont = 0 Then
            MsgBox("Seleccione Al Menos un T�cnico", MsgBoxStyle.Information)
        ElseIf cont > 0 Then
            FrmSelTrabajo_Rep.Show()
            Me.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class
'Public Class Tecnico


'    Private _id As Integer
'    Private _nombre As String

'    Private Sub Id()
'        Return _id
'    End Sub
'    Private Sub Nombre()
'        Return _nombre
'    End Sub
'    Public Sub persona(ByVal id As Integer, ByVal nombre As String)
'        this._id = id
'        this._nombre = nombre
'    End Sub
'End Class

