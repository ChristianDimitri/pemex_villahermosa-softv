﻿Imports System.Data.SqlClient
Imports System.Math


Public Class FrmRoboDeSeñal

    Dim RoboSeñal As New classRoboDeSeñal
    Dim monto As Double
    Dim DataTable As DataTable = Nothing
    'Guardar
    Private Sub ConRoboDeSeñalBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConRoboDeSeñalBindingNavigatorSaveItem.Click

        If Me.DescripcionTextBox.Text.Length = 0 Then
            MsgBox("Captura la Descripción.", MsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            If Me.txtMonto.Text = "" Then
                Me.txtMonto.Text = 0
            End If
            If IsNumeric(Me.txtMonto.Text) = False Then
                MsgBox("El campo Monto sólo acepta valores numéricos")
                Exit Sub
            End If
            RoboSeñal.Actualiza(eGloContrato, Me.DescripcionTextBox.Text, Convert.ToDecimal(Me.txtMonto.Text), cbxCobrado.Checked)
            MsgBox(mensaje5)
            Me.BindingNavigatorDeleteItem.Enabled = True
        Catch
            MsgBox("Se ha producido un Error.")
        End Try
    End Sub
    'Eliminar
    Private Sub BindingNavigatorDeleteItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Try
            RoboSeñal.Elimina(eGloContrato)
            MsgBox(mensaje6)
            cbxCobrado.Checked = False
            DescripcionTextBox.Text = ""
            txtMonto.Text = ""
            Me.BindingNavigatorDeleteItem.Enabled = False
        Catch
            MsgBox("Se ha producido un Error.")
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub FrmRoboDeSeñal_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        eRobo = True
    End Sub

    Private Sub FrmRoboDeSeñal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       Try
            colorea(Me, Me.Name)
            If OpcionCli = "C" Then
                DataTable = RoboSeñal.buscaDT(eGloContrato)
                If (DataTable.Rows.Count > 0) Then
                    DescripcionTextBox.Text = DataTable.Rows(0)(1).ToString
                    txtMonto.Text = Round(Convert.ToDecimal(DataTable.Rows(0)(2)), 2)
                    If Convert.ToBoolean(DataTable.Rows(0)(3)) = True Then
                        cbxCobrado.Checked = True
                    Else
                        cbxCobrado.Checked = False
                    End If
                Else
                    DescripcionTextBox.Text = ""
                    txtMonto.Text = ""
                    cbxCobrado.Checked = False
                End If

                Me.ConRoboDeSeñalBindingNavigator.Enabled = False
                Me.DescripcionTextBox.Enabled = False
                Me.txtMonto.Enabled = False
                Me.cbxCobrado.Enabled = False
            End If

            If OpcionCli = "M" Then
                Me.DescripcionTextBox.Enabled = True
                Me.txtMonto.Enabled = True
                Me.cbxCobrado.Enabled = False
                DataTable = RoboSeñal.buscaDT(eGloContrato)
                If (DataTable.Rows.Count > 0) Then
                    BindingNavigatorDeleteItem.Enabled = True
                    ConRoboDeSeñalBindingNavigatorSaveItem.Enabled = True
                    DescripcionTextBox.Text = DataTable.Rows(0)(1).ToString
                    txtMonto.Text = Round(Convert.ToDecimal(DataTable.Rows(0)(2)), 2)
                    If Convert.ToBoolean(DataTable.Rows(0)(3)) = True Then
                        cbxCobrado.Checked = True
                    Else
                        cbxCobrado.Checked = False
                    End If
                Else
                    DescripcionTextBox.Text = ""
                    txtMonto.Text = ""
                    cbxCobrado.Checked = False
                End If
                If Me.DescripcionTextBox.Text.Length < 0 Then
                    Me.BindingNavigatorDeleteItem.Enabled = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    'End Sub
End Class