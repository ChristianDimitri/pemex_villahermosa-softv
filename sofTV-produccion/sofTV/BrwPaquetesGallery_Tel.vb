﻿
Imports System.Data.SqlClient

Public Class BrwPaquetesGallery_Tel
    Dim ID_eliminando As Integer = 0
    Dim MTablas As String = "PaquetesGallery_Tel"
    Dim Mvalues As String = "Id_tel,Clv_Txt as Clave, Descripcion, Bnd"
    Dim MValores As String = ""
    Dim MCondicion As String = ""
    Dim MTipMov As Integer = 0
    Dim MBNDIDENTITY As Boolean = False
    Dim MClv_ID As Long = 0

    Private Sub BrwClasificacionLlamadas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bec_bnd = True Then
            bec_bnd = False
            MCondicion = ""
            MTipMov = 0
            cargaReales(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
        End If
    End Sub

    Private Sub BrwClasificacionLlamadas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'MiConexion = "Data Source=TEAMEDGAR-PC;Initial Catalog=Sahuayo ;Persist Security Info=True;User ID=sa;Password=06011975;Connect Timeout=0"
        'colorea(Me)

        cargaReales(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
    End Sub

    Public Sub cargaReales(ByVal Tablas As String, ByVal values As String, ByVal Valores As String, ByVal Condicion As String, ByVal TipMov As Integer, ByVal BNDIDENTITY As Boolean)
        Try
            Dim conn As New SqlConnection(MiConexion)
            conn.Open()
            Dim comando As New SqlClient.SqlCommand("UP_PROCESA_TODO", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0
            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando
            '@Tablas varchar(150),@values varchar(max),@Valores varchar(max),@Condicion varchar(150),
            '@TipMov int,@BNDIDENTITY BIT,@CLV_ID BIGINT OUTPUT

            'Agrego el parámetro
            Adaptador.SelectCommand.Parameters.Add("@Tablas", SqlDbType.VarChar, 150).Value = Tablas
            Adaptador.SelectCommand.Parameters.Add("@values", SqlDbType.VarChar, 250).Value = values
            Adaptador.SelectCommand.Parameters.Add("@Valores", SqlDbType.VarChar, 250).Value = Valores
            Adaptador.SelectCommand.Parameters.Add("@Condicion", SqlDbType.VarChar, 250).Value = Condicion
            Adaptador.SelectCommand.Parameters.Add("@TipMov", SqlDbType.Int).Value = TipMov
            Adaptador.SelectCommand.Parameters.Add("@BNDIDENTITY", SqlDbType.Bit).Value = BNDIDENTITY
            Adaptador.SelectCommand.Parameters.Add("@CLV_ID", SqlDbType.BigInt).Value = 0


            Dim Dataset As New DataSet
            'Creamos nuestros BidingSource
            Dim Bs As New BindingSource
            'Llenamos el Adaptador con el DataSet
            Adaptador.Fill(Dataset, "resultadoClasificacion")
            Bs.DataSource = Dataset.Tables("resultadoClasificacion")
            'dgvResultadosClasificacion.DataSource = Bs
            Me.dgvResultadosClasificacion.DataSource = Bs
            conn.Close()
            Me.dgvResultadosClasificacion.Columns(0).Visible = False
            Me.dgvResultadosClasificacion.Columns(3).Visible = False
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        OpcionClasificacionLlamadas = "N"
        FrmPaquetesGallery_Tel.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        OpcionClasificacionLlamadas = "C"
        FrmPaquetesGallery_Tel.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        OpcionClasificacionLlamadas = "M"
        FrmPaquetesGallery_Tel.Show()
    End Sub

    Private Sub dgvResultadosClasificacion_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvResultadosClasificacion.CurrentCellChanged
        Try

            ID_ClasificacionLlamadas = Me.dgvResultadosClasificacion.CurrentRow.Cells(0).Value
            Me.LblId.Text = ID_ClasificacionLlamadas
            NombreClasificacion = Me.dgvResultadosClasificacion.CurrentRow.Cells(1).Value
            Me.LblClave.Text = NombreClasificacion
            DescClasificacion = Me.dgvResultadosClasificacion.CurrentRow.Cells(2).Value
            Me.LblDescripcion.Text = DescClasificacion
            es_LD = Me.dgvResultadosClasificacion.CurrentRow.Cells(3).Value


        Catch ex As Exception
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If IsNumeric(Me.LblId.Text) = True Then
            If Me.LblId.Text > 0 Then
                MCondicion = "Id_Tel= " & Me.LblId.Text
                MTipMov = 3
                cargaReales(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
                MCondicion = ""
                MTipMov = 0
                MsgBox("Se Elimino con Éxito", MsgBoxStyle.Information)
            Else
                MsgBox("Seleccione el Servicio que desea eliminar ", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Seleccione el Servicio que desea eliminar ", MsgBoxStyle.Information)
        End If
      
    End Sub

    
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        MCondicion = ""
        If Clv_Txt.Text.Length > 0 Then
            MCondicion = Clv_Txt.Name & " Like $" & Clv_Txt.Text & "$"
        End If
        If Descripcion.Text.Length > 0 Then
            If Len(MCondicion) = 0 Then
                MCondicion = Descripcion.Name & " Like $" & Descripcion.Text & "$"
            Else
                MCondicion = MCondicion & " and " & Descripcion.Name & " Like $" & Descripcion.Text & "$"
            End If
        End If

        cargaReales(MTablas, Mvalues, MValores, MCondicion, MTipMov, MBNDIDENTITY)
    End Sub

    

    Private Sub dgvResultadosClasificacion_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvResultadosClasificacion.CellContentClick

    End Sub
End Class