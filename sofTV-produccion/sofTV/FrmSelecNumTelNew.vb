﻿Imports System.Data.SqlClient
Imports System.Text
Public Class FrmSelecNumTelNew
    Dim tel As Integer = 0

    Private Sub FrmSelecNumTelNew_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        opcion = ""
        GloClv_TipSer = 0
        GLOCONTRATOSEL = 0
        eGloContrato = 0
        GloOpcionCANUM = 0
    End Sub
    Private Sub FrmSelecNumTelNew_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)

        If (opcion = "M" Or opcion = "C") And Bloquea = True Then
            muestaNum()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If Me.CMDRadioButton2.Checked = True Then
            Me.Panel3.Hide()
            Me.Button1.Visible = False
            Muestra_EquiposTel(1)
            muestraTelefonosDisponibles(eGloContrato)
            newcontrato = eGloContrato

        ElseIf Me.CMBRadioButton1.Checked = True Then
            Me.Panel3.Hide()
            Me.Button1.Visible = False
            Muestra_EquiposTel(1)
            muestraTelefonosDisponibles(eGloContrato)
            newcontrato = eGloContrato
            Me.ComboBox3.Enabled = False
            'muestraTelefonosDisponibles(eGloContrato)
            'newcontrato = eGloContrato
            'asignaNumAuto()
            'tel_old = CInt(Me.ComboBox1.SelectedValue)
            'tel_new = tel

            'Me.Close()
        End If

    End Sub
    Private Sub Muestra_EquiposTel(ByVal op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC Muestra_EquiposTel ")
        strSQL.Append(CStr(op))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable


            Me.ComboBox3.DisplayMember = "MACCABLEMODEM"
            Me.ComboBox3.ValueMember = "Clv_Equipo"
            Me.ComboBox3.DataSource = bindingSource


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        If IsNumeric(Me.ComboBox1.SelectedValue) = True And IsNumeric(Me.ComboBox3.SelectedValue) = True Then

            tel_old = CInt(Me.ComboBox1.SelectedValue)
            tel_new = CInt(Me.ComboBox3.SelectedValue)
            GloOpcion = 666
            MsgBox("El Cambio Se Realizo Con Exito", MsgBoxStyle.Information, "Cambio De Numero")
            Me.Close()

        Else
            MsgBox("No Hay Numero Telefonicos En El Catalogo", MsgBoxStyle.Information)
            tel_old = 0
            tel_new = 0
            Me.Close()
        End If
        GloClv_TipSer = 0
        eGloContrato = 0
        GLOCONTRATOSEL = 0
    End Sub
   
    Public Sub muestraTelefonosDisponibles(ByVal contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC muestraTelefonosDisponibles ")
        strSQL.Append(CStr(contrato))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable

            Me.ComboBox1.DisplayMember = "TELEFONO"
            Me.ComboBox1.ValueMember = "CLAVE"
            Me.ComboBox1.DataSource = bindingSource

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub muestraTelefonosCANUM(ByVal contrato As Long)
        Dim con60 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim error1 As Integer = Nothing
        Try
            con60.Open()
            cmd = New SqlClient.SqlCommand()
            With cmd
                .CommandText = "muestraTelefonosCANUM"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con60

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = contrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@telefono1", SqlDbType.VarChar, 50)
                prm1.Direction = ParameterDirection.Output
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@telefono2", SqlDbType.VarChar, 50)
                prm2.Direction = ParameterDirection.Output
                .Parameters.Add(prm2)

                Dim i As Integer = cmd.ExecuteNonQuery()


                Me.Label5.Text = CStr(prm1.Value.ToString)
                Me.Label6.Text = CStr(prm2.Value.ToString)

            End With
            con60.Close()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        opcion = ""
        GloClv_TipSer = 0
        GLOCONTRATOSEL = 0
        eGloContrato = 0
        GloOpcionCANUM = 0
        Me.Close()
    End Sub
    Private Sub asignaNumAuto()
        Dim con60 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim error1 As Integer = Nothing
        Try
            con60.Open()
            cmd = New SqlClient.SqlCommand()
            With cmd
                .CommandText = "asignaNumAuto"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con60

                Dim prm As New SqlParameter("@clv_telnew", SqlDbType.Int)
                prm.Direction = ParameterDirection.Output
                .Parameters.Add(prm)

                
                Dim i As Integer = cmd.ExecuteNonQuery()


                tel = CInt(prm.Value)


            End With
            con60.Close()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub muestaNum()
        Panel2.Visible = True
        Button3.Visible = True
        Panel3.Hide()
        Panel1.Hide()
        Button1.Hide()
        Button2.Hide()
        muestraTelefonosCANUM(eGloContrato)
    End Sub

End Class