Imports System.Data.SqlClient
Public Class FrmIAPARAsigna
    Dim CON As New SqlConnection(MiConexion)
    Dim CON1 As New SqlConnection(MiConexion)
    Dim CON2 As New SqlConnection(MiConexion)
    Dim CON3 As New SqlConnection(MiConexion)
    Dim CON4 As New SqlConnection(MiConexion)
    Dim CON5 As New SqlConnection(MiConexion)
    'INICIO

    Public Sub CREAARBOL2()
        Try
            'Eric
            Bloquea = False
            Dim I As Integer = 0
            Dim X As Integer = 0
            If GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "RIAPA" Then
                CON.Open()
                Me.MUESTRAICAM_porSOLDIGTableAdapter.Connection = CON
                Me.MUESTRAICAM_porSOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAICAM_porSOLDIG, GloDetClave, gloClv_Orden, 0)
                CON.Close()
                Dim FilaRow As DataRow
                Dim FilacontNet As DataRow
                Me.TreeView2.Nodes.Clear()
                For Each FilaRow In Me.NewSofTvDataSet.MUESTRAICAM_porSOLDIG.Rows
                    'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                    X = 0
                    Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                    Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                    CON1.Open()
                    Me.MUESTRACONTDIGTableAdapter.Connection = CON1
                    Me.MUESTRACONTDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                    CON1.Close()
                    Me.TreeView2.Nodes(I).ForeColor = Color.Black
                    For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTDIG.Rows
                        Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & Trim(FilacontNet("STATUS").ToString()))
                        Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                        If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                            Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                        ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                            Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                        Else
                            Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                        End If
                        X += 1
                    Next
                    I += 1
                Next
            ElseIf GLOTRABAJO = "CAPAR" Then
                'Me.MUESTRAICAM_porSOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAICAM_porSOLDIG, GloDetClave, gloClv_Orden, 2)
                'Eric-------------------------------
                'If opcion = "M" Then Bloquea = True
                If Bloquea = False Then
                    CON2.Open()
                    Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.Connection = CON2
                    Me.MUESTRACABLEMODEMSDELCLI_porOpcionDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcionDIG, New System.Nullable(Of Long)(CType(Contrato, Long)), "P", 16)
                    CON2.Close()
                    Dim FilaRow As DataRow
                    Dim FilacontNet As DataRow
                    Me.TreeView2.Nodes.Clear()
                    For Each FilaRow In Me.NewSofTvDataSet.MUESTRACABLEMODEMSDELCLI_porOpcionDIG.Rows
                        'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                        X = 0
                        Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                        Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                        CON3.Open()
                        Me.MUESTRACONTDIGTableAdapter.Connection = CON3
                        Me.MUESTRACONTDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                        CON3.Close()
                        Me.TreeView2.Nodes(I).ForeColor = Color.Black
                        For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTDIG.Rows
                            Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & Trim(FilacontNet("STATUS").ToString()))
                            Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                            If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                            ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                            Else
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                            End If
                            X += 1
                        Next
                        I += 1
                    Next
                Else
                    CON4.Open()
                    Me.MUESTRAICAM_porSOLDIGTableAdapter.Connection = CON4
                    Me.MUESTRAICAM_porSOLDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAICAM_porSOLDIG, GloDetClave, gloClv_Orden, 17)
                    CON4.Close()
                    Dim FilaRow As DataRow
                    Dim FilacontNet As DataRow
                    Me.TreeView2.Nodes.Clear()
                    For Each FilaRow In Me.NewSofTvDataSet.MUESTRAICAM_porSOLDIG.Rows
                        'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                        X = 0
                        Me.TreeView2.Nodes.Add(Trim(FilaRow("CONTRATONET").ToString()), Trim(FilaRow("MACCABLEMODEM").ToString()))
                        Me.TreeView2.Nodes(I).Tag = Trim(FilaRow("CONTRATONET").ToString())
                        CON5.Open()
                        Me.MUESTRACONTDIGTableAdapter.Connection = CON5
                        Me.MUESTRACONTDIGTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACONTDIG, New System.Nullable(Of Long)(CType(Trim(FilaRow("CONTRATONET").ToString()), Long)))
                        CON5.Close()
                        Me.TreeView2.Nodes(I).ForeColor = Color.Black
                        For Each FilacontNet In Me.NewSofTvDataSet.MUESTRACONTDIG.Rows
                            Me.TreeView2.Nodes(I).Nodes.Add(Trim(FilacontNet("CLV_UNICANET").ToString()), Trim(FilacontNet("DESCRIPCION").ToString()) & Trim(FilacontNet("STATUS").ToString()))
                            Me.TreeView2.Nodes(I).Nodes(X).Tag = Trim(FilacontNet("CLV_UNICANET").ToString())
                            If Trim(FilacontNet("STATUS").ToString()) = "Suspendido" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Olive
                            ElseIf Trim(FilacontNet("STATUS").ToString()) = "Instalado" Or Trim(FilacontNet("STATUS").ToString()) = "Contratado" Then
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Navy
                            Else
                                Me.TreeView2.Nodes(I).Nodes(X).ForeColor = Color.Red
                            End If
                            X += 1
                        Next
                        I += 1
                    Next
                End If
            End If
            Me.TreeView2.ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub busca()
        Dim CON As New SqlConnection(MiConexion)
        Try
            If GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "RIAPA" Then
                CON.Open()
                Me.CONIAPARTableAdapter.Connection = CON
                Me.CONIAPARTableAdapter.Fill(Me.NewSofTvDataSet.CONIAPAR, New System.Nullable(Of Long)(CType(GloDetClave, Long)), New System.Nullable(Of Long)(CType(gloClv_Orden, Long)), New System.Nullable(Of Long)(CType(Contratonet1.Text, Long)))
                CON.Close()
            ElseIf GLOTRABAJO = "CAPAR" Then

            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub TreeView2_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView2.AfterSelect
        Try
            If e.Node.Level = 0 Then
                If IsNumeric(e.Node.Tag) = True Then
                    'Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(e.Node.Tag, Long))
                    Contratonet1.Text = e.Node.Tag
                    MacCablemodem1.Text = e.Node.Text
                    Contratonet2.Text = e.Node.Tag
                    MacCablemodem2.Text = e.Node.Text
                    busca()
                    '    Me.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(e.Node.Tag, Long))
                    'Else
                    '    Me.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(0, Long))
                    'End If
                    'Else
                    'If IsNumeric(e.Node.Tag) Then
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(e.Node.Tag, Long)))
                    'Else
                    '    Me.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(0, Long)))
                    'End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmIAPARAsigna_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBndClv_CablemodemSel = True Then
            If GLOTRABAJO = "CAPAR" Then
                Me.CLV_CABLEMODEMNewTextBox.Text = GloClv_CablemodemSel
                Me.MaccablemodemNew.Text = GloMacCablemodemSel
            ElseIf GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "RIAPA" Then
                Me.CLV_CABLEMODEMTextBox.Text = GloClv_CablemodemSel
                Me.MacCableModemTextBox.Text = GloMacCablemodemSel
            End If
            GloBndClv_CablemodemSel = False
        End If
    End Sub






    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub BORRARIAPAR()
        Dim CON As New SqlConnection(MiConexion)
        If IsNumeric(GloDetClave) = True And IsNumeric(gloClv_Orden) = True And IsNumeric(Me.Contratonet1.Text) = True And IsNumeric(Me.CLV_CABLEMODEMTextBox.Text) = True Then
            If GloDetClave > 0 And gloClv_Orden > 0 And Me.Contratonet1.Text > 0 And Me.CLV_CABLEMODEMTextBox.Text > 0 Then
                CON.Open()
                Me.CONIAPARTableAdapter.Connection = CON
                Me.CONIAPARTableAdapter.Delete(GloDetClave, gloClv_Orden, Me.Contratonet1.Text, Me.CLV_CABLEMODEMTextBox.Text)
                Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Connection = CON
                Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMS_disponibles, 3)
                CON.Close()
                Me.CREAARBOL2()
                Me.CLV_CABLEMODEMTextBox.Text = 0
                Me.MacCableModemTextBox.Text = ""
                Me.Close()
            End If
        End If
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim CON As New SqlConnection(MiConexion)
        Try
            If IsNumeric(GloDetClave) = True And IsNumeric(gloClv_Orden) = True And IsNumeric(Me.Contratonet2.Text) = True And IsNumeric(Me.CLV_CABLEMODEMNewTextBox.Text) > 0 Then
                CON.Open()
                Me.CONCAPARTableAdapter.Connection = CON
                Me.CONCAPARTableAdapter.Fill(Me.NewSofTvDataSet.CONCAPAR, GloDetClave, gloClv_Orden, Me.Contratonet2.Text)
                CON.Close()
            Else
                MsgBox("Seleccione el Cablemode Nuevo ", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FrmSelCablemodemsDisponibles.Show()
    End Sub

    Private Sub Contratonet2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Contratonet2.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        If IsNumeric(Me.Contratonet2.Text) = True Then
            CON.Open()
            Me.CONCAPARTableAdapter.Connection = CON
            Me.CONCAPARTableAdapter.Fill(Me.NewSofTvDataSet.CONCAPAR, GloDetClave, gloClv_Orden, Me.Contratonet2.Text)
            CON.Close()
        End If
    End Sub




    Private Sub BORRARCAMBIO()
        Dim CON As New SqlConnection(MiConexion)
        If IsNumeric(Me.CLV_CABLEMODEMNewTextBox.Text) = False Then Me.CLV_CABLEMODEMNewTextBox.Text = 0
        If IsNumeric(GloDetClave) = True And IsNumeric(gloClv_Orden) = True And IsNumeric(Me.Contratonet2.Text) = True And Me.CLV_CABLEMODEMNewTextBox.Text > 0 Then
            CON.Open()
            Me.BORCAPARTableAdapter.Connection = CON
            Me.BORCAPARTableAdapter.Fill(Me.NewSofTvDataSet.BORCAPAR, GloDetClave, gloClv_Orden, Me.Contratonet2.Text, Me.CLV_CABLEMODEMNewTextBox.Text)
            Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Connection = CON
            Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMS_disponibles, 3)
            CON.Close()
            Me.MaccablemodemNew.Text = ""
            Me.CLV_CABLEMODEMNewTextBox.Text = 0
            MsgBox("Se quito el Cablemodem Nuevo ")
            Me.Close()
        Else
            MsgBox("No tiene asginado un Cablemodem Nuevo")
        End If
    End Sub

    Private Sub NoFuncionaCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoFuncionaCheckBox.CheckedChanged
        Me.CheckBox1.Checked = Me.NoFuncionaCheckBox.Checked
    End Sub


    Private Sub CLV_CABLEMODEMNewTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLV_CABLEMODEMNewTextBox.TextChanged
        If IsNumeric(Me.CLV_CABLEMODEMNewTextBox.Text) = False Then Me.CLV_CABLEMODEMNewTextBox.Text = 0
        If Me.CLV_CABLEMODEMNewTextBox.Text = 0 Then
            Me.Button1.Enabled = True
        ElseIf Me.CLV_CABLEMODEMNewTextBox.Text > 0 Then
            Me.Button1.Enabled = False
        End If
    End Sub


    Private Sub CLV_CABLEMODEMTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLV_CABLEMODEMTextBox.TextChanged
        If IsNumeric(Me.CLV_CABLEMODEMTextBox.Text) = False Then Me.CLV_CABLEMODEMTextBox.Text = 0
        If Me.CLV_CABLEMODEMTextBox.Text = 0 Then
            Me.Button2.Enabled = True
        ElseIf Me.CLV_CABLEMODEMTextBox.Text > 0 Then
            Me.Button2.Enabled = False
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        FrmSelCablemodemsDisponibles.Show()
    End Sub

    Private Sub FrmIAPARAsigna_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        Try
            'If GLOTRABAJO = "CAPAR" Then
            CON.Open()
            Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
            Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
            CON.Close()
            GloBndTrabajo = True
            GloBloqueaDetalle = True
            'End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    '

    Private Sub FrmIAPARAsigna_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Connection = CON
        Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMS_disponibles, 3)
        CON.Close()
        Me.CREAARBOL2()
        If opcion = "N" Then Bloquea = True
        If GLOTRABAJO = "IAPAR" Or GLOTRABAJO = "RIAPA" Then
            Me.Panel1.Visible = True
            Me.Panel2.Visible = False
            If opcion = "C" Then
                'Me.Panel2.Enabled = False
                Me.Button2.Enabled = False
            End If
            'If Bloquea = True Then
            'CONCAPARBindingNavigator.Enabled = False
            'Me.CONIAPARBindingNavigator.Enabled = False
            'End If
        ElseIf GLOTRABAJO = "CAPAR" Then
            Me.Panel2.Visible = True
            Me.Panel1.Visible = False
            If Bloquea = True Or opcion = "N" Then
                CONCAPARBindingNavigator.Enabled = True
                Me.CONIAPARBindingNavigator.Enabled = True
            End If
        End If

        If opcion = "C" Then
            'Me.Panel2.Enabled = False
            Me.TreeView2.Enabled = False
            CONCAPARBindingNavigator.Enabled = False
            Me.CONIAPARBindingNavigator.Enabled = False
            Me.Button2.Enabled = False
        End If
    End Sub


    Private Sub ACEPTARCAMBIO()
        Dim CON As New SqlConnection(MiConexion)
        Dim BND As Boolean = False
        If Me.CheckBox1.Checked = True Then BND = True Else BND = False
        If IsNumeric(Me.CLV_CABLEMODEMNewTextBox.Text) = False Then Me.CLV_CABLEMODEMNewTextBox.Text = 0
        If IsNumeric(GloDetClave) = True And IsNumeric(gloClv_Orden) = True And IsNumeric(Me.Contratonet2.Text) = True And Me.CLV_CABLEMODEMNewTextBox.Text > 0 Then
            GloGuardo = True
            Me.Validate()
            CON.Open()
            Me.NUECAPARTableAdapter.Connection = CON
            Me.NUECAPARTableAdapter.Fill(Me.NewSofTvDataSet.NUECAPAR, GloDetClave, gloClv_Orden, Me.Contratonet2.Text, Me.CLV_CABLEMODEMNewTextBox.Text, BND)
            'Me.CONCAPARTableAdapter.Insert(GloDetClave, gloClv_Orden, Me.Contratonet2.Text, Me.CLV_CABLEMODEMNewTextBox.Text, BND)
            Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Connection = CON
            Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMS_disponibles, 3)
            CON.Close()
            MsgBox(mensaje5)
            Me.Close()
        Else
            MsgBox("Seleccione el Cablemode Nuevo ", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub CONIAPARACEPTAR()
        Dim CON As New SqlConnection(MiConexion)
        If IsNumeric(Me.CLV_CABLEMODEMTextBox.Text) = True Then
            If IsNumeric(Me.CLV_CABLEMODEMTextBox.Text) = False Then Me.CLV_CABLEMODEMTextBox.Text = 0
            If Me.CLV_CABLEMODEMTextBox.Text > 0 Then
                Me.Validate()
                GloGuardo = True
                Me.CONIAPARBindingSource.EndEdit()
                CON.Open()
                Me.CONIAPARTableAdapter.Connection = CON
                Me.CONIAPARTableAdapter.Update(Me.NewSofTvDataSet.CONIAPAR)
                Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Connection = CON
                Me.MUESTRACABLEMODEMS_disponiblesTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACABLEMODEMS_disponibles, 3)
                CON.Close()
                Me.CREAARBOL2()
                Me.Close()
            Else
                MsgBox("Ya tiene asignado un Cablemodem ", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Seleccione el Cablemodem")
        End If
    End Sub

    Private Sub CONIAPARBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONIAPARBindingNavigatorSaveItem.Click
        CONIAPARACEPTAR()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        BORRARIAPAR()
    End Sub

    Private Sub CONCAPARBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCAPARBindingNavigatorSaveItem.Click
        ACEPTARCAMBIO()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        BORRARCAMBIO()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

    End Sub
End Class