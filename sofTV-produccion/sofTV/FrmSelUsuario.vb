Imports System.Data.SqlClient
Public Class FrmSelUsuario


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If Me.NombreListBox1.Items.Count > 0 Then
            If eOpVentas = 49 Then
                FrmImprimirComision.Show()
            Else
                FrmSelServicioE.Show()
            End If
            Me.Close()
        End If
    End Sub

    Private Sub FrmSelUsuario_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Dame_clv_session_ReportesTableAdapter.Connection = CON
            Me.Dame_clv_session_ReportesTableAdapter.Fill(Me.DataSetEric2.Dame_clv_session_Reportes, eClv_Session)
            Me.ConUsuariosProTableAdapter.Connection = CON
            Me.ConUsuariosProTableAdapter.Fill(Me.DataSetEric2.ConUsuariosPro, eClv_Session, eClv_Grupo, 0)
            CON.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            If Me.NombreListBox.Items.Count > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.InsertarUsuarioTmpTableAdapter.Connection = CON
                Me.InsertarUsuarioTmpTableAdapter.Fill(Me.DataSetEric2.InsertarUsuarioTmp, CInt(Me.NombreListBox.SelectedValue), eClv_Session, 0)
                Me.ConUsuariosProTableAdapter.Connection = CON
                Me.ConUsuariosProTableAdapter.Fill(Me.DataSetEric2.ConUsuariosPro, eClv_Session, 0, 1)
                Me.ConUsuariosTmpTableAdapter.Connection = CON
                Me.ConUsuariosTmpTableAdapter.Fill(Me.DataSetEric2.ConUsuariosTmp, eClv_Session)
                CON.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            If Me.NombreListBox.Items.Count > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.InsertarUsuarioTmpTableAdapter.Connection = CON
                Me.InsertarUsuarioTmpTableAdapter.Fill(Me.DataSetEric2.InsertarUsuarioTmp, 0, eClv_Session, 1)
                Me.ConUsuariosProTableAdapter.Connection = CON
                Me.ConUsuariosProTableAdapter.Fill(Me.DataSetEric2.ConUsuariosPro, eClv_Session, 0, 1)
                Me.ConUsuariosTmpTableAdapter.Connection = CON
                Me.ConUsuariosTmpTableAdapter.Fill(Me.DataSetEric2.ConUsuariosTmp, eClv_Session)
                CON.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            If Me.NombreListBox1.Items.Count > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.BorrarUsuarioTmpTableAdapter.Connection = CON
                Me.BorrarUsuarioTmpTableAdapter.Fill(Me.DataSetEric2.BorrarUsuarioTmp, CInt(Me.NombreListBox1.SelectedValue), eClv_Session, 0)
                Me.ConUsuariosProTableAdapter.Connection = CON
                Me.ConUsuariosProTableAdapter.Fill(Me.DataSetEric2.ConUsuariosPro, eClv_Session, 0, 1)
                Me.ConUsuariosTmpTableAdapter.Connection = CON
                Me.ConUsuariosTmpTableAdapter.Fill(Me.DataSetEric2.ConUsuariosTmp, eClv_Session)
                CON.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Try
            If Me.NombreListBox1.Items.Count > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.BorrarUsuarioTmpTableAdapter.Connection = CON
                Me.BorrarUsuarioTmpTableAdapter.Fill(Me.DataSetEric2.BorrarUsuarioTmp, 0, eClv_Session, 1)
                Me.ConUsuariosProTableAdapter.Connection = CON
                Me.ConUsuariosProTableAdapter.Fill(Me.DataSetEric2.ConUsuariosPro, eClv_Session, 0, 1)
                Me.ConUsuariosTmpTableAdapter.Connection = CON
                Me.ConUsuariosTmpTableAdapter.Fill(Me.DataSetEric2.ConUsuariosTmp, eClv_Session)
                CON.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub
End Class