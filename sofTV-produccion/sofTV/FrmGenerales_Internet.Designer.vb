<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGenerales_Internet
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Clv_IdLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmGenerales_Internet))
        Me.Button2 = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtcom = New System.Windows.Forms.TextBox
        Me.txtcriteria = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.txtcorreo = New System.Windows.Forms.TextBox
        Me.txtpass = New System.Windows.Forms.TextBox
        Me.Txtusuario = New System.Windows.Forms.TextBox
        Me.Txtruta = New System.Windows.Forms.TextBox
        Me.txtpolsus = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.CONGenerales_InternetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Paq_defaultTextBox = New System.Windows.Forms.TextBox
        Me.Num_dia_habilitarTextBox = New System.Windows.Forms.TextBox
        Me.Clv_IdTextBox = New System.Windows.Forms.TextBox
        Me.CONGenerales_InternetBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.CONGenerales_InternetBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.CONGenerales_InternetTableAdapter = New sofTV.DataSetEDGARTableAdapters.CONGenerales_InternetTableAdapter
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        Clv_IdLabel = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        CType(Me.CONGenerales_InternetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONGenerales_InternetBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONGenerales_InternetBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_IdLabel
        '
        Clv_IdLabel.AutoSize = True
        Clv_IdLabel.Location = New System.Drawing.Point(73, 92)
        Clv_IdLabel.Name = "Clv_IdLabel"
        Clv_IdLabel.Size = New System.Drawing.Size(37, 13)
        Clv_IdLabel.TabIndex = 15
        Clv_IdLabel.Text = "Clv Id:"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(394, 468)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(142, 29)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "&CERRAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.txtcom)
        Me.Panel1.Controls.Add(Me.txtcriteria)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.txtcorreo)
        Me.Panel1.Controls.Add(Me.txtpass)
        Me.Panel1.Controls.Add(Me.Txtusuario)
        Me.Panel1.Controls.Add(Me.Txtruta)
        Me.Panel1.Controls.Add(Me.txtpolsus)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.TextBox2)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Paq_defaultTextBox)
        Me.Panel1.Controls.Add(Me.Num_dia_habilitarTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 39)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(867, 423)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'txtcom
        '
        Me.txtcom.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtcom.Location = New System.Drawing.Point(418, 370)
        Me.txtcom.Name = "txtcom"
        Me.txtcom.Size = New System.Drawing.Size(285, 21)
        Me.txtcom.TabIndex = 41
        '
        'txtcriteria
        '
        Me.txtcriteria.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtcriteria.Location = New System.Drawing.Point(418, 340)
        Me.txtcriteria.Name = "txtcriteria"
        Me.txtcriteria.Size = New System.Drawing.Size(285, 21)
        Me.txtcriteria.TabIndex = 40
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label11.Location = New System.Drawing.Point(212, 370)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(200, 15)
        Me.Label11.TabIndex = 39
        Me.Label11.Text = "Complemento de MAC Adress:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label10.Location = New System.Drawing.Point(354, 343)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(58, 15)
        Me.Label10.TabIndex = 38
        Me.Label10.Text = "Criteria:"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(793, 220)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(25, 23)
        Me.Button1.TabIndex = 37
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtcorreo
        '
        Me.txtcorreo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtcorreo.Location = New System.Drawing.Point(418, 313)
        Me.txtcorreo.Name = "txtcorreo"
        Me.txtcorreo.Size = New System.Drawing.Size(285, 21)
        Me.txtcorreo.TabIndex = 36
        '
        'txtpass
        '
        Me.txtpass.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtpass.Location = New System.Drawing.Point(418, 283)
        Me.txtpass.Name = "txtpass"
        Me.txtpass.Size = New System.Drawing.Size(239, 21)
        Me.txtpass.TabIndex = 35
        '
        'Txtusuario
        '
        Me.Txtusuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Txtusuario.Location = New System.Drawing.Point(418, 256)
        Me.Txtusuario.Name = "Txtusuario"
        Me.Txtusuario.Size = New System.Drawing.Size(239, 21)
        Me.Txtusuario.TabIndex = 34
        '
        'Txtruta
        '
        Me.Txtruta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Txtruta.Location = New System.Drawing.Point(418, 221)
        Me.Txtruta.Name = "Txtruta"
        Me.Txtruta.Size = New System.Drawing.Size(367, 21)
        Me.Txtruta.TabIndex = 33
        Me.Txtruta.Text = "C:\"
        '
        'txtpolsus
        '
        Me.txtpolsus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtpolsus.Location = New System.Drawing.Point(418, 187)
        Me.txtpolsus.Name = "txtpolsus"
        Me.txtpolsus.Size = New System.Drawing.Size(168, 21)
        Me.txtpolsus.TabIndex = 32
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label9.Location = New System.Drawing.Point(201, 314)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(212, 15)
        Me.Label9.TabIndex = 31
        Me.Label9.Text = "Correo para reportar insidentes:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(212, 284)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(200, 15)
        Me.Label8.TabIndex = 30
        Me.Label8.Text = "Password de Usuario de CNR:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label7.Location = New System.Drawing.Point(298, 257)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(114, 15)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Usuario de CNR:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label6.Location = New System.Drawing.Point(121, 224)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(291, 15)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "Ruta de Archivos de Procesamiento de CNR:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label5.Location = New System.Drawing.Point(247, 192)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(165, 15)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Politicas de Suspención:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(332, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 15)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "Comunidad"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(327, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 15)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Ip Server C4"
        '
        'TextBox1
        '
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_InternetBindingSource, "Comunidad", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(418, 70)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(168, 21)
        Me.TextBox1.TabIndex = 1
        '
        'CONGenerales_InternetBindingSource
        '
        Me.CONGenerales_InternetBindingSource.DataMember = "CONGenerales_Internet"
        Me.CONGenerales_InternetBindingSource.DataSource = Me.DataSetEDGAR
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBox2
        '
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_InternetBindingSource, "IpServer", True))
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(418, 29)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(168, 21)
        Me.TextBox2.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(176, 154)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(236, 15)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Paquete(s) Asignado(s) por Default:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(143, 113)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(269, 15)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Numero de Dìas de Prueba de Paquetes:"
        '
        'Paq_defaultTextBox
        '
        Me.Paq_defaultTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_InternetBindingSource, "paq_default", True))
        Me.Paq_defaultTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Paq_defaultTextBox.Location = New System.Drawing.Point(418, 148)
        Me.Paq_defaultTextBox.Name = "Paq_defaultTextBox"
        Me.Paq_defaultTextBox.Size = New System.Drawing.Size(168, 21)
        Me.Paq_defaultTextBox.TabIndex = 3
        '
        'Num_dia_habilitarTextBox
        '
        Me.Num_dia_habilitarTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_InternetBindingSource, "num_dia_habilitar", True))
        Me.Num_dia_habilitarTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Num_dia_habilitarTextBox.Location = New System.Drawing.Point(418, 107)
        Me.Num_dia_habilitarTextBox.Name = "Num_dia_habilitarTextBox"
        Me.Num_dia_habilitarTextBox.Size = New System.Drawing.Size(90, 21)
        Me.Num_dia_habilitarTextBox.TabIndex = 2
        '
        'Clv_IdTextBox
        '
        Me.Clv_IdTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONGenerales_InternetBindingSource, "Clv_Id", True))
        Me.Clv_IdTextBox.Location = New System.Drawing.Point(116, 85)
        Me.Clv_IdTextBox.Name = "Clv_IdTextBox"
        Me.Clv_IdTextBox.ReadOnly = True
        Me.Clv_IdTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_IdTextBox.TabIndex = 16
        Me.Clv_IdTextBox.TabStop = False
        '
        'CONGenerales_InternetBindingNavigator
        '
        Me.CONGenerales_InternetBindingNavigator.AddNewItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.BindingSource = Me.CONGenerales_InternetBindingSource
        Me.CONGenerales_InternetBindingNavigator.CountItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONGenerales_InternetBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONGenerales_InternetBindingNavigatorSaveItem})
        Me.CONGenerales_InternetBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONGenerales_InternetBindingNavigator.MoveFirstItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.MoveLastItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.MoveNextItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.MovePreviousItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.Name = "CONGenerales_InternetBindingNavigator"
        Me.CONGenerales_InternetBindingNavigator.PositionItem = Nothing
        Me.CONGenerales_InternetBindingNavigator.Size = New System.Drawing.Size(891, 25)
        Me.CONGenerales_InternetBindingNavigator.TabIndex = 4
        Me.CONGenerales_InternetBindingNavigator.TabStop = True
        Me.CONGenerales_InternetBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(72, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONGenerales_InternetBindingNavigatorSaveItem
        '
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONGenerales_InternetBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Name = "CONGenerales_InternetBindingNavigatorSaveItem"
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Size = New System.Drawing.Size(108, 22)
        Me.CONGenerales_InternetBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'CONGenerales_InternetTableAdapter
        '
        Me.CONGenerales_InternetTableAdapter.ClearBeforeFill = True
        '
        'FrmGenerales_Internet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(891, 509)
        Me.Controls.Add(Me.CONGenerales_InternetBindingNavigator)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Clv_IdLabel)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Clv_IdTextBox)
        Me.Name = "FrmGenerales_Internet"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generales de Internet"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONGenerales_InternetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONGenerales_InternetBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONGenerales_InternetBindingNavigator.ResumeLayout(False)
        Me.CONGenerales_InternetBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Paq_defaultTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Num_dia_habilitarTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_IdTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents CONGenerales_InternetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONGenerales_InternetTableAdapter As sofTV.DataSetEDGARTableAdapters.CONGenerales_InternetTableAdapter
    Friend WithEvents CONGenerales_InternetBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONGenerales_InternetBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtcorreo As System.Windows.Forms.TextBox
    Friend WithEvents txtpass As System.Windows.Forms.TextBox
    Friend WithEvents Txtusuario As System.Windows.Forms.TextBox
    Friend WithEvents Txtruta As System.Windows.Forms.TextBox
    Friend WithEvents txtpolsus As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtcom As System.Windows.Forms.TextBox
    Friend WithEvents txtcriteria As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
End Class
