Imports System.Data.SqlClient
Public Class BrwPromociones

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "SA" Then
            FrmPromocionesTel.Show()
        Else
            FrmPromociones.Show()
        End If
    End Sub

    Private Sub consultar()
        If gloClave > 0 Then
            opcion = "C"
            gloClave = Me.Clv_calleLabel2.Text
            If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "SA" Then
                FrmPromocionesTel.Show()
            Else
                FrmPromociones.Show()
            End If
        Else
            MsgBox(mensaje1)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            opcion = "M"
            gloClave = Me.Clv_calleLabel2.Text
            If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "SA" Then
                FrmPromocionesTel.Show()
            Else
                FrmPromociones.Show()
            End If
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            modificar()
        Else
            MsgBox(mensaje1)
        End If
    End Sub
    Private Sub Busca(ByVal op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If IdSistema <> "LO" And IdSistema <> "YU" And IdSistema <> "SA" Then
                GloClv_TipSer = Me.ComboBox4.SelectedValue
            Else
                GloClv_TipSer = 0
            End If
            If op = 0 Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    Me.BUSTipoPromocionTableAdapter.Connection = CON
                    Me.BUSTipoPromocionTableAdapter.Fill(Me.NewSofTvDataSet.BUSTipoPromocion, New System.Nullable(Of Integer)(CType(Me.TextBox1.Text, Integer)), "", GloClv_TipSer, New System.Nullable(Of Integer)(CType(0, Integer)))
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If

            ElseIf op = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BUSTipoPromocionTableAdapter.Connection = CON
                    Me.BUSTipoPromocionTableAdapter.Fill(Me.NewSofTvDataSet.BUSTipoPromocion, New System.Nullable(Of Integer)(CType(0, Integer)), Me.TextBox2.Text, GloClv_TipSer, New System.Nullable(Of Integer)(CType(1, Integer)))
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            Else
                Me.BUSTipoPromocionTableAdapter.Connection = CON
                Me.BUSTipoPromocionTableAdapter.Fill(Me.NewSofTvDataSet.BUSTipoPromocion, New System.Nullable(Of Integer)(CType(0, Integer)), "", GloClv_TipSer, New System.Nullable(Of Integer)(CType(2, Integer)))
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub
    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            gloClave = Me.Clv_calleLabel2.Text
        End If
    End Sub
    Private Sub BrwPromociones_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Busca(2)
        End If
    End Sub
    Private Sub BrwPromociones_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        If IdSistema <> "LO" And IdSistema <> "YU" And IdSistema <> "SA" Then
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
            Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
            Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        Else
            Me.ComboBox4.Visible = False
            Me.CMBLabel5.Visible = False
        End If
        Busca(2)
        CON.Close()
    End Sub
    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            modificar()
        End If
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        GloClv_TipSer = Me.ComboBox4.SelectedValue
        Busca(2)
    End Sub
End Class