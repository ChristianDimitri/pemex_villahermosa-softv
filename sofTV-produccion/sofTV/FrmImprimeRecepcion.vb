﻿Imports System.Data.SqlClient
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Public Class FrmImprimeRecepcion
    Dim claves As Integer = 0
    Private customersByCityReport As ReportDocument
    Dim Impresora_Tarjetas As String = Nothing
    Dim Impresora_Contratos As String = Nothing
    Private Sub Form3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        MuestraRecepciones(0, "1900-01-01", "1900-01-01")
    End Sub
    Private Sub MuestraRecepciones(ByVal op As Integer, ByVal fecha_ini As DateTime, ByVal fecha_fin As DateTime)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraRecepciones ")
        strSQL.Append(CStr(op) & ",")
        strSQL.Append("'" & fecha_ini & "',")
        strSQL.Append("'" & fecha_fin & "'")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.DataGridView1.Refresh()
            Me.DataGridView1.DataSource = bindingSource

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Dispose()
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(claves) = True Then
            GloOpcionRecepcion = 1
            GloFolioRecepcion = claves
            eOpVentas = 120
            FrmImprimirComision.Show()
            'ConfigureCrystalReportsPortabilidad(1, claves)
        Else
            MsgBox("Seleccione Un Renglon Valido", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        MuestraRecepciones(1, Me.DateTimePicker1.Text, Me.DateTimePicker2.Text)
    End Sub
    Private Sub DataGridView1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged

        claves = (Me.DataGridView1.Item("clave", Me.DataGridView1.CurrentRow.Index).Value)

    End Sub
    Private Sub ConfigureCrystalReportsPortabilidad(ByVal opcion As Integer, ByVal folio As Long)
        Dim impresora As String = Nothing
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\RptRecepcionSoftv.rpt"
        'reportPath = "D:\Exes\Softv\reportes\repotrtePortabilidad.rpt"
        customersByCityReport.Load(reportPath)


        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@Contrato 
        customersByCityReport.SetParameterValue(0, opcion)
        customersByCityReport.SetParameterValue(0, folio)
        'CrystalReportViewer1.ReportSource = customersByCityReport
        'CrystalReportViewer1.ShowPrintButton = True

        'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)


        customersByCityReport.PrintOptions.PrinterName = Impresora_Contratos
        customersByCityReport.PrintToPrinter(0, True, 1, 1)


        'customersByCityReport = Nothing


    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub
    Private Sub Selecciona_Impresora_Sucursal(ByVal sucursal As Integer)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Selecciona_Impresora_Sucursal", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_sucursal", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = sucursal
        com.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Impresora_Tarjetas", SqlDbType.VarChar, 50)
        par2.Direction = ParameterDirection.Output
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Impresora_Contratos", SqlDbType.VarChar, 50)
        par3.Direction = ParameterDirection.Output
        com.Parameters.Add(par3)

        Try
            con.Open()
            com.ExecuteNonQuery()
            Impresora_Tarjetas = CStr(par2.Value.ToString)
            Impresora_Contratos = CStr(par3.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try
    End Sub
End Class