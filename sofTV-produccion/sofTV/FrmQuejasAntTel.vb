﻿
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class FrmQuejasAntTel
    Private customersByCityReport As ReportDocument
    Dim bloq As Integer = 0
    Private eRespuesta As Integer = 0
    Private eMensaje As String = Nothing
    Private eClv_Queja As Integer = 0
    Private eSolucion As String = Nothing
    Private eClv_Trabajo As Integer = 0
    Private eRes As Integer = 0
    Private eMsg As String = Nothing


    Private Sub NuevoTipo_Atencion(ByVal Clv_Atencion As Long)
        Dim Con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        'Contrato
        If Contrato = Nothing Then Contrato = 0
        Try
            'Muestra_Plazo_Forzozo (@contrato bigint,@Clv_unicanet bigint,@Plazo varchar(10) output)
            cmd = New SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "TELNUEVORel_Lugar_Atencion_Queja"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clv_AtenQueja", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Me.Clv_llamadaTextBox.Text
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Tipo_Atencion", SqlDbType.VarChar, 1)
                prm1.Direction = ParameterDirection.Input
                If Me.EnTelefonia.Checked = True Then
                    prm1.Value = "T"
                Else
                    prm1.Value = "S"
                End If
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()

            End With
            Con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub MuestraTipo_Atencion(ByVal Clv_Atencion As Long)
        Dim Con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        'Contrato
        If Contrato = Nothing Then Contrato = 0
        Try
            'Muestra_Plazo_Forzozo (@contrato bigint,@Clv_unicanet bigint,@Plazo varchar(10) output)
            cmd = New SqlCommand()
            Con.Open()
            With cmd
                .CommandText = "TELCONSULTARRel_Lugar_Atencion_Queja"
                .Connection = Con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clv_AtenQueja", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Me.Clv_llamadaTextBox.Text
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Tipo_Atencion", SqlDbType.VarChar, 1)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = ""
                .Parameters.Add(prm1)

                Dim ia As Integer = .ExecuteNonQuery()
                Dim tipoatencion As String = "T"
                tipoatencion = prm1.Value
                If tipoatencion = "T" Then
                    Me.EnTelefonia.Checked = True
                    Me.EnSitio.Checked = False
                Else
                    Me.EnSitio.Checked = True
                    Me.EnTelefonia.Checked = False
                End If
            End With
            Con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub FrmQuejasAntTel_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GLOCONTRATOSEL > 0 Then
            Me.ContratoTextBox.Text = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0
        End If
        If GloBndTipSer = True Then
            GloBndTipSer = False
            Me.ComboBox5.SelectedValue = GloClv_TipSer
            Me.ComboBox5.Text = GloNom_TipSer
            Me.ComboBox5.FindString(GloNom_TipSer)
            Me.ComboBox5.Text = GloNom_TipSer
            GloBndTipSer = False
            Me.TextBox2.Text = GloNom_TipSer
            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.MUESTRATRABAJOSQUEJASTableAdapter.Connection = CON
            'Me.MUESTRATRABAJOSQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOSQUEJAS, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Integer)))
            'CON.Close()
        End If
        If bloq = 1 Then
            bloq = 0
            eGloContrato = Me.ContratoTextBox.Text
            FrmBloqueo.Show()
            Me.Panel1.Enabled = False
        End If
    End Sub



    Private Sub FrmQuejasAntTel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        Dim cone As New SqlClient.SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        ' Me.ComboBox1.Text = "< Seleccionar >"

        If IdSistema = "LO" Or IdSistema = "YU" Then
            Me.ESHOTELLabel1.Visible = False
            Me.ESHOTELCheckBox.Visible = False
        End If
        CON.Open()
        'TODO: esta línea de código carga datos en la tabla 'DataSetEdgarRev2.DAMEFECHADELSERVIDOR_2' Puede moverla o quitarla según sea necesario.
        Me.DAMEFECHADELSERVIDOR_2TableAdapter.Connection = CON
        Me.DAMEFECHADELSERVIDOR_2TableAdapter.Fill(Me.DataSetEdgarRev2.DAMEFECHADELSERVIDOR_2)
        'TODO: esta línea de código carga datos en la tabla 'DataSetEdgarRev2.DameFechadelServidorHora_2' Puede moverla o quitarla según sea necesario.
        Me.DameFechadelServidorHora_2TableAdapter.Connection = CON
        Me.DameFechadelServidorHora_2TableAdapter.Fill(Me.DataSetEdgarRev2.DameFechadelServidorHora_2)

        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla según sea necesario.
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRATIPOQUEJAS' Puede moverla o quitarla según sea necesario.
        Me.MUESTRATIPOQUEJASTableAdapter.Connection = CON
        Me.MUESTRATIPOQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATIPOQUEJAS)
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRATECNICOS' Puede moverla o quitarla según sea necesario.
        'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRACLASIFICACIONQUEJAS' Puede moverla o quitarla según sea necesario.
        Me.MUESTRACLASIFICACIONQUEJASTableAdapter.Connection = CON
        Me.MUESTRACLASIFICACIONQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACLASIFICACIONQUEJAS)
        Me.MUESTRATECNICOSTableAdapter.Connection = CON
        Me.MUESTRATECNICOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATECNICOS)
        Me.Muestra_Tecnicos_AlmacenTableAdapter.Connection = CON
        Me.Muestra_Tecnicos_AlmacenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Tecnicos_Almacen, 0)
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        Me.MUESTRAUSUARIOSTableAdapter.Connection = CON
        Me.MUESTRAUSUARIOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAUSUARIOS, 0)
        'Me.MUESTRATRABAJOSQUEJASTableAdapter.Connection = CON
        'Me.MUESTRATRABAJOSQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOSQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox5.SelectedValue, Integer)))
        'Me.MUESTRATRABAJOSQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOSQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox5.SelectedValue, Integer)))
        If opcion = "N" Then
            Me.ComboBox1.Text = "< Seleccionar >"
            Me.Label3.Visible = False
            Me.Label4.Visible = False
            Me.CONllamadasdeinternetBindingSource.AddNew()
            Me.Clv_UsuarioTextBox.Text = GloClvUsuario
            ' FrmSelTipServicio.Show()
            GloClv_TipSer = 0
            Me.HoraInicialMaskedTextBox.Text = Me.FechaTextBox.Text
            Me.FechaMaskedTextBox.Text = Me.FECHATextBox1.Text

            'Eric------
            'If IdSistema = "SA" Then
            '    Me.Tecnicos.Enabled = False
            'End If
            '----------
        ElseIf opcion = "C" Then
            Me.Consulta_Rel_AtencionTelUsuarioTableAdapter.Connection = CON
            Me.Consulta_Rel_AtencionTelUsuarioTableAdapter.Fill(Me.ProcedimientosArnoldo2.Consulta_Rel_AtencionTelUsuario, gloClave)
            Me.CONllamadasdeinternetTableAdapter.Connection = CON
            Me.CONllamadasdeinternetTableAdapter.Fill(Me.NewSofTvDataSet.CONllamadasdeinternet, gloClave)
            Me.Panel1.Enabled = False
            MuestraTipo_Atencion(Me.Clv_llamadaTextBox.Text)
        ElseIf opcion = "M" Then
            Me.Consulta_Rel_AtencionTelUsuarioTableAdapter.Connection = CON
            Me.Consulta_Rel_AtencionTelUsuarioTableAdapter.Fill(Me.ProcedimientosArnoldo2.Consulta_Rel_AtencionTelUsuario, gloClave)
            Me.CONllamadasdeinternetTableAdapter.Connection = CON
            Me.CONllamadasdeinternetTableAdapter.Fill(Me.NewSofTvDataSet.CONllamadasdeinternet, gloClave)
            Me.ComboBox5.Enabled = False
            Me.ComboBox1.Enabled = False
            Me.Button1.Enabled = False
            Me.ContratoTextBox.Enabled = False
            Me.FechaMaskedTextBox.Enabled = False
            Me.HoraInicialMaskedTextBox.Enabled = False
            MuestraTipo_Atencion(Me.Clv_llamadaTextBox.Text)
        End If
        CON.Close()
        If opcion = "C" Or opcion = "M" Then
            cone.Open()
            NUM = 0
            num2 = 0
            Me.BuscaBloqueadoTableAdapter.Connection = cone
            Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoTextBox.Text, NUM, num2)
            cone.Close()
            If num2 = 1 Then
                eGloContrato = Me.ContratoTextBox.Text
                bloq = 1
            End If
        End If
    End Sub

    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 21))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 19) = "Servicio Televisión" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisión Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Telefonía" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Red  ' System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                            pasa = False
                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            CON.Close()
            Me.TreeView1.ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CREAARBOL99()

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If



            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                Me.TreeView1.Nodes.Add(Trim(FilaRow("Servicio").ToString()))
                I += 1
            Next
            CON.Close()
            Me.TreeView1.ExpandAll()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            NUM = 0
            num2 = 0
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                eRespuesta = 0
                eMensaje = ""
                Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(0, Integer)), GloClv_TipSer)
                Me.ConAtenTelCteTableAdapter.Connection = CON
                Me.ConAtenTelCteTableAdapter.Fill(Me.DataSetEric.ConAtenTelCte, CLng(Me.ContratoTextBox.Text), eRes, eMsg)
                Me.BuscaBloqueadoTableAdapter.Connection = CON
                Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoTextBox.Text, NUM, num2)
                Me.ValidaQuejasAtenTelTableAdapter.Connection = CON
                Me.ValidaQuejasAtenTelTableAdapter.Fill(Me.DataSetEric2.ValidaQuejasAtenTel, CLng(Me.ContratoTextBox.Text), GloClv_TipSer, eRespuesta, eMensaje)
                If eRespuesta = 1 And opcion = "N" Then
                    MsgBox(eMensaje)
                    Me.CONllamadasdeinternetBindingNavigator.Enabled = False
                    Me.Button2.Enabled = False
                Else
                    Me.CONllamadasdeinternetBindingNavigator.Enabled = True
                    Me.Button2.Enabled = True

                End If

                Me.BuscaSiTieneQuejaTableAdapter.Connection = CON
                Me.BuscaSiTieneQuejaTableAdapter.Fill(Me.DataSetEric2.BuscaSiTieneQueja, GloClv_TipSer, CLng(Me.ContratoTextBox.Text), eRes, eMsg)
                If eRes = 1 Then
                    Me.Label5.Text = eMsg
                Else
                    Me.Label5.Text = ""
                End If
                CREAARBOL()
                If num2 = 1 Then
                    bloq = 1
                End If
            Else
                Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(0, Integer)), GloClv_TipSer)
                CREAARBOL()
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    'Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged
    '    Me.Button8.Enabled = False
    '    BUSCACLIENTES(0)
    '    If IsNumeric(Me.ContratoTextBox.Text) = True Then
    '        Contrato = Me.ContratoTextBox.Text
    '    Else
    '        Contrato = 0
    '        Me.Label5.Text = ""
    '    End If
    '    If Contrato > 0 Then
    '        GloContratoVer = Contrato
    '        Me.Button4.Enabled = True
    '        Me.Button8.Enabled = True
    '    End If
    'End Sub



    Private Sub nuevo()
        ComentarioAgenda = ""
        FechaAgenda = ""
        HoraAgenda = ""
        Clv_HoraAgenda = 0
        Me.CONllamadasdeinternetBindingSource.AddNew()
        Me.HoraInicialMaskedTextBox.Text = Now
        Me.FechaMaskedTextBox.Text = Now

    End Sub

    Private Sub CONQUEJASBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONQUEJASBindingNavigatorSaveItem.Click
        '--Validación Para que solo Pueda guardar Telemarketing y  Soporte Técnico
        If IdSistema = "AG" Then
            Dim bnd As Integer = Nothing
            bnd = Dame_Perfil_Usuario(GloUsuario, 0)
            If bnd = 1 Then
                MsgBox("Solo Pueden Generar Una Atención Telefónica Un Usuario Debe Tener Perfil De Telemarketing O Soporte Técnico", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If
        '--Fin Validacion 

        If IsNumeric(GloClv_TipSer) = True Then
            If IsNumeric(Me.ContratoTextBox.Text) = False Then
                MsgBox("Selecciona el Contrato.")
                Exit Sub
            End If
            If Me.DescripcionTextBox.Text.Length = 0 Then
                MsgBox("Captura el Problema.")
                Exit Sub
            End If
            If Me.ComboBox5.Text.Length = 0 Then
                MsgBox("Por Favor Elija El Tipo De Servicio", MsgBoxStyle.Information)
                Me.ComboBox5.Focus()
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            eSolucion = Me.SolucionTextBox.Text
            If Me.ComboBox1.Text.Length = 0 Then
                eClv_Trabajo = 0
            Else
                eClv_Trabajo = Me.ComboBox1.SelectedValue
            End If
            If IsNumeric(Me.Clv_quejaTextBox.Text) = True Then
                eClv_Queja = CInt(Me.Clv_quejaTextBox.Text)
            Else
                eClv_Queja = 0
            End If
            'Me.Validate()
            'Me.CONllamadasdeinternetBindingSource.EndEdit()
            'Me.CONllamadasdeinternetTableAdapter.Connection = CON
            'Me.CONllamadasdeinternetTableAdapter.Update(Me.NewSofTvDataSet.CONllamadasdeinternet)
            If opcion = "N" Then
                CON.Open()
                Me.CONllamadasdeinternetTableAdapter.Connection = CON
                Me.CONllamadasdeinternetTableAdapter.Insert(Me.Clv_UsuarioTextBox.Text, Me.ContratoTextBox.Text, Me.DescripcionTextBox.Text, eSolucion, Me.HoraInicialMaskedTextBox.Text, Today, Me.FechaMaskedTextBox.Text, eClv_Trabajo, eClv_Queja, GloClv_TipSer, Me.Clv_llamadaTextBox.Text)
                Me.InsertaRel_AtencionTelUsuarioTableAdapter.Connection = CON
                Me.InsertaRel_AtencionTelUsuarioTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaRel_AtencionTelUsuario, CLng(Me.Clv_llamadaTextBox.Text), GloClvUsuario)
                CON.Close()
                NuevoTipo_Atencion(Me.Clv_llamadaTextBox.Text)
                MsgBox("El numero del reporte es : " + Me.Clv_llamadaTextBox.Text, MsgBoxStyle.Exclamation)

            End If

            If opcion = "M" Then
                CON.Open()
                Me.CONllamadasdeinternetTableAdapter.Connection = CON
                Me.CONllamadasdeinternetTableAdapter.Update(Me.Clv_llamadaTextBox.Text, Me.Clv_UsuarioTextBox.Text, Me.ContratoTextBox.Text, Me.DescripcionTextBox.Text, eSolucion, Me.HoraInicialMaskedTextBox.Text, "", Me.FechaMaskedTextBox.Text, eClv_Trabajo, eClv_Queja, GloClv_TipSer)
                CON.Close()
                NuevoTipo_Atencion(Me.Clv_llamadaTextBox.Text)
            End If
            GloBnd = True

            Me.Close()
        End If
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONllamadasdeinternetBindingSource.CancelEdit()
        GloBnd = True
    End Sub



    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox5.SelectedIndexChanged
        If Me.ComboBox5.SelectedValue <> Nothing Then

            GloClv_TipSer = Me.ComboBox5.SelectedValue
            Me.TextBox2.Text = Me.ComboBox5.Text
            GloNom_TipSer = Me.ComboBox5.Text
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'Me.MUESTRATRABAJOSTableAdapter.Connection = CON
            'Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, New System.Nullable(Of Integer)(CType(Me.ComboBox5.SelectedValue, Integer)))
            Me.MUESTRATRABAJOSQUEJASTableAdapter.Connection = CON
            Me.MUESTRATRABAJOSQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOSQUEJAS, New System.Nullable(Of Integer)(CType(Me.ComboBox5.SelectedValue, Integer)))
            CON.Close()
            'Me.ComboBox1.Text = ""
            If opcion = "N" Then
                Me.ComboBox1.Text = "< Seleccionar >"
            End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GLOCONTRATOSEL = 0
        GloClv_TipSer = Me.ComboBox5.SelectedValue
        If GloClv_TipSer = Nothing Or GloClv_TipSer = 0 Then
            MsgBox(" Por Favor Seleccione El Tipo de Servicio de la Atención Telefonica", MsgBoxStyle.Information)
            Exit Sub
        End If
        FrmSelCliente.Show()
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.Label5.Text.Length > 0 And IdSistema = "VA" Then
            MsgBox("No se puede generar una Queja, ya que el Cliente cuenta con una Pendiente.", , "Atención")
            Exit Sub
        End If
        If IsNumeric(Me.Clv_llamadaTextBox.Text) = False Then Me.Clv_llamadaTextBox.Text = 0
        If Me.Clv_llamadaTextBox.Text > 0 Then
            If IsNumeric(Me.Clv_quejaTextBox.Text) = False Then Me.Clv_quejaTextBox.Text = 0
            If Me.Clv_quejaTextBox.Text > 0 Then
                opcion = "C"
                GloClv_TipSer = Me.ComboBox5.SelectedValue
                GloNom_TipSer = Me.ComboBox5.Text
                gloClave = Me.Clv_quejaTextBox.Text
                FrmQueja.Show()
            Else
                nuevaqueja()
            End If
        Else
            nuevaqueja()
        End If
    End Sub

    Private Sub nuevaqueja()
        If Len(Trim(Me.NOMBRELabel1.Text)) > 0 Then
            If Len(Trim(Me.DescripcionTextBox.Text)) > 0 Then
                'GloClv_TipSer = Me.ComboBox5.SelectedValue
                If Len(Me.ComboBox5.Text) = 0 Then
                    GloClv_TipSer = 0
                    GloNom_TipSer = ""
                ElseIf Len(Me.ComboBox5.Text) > 0 Then
                    GloClv_TipSer = Me.ComboBox5.SelectedValue
                    GloNom_TipSer = Me.ComboBox5.Text
                End If
                If IsNumeric(GloClv_TipSer) = True Then
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.Validate()
                    Me.CONllamadasdeinternetBindingSource.EndEdit()
                    Me.CONllamadasdeinternetTableAdapter.Connection = CON
                    Me.CONllamadasdeinternetTableAdapter.Update(Me.NewSofTvDataSet.CONllamadasdeinternet)
                    CON.Close()
                    opcion = "N"
                    'GloClv_TipSer = Me.ComboBox5.SelectedValue
                    Panel2.Visible = True
                    Me.Tecnicos.Text = ""
                    Me.ComboBox3.Text = ""
                    Me.ComboBox6.SelectedIndex = 1
                    'FrmQueja.Show()
                End If
            Else
                MsgBox("Se Requiere la descripción del Problema por favor ", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Seleccione un Cliente por Favor")
        End If
    End Sub
    Private Sub ConfigureCrystalReports(ByVal op As Integer, ByVal Titulo As String)
        'Try
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"
        Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
        Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
        Dim Num1 As String = 0, Num2 As String = 0
        Dim nclv_trabajo As String = "0"
        Dim nClv_colonia As String = "0"
        Dim a As Integer = 0
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()


        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim Impresora_Ordenes As String = Nothing
        Dim mySelectFormula As String = Titulo
        Dim OpOrdenar As String = "0"


        Dim reportPath As String = Nothing

        If IdSistema = "TO" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCabStar.rpt"
        ElseIf IdSistema = "AG" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBueno.rpt"
        ElseIf IdSistema = "SA" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoTvRey.rpt"
        ElseIf IdSistema = "VA" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCosmo.rpt"
        ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoLogitel.rpt"
        End If

        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@Clv_TipSer int
        customersByCityReport.SetParameterValue(0, CStr(GloClv_TipSer))
        ',@op1 smallint
        customersByCityReport.SetParameterValue(1, 1)
        ',@op2 smallint
        customersByCityReport.SetParameterValue(2, 0)
        ',@op3 smallint
        customersByCityReport.SetParameterValue(3, 0)
        ',@op4 smallint,
        customersByCityReport.SetParameterValue(4, 0)
        '@op5 smallint
        customersByCityReport.SetParameterValue(5, 0)
        '@op6 smallint
        customersByCityReport.SetParameterValue(6, 0)
        ',@StatusPen bit
        customersByCityReport.SetParameterValue(7, 0)
        ',@StatusEje bit
        customersByCityReport.SetParameterValue(8, 0)
        ',@StatusVis bit,
        customersByCityReport.SetParameterValue(9, 0)
        '@Clv_OrdenIni bigint
        customersByCityReport.SetParameterValue(10, CInt(Me.Clv_quejaTextBox.Text))
        ',@Clv_OrdenFin bigint
        customersByCityReport.SetParameterValue(11, CInt(Me.Clv_quejaTextBox.Text))
        ',@Fec1Ini Datetime
        customersByCityReport.SetParameterValue(12, "01/01/1900")
        ',@Fec1Fin Datetime,
        customersByCityReport.SetParameterValue(13, "01/01/1900")
        '@Fec2Ini Datetime
        customersByCityReport.SetParameterValue(14, "01/01/1900")
        ',@Fec2Fin Datetime
        customersByCityReport.SetParameterValue(15, "01/01/1900")
        ',@Clv_Trabajo int
        customersByCityReport.SetParameterValue(16, 0)
        ',@Clv_Colonia int
        customersByCityReport.SetParameterValue(17, 0)
        ',@OpOrden int
        customersByCityReport.SetParameterValue(18, 0)
        '@Clv_Departamento
        customersByCityReport.SetParameterValue(19, 0)
        '@Op7
        customersByCityReport.SetParameterValue(20, 0)
        '@Contrato
        customersByCityReport.SetParameterValue(21, 0)

        'Titulos de Reporte

        mySelectFormula = "Quejas " & Me.TextBox2.Text
        customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
        Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
        Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora_Ordenes, a)
        If a = 1 Then
            MsgBox("No se ha asignado una impresora para Quejas")
            Exit Sub
        Else
            customersByCityReport.PrintOptions.PrinterName = Impresora_Ordenes
            customersByCityReport.PrintToPrinter(1, True, 1, 1)
        End If
        CON.Close()
        'CrystalReportViewer1.ReportSource = customersByCityReport


        customersByCityReport = Nothing
        'Catch ex As System.Exception
        'System.Windows.Forms.MessageBox.Show(ex.Message)
        ' End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Try
            '--Validación Para que solo Pueda guardar Telemarketing y  Soporte Técnico
            If IdSistema = "AG" Then
                Dim bnd As Integer = Nothing
                bnd = Dame_Perfil_Usuario(GloUsuario, 0)
                If bnd = 1 Then
                    MsgBox("Solo Pueden Generar Una Atención Telefónica Un Usuario Debe Tener Perfil De Telemarketing O Soporte Técnico", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            '--Fin Validacion
            If Len(Me.ComboBox5.Text) > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Dim clave As Long
                Dim gloclv_queja1 As Long = 0
                'If Me.Tecnicos.Text.Length = 0 Then
                '    MsgBox("Selecciona el Técnico.")
                '    Exit Sub
                'End If
                If Me.ComboBox3.Text.Length = 0 Then
                    MsgBox("Seleccina un Tipo de Prioridad.")
                    Exit Sub
                End If
                Me.CONQUEJASTableAdapter.Connection = CON
                'If IdSistema = "AG" Or IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "VA" Or IdSistema = "LO" Then
                Me.CONQUEJASTableAdapter.Insert(Me.ContratoTextBox.Text, GloClv_TipSer, Nothing, Nothing, "Procede de una Atenciòn Telefònica", "P", Me.Tecnicos.SelectedValue, Me.DescripcionTextBox.Text, "", Nothing, Now, Me.ComboBox3.SelectedValue, "N", Me.ComboBox6.SelectedValue, Nothing, Me.ComboBox1.SelectedValue, False, gloclv_queja1, "01/01/1900", "01/01/1900")
                'End If
                'Eric
                'If IdSistema = "SA" Then
                '    Me.CONQUEJASTableAdapter.Insert(Me.ContratoTextBox.Text, GloClv_TipSer, Nothing, Nothing, "Procede de una Atenciòn Telefònica", "P", 0, Me.DescripcionTextBox.Text, "", Nothing, Now, Me.ComboBox3.SelectedValue, "N", Me.ComboBox6.SelectedValue, Nothing, Me.ComboBox1.SelectedValue, False, gloclv_queja1, "01/01/1900", "01/01/1900")
                'End If
                Me.Clv_quejaTextBox.Visible = True
                Me.Label2.Visible = True
                MsgBox("El # de Queja que se genero es el : " & gloclv_queja1 & " y el numero de Atención Telefónica es : " & Me.Clv_llamadaTextBox.Text)
                Me.Clv_quejaTextBox.Text = gloclv_queja1
                'Eric
                Me.NueRelQuejaUsuarioTableAdapter.Connection = CON
                Me.NueRelQuejaUsuarioTableAdapter.Fill(Me.DataSetEric.NueRelQuejaUsuario, CLng(gloclv_queja1), GloClvUsuario, "N")
                '---------------------------------------
                If IsNumeric(GloClv_TipSer) = True Then
                    Me.Validate()
                    Me.CONllamadasdeinternetBindingSource.EndEdit()
                    Me.CONllamadasdeinternetTableAdapter.Connection = CON
                    Me.CONllamadasdeinternetTableAdapter.Update(Me.NewSofTvDataSet.CONllamadasdeinternet)
                End If

                Dim Clv_Cita As Long
                If Len(Trim(FechaAgenda)) > 0 And Len(Trim(HoraAgenda)) Then
                    Me.NUE_CITASTableAdapter.Connection = CON
                    Me.NUE_CITASTableAdapter.Fill(Me.NewSofTvDataSet.NUE_CITAS, Me.Tecnicos.SelectedValue, New System.Nullable(Of Date)(CType(FechaAgenda, Date)), New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), "", "", "Q", Clv_Cita)
                    Me.CONDetCitasTableAdapter.Connection = CON
                    Me.CONDetCitasTableAdapter.Delete(Clv_Cita)
                    Me.CONDetCitasTableAdapter.Connection = CON
                    Me.CONDetCitasTableAdapter.Insert(Clv_HoraAgenda, Clv_Cita, clave)
                    Me.NUEREL_CITAS_QUEJASTableAdapter.Connection = CON
                    Me.NUEREL_CITAS_QUEJASTableAdapter.Fill(Me.NewSofTvDataSet.NUEREL_CITAS_QUEJAS, New System.Nullable(Of Long)(CType(Clv_Cita, Long)), New System.Nullable(Of Long)(CType(Me.Clv_quejaTextBox.Text, Long)))
                    NUEMOVREL_CITAS(Clv_Cita, ComentarioAgenda)
                    ComentarioAgenda = ""
                End If
                CON.Close()
                Panel2.Visible = False
                GloBnd = True
                ConfigureCrystalReports(0, "")
                Me.Close()
            Else
                MsgBox("Por Favor Elija El Tipo De Servicio", MsgBoxStyle.Information)
                Me.ComboBox5.Focus()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        FechaAgenda = ""
        HoraAgenda = ""
        Clv_HoraAgenda = 0
        Panel2.Visible = False
    End Sub

    Private Sub Clv_llamadaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_llamadaTextBox.TextChanged
        If IsNumeric(Me.Clv_llamadaTextBox.Text) = True Then
            If Me.Clv_llamadaTextBox.Text > 0 Then
                'Me.Button2.Enabled = True
                Me.Button3.Enabled = True
                Me.Button4.Enabled = True
            Else
                'Me.Button2.Enabled = False
                Me.Button3.Enabled = False
                Me.Button4.Enabled = False
            End If
        Else
            'Me.Button2.Enabled = False
            Me.Button3.Enabled = False
            Me.Button4.Enabled = False
        End If
    End Sub

    Private Sub Clv_quejaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_quejaTextBox.TextChanged
        If IsNumeric(Me.Clv_quejaTextBox.Text) = True Then
            If Me.Clv_quejaTextBox.Text > 0 Then
                Me.Clv_quejaTextBox.Visible = True
                Me.Label2.Visible = True
            Else
                Me.Clv_quejaTextBox.Visible = False
                Me.Label2.Visible = False
            End If
        Else
            Me.Clv_quejaTextBox.Visible = False
            Me.Label2.Visible = False
        End If
    End Sub


    Private Sub ComboBox5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Tecnicos.SelectedIndexChanged
        If IsNumeric(Me.ContratoTextBox.Text) = True And IsNumeric(Me.Tecnicos.SelectedValue) = True Then
            If Me.ContratoTextBox.Text > 0 And Me.Tecnicos.SelectedValue > 0 Then
                GLOCONTRATOSEL_agenda = Me.ContratoTextBox.Text
                GloClv_tecnico = Me.Tecnicos.SelectedValue
                GLONOM_TECNICO = Me.Tecnicos.Text
                FrmAgendaRapida.Show()
            End If
        End If
    End Sub




    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If IsNumeric(Contrato) = True And (Contrato > 0) Then
            GloOpFacturas = 3
            Bwr_FacturasCancelar.Show()
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information)
        End If
    End Sub




    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        VerBRWQUEJAS.Show()
    End Sub

    Private Sub GeneroTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneroTextBox.TextChanged
        If Me.GeneroTextBox.Text = "" Then
            Me.Label4.Text = "-------"
        Else
            Me.Label4.Text = Me.GeneroTextBox.Text
        End If
    End Sub



    Private Sub CMBEnSitio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnSitio.CheckedChanged

    End Sub

    Private Sub ContratoTextBox_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ContratoTextBox.KeyDown
        If e.KeyValue = 13 Then
            Me.Button8.Enabled = False
            BUSCACLIENTES(0)
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Contrato = Me.ContratoTextBox.Text
            Else
                Contrato = 0
                Me.Label5.Text = ""
            End If
            If Contrato > 0 Then
                GloContratoVer = Contrato
                Me.Button4.Enabled = True
                Me.Button8.Enabled = True
            End If
        End If
    End Sub
End Class
