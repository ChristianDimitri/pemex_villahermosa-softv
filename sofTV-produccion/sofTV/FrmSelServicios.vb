﻿Imports System.Data.SqlClient
Public Class FrmSelServicios

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Try
            If IsNumeric(ComboBox1.SelectedValue) = True Then
                GloGuardarNet = False
                'If GloClv_TipSer <> 5 Then
                GloClv_Servicio = ComboBox1.SelectedValue
                locGloClv_servicioPromocion = ComboBox1.SelectedValue
                GloClv_servTel = Me.ComboBox1.SelectedValue
                GloClv_servTel_1 = Me.ComboBox1.SelectedValue

                'Else
                '    GloClv_servTel = Me.ComboBox1.SelectedValue
                '    locGloClv_servicioPromocion = GloClv_servTel
                'End If

                'If UCase(Pantalla) = UCase("FrmClientes") Then
                '    If GloClv_Servicio > 0 Then
                '        Me.Validate()
                '        Me.ConsultaclientesnetTableAdapter1.Insert(Contrato, "C", 0, 0, False, False, 0, "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", 0, "", False, LoContratonet)
                '        Me.ConsultaclientesnetTableAdapter1.Update(Me.NewSofTvDataSet.CONSULTACLIENTESNET)
                '        Me.ConsultacontnetTableAdapter1.Insert(LoContratonet, GloClv_Servicio, "C", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", True, 0, 0, False, "C", "", True, "", 0, 0, "", "", LoClv_Unicanet)
                '        Me.ConsultacontnetTableAdapter1.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
                '        FrmClientes.creaarbol()
                '        FrmClientes.Panel5.Visible = False
                '        FrmClientes.Panel6.Visible = True
                '        FrmClientes.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(LoContratonet, Long))
                '        FrmClientes.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(LoContratonet, Long))
                '        FrmClientes.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(LoClv_Unicanet, Long)))
                '        FrmClientes.CREAARBOL()

                '        GloClv_Servicio = 0
                '    End If
                If GloClv_TipSer = 2 Then

                    '----------------------------------------------------------------------------------------------




                    If Equip_tel = True Then
                        numtel = True
                    End If
                    GloBndSer = True
                ElseIf GloClv_TipSer = 5 Then
                    globndTel = True
                    'JUANJO HOMOLOGACION SERVICIOS TELEFONIA
                    If BndCajasHomo = 1 Then
                        FrmCtrl_ServiciosCli.CambioAnalogoDigital(GloNoCajasDig)
                        BndCajasHomo = 0
                        FrmClientes.CREAARBOLDIGITAL2()
                    End If
                End If
                Me.Close()
                frmctr.Activar()
            Else
                MsgBox("No ha Seleccionado un Servicio", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GloClv_Servicio = 0
        GloClv_servTel = 0
        De_Tele = False
        'globndTel = False
        'GloClv_servTel_1 = 0

        Me.Close()
    End Sub

    Private Sub FrmSelServicios_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'GloClv_Servicio = 0
        'GloClv_servTel = 0
        'globndTel = False
        ''GloClv_servTel_1 = 0
    End Sub

    Private Sub FrmSelServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        locGloClv_servicioPromocion = 0
        frmctr.MdiParent = FrmClientes
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraServiciosTableAdapter.Connection = CON
            Me.MuestraServiciosTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Long)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    'Procedimiento para Guardar la relación del Bono de Garantía de Aparato (  Prmoción de Estudiantes  )
    'Public Sub GuardaDepositoNet()

    '    Dim CON80 As New SqlClient.SqlConnection(MiConexion)
    '    Dim CMD As New SqlClient.SqlCommand()

    '    Try
    '        CMD = New SqlClient.SqlCommand()
    '        CON80.Open()
    '        With CMD
    '            .CommandText = "GuardaDepositonet"
    '            .CommandType = CommandType.StoredProcedure
    '            .Connection = CON80
    '            .CommandTimeout = 8

    '            Dim prmcontratonet As New SqlParameter("@contratonet", SqlDbType.BigInt)
    '            prmcontratonet.Value = GloContratonet
    '            .Parameters.Add(prmcontratonet)

    '            Dim prmContrato As New SqlParameter("@contrato ", SqlDbType.BigInt)
    '            prmContrato.Value = GloContrato
    '            .Parameters.Add(prmContrato)

    '            Dim prmstatus As New SqlParameter("@status", SqlDbType.VarChar, 1)
    '            prmstatus.Value = ""
    '            .Parameters.Add(prmstatus)

    '            Dim prmtipocablemodem As New SqlParameter("@tipocablemodem ", SqlDbType.TinyInt)
    '            prmtipocablemodem.Value = 0
    '            .Parameters.Add(prmtipocablemodem)

    '            Dim Result As Integer = .ExecuteNonQuery()


    '        End With

    '        CON80.Dispose()
    '        CON80.Close()

    '    Catch ex As Exception
    '        CON80.Dispose()
    '        CON80.Close()
    '        MsgBox("Error al guardar la relación del Cliente con promoción de Estudiante", MsgBoxStyle.Exclamation, "Ocurrió un Error")
    '        MsgBox(ex.Message)

    '    End Try

    'End Sub

    'Public Sub VerificaClientePromocionEstudiante()

    '    '----------------------------------------------------------------------------------------------
    '    'Preguntamos si es que el cliente aplicará para la Promoción de Estudiante

    '    Dim esEstudiante = MsgBox("¿El Cliente aplica para la Promoción de Estuduantes?", MsgBoxStyle.YesNo, "PROMOCIÓN DE ESTUDIANTES")
    '    If esEstudiante = MsgBoxResult.Yes Then
    '        MsgBox("Se cargará un cobro por concepto de: BONO DE GARANTÍA ( $500.00 ) por el aparato, ¿desea continuar?", MsgBoxStyle.YesNo, "¿CONTINUAR CON LA PROMOCIÓN?")

    '        'Guardamos la Relación del Cliente con el Bono de Garantía
    '        'Procedimiento para Guardar la relación del Bono de Garantía de Aparato (  Prmoción de Estudiantes  )
    '        GuardaDepositoNet()

    '    Else
    '    End If

    'End Sub

End Class