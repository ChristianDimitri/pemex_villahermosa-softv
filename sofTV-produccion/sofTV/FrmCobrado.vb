﻿Public Class FrmCobrado
    Public Tipo As Integer
    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If cbxNoCobrados.Checked = False And cbxCobrado.Checked = False Then
            MsgBox("Seleccione al menos uno de los Status")
            Exit Sub
        ElseIf cbxCobrado.Checked = True And cbxNoCobrados.Checked = True Then
            Tipo = 3
            Me.DialogResult = Windows.Forms.DialogResult.OK
        ElseIf cbxCobrado.Checked = True Then
            Tipo = 2
            Me.DialogResult = Windows.Forms.DialogResult.OK
        ElseIf cbxNoCobrados.Checked = True Then
            Tipo = 1
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If
    End Sub

    Private Sub FrmCobrado_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        cbxCobrado.Checked = False
        cbxNoCobrados.Checked = False
    End Sub
End Class