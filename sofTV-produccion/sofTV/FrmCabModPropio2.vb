Imports System.Data.SqlClient
Public Class FrmCabModPropio2
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing

    Private Sub ValidarMac()

        If Me.mac.Text.Length <> 14 Then
            MsgBox("Captura los 12 D�gitos de la MAC.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        eRes = 0

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaCablemodem2", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@MACCABLEMODEM", SqlDbType.VarChar, 15)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Me.mac.Text
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@ContratoCli", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = 0
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Cablemodem", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = 0
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Respuesta", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        parametro4.Value = 0
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Msg", SqlDbType.VarChar, 250)
        parametro5.Direction = ParameterDirection.Output
        parametro5.Value = ""
        comando.Parameters.Add(parametro5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = CInt(parametro4.Value.ToString)
            eMsg = parametro5.Value.ToString
            conexion.Close()

            If eRes = 1 Or eRes = 2 Then
                AgregarMac()
            Else
                MsgBox(eMsg, MsgBoxStyle.Information)
            End If

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub AgregarMac()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("AGREGAMAC2", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@MAC", SqlDbType.VarChar, 150)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Me.mac.Text
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@CLV_CABLEMODEM", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = 0
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            GloClv_CablemodemSel = CInt(parametro2.Value.ToString)
            conexion.Close()

            GloMacCablemodemSel = Me.mac.Text
            GloBndClv_CablemodemSel = True
            eBndClv_CablemodemSelPropio = True
            MsgBox(eMsg, MsgBoxStyle.Information)
            Me.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
        End Try

        
    End Sub

    Private Sub ButtonValida_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonValida.Click
        ValidarMac()
    End Sub

    Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        GloClv_CablemodemSel = 0
        GloMacCablemodemSel = ""
        GloBndClv_CablemodemSel = False
        Me.Close()
    End Sub

    Private Sub FrmCabModPropio2_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        GloClv_CablemodemSel = 0
        GloMacCablemodemSel = ""
        GloBndClv_CablemodemSel = False
        If IdSistema = "LO" Or IdSistema = "YU" Or IdSistema = "SA" Then
            Me.Label3.Text = "Tecle� la Mac del ATA Propio:"
            Me.ButtonValida.Text = "Agregar Mac del ATA"
            Me.Text = "ATA Propio"
        End If
    End Sub
End Class