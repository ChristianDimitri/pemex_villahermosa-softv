Imports System.Data.SqlClient
Public Class VerBrwOrdSer
    Dim autom As Boolean
    Dim bndbuscastatus As Boolean = False
    Private Sub VerBrwOrdSer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Button6.Visible = False
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        'Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        'Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
        'GloClv_TipSer = Me.ComboBox4.SelectedValue
        CON.Close()
        Busca(30)
        bndbuscastatus = True
    End Sub



    Private Sub consultar()
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            If gloClave > 0 And Me.Clv_calleLabel2.Text > 0 Then
                opcion = "C"
                GloBnd = False
                gloClave = Me.Clv_calleLabel2.Text
                'GloClv_TipSer = Me.Label9.Text
                FrmOrdSer.Show()
            Else
                MsgBox(mensaje2)
            End If
        Else
            MsgBox("No tiene una Orden seleccionada", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        consultar()
    End Sub

    Private Sub modificar()
        If gloClave > 0 Then
            opcion = "M"
            GloBnd = False
            gloClave = Me.Clv_calleLabel2.Text
            'GloClv_TipSer = Me.ComboBox4.SelectedValue
            FrmOrdSer.Show()
        End If
    End Sub


    Private Sub Busca(ByVal op As Integer)
        Dim sTATUS As String = "P"
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            'If IsNumeric(Me.ComboBox4.SelectedValue) = True Then
            If Me.RadioButton1.Checked = True Then
                sTATUS = "P"
            ElseIf Me.RadioButton2.Checked = True Then
                sTATUS = "E"
            ElseIf Me.RadioButton3.Checked = True Then
                sTATUS = "V"
            End If

            If op = 30 Then 'contrato

                If IsNumeric(GloContratoVer) = True Then
                    Me.BUSCAORDSERTableAdapter.Connection = CON
                    Me.BUSCAORDSERTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAORDSER, 0, 0, GloContratoVer, sTATUS, "", "", 300, False)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 499 Then 'contrato
                Me.BUSCAORDSERTableAdapter.Connection = CON
                Me.BUSCAORDSERTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAORDSER, 0, 0, GloContratoVer, sTATUS, "", "", 4990, False)
            ElseIf op = 31 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BUSCAORDSERTableAdapter.Connection = CON
                    Me.BUSCAORDSERTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAORDSER, 0, 0, GloContratoVer, Me.TextBox2.Text, "", "", 310, False)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If
            ElseIf op = 32 Then 'Calle y numero
                Me.BUSCAORDSERTableAdapter.Connection = CON
                Me.BUSCAORDSERTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAORDSER, 0, 0, GloContratoVer, "", Me.BCALLE.Text, Me.BNUMERO.Text, 320, False)
            ElseIf op = 33 Then 'clv_Orden
                If IsNumeric(Me.TextBox3.Text) = True Then
                    Me.BUSCAORDSERTableAdapter.Connection = CON
                    Me.BUSCAORDSERTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAORDSER, 0, Me.TextBox3.Text, GloContratoVer, "", "", "", 330, False)
                Else
                    MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                End If

            Else
                Me.BUSCAORDSERTableAdapter.Connection = CON
                Me.BUSCAORDSERTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAORDSER, 0, 0, 0, "", "", "", 340, False)
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            Me.TextBox3.Clear()
            Me.BNUMERO.Clear()
            Me.BCALLE.Clear()
            'End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(30)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(31)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(30)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(31)
        End If
    End Sub

    Private Sub BRWORDSER_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            'GloClv_TipSer = Me.ComboBox4.SelectedValue
            Busca(30)
        End If
    End Sub





    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    'Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Me.ComboBox4.SelectedIndex <> -1 Then
    '        GloClv_TipSer = Me.ComboBox4.SelectedValue
    '        Busca(30)
    '    End If
    'End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(33)
        End If
    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(33)
    End Sub

   


    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        gloClave = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If bndbuscastatus = True Then
            Busca(499)
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If bndbuscastatus = True Then
            Busca(499)
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If bndbuscastatus = True Then
            Busca(499)
        End If
    End Sub
End Class