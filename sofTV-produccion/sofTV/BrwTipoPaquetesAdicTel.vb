Imports System.Data.SqlClient
Public Class BrwTipoPaquetesAdicTel

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BrwTipoPaquetesAdicTel_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If LocTipoPaquerebnd = True Then
            LocTipoPaquerebnd = False
            Busca(0)
        End If
    End Sub

    Private Sub BrwTipoPaquetesAdicTel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Muestra_CatalogoTipoCobrosTelTableAdapter.Connection = CON
        Me.Muestra_CatalogoTipoCobrosTelTableAdapter.Fill(Me.Procedimientosarnoldo4.Muestra_CatalogoTipoCobrosTel, 0)
        Me.ComboBox1.Text = ""
        CON.Close()
        Busca(0)
    End Sub

    Private Sub Busca(ByVal Op As Integer)
        Dim con As New SqlConnection(MiConexion)
        Select Case Op
            Case 0 'Todos
                con.Open()
                Me.Busca_Tipo_Paquete_AdicionalTableAdapter.Connection = con
                Me.Busca_Tipo_Paquete_AdicionalTableAdapter.Fill(Me.Procedimientosarnoldo4.Busca_Tipo_Paquete_Adicional, Op, 0, "", 0)
                con.Close()
            Case 1 'clave
                If IsNumeric(Me.TextBox1.Text) = True Then
                    con.Open()
                    Me.Busca_Tipo_Paquete_AdicionalTableAdapter.Connection = con
                    Me.Busca_Tipo_Paquete_AdicionalTableAdapter.Fill(Me.Procedimientosarnoldo4.Busca_Tipo_Paquete_Adicional, Op, CLng(Me.TextBox1.Text), "", 0)
                    con.Close()
                End If
            Case 2 'Descripcion
                con.Open()
                Me.Busca_Tipo_Paquete_AdicionalTableAdapter.Connection = con
                Me.Busca_Tipo_Paquete_AdicionalTableAdapter.Fill(Me.Procedimientosarnoldo4.Busca_Tipo_Paquete_Adicional, Op, 0, Me.TextBox2.Text, 0)
                con.Close()
            Case 3 'Tipo
                If Me.ComboBox1.Text <> "" Then
                    con.Open()
                    Me.Busca_Tipo_Paquete_AdicionalTableAdapter.Connection = con
                    Me.Busca_Tipo_Paquete_AdicionalTableAdapter.Fill(Me.Procedimientosarnoldo4.Busca_Tipo_Paquete_Adicional, Op, 0, "", CLng(Me.ComboBox1.SelectedValue))
                    con.Close()
                End If
        End Select

    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub CMBLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel1.Click

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(1)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(2)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Busca(3)
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If Me.ComboBox1.Text <> "" Then
            Busca(3)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        LocOpTipoPaqAdic = "N"
        FrmTipoPaqAdicionalesTel.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        LocOpTipoPaqAdic = "C"
        If IsNumeric(Me.Clv_tipo_paquete_AdiocionalLabel1.Text) Then
            LocGloTipoPaquete = CLng(Me.Clv_tipo_paquete_AdiocionalLabel1.Text)
            FrmTipoPaqAdicionalesTel.Show()
        Else
            MsgBox("No Se Puede Consultar Sin Datos", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        LocOpTipoPaqAdic = "M"
        If IsNumeric(Me.Clv_tipo_paquete_AdiocionalLabel1.Text) Then
            LocGloTipoPaquete = CLng(Me.Clv_tipo_paquete_AdiocionalLabel1.Text)
            FrmTipoPaqAdicionalesTel.Show()
        Else
            MsgBox("No Se Puede Modificar Sin Datos", MsgBoxStyle.Information)
        End If
    End Sub
End Class